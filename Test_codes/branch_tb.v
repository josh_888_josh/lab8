module branch_tb; 
	reg clk, reset, error;
	reg [15:0] read_data; 
	output [1:0] m_cmd; 
	output [15:0] write_data; 
	output [8:0] mem_addr; 
	
	cpu DUT(clk,reset,read_data,write_data,mem_addr,m_cmd); 
	
	initial begin 
		clk = 1'b0; 
		forever begin 
			clk = 1'b1; 
			clk = 1'b0; 
		end 
	end 
	
	
	task PCchecker; 
		input [8:0] expected_value, actual_value; 
		
		if(expected_value != actual_value)
			$display("***ERROR, PC is %b, expected %b",actual_value, expected_value);
			error = 1'b1; 
		end	
	endtask 
	
	
	initial begin 
	error = 1'b0; 
	reset = 1'b1; 
	read_data = 16'b001_00_000_10110011;  //first instruction to test 
	
	#10;   //should go to wait state
	$display("Check if PC=0 after reset");
	PCchecker(9'b0, DUT.PC);
	
	reset = 1'b0; 
	#10;   //should go to state if1
	
	#100;
	
	$stop; 	
	end 

endmodule 