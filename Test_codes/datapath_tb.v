module datapath_tb;

reg[15:0] datapath_in, sximm5;
reg[2:0]  readnum, writenum;
reg [1:0] shift, ALUop, vsel;
reg write, clk, asel, bsel, loads, loada,loadb, loadc;
wire[2:0] Z_out;
wire [15:0]  datapath_out;
reg err;

datapath DUT(16'b0, datapath_out, vsel, writenum, write, readnum,
clk, loada, loadb, shift, asel, bsel, ALUop, loads, loadc, Z_out, datapath_in, sximm5, 8'b0);

initial
begin
	//forever loop, generates clock signal
	forever begin
		clk = 1'b0;						//clock goes to 1
		#5;								//wait 5 timesteps
		clk = 1'b1; 					//clock goes back to 0
		#5;								//wait 5 timesteps
	end
end

task myChecker;
	input[15:0] expectedOutput;
	begin
		if (DUT.data_out !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.data_out, expectedOutput);
			err = 1'b1;
		end
	end
endtask

task setRegister;
	input[15:0] regValue;
	input[2:0] regNum;
	begin
		writenum = regNum;
		write = 1;
		vsel = 2'b10;
		datapath_in = regValue;
	end
endtask


initial
begin

	//first test case implements the following assembly code
	//MOV R0, #7
	//MOV R1, #2
	//ADD R2, R1, R0, LSL#1
	//at the end data_out = 16

	$display("MOV R0, #7");					//Storing the decimal number 7 in Register 0
	err = 0;								//error signal set to 0
	datapath_in = 16'd7;					//first test value, 7
	vsel = 2'b10;
	writenum = 3'b000;						//write to register 0
	write = 1;
	
	readnum = 3'b000;						//read from the first register
	loada = 0;								//don't load into either pipeline register
	loadb = 0;
	#10;
	myChecker(16'd7);						//check the data out to ensure the first register has taken in the value(7)
	
	$display("MOV R1, #2");					//Storing the decimal number 2 in Register 0
	datapath_in = 16'd2;					//second test value, 2
	vsel = 2'b10;
	writenum = 3'b001;						//write to register 1
	write = 1;
	
	readnum=3'b001;							//read from first register
	#10;
	myChecker(16'd2);						//check the data out to ensure the second register has taken in the value(2)

	$display("ADD R2, R1, R0, LSL#1");		//Adding the contents of Register 1 and Register 0 (shifted left by 1) and storing the sum in Register 2
	readnum = 3'b000;						//read from register
	loadb = 1;								//load it into pipeline register b
	#10;
	
	loadb = 0;								//set loadb to 0, so we can write to the next one
	readnum = 3'b001;						//read from register 1
	loada = 1;								//load register 1 into register a
	#10;
	
	loada = 0;								//set loada back to 0
	shift=2'b01;							//shift mode 1, shift left 1 bit
	
	//set asel and bsel to 0, so the ALU connects to load registers
	asel=0;									
	bsel=0;
	
	ALUop=2'b00;							//ALU opcode 0, addition
	loadc = 1;								//set loadc to 1, so it accepts the value from the ALU
	
	#10;									
	writenum = 3'b010;						//write back c to register 2
	write = 1;								
	vsel = 0;								//writeback mux, loads c into reg file
	readnum = 3'b010;						//read from register 2
	#10;
	
	//check data_out to ensure that the register has taken the new value, and that the operation returned the right value(16)
	myChecker(16'd16);						
	#10;
	
	//second test case implements the following assembly code
	//MOV R3, #4
	//MOV R4, #3
	//AND R5, R4, R3, RSR#1
	//at the end data_out = 16
	
	$display("MOV R3, #4");					//Storing the decimal number 16 in Register 3
	datapath_in = 16'd4;					//first test value for second case, 4
	vsel = 2'b10;
	writenum = 3'b011;						//write to register 3
	write = 1;
	
	readnum = 3'b011;						//read from 3
	loada = 0;
	loadb = 0;
	#10;
	myChecker(16'd4);						//check to make sure the register has taken in the new value(4)
	
	$display("MOV R4, #3");					//Storing the decimal number 3 in Register 4

	datapath_in = 16'd3;					//second test value for second case, 3
	vsel = 2'b10;
	writenum = 3'b100;						//write to register 4
	write = 1;
	readnum=3'b100;							//read from register 5
	#10;
	myChecker(16'd3);						//check to make sure the register has taken in the new value(3)
	
	$display("AND R5, R4, R3, RSR#1");		//Anding the contents of Register 3 and Register 4 (shifted right by 1) and storing the sum in Register 2
	readnum = 3'b011;						//read from register 3
	loadb = 1;								//load contents into pipeline register b
	#10;
	loadb = 0;								//set loadb to 0, so we can write to the next one
	readnum = 3'b100;						//read from register 4
	loada = 1;								//load the contents into rpipeline register a
	#10;
	loada = 0;
	
	shift=2'b10;							//shift mode 2, right shift by 1, MSB = 0
	asel=0;									//set asel, bsel 0, so ALU reads from regA and regB
	bsel=0;
	ALUop=2'b10;							//ALUop 2, and logical operation
	
	loadc = 1;								//save output to regC
	#10;
	writenum = 3'b101;						//writeback to register 5
	write = 1;
	vsel = 0;
	readnum = 3'b101;						//read from register 5
	#10;
	
	//check data_out to ensure that the register has taken the new value, and that the operation returned the right value(2)
	myChecker(16'd2);
	
	//third test case implements the following assembly code
	//MOV R6, #0
	//MOV R7, #0
	//ADD R0, R6, R7
	//at the end data_out = 0 and Z_out should be 1
	
	
	
	$display("MOV R6, #0");					//Storing the decimal number 0 in Register 6
	datapath_in = 16'd0;					//input 0
	vsel = 2'b10;
	writenum = 3'b110;						//write it to register 6
	write = 1;
	readnum = 3'b110;						//read from register 7
	loada = 0;
	loadb = 0;
	#10;
	
	myChecker(16'd0);						//check the data out to ensure the first register has taken in the value(0)
	
	$display("MOV R7, #0");					//Storing the decimal number 3 in Register 4
	datapath_in = 16'd0;					//input 0
	vsel = 2'b10;
	writenum = 3'b111;						//write it to register 7
	write = 1;
	readnum=3'b111;							//read from register 7
	#10;
	
	$display("ADD R0, R6, R7");				//Adding the contents of Register 6 and Register 7 and storing the sum in Register 0
	readnum = 3'b110;						//read from register 6
	loadb = 1;								//load it into pipeline regb
	#10;
	loadb = 0;								//set loadb to 0, so we can write to the next one
	readnum = 3'b111;						//read from register 7
	loada = 1;								//load into pipeline rega
	#10;
	loada = 0;
	
	shift=2'b00;							//no shift 
	asel=0;
	bsel=0;
	ALUop=2'b00;							//ALU opcode 0, add
	loads = 1;								//load the status register
	loadc=1;								//load c register
	#10;
	
	writenum = 3'b000;						//writeback to register 0
	write = 1;
	vsel = 0;
	readnum = 3'b000;						//read from register 0
	#10;
	
	//check data_out to ensure that the register has taken the new value, and that the operation returned the right value(0)
	myChecker(16'd0);
	#10;
	
	//check if Z_out is 1 as it should be for a result of 0
	if (DUT.Z_out !== 3'b1) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.Z_out, 1'b1);
			err = 1'b1;
	end
	
	setRegister(16'd8, 3'b0);				//set register 0 to 8
	#10;
	loada =1;
	readnum = 3'b0;
	#10;
	loada = 0;								//next, load the same value into register a
	setRegister(16'd9, 3'b1);				//set register 1 to 9	
	#10;
	loadb =1;
	readnum = 3'b1;
	#10;
	loadb = 0;
	
	loads = 1;
	loadc = 1;
	ALUop = 2'b1;
	#10
	//8-9 should equal -1, which is negative, so this should trigger the N flag
	if (DUT.Z_out !== 3'b010) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.Z_out, 3'b010);
			err = 1'b1;
	end
	
	setRegister(16'd32767, 3'b0);			//load 32767(largest 16bit pos numper in 2s compliment) into register 0
	#10;
	loada =1;								//load into a as well
	readnum = 3'b0;
	#10;
	loada = 0;
	setRegister(16'd1, 3'b1);				//load 1 into register 1, this should cause an overflow
	#10;
	loadb =1;								//now load int b
	readnum = 3'b1;
	#10;
	loadb = 0;
	
	loads = 1;								//make sure to load it into the status register
	loadc = 1;
	ALUop = 2'b0;
	#10
	
	//test to see if this caused an overflow, as it should
	//in addition, the result is negative, due to how 2s compliment works, therefore N flag should be set as well
	if (DUT.Z_out !== 3'b110) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.Z_out, 3'b100);
			err = 1'b1;
	end
	
	//check the error wire, it will be low if there are no errors
	if( ~err )
		$display("PASSED");	//if no errors print passed
	else 
		$display("FAILED");	//otherwise print failed
	
	$stop;
	end
endmodule
	
	
	
	
	
	
	
	



