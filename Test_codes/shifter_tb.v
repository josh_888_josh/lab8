module shifter_tb;
		reg[15:0] in, i;
		reg[1:0] shift;
		wire[15:0] out;
		reg err;
		
		shifter DUT(in, shift, out);
		
		task myChecker;     
			input [15:0] expected_output;       
			begin
				//if the output	is not correct, print the error message and set the error bit
				if( out !== expected_output ) begin
					$display("ERROR ** output is %b, expected %b", out, expected_output );
					err = 1'b1;
				end
			end
		endtask
		
		initial begin
			err = 1'b0;							//initially set error to 0
			i = 0;
			
			//simple enough design we can test every possible number
			repeat(65536) begin
				in = i;							//test every value of i
			
				shift = 2'b00;					//no shift occurs
				#1
				myChecker(in);					//2'b00 no shifting occurs, so output should be the same as input
			
				shift = 2'b01;					//shift bits left
				#1
				myChecker({in[14:0], 1'b0});	//should shift bits left 1 bit and LSB should be 0
			
				shift = 2'b10;					//shift bits right
				#1
				myChecker({1'b0, in[15:1]});	//should shift bits right 1 bit and MSB should be 0
			
				shift = 2'b11;					//shift bits right, copy MSB of in to MSB of out
				#1
				myChecker({in[15], in[15:1]});	//should shift bits right 1 bit and MSB should be 0
			
				i = i +1;
			end
			
			if( ~err )
				$display("PASSED");				//if no errors print passed
			else 
				$display("FAILED");				//otherwise print failed
			$stop;
		end
endmodule