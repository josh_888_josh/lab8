module lab8_stage1_tb;
  reg [3:0] KEY;
  reg [9:0] SW;
  wire [9:0] LEDR; 
  wire [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
  reg err;
  reg CLOCK_50;

  lab8_top DUT(KEY,SW,LEDR,HEX0,HEX1,HEX2,HEX3,HEX4,HEX5,CLOCK_50);

  initial forever begin
    CLOCK_50 = 0; #5;
    CLOCK_50 = 1; #5;
  end
  initial begin
    err = 0;
    KEY[1] = 1'b0; // reset asserted
    #10; // wait until next falling edge of clock
    KEY[1] = 1'b1; // reset de-asserted, PC still undefined if as in Figure 4

	#60;// Wait six clk cycles to see if N is in R0
	if (DUT.CPU.DP.REGFILE.R0 !== 16'd15) begin
		err=1;
		$display("2.)FAILED: R0 should contain the address of N which is 15");
	end
	
	#90;//Wait six clk cycles to see if R0=4;
	if (DUT.CPU.DP.REGFILE.R0 !== 16'd4) begin
		err=1;
		$display("3.)FAILED: R0 should contain N which is 4");
	end

	#60;//Wait six clk cycles to see if R1=0;
	if (DUT.CPU.DP.REGFILE.R1 !== 16'd0) begin
		err=1;
		$display("4.)FAILED: R1 should contain 0");
	end

	#60;//Wait six clk cycles to see if R2=0;
	if (DUT.CPU.DP.REGFILE.R2 !== 16'd0) begin
		err=1;
		$display("5.)FAILED: R2 should contain 0");
	end
	
	#60;//Wait six clk cycles to see if R3 has value of 16
	if (DUT.CPU.DP.REGFILE.R3 !== 16'd16) begin
		err=1;
		$display("6.)FAILED: R3 should contain 16");
	end
	//Also check to confirm that mem[16] has value 50, mem[17] has 200, mem[18] has 100 and mem[19] has 500
	if (DUT.MEM.mem[16] !== 16'd50) begin
		err=1;
		$display ("7.)FAILED: mem[16] should contain 50, instead contains %d", DUT.MEM.mem[16]);
	end

	if (DUT.MEM.mem[17] !== 16'd200) begin
		err=1;
		$display ("8.)FAILED: mem[16] should contain 200, instead contains %d", DUT.MEM.mem[17]);
	end

	if (DUT.MEM.mem[18] !== 16'd100) begin
		err=1;
		$display ("9.)FAILED: mem[18] should contain 100, instead contains %d", DUT.MEM.mem[18]);
	end

	if (DUT.MEM.mem[19] !== 16'd500) begin
		err=1;
		$display ("9.)FAILED: mem[19] should contain 500, instead contains %d", DUT.MEM.mem[19]);
	end

	#60; //Wait six clk cycles to see if R4 has value 1 in it

	if (DUT.CPU.DP.REGFILE.R4 !== 16'd1) begin
		err=1;
		$display("10.)FAILED: R4 should contain 1");
	end

	//////////////////// **LOOP BEGINS** ////////////////////////////////////// for(i=0; i<N; i++) N=4

	/////////////////// **FIRST ITERATION OF LOOP** //////////////////////////

	#60; //Wait six clk cycles to see if R5 has value of address amount[i] in it which is 16

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd16) begin
		err=1;
		$display("11.)FAILED: R5 should contain 16");
	end

	#70;//Wait six clk cycle to see if R5 has value amount[i] in it which is 50

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd50) begin
		err=1;
		$display("12.)FAILED: R5 should contain 50");
	end

	#80; //Wait six clk cycles to see if R2 has value 50 in it

	if (DUT.CPU.DP.REGFILE.R2 !== 16'd50) begin
		err=1;
		$display("13.)FAILED: R2 should contain 50");
	end

	#80; //Wait six clk cycles to see if R1 has value 1 in it; //i goes from 0 to 1

	if (DUT.CPU.DP.REGFILE.R1 !== 16'd1) begin
		err=1;
		$display("14.)FAILED: R1 should contain 1");
	end

	#70;//Check if N is high (not to be confused with variable N in for loop

	if (DUT.CPU.N !== 16'd1) begin
		err=1;
		$display("15.)FAILED: N should contain 1");
	end
	#80;
	/////////////////// **SECOND ITERATION OF LOOP** //////////////////////////

	#60; //Wait six clk cycles to see if R5 has value of address amount[i] in it which is 17

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd17) begin
		err=1;
		$display("16.)FAILED: R5 should contain 17");
	end

	#70;//Wait six clk cycle to see if R5 has value amount[i] in it which is 200

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd200) begin
		err=1;
		$display("17.)FAILED: R5 should contain 200");
	end

	#80; //Wait six clk cycles to see if R2 has value 250 in it

	if (DUT.CPU.DP.REGFILE.R2 !== 16'd250) begin
		err=1;
		$display("18.)FAILED: R2 should contain 250");
	end

	#80; //Wait six clk cycles to see if R1 has value 2 in it; //i goes from 1 to 2

	if (DUT.CPU.DP.REGFILE.R1 !== 16'd2) begin
		err=1;
		$display("19.)FAILED: R1 should contain 2");
	end

	#70;//Check if N is high (not to be confused with variable N in for loop

	if (DUT.CPU.N !== 16'd1) begin
		err=1;
		$display("20.)FAILED: N should contain 1");
	end

	#80;
	/////////////////// **THIRD ITERATION OF LOOP** //////////////////////////

	#60; //Wait six clk cycles to see if R5 has value of address amount[i] in it which is 18

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd18) begin
		err=1;
		$display("21.)FAILED: R5 should contain 18");
	end

	#70;//Wait six clk cycle to see if R5 has value amount[i] in it which is 100

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd100) begin
		err=1;
		$display("22.)FAILED: R5 should contain 100");
	end

	#80; //Wait six clk cycles to see if R2 has value 350 in it

	if (DUT.CPU.DP.REGFILE.R2 !== 16'd350) begin
		err=1;
		$display("23.)FAILED: R2 should contain 350");
	end

	#80; //Wait six clk cycles to see if R1 has value 3 in it; //i goes from 2 to 3

	if (DUT.CPU.DP.REGFILE.R1 !== 16'd3) begin
		err=1;
		$display("24.)FAILED: R1 should contain 3");
	end

	#70;//Check if N is high (not to be confused with variable N in for loop

	if (DUT.CPU.N !== 16'd1) begin
		err=1;
		$display("25.)FAILED: N should contain 1");
	end

	#80;
	/////////////////// **FOURTH ITERATION OF LOOP** //////////////////////////

	#60; //Wait six clk cycles to see if R5 has value of address amount[i] in it which is 19

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd19) begin
		err=1;
		$display("26.)FAILED: R5 should contain 19");
	end

	#70;//Wait six clk cycle to see if R5 has value amount[i] in it which is 500

	if (DUT.CPU.DP.REGFILE.R5 !== 16'd500) begin
		err=1;
		$display("27.)FAILED: R5 should contain 500");
	end

	#80; //Wait six clk cycles to see if R2 has value 850 in it

	if (DUT.CPU.DP.REGFILE.R2 !== 16'd850) begin
		err=1;
		$display("28.)FAILED: R2 should contain 850");
	end

	#80; //Wait six clk cycles to see if R1 has value 4 in it; //i goes from 3 to 4

	if (DUT.CPU.DP.REGFILE.R1 !== 16'd4) begin
		err=1;
		$display("29.)FAILED: R5 should contain 4");
	end

	#70;//Check if Z is high

	if (DUT.CPU.Z !== 16'd1) begin
		err=1;
		$display("30.)FAILED: Z should contain 1");
	end

	//////////////////// **LOOP ENDS** ////////////////////////////

	#100; //Wait six clk cycles to see if R3 has value 850 in it

	if (DUT.CPU.DP.REGFILE.R3 !== 16'd20) begin
		err=1;
		$display("31.)FAILED: R3 should contain 20");
	end

	#90; //Wait six clk cycles to see if mem[20] has 850 in it

	if (DUT.MEM.mem[20] !== 16'd850) begin
		err=1;
		$display ("32.)FAILED: mem[20] should contain 850, instead contains %d", DUT.MEM.mem[20]);
	end
	#10;


    if (~err) $display("PASSED");
	else $display("FAILED");
    $stop;
  end
endmodule
