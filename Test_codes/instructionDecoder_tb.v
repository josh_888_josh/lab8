module instructionDecoder_tb;

	reg	[15:0]	instructionIn;
	reg	[2:0] 	nsel;
	reg err;
	
	wire	[15:0] 	sximm5, sximm8;
	wire	[2:0] 	readnum, writenum, opcode, muxOut, Rn, Rd, Rm;
	wire	[1:0] 	ALUop, shift, op;
	wire	[7:0] 	imm8;
	wire	[4:0] 	imm5;
	
	InstructionDecoder	DUT(instructionIn, nsel, opcode, op, ALUop, sximm5, sximm8, shift, readnum, writenum);
	
	task check_opcode;
	input[2:0] expectedOutput;
	begin
		if (DUT.opcode !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for opcode is %b, expected %b", DUT.opcode, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_ALUop;
	input[1:0] expectedOutput;
	begin
		if (DUT.ALUop !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for ALUop is %b, expected %b", DUT.ALUop, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_op;
	input[1:0] expectedOutput;
	begin
		if (DUT.op !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for op is %b, expected %b", DUT.op, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_sximm5;
	input[15:0] expectedOutput;
	begin
		if (DUT.sximm5 !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for sximm5 is %b, expected %b", DUT.sximm5, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_sximm8;
	input[15:0] expectedOutput;
	begin
		if (DUT.sximm8 !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for sximm8 is %b, expected %b", DUT.sximm8, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_shift;
	input[1:0] expectedOutput;
	begin
		if (DUT.shift !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for shift is %b, expected %b", DUT.shift, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_readnum;
	input[2:0] expectedOutput;
	begin
		if (DUT.readnum !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for readnum is %b, expected %b", DUT.readnum, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_writenum;
	input[2:0] expectedOutput;
	begin
		if (DUT.writenum !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output for writenum is %b, expected %b", DUT.writenum, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	initial
	begin

		//first test case implements the following assembly code
		//ADD R2, R5, R3
		err=0;
		$display("ADD R2, R5, R3");					
		instructionIn = 16'b101_00_101_010_00_011;								
		nsel = 3'b010;					
		#10;
		check_opcode(3'b101);
		check_ALUop(2'b00);
		check_sximm5(16'b0000_0000_0000_0011);
		check_sximm8(16'b0000_0000_0100_0011);
		check_shift(2'b00);
		check_readnum (3'b010);
		check_writenum (3'b010);
		
		//second test case implements the following assembly code
		//ADD R2, R1, R0, LSL#1
		$display("ADD R2, R1, R0, LSL#1");
		instructionIn = 16'b101_00_001_010_01_000;								
		nsel = 3'b010;	
		#10;
		check_opcode(3'b101);
		check_ALUop(2'b00);
		check_sximm5(16'b0000_0000_0000_1000);
		check_sximm8(16'b0000_0000_0100_1000);
		check_shift(2'b01);
		check_readnum (3'b010);
		check_writenum (3'b010);
		
		//third test case implements the following assembly code
		//AND R7, R6, R5, RSR#1
		$display("AND R7, R6, R5, RSR#1");
		instructionIn = 16'b101_10_110_111_10_101;								
		nsel = 3'b010;	
		#10;
		check_opcode(3'b101);
		check_ALUop(2'b10);
		check_sximm5(16'b1111_1111_1111_0101);
		check_sximm8(16'b1111_1111_1111_0101);
		check_shift(2'b10);
		check_readnum (3'b111);
		check_writenum (3'b111);
		
		//check the error wire, it will be low if there are no errors
		if( ~err )
		$display("PASSED");	//if no errors print passed
		else 
		$display("FAILED");	//otherwise print failed
		
		$stop;
	end
endmodule
		
		
		
		
		
		
		
		
		
		
		