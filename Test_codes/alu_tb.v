module ALU_tb;

	reg err; 
	reg[15:0] Ain, Bin;
	reg[1:0] ALUop;
	
	wire[15:0] out;
	wire z;
	
	ALU DUT(Ain, Bin, ALUop, out, z);
	
	task myChecker;     
		input [15:0] expected_output;       
		begin
			//if the output	is not correct, print the error message and set the error bit
			if( out !== expected_output ) begin
				$display("ERROR ** output is %b, expected %b", out, expected_output );
				err = 1'b1;
			end
			//test to make sure the z(status output) is correct for the given value
			if( (z === 1)&&(expected_output !== 0) ) begin
				$display("ERROR ** z should be 0 for this output" );
				err = 1'b1;
			end
		end
	endtask
	
	task setALU;
		input[15:0] setA;
		input[15:0]	setB;
		input[1:0]	setOp;
		begin
			Ain 	= setA; 	//set Ain
			Bin 	= setB;		//set Bin
			ALUop	= setOp;	//set ALU op-code
		end
	endtask
	
	initial begin
		
		err   = 1'b0;			//set error line to zero
		
		setALU(20, 32, 2'b00);
		#10
		myChecker(20+32);		//check if the output is correct for 20 and 32(A+B)
		
		setALU(20, 32, 2'b01);
		#10
		myChecker(20-32);		//check if output is correct for 20 and 32(A-B)
		
		setALU(20, 32, 2'b10);
		#10
		myChecker(20&32);		//check if output is correct 20 and 32(A&B)
		
		setALU(20, 32, 2'b11);
		#10
		myChecker(~32);			//check if output is correct 20 and 32(~B)
		
		#10
		
		//next test some specific values of A and B, 
		//specifically ones that should trigger the z output
		
		//test 0+0. should equal zero and trigger theZ (status out)
		setALU(0, 0, 2'b00);
		#10
		myChecker(0);			//check if the output is correct 0
		
		//test 15-15. Should equal 0
		setALU(15, 15, 2'b01);
		#10
		myChecker(0);			//check if the output is correct 0
		
		//test 16'b1010101010101010&16'b0101010101010101. Should equal 0
		setALU(16'b1010101010101010, 16'b0101010101010101, 2'b10);
		#10
		myChecker(0);			//check if the output is correct 0
		
		//test 16'b1111111111111111. Should equal 0. 
		//*note* ain remains the same, should note affect output
		setALU(16'b1010101010101010, 16'b1111111111111111, 2'b11);
		#10
		myChecker(0);			//check if the output is correct 0
		
		
		if( ~err )
			$display("PASSED");	//if no errors print passed
		else 
			$display("FAILED");	//otherwise print failed
	
		
		$stop;
	end
endmodule