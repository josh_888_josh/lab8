//state macros
`define reset 		4'b0000		//starting stage
`define decode		4'b0001		//decode stage, reset busses 
`define MtoR		4'b0010		//MOV data from memory to register
`define getA		4'b0011 	//getA stage, writes data to the A load register
`define getB		4'b0100		//getB stage, writes data to the N load register
`define operate		4'b0101		//preform ALU operation 
`define writeback	4'b0110		//writeback the data from the ALU to a register
`define if1			4'b0111		
`define if2			4'b1000
`define updatePC	4'b1001		//update program counter
`define haltState	4'b1010		//cannnot return from this state
`define loadAddr	4'b1011
`define	readRAM		4'b1100
`define writeRAM 	4'b1101
`define DC4			4'bxxxx		//4 bit don't care

//Opcode macros
`define MOV 		3'b110		//MOV 
`define	ALU			3'b101		//preform an ALU instruction
`define LDR			3'b011		//load from memory
`define STR			3'b100		//store into memory
`define HALT		3'b111		//HALT instruction, puts cpu into a state it can't return from
`define	DC3			3'bxxx		//dont care(3bit)


//MOV op Macros
`define Mem			2'b10 		//MtoR 
`define RtoR		2'b00

//ALUop macros
`define ADD			2'b00		//add instruction
`define CMP			2'b01		//compare instruction
`define AND 		2'b10		//and instruction
`define	MVN			2'b11		//not instruction
`define DC2			2'bxx 		//Don't care (2bit)

//wait signal macros
`define Wait		1'b0		//FSM should wait, stay in the start stage
`define Go			1'b1		//FSM should go to the decode stage
`define	DC          1'bx		//don't care (1bit)

//register number macros
`define Rm 			3'b100		//nsel Rm
`define Rd			3'b010		//nsel Rd
`define Rn			3'b001		//nsel Rn

//memory commands
`define R 			2'b00		//read
module FSM_tb;
	
	reg[2:0] opcode;
	reg[1:0] op;
	reg reset,clk, error;
	
	wire[2:0] nsel; 
	wire[1:0] vsel, m_cmd;
	wire asel, bsel, loada, loadb, loadc, loads, write, reset_pc, load_pc, addr_sel, load_ir, load_addr;
	
	FSMcontroller DUT(reset, opcode, op, nsel, asel, bsel, vsel, loada, loadb, loadc, loads, write, clk, reset_pc, load_pc, addr_sel, load_ir, m_cmd, load_addr);
	
	
	//generic test for state machine, checks
	//the state and the output to ensure they 
	//are correct
	task myChecker;    
		input [3:0] expectedState; 
		input 		expectedOutput;
		input 		actualOutput;	
		begin
			//if the state is not correct, print the error message and set the error bit
			if( DUT.state !== expectedState ) begin
				$display("ERROR ** state is %b, expected %b", DUT.state, expectedState  );
				error = 1'b1;
			end
			//if the output	is not correct, print the error message and set the error bit
			if( actualOutput !== expectedOutput ) begin
				$display("ERROR ** output is %b, expected %b", actualOutput, expectedOutput );
				error = 1'b1;
			end
		end
	endtask
	
	task myChecker2;    
		input [3:0] expectedState; 
		input [1:0]	expectedOutput;
		input [1:0]	actualOutput;	
		begin
			//if the state is not correct, print the error message and set the error bit
			if( DUT.state !== expectedState ) begin
				$display("ERROR ** state is %b, expected %b", DUT.state, expectedState  );
				error = 1'b1;
			end
			//if the output	is not correct, print the error message and set the error bit
			if( actualOutput !== expectedOutput ) begin
				$display("ERROR ** output is %b, expected %b", actualOutput, expectedOutput );
				error = 1'b1;
			end
		end
	endtask
	
	task myChecker3;    
		input [3:0] expectedState; 
		input [2:0]	expectedOutput;
		input [2:0]	actualOutput;	
		begin
			//if the state is not correct, print the error message and set the error bit
			if( DUT.state !== expectedState ) begin
				$display("ERROR ** state is %b, expected %b", DUT.state, expectedState  );
				error = 1'b1;
			end
			//if the output	is not correct, print the error message and set the error bit
			if( actualOutput !== expectedOutput ) begin
				$display("ERROR ** output is %b, expected %b", actualOutput, expectedOutput );
				error = 1'b1;
			end
		end
	endtask
	
	initial begin
		forever begin
			clk = 0; 
			#5;
			clk = 1; 
			#5;
		end
    end
	
	initial begin
		error = 0;
		reset =1;
		#10;
		myChecker(`reset, 1, reset_pc);//check reset worked
		myChecker(`reset, 1, load_pc);//check reset worked
		
		
		reset =0;
		#10;
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
		
		
		//ADD
		opcode = `ALU;
		op = `ADD;
		#10
		myChecker(`getA, 1, loada);
		myChecker3(`getA, `Rn, nsel); 
		#10
		myChecker(`getB, 0, loada);
		myChecker(`getB, 1, loadb);
		myChecker3(`getB, `Rm, nsel);
		#10;
		myChecker(`operate, 0, asel);
		myChecker(`operate, 0, bsel);
		myChecker(`operate, 1, loadc);
		myChecker(`operate, 0, loads);
		#10
		myChecker2(`writeback, 2'b00, vsel);
		myChecker3(`writeback, `Rd, nsel);
		myChecker(`writeback, 1, write);
		#10
		
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
	
		
		//MOV MtoR
		opcode = `MOV;
		op = `Mem;
		#10
		myChecker(`MtoR, 1, write);
		myChecker2(`MtoR, 2'b10, vsel);
		myChecker3(`MtoR, `Rn, nsel);
		#10
  
  
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		myChecker(`updatePC, 1, load_pc);
		#10
		myChecker(`decode, 0, 0);
		
		//MOV Register to register
		opcode = `MOV;
		op = `RtoR;
		#10
		//should go straight to getB stage
		myChecker(`getB, 1, loadb);
		myChecker3(`getB, `Rm, nsel);
		#10
		//check its gone to operate
		myChecker(`operate, 1, asel);
		myChecker(`operate, 0, bsel);
		myChecker(`operate, 1, loadc);
		#10
		//check its gone to writeback
		myChecker2(`writeback, 2'b00, vsel);
		myChecker3(`writeback, `Rd, nsel);
		myChecker(`writeback, 1, write);
		#10;
		
		
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
		
		//test CMP instruction
		opcode = `ALU;
		op = `CMP;
		#10
		//check the getA state and outputs
		myChecker(`getA, 1, loada);
		myChecker3(`getA, `Rn, nsel); 
		#10
		//check the getB stage and outputs
		myChecker(`getB, 0, loada);
		myChecker(`getB, 1, loadb);
		myChecker3(`getB, `Rm, nsel);
		#10;
		//check the operate stage(last stage for CMP)
		myChecker(`operate, 0, asel);
		myChecker(`operate, 0, bsel);
		myChecker(`operate, 0, loadc);
		myChecker(`operate, 1, loads);

		
		#10;
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
		//MVN
		opcode = `ALU;
		op = `MVN;
		#10
		myChecker(`getB, 0, loada);
		myChecker(`getB, 1, loadb);
		myChecker3(`getB, `Rm, nsel);
		#10;
		myChecker(`operate, 0, asel);
		myChecker(`operate, 0, bsel);
		myChecker(`operate, 1, loadc);
		myChecker(`operate, 0, loads);
		#10
		myChecker2(`writeback, 2'b00, vsel);
		myChecker3(`writeback, `Rd, nsel);
		myChecker(`writeback, 1, write);
		
		
		#10;
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
		
		//AND
		opcode = `ALU;
		op = `AND;
		#10
		myChecker(`getA, 1, loada);
		myChecker3(`getA, `Rn, nsel); 
		#10
		myChecker(`getB, 0, loada);
		myChecker(`getB, 1, loadb);
		myChecker3(`getB, `Rm, nsel);
		#10;
		myChecker(`operate, 0, asel);
		myChecker(`operate, 0, bsel);
		myChecker(`operate, 1, loadc);
		myChecker(`operate, 0, loads);
		#10
		myChecker2(`writeback, 2'b00, vsel);
		myChecker3(`writeback, `Rd, nsel);
		myChecker(`writeback, 1, write);
		#10

		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		myChecker(`updatePC, 1, load_pc);
		#10
		myChecker(`decode, 0, 0);
		opcode = `LDR;
		#10
		myChecker(`getA, 0, 0);
		#10
		myChecker(`operate, 1, bsel);
		#10
		myChecker(`loadAddr, 1, load_addr);
		#10
		myChecker(`readRAM, 0, addr_sel);
		#10
		myChecker2(`MtoR, 2'b11, vsel); 
		#10
		
		
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
		
		
		opcode = `STR;
		#10
		myChecker(`getA, 0, 0);
		#10
		myChecker(`operate, 1, bsel);
		#10
		myChecker(`loadAddr, 1, load_addr);
		#10
		myChecker(`getB, `Rd, nsel);
		#10
		myChecker(`operate, 1, asel);
		myChecker(`operate, 0, bsel);
		#10
		myChecker(`writeRAM, 2'b01, m_cmd);
		#10
		
		
		
		
		//check to make sure its in IF1 and outputs are correct
		myChecker(`if1, 1, addr_sel);
		myChecker2(`if1, `R, m_cmd);
		#10;
		//check to make sure its in IF2 and outputs are correct
		myChecker(`if2, 1, addr_sel);
		myChecker2(`if2, `R, m_cmd);
		myChecker(`if2, 1, load_ir);
		#10
		//make sure its in the updatePC state and load PC is 1, so that pc updates
		myChecker(`updatePC, 1, load_pc);
		#10
		//make sure its transitioned to decode(no outputs to check)
		myChecker(`decode, 0, 0);
		
		opcode = `HALT;
		#10
		myChecker(`haltState, 0, 0);
		
		
		opcode = `ADD;
		#10
		myChecker(`haltState, 0, 0);
		
		if( ~error )
			$display("PASSED");//if no errors print passed
		else 
			$display("FAILED");//otherwise print failed
	
		$stop;
	end
	
	
	
	
endmodule