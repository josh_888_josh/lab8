module cpu_tb;

	reg clk, reset, load;
	reg [15:0] read_data;
	reg err;
	
	/*wire[15:0] instructionIn, sximm8, sximm5, out;
	wire[2:0] writenum, readnum, opcode, Z_out, nsel;
	wire[1:0] ALUop, shift, vsel, op;
	wire write, loada, loadb, loads, loadc, asel, bsel, N, V, Z, w; */

	cpu	DUT(clk,reset,load,read_data,write_data ,mem_addr,m_cmd);
	
	initial
	begin
		//forever loop, generates clock signal
		forever begin
			clk = 1'b0;						//clock goes to 1
			#5;								//wait 5 timesteps
			clk = 1'b1; 					//clock goes back to 0
			#5;								//wait 5 timesteps
		end
	end
	
	/*task check_PC;
	input[15:0] expectedOutput;
	begin
		if (DUT.PC !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.PC, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_m_cmd;
	input[15:0] expectedOutput;
	begin
		if (DUT.m_cmd !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.m_cmd, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	//temporary way of checking what is stored in a register
	task check_write_data;
	input[15:0] expectedOutput;
	begin
		if (DUT.write_data !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.write_data, expectedOutput);
			err = 1'b1;
		end
	end
	endtask
	
	task check_mem_addr;
	input expectedOutput;
	begin
		if (DUT.mem_addr !== expectedOutput) begin
			//if the output	is not correct, print the error message and set the error bit
			$display("ERROR ** output is %b, expected %b", DUT.mem_addr, expectedOutput);
			err = 1'b1;
		end
	end
	endtask */
	
	
	initial begin
		reset = 1;
		err=0;
		#10
		reset =0;
		
		//first test case implements the following assembly code
		//MOV R0, #7
		//MOV R1, #2
		//ADD R2, R0, R1, LSL#1
		//at the end out = 11
		
		$display("MOV R0, #7");			//move 7 into R0
		read_data = 16'b110_10_000_00000111;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R1, #2");			//move 2 into R1
		read_data = 16'b110_10_001_00000010;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("ADD R2, R0, R1, LSL#1");						
		read_data = 16'b101_00_000_010_01_001;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//second test case implements the following assembly code
		//MOV R2, R1
		//MOV R3, #2
		//CMP R3, R2
		//at the end Z = 1
		
		$display("MOV R2,R1");
		read_data = 16'b110_00_000_01000001;								
		load = 1;
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R3, #2");	
		read_data = 	16'b110_10_011_00000010;								
		load = 1;			
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("CMP R3, R2");					
		read_data = 16'b101_01_011_000_00_010;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//third test case implements the following assembly code
		//MOV R2, R0
		//MOV R3, #7
		//CMP R3, R2
		//at the end Z = 1
		
		$display("MOV R2,R0");
		read_data = 16'b110_00_000_01000000;								
		load = 1;			
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R3, #7");					
		read_data = 16'b110_10_011_00000111;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("CMP R3, R2");					
		read_data = 16'b101_01_010_000_00_000;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//fourth test case implements the following assembly code
		//MOV R4, R0
		//MOV R5, #8
		//CMP R4, R5
		//at the end Z = 0
		
		$display("MOV R4,R0");
		read_data = 16'b110_00_000_10000000;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R5, #8");					
		read_data = 16'b110_10_101_00001000;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("CMP R4, R5");					
		read_data = 16'b101_01_100_000_00_101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//fifth test case implements the following assembly code
		//AND R6, R5, R4
		//at the end out = 0
		
		$display("AND R6, R5, R4");					
		read_data = 16'b101_10_101_110_00_100;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("AND R2, R1, R0");					
		read_data = 16'b101_10_001_010_00_000;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
			
		//sixth test case implements the following assembly code
		//MOV R5, #5
		//MOV R6, #7
		//AND R7, R6, R5
		//at the end out = 5
		
		$display("MOV R5, #5");
		read_data = 16'b110_10_101_00000101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R6, #7");
		read_data = 16'b110_10_110_00000111;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("AND R7, R6, R5");					
		read_data = 16'b101_10_110_111_00_101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//seventh test case implements the following assembly code
		//MOV R5, #47
		//MOV R6, #61
		//AND R7, R6, R5, LSL#1
		//at the end out = 28
		
		$display("MOV R5, #47");
		read_data = 16'b110_10_101_00101111;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R6, #61");
		read_data = 16'b110_10_110_00111101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("AND R7, R6, R5, LSL#1");					
		read_data = 16'b101_10_110_111_01_101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//eighth test case implements the following assembly code
		//MVN R7, R5
		//at the end out = 16'b1111_1111_1101_0000
	
		$display("MVN R7, R5");
		read_data = 16'b101_11_000_11100101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//ninth test case implements the following assembly code
		//MVN R7, R6
		//at the end out = 16'b1111_1111_1100_0010
		
		$display("MVN R7, R6");
		read_data = 16'b101_11_000_11100110;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//tenth test case implements the following assembly code
		//MVN R7, R5, RSR#1
		//at the end out = 16'b1111_1111_1110_1000
		
		$display("MVN R7, R5, RSR#1");
		read_data = 16'b101_11_000_11110101;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//eleventh test case implements the following assembly code
		//MOV R0, #42
		//MOV R1, #30
		//ADD R2, R0, R1
		//at the end out = 72
		
		$display("MOV R0, #42");			
		read_data = 16'b110_10_000_00101010;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R1, #30");			
		read_data = 16'b110_10_001_00011110;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("ADD R2, R0, R1");						
		read_data = 16'b101_00_000_010_00_001;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//twelvth test case implements the following assembly code
		//MOV R0, #11
		//MOV R1, #22
		//ADD R2, R0, R1
		//at the end out = 33
		
		$display("MOV R0, #11");			
		read_data = 16'b110_10_000_00001011;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("MOV R1, #22");			
		read_data = 16'b110_10_001_00010110;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		$display("ADD R2, R0, R1");						
		read_data = 16'b101_00_000_010_00_001;								
		load = 1;					
		@(posedge DUT.PC or negedge DUT.PC);
		
		//check the error wire, it will be low if there are no errors
		if( ~err )
		$display("PASSED");	//if no errors print passed
		else 
		$display("FAILED");	//otherwise print failed
		
		
		$stop;
		end
endmodule
	
	
	
	
	
	
	
	
	
	
	
	
	
	