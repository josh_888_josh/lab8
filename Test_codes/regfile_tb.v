module regfile_tb;
	
	reg 			err, write, clk;
	reg		[15:0] 	data_in;
	reg		[2:0]	writenum, readnum, i;
	wire	[15:0]	data_out;
	
	regfile DUT(.data_in(data_in), .writenum(writenum), .write(write), .readnum(readnum), .clk(clk), .data_out(data_out));
	initial begin
		//generate a clock signal
		forever begin
			clk = 1; 
			#5;
			clk = 0; 
			#5;
		end
    end
	
	task checkRegister;   
		input	[16:0] 	expectedOutput;       
		begin
			//if the output	is not correct, print the error message and set the error bit
			if( data_out !== expectedOutput ) begin
				$display("ERROR ** output is %b, expected %b", data_out, expectedOutput );
				err = 1'b1;
			end
		end
	endtask
	
	task setRegister;
		input 	[2:0] 	regNum;
		input 	[15:0]	regValue;
		begin
			writenum 	= regNum;					//set the register to write to
			write 		= 1'b1;						//set write to one so it can be written to
			data_in 	= regValue;					//set the register's value
		end
	endtask
	
	initial begin
		err = 1'b0;
		i = 0; //counter variable
		//first set each of the registers to a bunch of random values, to ensure they work for multiple values
		repeat(8) begin
			setRegister(i, 3); 							//set the register to 3
			readnum = i;								//read from reg i
			#10
			checkRegister(3);							//check the register to make sure it took in the new value in a single cycle(3)
		
			setRegister(i, 5);							//set the register to 5
			#10
			checkRegister(5);							//check the register to make sure it took in the new value in a single cycle(5)
		
			setRegister(i, 2568);						//set the register to 2568
			#10
			checkRegister(2568);						//check the register to make sure it took in the new value in a single cycle(2568)
		
			setRegister(i, 2111);						//set the register to 2111
			#10
			checkRegister(2111);						//check the register to make sure it took in the new value in a single cycle(2111)
		
			setRegister(i, 16'b1111111111111111);		//set the register to all 1s(to ensure all bits of the are working, not just the first few)
			#10
			checkRegister(16'b1111111111111111);		//check the register to make sure it took in the new value in a single cycle(all 1s)
		
			setRegister(i, 16'b0000000000000000);		//set the register to all 0s(to ensure all bits of the are working, not just the first few)
			#10
			checkRegister(16'b0000000000000000);		//check the register to make sure it took in the new value in a single cycle(all 0s)
		
			setRegister(i, 27);							//set the register to 27
			#10
			checkRegister(27);							//check the register to make sure it took in the new value in a single cycle(27)
		
			setRegister(i, 16'b1010101010101010);		//set the register to 16'b1010101010101010
			#10
			checkRegister(16'b1010101010101010);		//check the register to make sure it took in the new value in a single cycle(16'b1010101010101010)
			i = i + 1;
		end
		
		
		//now check the registers again after many cycles, to make sure they persist for more than one clock cycle
		#100
		i = 0;
		repeat(8) begin
			readnum = i;								//read from reg i
			#10
			checkRegister(16'b1010101010101010);		//check the register to make sure it took in the new value in a single cycle(16'b1010101010101010)
			i = i + 1;
		end
		
		//check the error wire, it will be low if there are no errors
		if( ~err )
			$display("PASSED");//if no errors print passed
		else 
			$display("FAILED");//otherwise print failed
	
		
		$stop;
	end
endmodule