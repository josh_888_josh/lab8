-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 15.0.0 Build 145 04/22/2015 Patches 0.01we SJ Web Edition"

-- DATE "11/04/2018 22:15:25"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	lab8_top IS
    PORT (
	KEY : IN std_logic_vector(3 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0);
	HEX4 : OUT std_logic_vector(6 DOWNTO 0);
	HEX5 : OUT std_logic_vector(6 DOWNTO 0);
	CLOCK_50 : IN std_logic
	);
END lab8_top;

-- Design Ports Information
-- KEY[0]	=>  Location: PIN_AF25,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_F9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_AF26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_W21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_AF21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_AE16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_AG17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_AJ22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_AF20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_AG27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_AH23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_AE19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_AK14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_AH18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_AK19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_AF16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_AE17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_AK18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_AK16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_AA16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_AJ16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_AG21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_AJ19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_AK13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_AH15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_AK12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_AJ12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_AH10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_AG15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_AJ14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_AH17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_AH20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_AH12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_AG16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[0]	=>  Location: PIN_AD17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[1]	=>  Location: PIN_AK24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[2]	=>  Location: PIN_AK22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[3]	=>  Location: PIN_AK23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[4]	=>  Location: PIN_Y18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[5]	=>  Location: PIN_AF18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[6]	=>  Location: PIN_AA18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[0]	=>  Location: PIN_AH19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[1]	=>  Location: PIN_AG18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[2]	=>  Location: PIN_AJ21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[3]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[4]	=>  Location: PIN_AC18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[5]	=>  Location: PIN_AJ17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[6]	=>  Location: PIN_AG20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_AG22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_Y17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_AH22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AK21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AG23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AJ20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AE18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_AF19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_AH24,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF lab8_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX4 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX5 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(39 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(39 DOWNTO 0);
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \CLOCK_50~inputCLKENA0_outclk\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Decoder0~1_combout\ : std_logic;
SIGNAL \write~combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux1~3_combout\ : std_logic;
SIGNAL \CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux1~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Decoder0~3_combout\ : std_logic;
SIGNAL \rtl~4_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \CPU|FSM|CO|Decoder0~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux1~4_combout\ : std_logic;
SIGNAL \CPU|FSM|storeFlag|regInput[0]~0_combout\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \e~combout\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector7~0_combout\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux9~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Equal6~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux9~2_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Decoder0~4_combout\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector0~0_combout\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \CPU|Add0~30\ : std_logic;
SIGNAL \CPU|Add0~33_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ : std_logic;
SIGNAL \read_data[3]~15_combout\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \CPU|FSM|CO|loadb~0_combout\ : std_logic;
SIGNAL \rtl~9_combout\ : std_logic;
SIGNAL \rtl~28_combout\ : std_logic;
SIGNAL \rtl~32_combout\ : std_logic;
SIGNAL \CPU|Add0~34\ : std_logic;
SIGNAL \CPU|Add0~1_sumout\ : std_logic;
SIGNAL \rtl~29_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector15~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|load_pc~0_combout\ : std_logic;
SIGNAL \CPU|DP|Add0~22\ : std_logic;
SIGNAL \CPU|DP|Add0~26\ : std_logic;
SIGNAL \CPU|DP|Add0~30\ : std_logic;
SIGNAL \CPU|DP|Add0~1_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[8]~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg5|register|out[8]~feeder_combout\ : std_logic;
SIGNAL \rtl~36_combout\ : std_logic;
SIGNAL \rtl~33_combout\ : std_logic;
SIGNAL \rtl~34_combout\ : std_logic;
SIGNAL \rtl~31_combout\ : std_logic;
SIGNAL \rtl~35_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|comb~3_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|comb~5_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[1]~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg0|register|out[10]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[10]~30_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[10]~32_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[10]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[10]~31_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[1]~4_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[9]~13_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[10]~24_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[10]~14_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[10]~23_combout\ : std_logic;
SIGNAL \CPU|DP|a|register|out[7]~feeder_combout\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \CPU|DP|Ain[12]~3_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector11~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg6|register|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[14]~44_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg5|register|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg4|register|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[14]~42_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[14]~43_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[14]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|b|register|out[13]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[13]~17_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[13]~30_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[13]~31_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[13]~18_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg5|register|out[13]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[13]~39_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[13]~41_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[13]~40_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[11]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[12]~16_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[12]~16_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[12]~17_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[12]~38_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[12]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[12]~37_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg2|register|out[12]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[12]~36_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[11]~15_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[11]~14_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[11]~15_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[15]~45_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg5|register|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[15]~46_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[15]~47_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[15]~feeder_combout\ : std_logic;
SIGNAL \CPU|ID|shift[0]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector8~1_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[15]~19_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[15]~20_combout\ : std_logic;
SIGNAL \CPU|DP|a|register|out[14]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[14]~4_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[14]~18_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|comb~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[15]~32_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[15]~33_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[15]~21_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a1\ : std_logic;
SIGNAL \read_data[1]~11_combout\ : std_logic;
SIGNAL \CPU|Add0~38_cout\ : std_logic;
SIGNAL \CPU|Add0~6\ : std_logic;
SIGNAL \CPU|Add0~9_sumout\ : std_logic;
SIGNAL \CPU|DP|Add0~5_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[1]~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg4|register|out[1]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[1]~6_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[1]~7_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[1]~8_combout\ : std_logic;
SIGNAL \CPU|DP|Add0~6\ : std_logic;
SIGNAL \CPU|DP|Add0~10\ : std_logic;
SIGNAL \CPU|DP|Add0~14\ : std_logic;
SIGNAL \CPU|DP|Add0~17_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[4]~5_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[4]~15_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[4]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[4]~16_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[4]~17_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[3]~7_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[10]~13_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a9\ : std_logic;
SIGNAL \read_data[9]~10_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[9]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[9]~28_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg2|register|out[9]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[9]~27_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[9]~29_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[9]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[8]~12_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[9]~11_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[9]~12_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a10\ : std_logic;
SIGNAL \read_data[10]~12_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[8]~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[8]~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[8]~1_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[7]~11_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[8]~29_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[8]~28_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[8]~10_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a8\ : std_logic;
SIGNAL \read_data[8]~5_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|b[0]~0_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|b[2]~2_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|comb~4_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|b[1]~1_combout\ : std_logic;
SIGNAL \rtl~30_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|comb~1_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|comb~0_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|comb~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[0]~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[0]~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[0]~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[0]~5_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[1]~5_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[1]~1_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[0]~0_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[0]~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[2]~6_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[3]~3_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a2\ : std_logic;
SIGNAL \read_data[2]~13_combout\ : std_logic;
SIGNAL \CPU|Add0~10\ : std_logic;
SIGNAL \CPU|Add0~13_sumout\ : std_logic;
SIGNAL \CPU|Add0~14\ : std_logic;
SIGNAL \CPU|Add0~18\ : std_logic;
SIGNAL \CPU|Add0~21_sumout\ : std_logic;
SIGNAL \CPU|DP|Add0~18\ : std_logic;
SIGNAL \CPU|DP|Add0~21_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[5]~6_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[5]~19_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[5]~20_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg0|register|out[5]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg2|register|out[5]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[5]~18_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[7]~22_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[7]~9_combout\ : std_logic;
SIGNAL \CPU|DP|Add0~29_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[7]~8_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[7]~24_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[7]~26_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[7]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[7]~25_combout\ : std_logic;
SIGNAL \CPU|data_address|register|out[7]~feeder_combout\ : std_logic;
SIGNAL \CPU|mem_addr[7]~8_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a5\ : std_logic;
SIGNAL \read_data[5]~7_combout\ : std_logic;
SIGNAL \CPU|Add0~22\ : std_logic;
SIGNAL \CPU|Add0~25_sumout\ : std_logic;
SIGNAL \CPU|Add0~26\ : std_logic;
SIGNAL \CPU|Add0~29_sumout\ : std_logic;
SIGNAL \CPU|mem_addr[6]~7_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a4\ : std_logic;
SIGNAL \read_data[4]~14_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|out[6]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|Bin[6]~10_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[6]~7_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[6]~8_combout\ : std_logic;
SIGNAL \CPU|DP|Add0~25_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[6]~7_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg4|register|out[6]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg2|register|out[6]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[6]~21_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[6]~23_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[6]~22_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[5]~9_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[5]~6_combout\ : std_logic;
SIGNAL \CPU|mem_addr[5]~6_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a3\ : std_logic;
SIGNAL \CPU|DP|Add0~13_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[3]~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[3]~12_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[3]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[3]~13_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[3]~14_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[4]~8_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[4]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[4]~5_combout\ : std_logic;
SIGNAL \CPU|mem_addr[4]~5_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a7\ : std_logic;
SIGNAL \read_data[7]~6_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|out[11]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[11]~34_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[11]~33_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[11]~35_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[14]~26_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[14]~25_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[14]~27_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[14]~19_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|WideOr0~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|WideOr0~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|WideOr0~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|WideOr0~combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector12~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ovf~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ovf~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ovf~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector0~1_combout\ : std_logic;
SIGNAL \CPU|Add0~17_sumout\ : std_logic;
SIGNAL \CPU|mem_addr[3]~4_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a6\ : std_logic;
SIGNAL \read_data[6]~8_combout\ : std_logic;
SIGNAL \CPU|DP|Add0~9_sumout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|b[2]~3_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[2]~10_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg4|register|out[2]~feeder_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[2]~9_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|b[2]~11_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[2]~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[2]~2_combout\ : std_logic;
SIGNAL \CPU|mem_addr[2]~3_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a12\ : std_logic;
SIGNAL \read_data[12]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[1]~1_combout\ : std_logic;
SIGNAL \CPU|mem_addr[1]~2_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \read_data[0]~9_combout\ : std_logic;
SIGNAL \CPU|Add0~5_sumout\ : std_logic;
SIGNAL \CPU|mem_addr[0]~1_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a15\ : std_logic;
SIGNAL \read_data[15]~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr5~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector14~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideNor4~combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Decoder0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr3~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector14~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr3~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr3~2_combout\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \CPU|FSM|CS|WideNor7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideNor27~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr1~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr1~combout\ : std_logic;
SIGNAL \rtl~27_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a11\ : std_logic;
SIGNAL \read_data[11]~3_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux1~2_combout\ : std_logic;
SIGNAL \CPU|mem_addr[8]~0_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a14\ : std_logic;
SIGNAL \read_data[14]~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Mux9~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr7~3_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr7~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr7~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr5~3_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr7~4_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr5~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr5~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr5~4_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|WideOr5~5_combout\ : std_logic;
SIGNAL \rtl~26_combout\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ram_block1a13\ : std_logic;
SIGNAL \read_data[13]~0_combout\ : std_logic;
SIGNAL \CPU|DP|Ain[0]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|Selector8~0_combout\ : std_logic;
SIGNAL \CPU|DP|Bin[0]~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|out[0]~0_combout\ : std_logic;
SIGNAL \io|setLEDs~0_combout\ : std_logic;
SIGNAL \io|setLEDs~1_combout\ : std_logic;
SIGNAL \io|setLEDs~combout\ : std_logic;
SIGNAL \io|LEDregister|register|out[4]~feeder_combout\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \write~0_combout\ : std_logic;
SIGNAL \hexOut[3]~3_combout\ : std_logic;
SIGNAL \hexOut[2]~2_combout\ : std_logic;
SIGNAL \hexOut[1]~1_combout\ : std_logic;
SIGNAL \hexOut[0]~0_combout\ : std_logic;
SIGNAL \H0|WideOr6~0_combout\ : std_logic;
SIGNAL \H0|WideOr5~0_combout\ : std_logic;
SIGNAL \H0|WideOr4~0_combout\ : std_logic;
SIGNAL \H0|WideOr3~0_combout\ : std_logic;
SIGNAL \H0|WideOr2~0_combout\ : std_logic;
SIGNAL \H0|WideOr1~0_combout\ : std_logic;
SIGNAL \H0|WideOr0~0_combout\ : std_logic;
SIGNAL \hexOut[7]~7_combout\ : std_logic;
SIGNAL \hexOut[5]~5_combout\ : std_logic;
SIGNAL \hexOut[4]~4_combout\ : std_logic;
SIGNAL \hexOut[6]~6_combout\ : std_logic;
SIGNAL \H1|WideOr6~0_combout\ : std_logic;
SIGNAL \H1|WideOr5~0_combout\ : std_logic;
SIGNAL \H1|WideOr4~0_combout\ : std_logic;
SIGNAL \H1|WideOr3~0_combout\ : std_logic;
SIGNAL \H1|WideOr2~0_combout\ : std_logic;
SIGNAL \H1|WideOr1~0_combout\ : std_logic;
SIGNAL \H1|WideOr0~0_combout\ : std_logic;
SIGNAL \hexOut[8]~8_combout\ : std_logic;
SIGNAL \hexOut[9]~9_combout\ : std_logic;
SIGNAL \hexOut[10]~10_combout\ : std_logic;
SIGNAL \hexOut[11]~11_combout\ : std_logic;
SIGNAL \H2|WideOr6~0_combout\ : std_logic;
SIGNAL \H2|WideOr5~0_combout\ : std_logic;
SIGNAL \H2|WideOr4~0_combout\ : std_logic;
SIGNAL \H2|WideOr3~0_combout\ : std_logic;
SIGNAL \H2|WideOr2~0_combout\ : std_logic;
SIGNAL \H2|WideOr1~0_combout\ : std_logic;
SIGNAL \H2|WideOr0~0_combout\ : std_logic;
SIGNAL \hexOut[14]~14_combout\ : std_logic;
SIGNAL \hexOut[12]~12_combout\ : std_logic;
SIGNAL \hexOut[15]~15_combout\ : std_logic;
SIGNAL \hexOut[13]~13_combout\ : std_logic;
SIGNAL \H3|WideOr6~0_combout\ : std_logic;
SIGNAL \H3|WideOr5~0_combout\ : std_logic;
SIGNAL \H3|WideOr4~0_combout\ : std_logic;
SIGNAL \H3|WideOr3~0_combout\ : std_logic;
SIGNAL \H3|WideOr2~0_combout\ : std_logic;
SIGNAL \H3|WideOr1~0_combout\ : std_logic;
SIGNAL \H3|WideOr0~0_combout\ : std_logic;
SIGNAL \H4|WideOr6~0_combout\ : std_logic;
SIGNAL \H4|WideOr5~0_combout\ : std_logic;
SIGNAL \H4|WideOr4~0_combout\ : std_logic;
SIGNAL \H4|WideOr3~0_combout\ : std_logic;
SIGNAL \H4|WideOr2~0_combout\ : std_logic;
SIGNAL \H4|WideOr1~0_combout\ : std_logic;
SIGNAL \H4|WideOr0~0_combout\ : std_logic;
SIGNAL \H5|WideOr6~0_combout\ : std_logic;
SIGNAL \H5|WideOr5~0_combout\ : std_logic;
SIGNAL \H5|WideOr4~0_combout\ : std_logic;
SIGNAL \H5|WideOr3~0_combout\ : std_logic;
SIGNAL \H5|WideOr2~0_combout\ : std_logic;
SIGNAL \H5|WideOr1~0_combout\ : std_logic;
SIGNAL \H5|WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|DP|c|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \io|LEDregister|register|out\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \CPU|data_address|register|out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|as|p\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|FSM|stateRegister|out\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CPU|DP|WBmux|m|b\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|FSM|storeFlag|register|out\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|program_counter|register|out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|ID|RWmux|b\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg5|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|ai|g\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \CPU|InstructionRegister|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg0|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg2|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|readMux|m|b\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg4|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg1|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|regSelect\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg3|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|ai|c\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg6|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg7|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|a|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|b|register|out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|ai|p\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|ai|s\ : std_logic_vector(14 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|as|s\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|DP|status|register|out\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|DP|b|register|ALT_INV_out[13]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|b|register|ALT_INV_out[11]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|b|register|ALT_INV_out[9]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|b|register|ALT_INV_out[6]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|DP|b|register|ALT_INV_out[1]~DUPLICATE_q\ : std_logic;
SIGNAL \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_KEY[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[9]~input_o\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[15]~33_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[15]~32_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[13]~31_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[13]~30_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[8]~29_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[8]~28_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ALT_INV_ovf~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ALT_INV_ovf~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[14]~27_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[14]~26_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[14]~25_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[10]~24_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[10]~23_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[7]~22_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b\ : std_logic_vector(15 DOWNTO 1);
SIGNAL \CPU|DP|ALU|ALT_INV_WideOr0~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_WideOr0~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_WideOr0~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[7]~8_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[6]~7_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[5]~6_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[4]~5_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[3]~4_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[2]~3_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[1]~2_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[0]~1_combout\ : std_logic;
SIGNAL \ALT_INV_e~combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b\ : std_logic_vector(15 DOWNTO 7);
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~47_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~46_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~45_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~44_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~43_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~42_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~41_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~40_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~39_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~38_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~37_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~36_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~35_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~34_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~33_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~32_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~31_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~30_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~29_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~28_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~27_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[10]~12_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[9]~10_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~36_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_b[8]~0_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\ : std_logic;
SIGNAL \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux9~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux9~1_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[8]~5_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Selector0~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \CPU|DP|status|register|ALT_INV_out\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \CPU|FSM|CO|ALT_INV_Selector0~0_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~26_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~25_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~24_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~23_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~22_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~21_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~20_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~19_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~18_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~17_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~16_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~15_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~14_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~13_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~12_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~11_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~10_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~9_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~8_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~7_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~6_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~5_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~4_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[15]~20_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|as|ALT_INV_s\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|as|ALT_INV_p\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|DP|a|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|ALT_INV_comb~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[15]~19_combout\ : std_logic;
SIGNAL \CPU|ID|ALT_INV_shift[0]~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[14]~19_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\ : std_logic_vector(13 DOWNTO 3);
SIGNAL \CPU|DP|ALT_INV_Ain[14]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[14]~18_combout\ : std_logic;
SIGNAL \CPU|DP|b|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\ : std_logic_vector(13 DOWNTO 3);
SIGNAL \CPU|DP|ALU|ALT_INV_out[13]~18_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\ : std_logic_vector(13 DOWNTO 2);
SIGNAL \CPU|DP|ALT_INV_Bin[13]~17_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[12]~16_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_s\ : std_logic_vector(12 DOWNTO 12);
SIGNAL \CPU|DP|ALT_INV_Ain[12]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[12]~16_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[11]~15_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[11]~14_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[11]~15_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[10]~13_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[10]~14_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[9]~12_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[9]~11_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[9]~13_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[8]~10_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[8]~12_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[7]~9_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[7]~11_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[6]~8_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[6]~7_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[6]~10_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[5]~6_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[5]~9_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[4]~5_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[4]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[4]~8_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[3]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[3]~7_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Selector7~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[2]~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[2]~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[2]~6_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[1]~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|plusMinus|ai|ALT_INV_c[1]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Selector8~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[1]~1_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[1]~5_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[1]~4_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[1]~3_combout\ : std_logic;
SIGNAL \CPU|DP|ALU|ALT_INV_out[0]~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[0]~2_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Bin[0]~1_combout\ : std_logic;
SIGNAL \CPU|InstructionRegister|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \CPU|DP|ALT_INV_Bin[0]~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Selector8~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Ain[0]~0_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[12]~4_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[11]~3_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~4_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Decoder0~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~2_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~1_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~0_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_b\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \ALT_INV_rtl~35_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~34_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~33_combout\ : std_logic;
SIGNAL \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_b[2]~2_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_comb~5_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_b[1]~1_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_comb~4_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_b[0]~0_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_comb~3_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~32_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~9_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_comb~2_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_comb~1_combout\ : std_logic;
SIGNAL \CPU|ID|RWmux|ALT_INV_comb~0_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~31_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~30_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~29_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr7~3_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr7~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr7~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr1~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideNor7~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr5~4_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr5~3_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux1~4_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideNor27~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux9~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr5~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr5~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr3~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr1~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\ : std_logic;
SIGNAL \CPU|FSM|storeFlag|register|ALT_INV_out\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \CPU|FSM|CS|ALT_INV_WideNor4~combout\ : std_logic;
SIGNAL \CPU|FSM|CS|ALT_INV_WideOr3~0_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~28_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Decoder0~0_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[15]~2_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[14]~1_combout\ : std_logic;
SIGNAL \ALT_INV_read_data[13]~0_combout\ : std_logic;
SIGNAL \io|ALT_INV_setLEDs~1_combout\ : std_logic;
SIGNAL \io|ALT_INV_setLEDs~0_combout\ : std_logic;
SIGNAL \H5|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[7]~8_combout\ : std_logic;
SIGNAL \CPU|data_address|register|ALT_INV_out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \CPU|ALT_INV_mem_addr[6]~7_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[5]~6_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[4]~5_combout\ : std_logic;
SIGNAL \H4|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[3]~4_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[2]~3_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[1]~2_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[0]~1_combout\ : std_logic;
SIGNAL \H3|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[15]~15_combout\ : std_logic;
SIGNAL \CPU|DP|c|register|ALT_INV_out\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \ALT_INV_hexOut[14]~14_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[13]~13_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[12]~12_combout\ : std_logic;
SIGNAL \H2|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[11]~11_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[10]~10_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[9]~9_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[8]~8_combout\ : std_logic;
SIGNAL \CPU|ALT_INV_mem_addr[8]~0_combout\ : std_logic;
SIGNAL \H1|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[7]~7_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[6]~6_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[5]~5_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[4]~4_combout\ : std_logic;
SIGNAL \H0|ALT_INV_WideOr0~0_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[3]~3_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[2]~2_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[1]~1_combout\ : std_logic;
SIGNAL \ALT_INV_hexOut[0]~0_combout\ : std_logic;
SIGNAL \ALT_INV_write~0_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~27_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux1~2_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux1~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \ALT_INV_rtl~26_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Equal6~0_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Selector14~1_combout\ : std_logic;
SIGNAL \CPU|FSM|CO|ALT_INV_Selector14~0_combout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~29_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~25_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~21_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \CPU|DP|ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \CPU|program_counter|register|ALT_INV_out\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\ : std_logic;
SIGNAL \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\ : std_logic;
SIGNAL \CPU|FSM|stateRegister|ALT_INV_out\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CPU|DP|b|register|ALT_INV_out[14]~DUPLICATE_q\ : std_logic;

BEGIN

ww_KEY <= KEY;
ww_SW <= SW;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
ww_CLOCK_50 <= CLOCK_50;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & \CPU|DP|c|register|out\(15) & 
\CPU|DP|c|register|out\(14) & \CPU|DP|c|register|out\(13) & \CPU|DP|c|register|out\(12) & \CPU|DP|c|register|out\(11) & \CPU|DP|c|register|out\(10) & \CPU|DP|c|register|out\(9) & \CPU|DP|c|register|out\(8) & \CPU|DP|c|register|out\(7) & 
\CPU|DP|c|register|out\(6) & \CPU|DP|c|register|out\(5) & \CPU|DP|c|register|out\(4) & \CPU|DP|c|register|out\(3) & \CPU|DP|c|register|out\(2) & \CPU|DP|c|register|out\(1) & \CPU|DP|c|register|out\(0));

\MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\CPU|mem_addr[7]~8_combout\ & \CPU|mem_addr[6]~7_combout\ & \CPU|mem_addr[5]~6_combout\ & \CPU|mem_addr[4]~5_combout\ & \CPU|mem_addr[3]~4_combout\ & \CPU|mem_addr[2]~3_combout\ & 
\CPU|mem_addr[1]~2_combout\ & \CPU|mem_addr[0]~1_combout\);

\MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\CPU|mem_addr[7]~8_combout\ & \CPU|mem_addr[6]~7_combout\ & \CPU|mem_addr[5]~6_combout\ & \CPU|mem_addr[4]~5_combout\ & \CPU|mem_addr[3]~4_combout\ & \CPU|mem_addr[2]~3_combout\ & 
\CPU|mem_addr[1]~2_combout\ & \CPU|mem_addr[0]~1_combout\);

\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);
\MEM|mem_rtl_0|auto_generated|ram_block1a1\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(1);
\MEM|mem_rtl_0|auto_generated|ram_block1a2\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(2);
\MEM|mem_rtl_0|auto_generated|ram_block1a3\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(3);
\MEM|mem_rtl_0|auto_generated|ram_block1a4\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(4);
\MEM|mem_rtl_0|auto_generated|ram_block1a5\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(5);
\MEM|mem_rtl_0|auto_generated|ram_block1a6\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(6);
\MEM|mem_rtl_0|auto_generated|ram_block1a7\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(7);
\MEM|mem_rtl_0|auto_generated|ram_block1a8\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(8);
\MEM|mem_rtl_0|auto_generated|ram_block1a9\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(9);
\MEM|mem_rtl_0|auto_generated|ram_block1a10\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(10);
\MEM|mem_rtl_0|auto_generated|ram_block1a11\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(11);
\MEM|mem_rtl_0|auto_generated|ram_block1a12\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(12);
\MEM|mem_rtl_0|auto_generated|ram_block1a13\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(13);
\MEM|mem_rtl_0|auto_generated|ram_block1a14\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(14);
\MEM|mem_rtl_0|auto_generated|ram_block1a15\ <= \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(15);
\CPU|DP|b|register|ALT_INV_out[13]~DUPLICATE_q\ <= NOT \CPU|DP|b|register|out[13]~DUPLICATE_q\;
\CPU|DP|b|register|ALT_INV_out[11]~DUPLICATE_q\ <= NOT \CPU|DP|b|register|out[11]~DUPLICATE_q\;
\CPU|DP|b|register|ALT_INV_out[9]~DUPLICATE_q\ <= NOT \CPU|DP|b|register|out[9]~DUPLICATE_q\;
\CPU|DP|b|register|ALT_INV_out[6]~DUPLICATE_q\ <= NOT \CPU|DP|b|register|out[6]~DUPLICATE_q\;
\CPU|DP|b|register|ALT_INV_out[1]~DUPLICATE_q\ <= NOT \CPU|DP|b|register|out[1]~DUPLICATE_q\;
\CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\ <= NOT \CPU|InstructionRegister|register|out[14]~DUPLICATE_q\;
\ALT_INV_KEY[1]~input_o\ <= NOT \KEY[1]~input_o\;
\ALT_INV_SW[7]~input_o\ <= NOT \SW[7]~input_o\;
\ALT_INV_SW[6]~input_o\ <= NOT \SW[6]~input_o\;
\ALT_INV_SW[5]~input_o\ <= NOT \SW[5]~input_o\;
\ALT_INV_SW[4]~input_o\ <= NOT \SW[4]~input_o\;
\ALT_INV_SW[3]~input_o\ <= NOT \SW[3]~input_o\;
\ALT_INV_SW[2]~input_o\ <= NOT \SW[2]~input_o\;
\ALT_INV_SW[1]~input_o\ <= NOT \SW[1]~input_o\;
\ALT_INV_SW[0]~input_o\ <= NOT \SW[0]~input_o\;
\ALT_INV_SW[9]~input_o\ <= NOT \SW[9]~input_o\;
\CPU|DP|ALU|ALT_INV_out[15]~33_combout\ <= NOT \CPU|DP|ALU|out[15]~33_combout\;
\CPU|DP|ALU|ALT_INV_out[15]~32_combout\ <= NOT \CPU|DP|ALU|out[15]~32_combout\;
\CPU|DP|ALU|ALT_INV_out[13]~31_combout\ <= NOT \CPU|DP|ALU|out[13]~31_combout\;
\CPU|DP|ALU|ALT_INV_out[13]~30_combout\ <= NOT \CPU|DP|ALU|out[13]~30_combout\;
\CPU|DP|ALU|ALT_INV_out[8]~29_combout\ <= NOT \CPU|DP|ALU|out[8]~29_combout\;
\CPU|DP|ALU|ALT_INV_out[8]~28_combout\ <= NOT \CPU|DP|ALU|out[8]~28_combout\;
\CPU|DP|ALU|plusMinus|ALT_INV_ovf~2_combout\ <= NOT \CPU|DP|ALU|plusMinus|ovf~2_combout\;
\CPU|DP|ALU|plusMinus|ALT_INV_ovf~1_combout\ <= NOT \CPU|DP|ALU|plusMinus|ovf~1_combout\;
\CPU|DP|ALU|ALT_INV_out[14]~27_combout\ <= NOT \CPU|DP|ALU|out[14]~27_combout\;
\CPU|DP|ALU|ALT_INV_out[14]~26_combout\ <= NOT \CPU|DP|ALU|out[14]~26_combout\;
\CPU|DP|ALU|ALT_INV_out[14]~25_combout\ <= NOT \CPU|DP|ALU|out[14]~25_combout\;
\CPU|DP|ALU|ALT_INV_out[10]~24_combout\ <= NOT \CPU|DP|ALU|out[10]~24_combout\;
\CPU|DP|ALU|ALT_INV_out[10]~23_combout\ <= NOT \CPU|DP|ALU|out[10]~23_combout\;
\CPU|DP|ALU|ALT_INV_out[7]~22_combout\ <= NOT \CPU|DP|ALU|out[7]~22_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(15) <= NOT \CPU|DP|WBmux|m|b\(15);
\CPU|DP|WBmux|m|ALT_INV_b\(14) <= NOT \CPU|DP|WBmux|m|b\(14);
\CPU|DP|WBmux|m|ALT_INV_b\(13) <= NOT \CPU|DP|WBmux|m|b\(13);
\CPU|DP|WBmux|m|ALT_INV_b\(12) <= NOT \CPU|DP|WBmux|m|b\(12);
\CPU|DP|WBmux|m|ALT_INV_b\(11) <= NOT \CPU|DP|WBmux|m|b\(11);
\CPU|DP|WBmux|m|ALT_INV_b\(10) <= NOT \CPU|DP|WBmux|m|b\(10);
\CPU|DP|WBmux|m|ALT_INV_b\(9) <= NOT \CPU|DP|WBmux|m|b\(9);
\CPU|DP|ALU|ALT_INV_WideOr0~3_combout\ <= NOT \CPU|DP|ALU|WideOr0~3_combout\;
\CPU|DP|ALU|ALT_INV_WideOr0~2_combout\ <= NOT \CPU|DP|ALU|WideOr0~2_combout\;
\CPU|DP|ALU|ALT_INV_WideOr0~1_combout\ <= NOT \CPU|DP|ALU|WideOr0~1_combout\;
\CPU|DP|ALU|ALT_INV_WideOr0~0_combout\ <= NOT \CPU|DP|ALU|WideOr0~0_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(7) <= NOT \CPU|DP|WBmux|m|b\(7);
\CPU|DP|WBmux|m|ALT_INV_b[7]~8_combout\ <= NOT \CPU|DP|WBmux|m|b[7]~8_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(6) <= NOT \CPU|DP|WBmux|m|b\(6);
\CPU|DP|WBmux|m|ALT_INV_b[6]~7_combout\ <= NOT \CPU|DP|WBmux|m|b[6]~7_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(5) <= NOT \CPU|DP|WBmux|m|b\(5);
\CPU|DP|WBmux|m|ALT_INV_b[5]~6_combout\ <= NOT \CPU|DP|WBmux|m|b[5]~6_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(4) <= NOT \CPU|DP|WBmux|m|b\(4);
\CPU|DP|WBmux|m|ALT_INV_b[4]~5_combout\ <= NOT \CPU|DP|WBmux|m|b[4]~5_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(3) <= NOT \CPU|DP|WBmux|m|b\(3);
\CPU|DP|WBmux|m|ALT_INV_b[3]~4_combout\ <= NOT \CPU|DP|WBmux|m|b[3]~4_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(2) <= NOT \CPU|DP|WBmux|m|b\(2);
\CPU|DP|WBmux|m|ALT_INV_b[2]~3_combout\ <= NOT \CPU|DP|WBmux|m|b[2]~3_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(1) <= NOT \CPU|DP|WBmux|m|b\(1);
\CPU|DP|WBmux|m|ALT_INV_b[1]~2_combout\ <= NOT \CPU|DP|WBmux|m|b[1]~2_combout\;
\CPU|DP|WBmux|m|ALT_INV_b[0]~1_combout\ <= NOT \CPU|DP|WBmux|m|b[0]~1_combout\;
\ALT_INV_e~combout\ <= NOT \e~combout\;
\CPU|DP|REGFILE|readMux|m|ALT_INV_b\(15) <= NOT \CPU|DP|REGFILE|readMux|m|b\(15);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~47_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[15]~47_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(15);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(15);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~46_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[15]~46_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(15);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(15);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~45_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[15]~45_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(15);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(15);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(15);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(15) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(15);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b\(14) <= NOT \CPU|DP|REGFILE|readMux|m|b\(14);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~44_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[14]~44_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(14);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(14);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~43_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[14]~43_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(14);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(14);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~42_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[14]~42_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(14);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(14);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(14);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(14) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(14);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~41_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[13]~41_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(13);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(13);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~40_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[13]~40_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(13);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(13);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~39_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[13]~39_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(13);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(13);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(13);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(13) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(13);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~38_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[12]~38_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(12);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(12);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~37_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[12]~37_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(12);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(12);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~36_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[12]~36_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(12);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(12);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(12);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(12) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(12);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~35_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[11]~35_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(11);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(11);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~34_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[11]~34_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(11);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(11);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~33_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[11]~33_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(11);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(11);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(11);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(11) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(11);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~32_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[10]~32_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(10);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(10);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~31_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[10]~31_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(10);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(10);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~30_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[10]~30_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(10);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(10);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(10);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(10) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(10);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~29_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[9]~29_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(9);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(9);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~28_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[9]~28_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(9);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(9);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~27_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[9]~27_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(9);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(9);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(9);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(9) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(9);
\ALT_INV_read_data[10]~12_combout\ <= NOT \read_data[10]~12_combout\;
\ALT_INV_read_data[9]~10_combout\ <= NOT \read_data[9]~10_combout\;
\ALT_INV_rtl~36_combout\ <= NOT \rtl~36_combout\;
\CPU|DP|WBmux|m|ALT_INV_b\(8) <= NOT \CPU|DP|WBmux|m|b\(8);
\CPU|DP|WBmux|m|ALT_INV_b[8]~0_combout\ <= NOT \CPU|DP|WBmux|m|b[8]~0_combout\;
\CPU|DP|WBmux|m|ALT_INV_comb~0_combout\ <= NOT \CPU|DP|WBmux|m|comb~0_combout\;
\CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\ <= NOT \CPU|DP|WBmux|d|ShiftLeft0~3_combout\;
\CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\ <= NOT \CPU|DP|WBmux|d|ShiftLeft0~2_combout\;
\CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\ <= NOT \CPU|DP|WBmux|d|ShiftLeft0~1_combout\;
\CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\ <= NOT \CPU|DP|WBmux|d|ShiftLeft0~0_combout\;
\CPU|FSM|CO|ALT_INV_Mux9~2_combout\ <= NOT \CPU|FSM|CO|Mux9~2_combout\;
\CPU|FSM|CO|ALT_INV_Mux9~1_combout\ <= NOT \CPU|FSM|CO|Mux9~1_combout\;
\ALT_INV_read_data[8]~5_combout\ <= NOT \read_data[8]~5_combout\;
\CPU|FSM|CO|ALT_INV_Selector0~1_combout\ <= NOT \CPU|FSM|CO|Selector0~1_combout\;
\CPU|FSM|CO|ALT_INV_Mux0~0_combout\ <= NOT \CPU|FSM|CO|Mux0~0_combout\;
\CPU|DP|status|register|ALT_INV_out\(0) <= NOT \CPU|DP|status|register|out\(0);
\CPU|DP|status|register|ALT_INV_out\(2) <= NOT \CPU|DP|status|register|out\(2);
\CPU|DP|status|register|ALT_INV_out\(1) <= NOT \CPU|DP|status|register|out\(1);
\CPU|FSM|CO|ALT_INV_Selector0~0_combout\ <= NOT \CPU|FSM|CO|Selector0~0_combout\;
\CPU|DP|REGFILE|readMux|m|ALT_INV_b\(7) <= NOT \CPU|DP|REGFILE|readMux|m|b\(7);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~26_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[7]~26_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(7);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(7);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~25_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[7]~25_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(7);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(7);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~24_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[7]~24_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(7);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(7);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(7);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(7) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(7);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~23_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[6]~23_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(6);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(6);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~22_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[6]~22_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(6);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(6);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~21_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[6]~21_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(6);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(6);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(6);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(6) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(6);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~20_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[5]~20_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(5);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(5);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~19_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[5]~19_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(5);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(5);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~18_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[5]~18_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(5);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(5);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(5);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(5) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(5);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~17_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[4]~17_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(4);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(4);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~16_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[4]~16_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(4);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(4);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~15_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[4]~15_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(4);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(4);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(4);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(4) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(4);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~14_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[3]~14_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(3);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(3);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~13_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[3]~13_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(3);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(3);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~12_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[3]~12_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(3);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(3);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(3);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(3) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(3);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~11_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[2]~11_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(2);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(2);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~10_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[2]~10_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(2);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(2);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~9_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[2]~9_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(2);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(2);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(2);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(2) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(2);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~8_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[1]~8_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(1);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(1);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~7_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[1]~7_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(1);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(1);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~6_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[1]~6_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(1);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(1);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(1);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(1) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(1);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~5_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[0]~5_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(0);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(0);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~4_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[0]~4_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(0);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(0);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~3_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[0]~3_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(0);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(0);
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(0);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(0) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(0);
\CPU|DP|ALU|ALT_INV_out[15]~20_combout\ <= NOT \CPU|DP|ALU|out[15]~20_combout\;
\CPU|DP|ALU|plusMinus|as|ALT_INV_s\(0) <= NOT \CPU|DP|ALU|plusMinus|as|s\(0);
\CPU|DP|ALU|plusMinus|as|ALT_INV_p\(0) <= NOT \CPU|DP|ALU|plusMinus|as|p\(0);
\CPU|DP|a|register|ALT_INV_out\(15) <= NOT \CPU|DP|a|register|out\(15);
\CPU|DP|ALU|plusMinus|ALT_INV_comb~2_combout\ <= NOT \CPU|DP|ALU|plusMinus|comb~2_combout\;
\CPU|DP|ALT_INV_Bin[15]~19_combout\ <= NOT \CPU|DP|Bin[15]~19_combout\;
\CPU|ID|ALT_INV_shift[0]~0_combout\ <= NOT \CPU|ID|shift[0]~0_combout\;
\CPU|DP|ALU|ALT_INV_out[14]~19_combout\ <= NOT \CPU|DP|ALU|out[14]~19_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(13) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(13);
\CPU|DP|ALT_INV_Ain[14]~4_combout\ <= NOT \CPU|DP|Ain[14]~4_combout\;
\CPU|DP|a|register|ALT_INV_out\(14) <= NOT \CPU|DP|a|register|out\(14);
\CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\ <= NOT \CPU|DP|ALU|plusMinus|comb~1_combout\;
\CPU|DP|ALT_INV_Bin[14]~18_combout\ <= NOT \CPU|DP|Bin[14]~18_combout\;
\CPU|DP|b|register|ALT_INV_out\(15) <= NOT \CPU|DP|b|register|out\(15);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(13) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(13);
\CPU|DP|ALU|ALT_INV_out[13]~18_combout\ <= NOT \CPU|DP|ALU|out[13]~18_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(13) <= NOT \CPU|DP|ALU|plusMinus|ai|c\(13);
\CPU|DP|a|register|ALT_INV_out\(13) <= NOT \CPU|DP|a|register|out\(13);
\CPU|DP|ALT_INV_Bin[13]~17_combout\ <= NOT \CPU|DP|Bin[13]~17_combout\;
\CPU|DP|b|register|ALT_INV_out\(14) <= NOT \CPU|DP|b|register|out\(14);
\CPU|DP|ALU|ALT_INV_out[12]~16_combout\ <= NOT \CPU|DP|ALU|out[12]~16_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_s\(12) <= NOT \CPU|DP|ALU|plusMinus|ai|s\(12);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(11) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(11);
\CPU|DP|ALT_INV_Ain[12]~3_combout\ <= NOT \CPU|DP|Ain[12]~3_combout\;
\CPU|DP|a|register|ALT_INV_out\(12) <= NOT \CPU|DP|a|register|out\(12);
\CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\ <= NOT \CPU|DP|ALU|plusMinus|comb~0_combout\;
\CPU|DP|ALT_INV_Bin[12]~16_combout\ <= NOT \CPU|DP|Bin[12]~16_combout\;
\CPU|DP|b|register|ALT_INV_out\(13) <= NOT \CPU|DP|b|register|out\(13);
\CPU|DP|ALU|ALT_INV_out[11]~15_combout\ <= NOT \CPU|DP|ALU|out[11]~15_combout\;
\CPU|DP|ALU|ALT_INV_out[11]~14_combout\ <= NOT \CPU|DP|ALU|out[11]~14_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\ <= NOT \CPU|DP|ALU|plusMinus|ai|comb~1_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(10) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(10);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(10) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(10);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(11);
\CPU|DP|a|register|ALT_INV_out\(11) <= NOT \CPU|DP|a|register|out\(11);
\CPU|DP|ALT_INV_Bin[11]~15_combout\ <= NOT \CPU|DP|Bin[11]~15_combout\;
\CPU|DP|b|register|ALT_INV_out\(12) <= NOT \CPU|DP|b|register|out\(12);
\CPU|DP|ALU|ALT_INV_out[10]~13_combout\ <= NOT \CPU|DP|ALU|out[10]~13_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(9) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(9);
\CPU|DP|a|register|ALT_INV_out\(10) <= NOT \CPU|DP|a|register|out\(10);
\CPU|DP|ALT_INV_Bin[10]~14_combout\ <= NOT \CPU|DP|Bin[10]~14_combout\;
\CPU|DP|b|register|ALT_INV_out\(11) <= NOT \CPU|DP|b|register|out\(11);
\CPU|DP|ALU|ALT_INV_out[9]~12_combout\ <= NOT \CPU|DP|ALU|out[9]~12_combout\;
\CPU|DP|ALU|ALT_INV_out[9]~11_combout\ <= NOT \CPU|DP|ALU|out[9]~11_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(8) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(8);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(9) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(9);
\CPU|DP|a|register|ALT_INV_out\(9) <= NOT \CPU|DP|a|register|out\(9);
\CPU|DP|ALT_INV_Bin[9]~13_combout\ <= NOT \CPU|DP|Bin[9]~13_combout\;
\CPU|DP|b|register|ALT_INV_out\(10) <= NOT \CPU|DP|b|register|out\(10);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(8) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(8);
\CPU|DP|ALU|ALT_INV_out[8]~10_combout\ <= NOT \CPU|DP|ALU|out[8]~10_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(8) <= NOT \CPU|DP|ALU|plusMinus|ai|c\(8);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(7) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(7);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(7) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(7);
\CPU|DP|a|register|ALT_INV_out\(8) <= NOT \CPU|DP|a|register|out\(8);
\CPU|DP|ALT_INV_Bin[8]~12_combout\ <= NOT \CPU|DP|Bin[8]~12_combout\;
\CPU|DP|b|register|ALT_INV_out\(9) <= NOT \CPU|DP|b|register|out\(9);
\CPU|DP|ALU|ALT_INV_out[7]~9_combout\ <= NOT \CPU|DP|ALU|out[7]~9_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(6) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(6);
\CPU|DP|a|register|ALT_INV_out\(7) <= NOT \CPU|DP|a|register|out\(7);
\CPU|DP|ALT_INV_Bin[7]~11_combout\ <= NOT \CPU|DP|Bin[7]~11_combout\;
\CPU|DP|b|register|ALT_INV_out\(8) <= NOT \CPU|DP|b|register|out\(8);
\CPU|DP|ALU|ALT_INV_out[6]~8_combout\ <= NOT \CPU|DP|ALU|out[6]~8_combout\;
\CPU|DP|ALU|ALT_INV_out[6]~7_combout\ <= NOT \CPU|DP|ALU|out[6]~7_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~0_combout\ <= NOT \CPU|DP|ALU|plusMinus|ai|comb~0_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(5) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(5);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(5) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(5);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(6) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(6);
\CPU|DP|a|register|ALT_INV_out\(6) <= NOT \CPU|DP|a|register|out\(6);
\CPU|DP|ALT_INV_Bin[6]~10_combout\ <= NOT \CPU|DP|Bin[6]~10_combout\;
\CPU|DP|b|register|ALT_INV_out\(7) <= NOT \CPU|DP|b|register|out\(7);
\CPU|DP|ALU|ALT_INV_out[5]~6_combout\ <= NOT \CPU|DP|ALU|out[5]~6_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(5) <= NOT \CPU|DP|ALU|plusMinus|ai|c\(5);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(4) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(4);
\CPU|DP|a|register|ALT_INV_out\(5) <= NOT \CPU|DP|a|register|out\(5);
\CPU|DP|ALT_INV_Bin[5]~9_combout\ <= NOT \CPU|DP|Bin[5]~9_combout\;
\CPU|DP|b|register|ALT_INV_out\(6) <= NOT \CPU|DP|b|register|out\(6);
\CPU|DP|ALU|ALT_INV_out[4]~5_combout\ <= NOT \CPU|DP|ALU|out[4]~5_combout\;
\CPU|DP|ALU|ALT_INV_out[4]~4_combout\ <= NOT \CPU|DP|ALU|out[4]~4_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(3) <= NOT \CPU|DP|ALU|plusMinus|ai|g\(3);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(4) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(4);
\CPU|DP|a|register|ALT_INV_out\(4) <= NOT \CPU|DP|a|register|out\(4);
\CPU|DP|ALT_INV_Bin[4]~8_combout\ <= NOT \CPU|DP|Bin[4]~8_combout\;
\CPU|DP|b|register|ALT_INV_out\(5) <= NOT \CPU|DP|b|register|out\(5);
\CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(3) <= NOT \CPU|DP|ALU|plusMinus|ai|p\(3);
\CPU|DP|ALU|ALT_INV_out[3]~3_combout\ <= NOT \CPU|DP|ALU|out[3]~3_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(3) <= NOT \CPU|DP|ALU|plusMinus|ai|c\(3);
\CPU|DP|a|register|ALT_INV_out\(3) <= NOT \CPU|DP|a|register|out\(3);
\CPU|DP|ALT_INV_Bin[3]~7_combout\ <= NOT \CPU|DP|Bin[3]~7_combout\;
\CPU|DP|b|register|ALT_INV_out\(4) <= NOT \CPU|DP|b|register|out\(4);
\CPU|FSM|CO|ALT_INV_Selector7~0_combout\ <= NOT \CPU|FSM|CO|Selector7~0_combout\;
\CPU|DP|ALU|ALT_INV_out[2]~2_combout\ <= NOT \CPU|DP|ALU|out[2]~2_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(2) <= NOT \CPU|DP|ALU|plusMinus|ai|c\(2);
\CPU|DP|ALT_INV_Ain[2]~2_combout\ <= NOT \CPU|DP|Ain[2]~2_combout\;
\CPU|DP|a|register|ALT_INV_out\(2) <= NOT \CPU|DP|a|register|out\(2);
\CPU|DP|ALT_INV_Bin[2]~6_combout\ <= NOT \CPU|DP|Bin[2]~6_combout\;
\CPU|DP|b|register|ALT_INV_out\(3) <= NOT \CPU|DP|b|register|out\(3);
\CPU|DP|ALU|ALT_INV_out[1]~1_combout\ <= NOT \CPU|DP|ALU|out[1]~1_combout\;
\CPU|DP|ALU|plusMinus|ai|ALT_INV_c[1]~0_combout\ <= NOT \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\;
\CPU|FSM|CO|ALT_INV_Selector8~1_combout\ <= NOT \CPU|FSM|CO|Selector8~1_combout\;
\CPU|DP|ALT_INV_Ain[1]~1_combout\ <= NOT \CPU|DP|Ain[1]~1_combout\;
\CPU|DP|a|register|ALT_INV_out\(1) <= NOT \CPU|DP|a|register|out\(1);
\CPU|DP|ALT_INV_Bin[1]~5_combout\ <= NOT \CPU|DP|Bin[1]~5_combout\;
\CPU|DP|ALT_INV_Bin[1]~4_combout\ <= NOT \CPU|DP|Bin[1]~4_combout\;
\CPU|DP|ALT_INV_Bin[1]~3_combout\ <= NOT \CPU|DP|Bin[1]~3_combout\;
\CPU|DP|b|register|ALT_INV_out\(2) <= NOT \CPU|DP|b|register|out\(2);
\CPU|DP|ALU|ALT_INV_out[0]~0_combout\ <= NOT \CPU|DP|ALU|out[0]~0_combout\;
\CPU|DP|ALT_INV_Bin[0]~2_combout\ <= NOT \CPU|DP|Bin[0]~2_combout\;
\CPU|DP|ALT_INV_Bin[0]~1_combout\ <= NOT \CPU|DP|Bin[0]~1_combout\;
\CPU|DP|b|register|ALT_INV_out\(0) <= NOT \CPU|DP|b|register|out\(0);
\CPU|InstructionRegister|register|ALT_INV_out\(3) <= NOT \CPU|InstructionRegister|register|out\(3);
\CPU|DP|ALT_INV_Bin[0]~0_combout\ <= NOT \CPU|DP|Bin[0]~0_combout\;
\CPU|DP|b|register|ALT_INV_out\(1) <= NOT \CPU|DP|b|register|out\(1);
\CPU|InstructionRegister|register|ALT_INV_out\(4) <= NOT \CPU|InstructionRegister|register|out\(4);
\CPU|FSM|CO|ALT_INV_Selector8~0_combout\ <= NOT \CPU|FSM|CO|Selector8~0_combout\;
\CPU|DP|ALT_INV_Ain[0]~0_combout\ <= NOT \CPU|DP|Ain[0]~0_combout\;
\CPU|DP|a|register|ALT_INV_out\(0) <= NOT \CPU|DP|a|register|out\(0);
\ALT_INV_read_data[12]~4_combout\ <= NOT \read_data[12]~4_combout\;
\ALT_INV_read_data[11]~3_combout\ <= NOT \read_data[11]~3_combout\;
\ALT_INV_rtl~4_combout\ <= NOT \rtl~4_combout\;
\CPU|FSM|CO|ALT_INV_Decoder0~2_combout\ <= NOT \CPU|FSM|CO|Decoder0~2_combout\;
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~2_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[8]~2_combout\;
\CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg7|register|out\(8);
\CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg6|register|out\(8);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~1_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[8]~1_combout\;
\CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg3|register|out\(8);
\CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg1|register|out\(8);
\CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~0_combout\ <= NOT \CPU|DP|REGFILE|readMux|m|b[8]~0_combout\;
\CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg4|register|out\(8);
\CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg2|register|out\(8);
\CPU|ID|RWmux|ALT_INV_b\(2) <= NOT \CPU|ID|RWmux|b\(2);
\CPU|ID|RWmux|ALT_INV_b\(1) <= NOT \CPU|ID|RWmux|b\(1);
\CPU|ID|RWmux|ALT_INV_b\(0) <= NOT \CPU|ID|RWmux|b\(0);
\ALT_INV_rtl~35_combout\ <= NOT \rtl~35_combout\;
\ALT_INV_rtl~34_combout\ <= NOT \rtl~34_combout\;
\ALT_INV_rtl~33_combout\ <= NOT \rtl~33_combout\;
\CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg0|register|out\(8);
\CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\ <= NOT \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\;
\CPU|ID|RWmux|ALT_INV_b[2]~2_combout\ <= NOT \CPU|ID|RWmux|b[2]~2_combout\;
\CPU|ID|RWmux|ALT_INV_comb~5_combout\ <= NOT \CPU|ID|RWmux|comb~5_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(2) <= NOT \CPU|InstructionRegister|register|out\(2);
\CPU|InstructionRegister|register|ALT_INV_out\(10) <= NOT \CPU|InstructionRegister|register|out\(10);
\CPU|ID|RWmux|ALT_INV_b[1]~1_combout\ <= NOT \CPU|ID|RWmux|b[1]~1_combout\;
\CPU|ID|RWmux|ALT_INV_comb~4_combout\ <= NOT \CPU|ID|RWmux|comb~4_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(1) <= NOT \CPU|InstructionRegister|register|out\(1);
\CPU|InstructionRegister|register|ALT_INV_out\(9) <= NOT \CPU|InstructionRegister|register|out\(9);
\CPU|ID|RWmux|ALT_INV_b[0]~0_combout\ <= NOT \CPU|ID|RWmux|b[0]~0_combout\;
\CPU|ID|RWmux|ALT_INV_comb~3_combout\ <= NOT \CPU|ID|RWmux|comb~3_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(0) <= NOT \CPU|InstructionRegister|register|out\(0);
\ALT_INV_rtl~32_combout\ <= NOT \rtl~32_combout\;
\ALT_INV_rtl~9_combout\ <= NOT \rtl~9_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(8) <= NOT \CPU|InstructionRegister|register|out\(8);
\CPU|ID|RWmux|ALT_INV_comb~2_combout\ <= NOT \CPU|ID|RWmux|comb~2_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(6) <= NOT \CPU|InstructionRegister|register|out\(6);
\CPU|ID|RWmux|ALT_INV_comb~1_combout\ <= NOT \CPU|ID|RWmux|comb~1_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(5) <= NOT \CPU|InstructionRegister|register|out\(5);
\CPU|ID|RWmux|ALT_INV_comb~0_combout\ <= NOT \CPU|ID|RWmux|comb~0_combout\;
\ALT_INV_rtl~31_combout\ <= NOT \rtl~31_combout\;
\ALT_INV_rtl~30_combout\ <= NOT \rtl~30_combout\;
\ALT_INV_rtl~29_combout\ <= NOT \rtl~29_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(7) <= NOT \CPU|InstructionRegister|register|out\(7);
\CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(8) <= NOT \CPU|DP|REGFILE|Reg5|register|out\(8);
\CPU|FSM|CS|ALT_INV_WideOr7~3_combout\ <= NOT \CPU|FSM|CS|WideOr7~3_combout\;
\CPU|FSM|CS|ALT_INV_WideOr7~2_combout\ <= NOT \CPU|FSM|CS|WideOr7~2_combout\;
\CPU|FSM|CS|ALT_INV_WideOr7~1_combout\ <= NOT \CPU|FSM|CS|WideOr7~1_combout\;
\CPU|FSM|CS|ALT_INV_WideOr7~0_combout\ <= NOT \CPU|FSM|CS|WideOr7~0_combout\;
\CPU|FSM|CS|ALT_INV_WideOr1~1_combout\ <= NOT \CPU|FSM|CS|WideOr1~1_combout\;
\CPU|FSM|CS|ALT_INV_WideNor7~0_combout\ <= NOT \CPU|FSM|CS|WideNor7~0_combout\;
\CPU|FSM|CS|ALT_INV_WideOr5~4_combout\ <= NOT \CPU|FSM|CS|WideOr5~4_combout\;
\CPU|FSM|CS|ALT_INV_WideOr5~3_combout\ <= NOT \CPU|FSM|CS|WideOr5~3_combout\;
\CPU|FSM|CO|ALT_INV_Mux1~4_combout\ <= NOT \CPU|FSM|CO|Mux1~4_combout\;
\CPU|FSM|CS|ALT_INV_WideNor27~0_combout\ <= NOT \CPU|FSM|CS|WideNor27~0_combout\;
\CPU|FSM|CO|ALT_INV_Mux9~0_combout\ <= NOT \CPU|FSM|CO|Mux9~0_combout\;
\CPU|FSM|CS|ALT_INV_WideOr5~2_combout\ <= NOT \CPU|FSM|CS|WideOr5~2_combout\;
\CPU|FSM|CS|ALT_INV_WideOr5~1_combout\ <= NOT \CPU|FSM|CS|WideOr5~1_combout\;
\CPU|FSM|CS|ALT_INV_WideOr3~1_combout\ <= NOT \CPU|FSM|CS|WideOr3~1_combout\;
\CPU|FSM|CS|ALT_INV_WideOr1~0_combout\ <= NOT \CPU|FSM|CS|WideOr1~0_combout\;
\CPU|FSM|CO|ALT_INV_Decoder0~1_combout\ <= NOT \CPU|FSM|CO|Decoder0~1_combout\;
\CPU|FSM|CS|ALT_INV_WideOr5~0_combout\ <= NOT \CPU|FSM|CS|WideOr5~0_combout\;
\CPU|FSM|storeFlag|register|ALT_INV_out\(0) <= NOT \CPU|FSM|storeFlag|register|out\(0);
\CPU|FSM|CS|ALT_INV_WideNor4~combout\ <= NOT \CPU|FSM|CS|WideNor4~combout\;
\CPU|FSM|CS|ALT_INV_WideOr3~0_combout\ <= NOT \CPU|FSM|CS|WideOr3~0_combout\;
\ALT_INV_rtl~28_combout\ <= NOT \rtl~28_combout\;
\CPU|FSM|CO|ALT_INV_Decoder0~0_combout\ <= NOT \CPU|FSM|CO|Decoder0~0_combout\;
\ALT_INV_read_data[15]~2_combout\ <= NOT \read_data[15]~2_combout\;
\ALT_INV_read_data[14]~1_combout\ <= NOT \read_data[14]~1_combout\;
\ALT_INV_read_data[13]~0_combout\ <= NOT \read_data[13]~0_combout\;
\io|ALT_INV_setLEDs~1_combout\ <= NOT \io|setLEDs~1_combout\;
\io|ALT_INV_setLEDs~0_combout\ <= NOT \io|setLEDs~0_combout\;
\H5|ALT_INV_WideOr0~0_combout\ <= NOT \H5|WideOr0~0_combout\;
\CPU|ALT_INV_mem_addr[7]~8_combout\ <= NOT \CPU|mem_addr[7]~8_combout\;
\CPU|data_address|register|ALT_INV_out\(7) <= NOT \CPU|data_address|register|out\(7);
\CPU|ALT_INV_mem_addr[6]~7_combout\ <= NOT \CPU|mem_addr[6]~7_combout\;
\CPU|data_address|register|ALT_INV_out\(6) <= NOT \CPU|data_address|register|out\(6);
\CPU|ALT_INV_mem_addr[5]~6_combout\ <= NOT \CPU|mem_addr[5]~6_combout\;
\CPU|data_address|register|ALT_INV_out\(5) <= NOT \CPU|data_address|register|out\(5);
\CPU|ALT_INV_mem_addr[4]~5_combout\ <= NOT \CPU|mem_addr[4]~5_combout\;
\CPU|data_address|register|ALT_INV_out\(4) <= NOT \CPU|data_address|register|out\(4);
\H4|ALT_INV_WideOr0~0_combout\ <= NOT \H4|WideOr0~0_combout\;
\CPU|ALT_INV_mem_addr[3]~4_combout\ <= NOT \CPU|mem_addr[3]~4_combout\;
\CPU|data_address|register|ALT_INV_out\(3) <= NOT \CPU|data_address|register|out\(3);
\CPU|ALT_INV_mem_addr[2]~3_combout\ <= NOT \CPU|mem_addr[2]~3_combout\;
\CPU|data_address|register|ALT_INV_out\(2) <= NOT \CPU|data_address|register|out\(2);
\CPU|ALT_INV_mem_addr[1]~2_combout\ <= NOT \CPU|mem_addr[1]~2_combout\;
\CPU|data_address|register|ALT_INV_out\(1) <= NOT \CPU|data_address|register|out\(1);
\CPU|ALT_INV_mem_addr[0]~1_combout\ <= NOT \CPU|mem_addr[0]~1_combout\;
\CPU|data_address|register|ALT_INV_out\(0) <= NOT \CPU|data_address|register|out\(0);
\H3|ALT_INV_WideOr0~0_combout\ <= NOT \H3|WideOr0~0_combout\;
\ALT_INV_hexOut[15]~15_combout\ <= NOT \hexOut[15]~15_combout\;
\CPU|DP|c|register|ALT_INV_out\(15) <= NOT \CPU|DP|c|register|out\(15);
\ALT_INV_hexOut[14]~14_combout\ <= NOT \hexOut[14]~14_combout\;
\CPU|DP|c|register|ALT_INV_out\(14) <= NOT \CPU|DP|c|register|out\(14);
\ALT_INV_hexOut[13]~13_combout\ <= NOT \hexOut[13]~13_combout\;
\CPU|DP|c|register|ALT_INV_out\(13) <= NOT \CPU|DP|c|register|out\(13);
\ALT_INV_hexOut[12]~12_combout\ <= NOT \hexOut[12]~12_combout\;
\CPU|DP|c|register|ALT_INV_out\(12) <= NOT \CPU|DP|c|register|out\(12);
\H2|ALT_INV_WideOr0~0_combout\ <= NOT \H2|WideOr0~0_combout\;
\ALT_INV_hexOut[11]~11_combout\ <= NOT \hexOut[11]~11_combout\;
\CPU|DP|c|register|ALT_INV_out\(11) <= NOT \CPU|DP|c|register|out\(11);
\ALT_INV_hexOut[10]~10_combout\ <= NOT \hexOut[10]~10_combout\;
\CPU|DP|c|register|ALT_INV_out\(10) <= NOT \CPU|DP|c|register|out\(10);
\ALT_INV_hexOut[9]~9_combout\ <= NOT \hexOut[9]~9_combout\;
\CPU|DP|c|register|ALT_INV_out\(9) <= NOT \CPU|DP|c|register|out\(9);
\ALT_INV_hexOut[8]~8_combout\ <= NOT \hexOut[8]~8_combout\;
\CPU|DP|c|register|ALT_INV_out\(8) <= NOT \CPU|DP|c|register|out\(8);
\CPU|ALT_INV_mem_addr[8]~0_combout\ <= NOT \CPU|mem_addr[8]~0_combout\;
\H1|ALT_INV_WideOr0~0_combout\ <= NOT \H1|WideOr0~0_combout\;
\ALT_INV_hexOut[7]~7_combout\ <= NOT \hexOut[7]~7_combout\;
\CPU|DP|c|register|ALT_INV_out\(7) <= NOT \CPU|DP|c|register|out\(7);
\ALT_INV_hexOut[6]~6_combout\ <= NOT \hexOut[6]~6_combout\;
\CPU|DP|c|register|ALT_INV_out\(6) <= NOT \CPU|DP|c|register|out\(6);
\ALT_INV_hexOut[5]~5_combout\ <= NOT \hexOut[5]~5_combout\;
\CPU|DP|c|register|ALT_INV_out\(5) <= NOT \CPU|DP|c|register|out\(5);
\ALT_INV_hexOut[4]~4_combout\ <= NOT \hexOut[4]~4_combout\;
\CPU|DP|c|register|ALT_INV_out\(4) <= NOT \CPU|DP|c|register|out\(4);
\H0|ALT_INV_WideOr0~0_combout\ <= NOT \H0|WideOr0~0_combout\;
\ALT_INV_hexOut[3]~3_combout\ <= NOT \hexOut[3]~3_combout\;
\CPU|DP|c|register|ALT_INV_out\(3) <= NOT \CPU|DP|c|register|out\(3);
\ALT_INV_hexOut[2]~2_combout\ <= NOT \hexOut[2]~2_combout\;
\CPU|DP|c|register|ALT_INV_out\(2) <= NOT \CPU|DP|c|register|out\(2);
\ALT_INV_hexOut[1]~1_combout\ <= NOT \hexOut[1]~1_combout\;
\CPU|DP|c|register|ALT_INV_out\(1) <= NOT \CPU|DP|c|register|out\(1);
\ALT_INV_hexOut[0]~0_combout\ <= NOT \hexOut[0]~0_combout\;
\CPU|DP|c|register|ALT_INV_out\(0) <= NOT \CPU|DP|c|register|out\(0);
\ALT_INV_write~0_combout\ <= NOT \write~0_combout\;
\ALT_INV_rtl~27_combout\ <= NOT \rtl~27_combout\;
\CPU|data_address|register|ALT_INV_out\(8) <= NOT \CPU|data_address|register|out\(8);
\CPU|FSM|CO|ALT_INV_Mux1~2_combout\ <= NOT \CPU|FSM|CO|Mux1~2_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(12) <= NOT \CPU|InstructionRegister|register|out\(12);
\CPU|InstructionRegister|register|ALT_INV_out\(11) <= NOT \CPU|InstructionRegister|register|out\(11);
\CPU|FSM|CO|ALT_INV_Mux1~1_combout\ <= NOT \CPU|FSM|CO|Mux1~1_combout\;
\CPU|FSM|CO|ALT_INV_Mux1~0_combout\ <= NOT \CPU|FSM|CO|Mux1~0_combout\;
\ALT_INV_rtl~26_combout\ <= NOT \rtl~26_combout\;
\CPU|FSM|CO|ALT_INV_Equal6~0_combout\ <= NOT \CPU|FSM|CO|Equal6~0_combout\;
\CPU|FSM|CO|ALT_INV_Selector14~1_combout\ <= NOT \CPU|FSM|CO|Selector14~1_combout\;
\CPU|FSM|CO|ALT_INV_Selector14~0_combout\ <= NOT \CPU|FSM|CO|Selector14~0_combout\;
\CPU|InstructionRegister|register|ALT_INV_out\(15) <= NOT \CPU|InstructionRegister|register|out\(15);
\CPU|InstructionRegister|register|ALT_INV_out\(14) <= NOT \CPU|InstructionRegister|register|out\(14);
\CPU|InstructionRegister|register|ALT_INV_out\(13) <= NOT \CPU|InstructionRegister|register|out\(13);
\CPU|DP|ALT_INV_Add0~29_sumout\ <= NOT \CPU|DP|Add0~29_sumout\;
\CPU|DP|ALT_INV_Add0~25_sumout\ <= NOT \CPU|DP|Add0~25_sumout\;
\CPU|DP|ALT_INV_Add0~21_sumout\ <= NOT \CPU|DP|Add0~21_sumout\;
\CPU|DP|ALT_INV_Add0~17_sumout\ <= NOT \CPU|DP|Add0~17_sumout\;
\CPU|DP|ALT_INV_Add0~13_sumout\ <= NOT \CPU|DP|Add0~13_sumout\;
\CPU|DP|ALT_INV_Add0~9_sumout\ <= NOT \CPU|DP|Add0~9_sumout\;
\CPU|DP|ALT_INV_Add0~5_sumout\ <= NOT \CPU|DP|Add0~5_sumout\;
\CPU|DP|ALT_INV_Add0~1_sumout\ <= NOT \CPU|DP|Add0~1_sumout\;
\CPU|program_counter|register|ALT_INV_out\(7) <= NOT \CPU|program_counter|register|out\(7);
\CPU|program_counter|register|ALT_INV_out\(6) <= NOT \CPU|program_counter|register|out\(6);
\CPU|program_counter|register|ALT_INV_out\(5) <= NOT \CPU|program_counter|register|out\(5);
\CPU|program_counter|register|ALT_INV_out\(4) <= NOT \CPU|program_counter|register|out\(4);
\CPU|program_counter|register|ALT_INV_out\(3) <= NOT \CPU|program_counter|register|out\(3);
\CPU|program_counter|register|ALT_INV_out\(2) <= NOT \CPU|program_counter|register|out\(2);
\CPU|program_counter|register|ALT_INV_out\(1) <= NOT \CPU|program_counter|register|out\(1);
\CPU|program_counter|register|ALT_INV_out\(0) <= NOT \CPU|program_counter|register|out\(0);
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a1\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a2\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a3\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a4\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a5\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a6\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a7\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a8\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a9\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a10\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a11\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a12\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a13\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a14\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a15\;
\MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\ <= NOT \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\;
\CPU|program_counter|register|ALT_INV_out\(8) <= NOT \CPU|program_counter|register|out\(8);
\CPU|FSM|stateRegister|ALT_INV_out\(0) <= NOT \CPU|FSM|stateRegister|out\(0);
\CPU|FSM|stateRegister|ALT_INV_out\(3) <= NOT \CPU|FSM|stateRegister|out\(3);
\CPU|FSM|stateRegister|ALT_INV_out\(1) <= NOT \CPU|FSM|stateRegister|out\(1);
\CPU|FSM|stateRegister|ALT_INV_out\(2) <= NOT \CPU|FSM|stateRegister|out\(2);
\CPU|DP|b|register|ALT_INV_out[14]~DUPLICATE_q\ <= NOT \CPU|DP|b|register|out[14]~DUPLICATE_q\;

-- Location: IOOBUF_X70_Y0_N19
\LEDR[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(0),
	devoe => ww_devoe,
	o => ww_LEDR(0));

-- Location: IOOBUF_X52_Y0_N36
\LEDR[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(1),
	devoe => ww_devoe,
	o => ww_LEDR(1));

-- Location: IOOBUF_X50_Y0_N93
\LEDR[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(2),
	devoe => ww_devoe,
	o => ww_LEDR(2));

-- Location: IOOBUF_X40_Y0_N2
\LEDR[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(3),
	devoe => ww_devoe,
	o => ww_LEDR(3));

-- Location: IOOBUF_X70_Y0_N53
\LEDR[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(4),
	devoe => ww_devoe,
	o => ww_LEDR(4));

-- Location: IOOBUF_X70_Y0_N2
\LEDR[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(5),
	devoe => ww_devoe,
	o => ww_LEDR(5));

-- Location: IOOBUF_X89_Y4_N79
\LEDR[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(6),
	devoe => ww_devoe,
	o => ww_LEDR(6));

-- Location: IOOBUF_X70_Y0_N36
\LEDR[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \io|LEDregister|register|out\(7),
	devoe => ww_devoe,
	o => ww_LEDR(7));

-- Location: IOOBUF_X66_Y0_N59
\LEDR[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \CPU|FSM|CO|Selector14~1_combout\,
	devoe => ww_devoe,
	o => ww_LEDR(8));

-- Location: IOOBUF_X36_Y81_N36
\LEDR[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_LEDR(9));

-- Location: IOOBUF_X40_Y0_N53
\HEX0[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(0));

-- Location: IOOBUF_X56_Y0_N53
\HEX0[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(1));

-- Location: IOOBUF_X56_Y0_N19
\HEX0[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(2));

-- Location: IOOBUF_X60_Y0_N53
\HEX0[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(3));

-- Location: IOOBUF_X52_Y0_N53
\HEX0[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(4));

-- Location: IOOBUF_X50_Y0_N42
\HEX0[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(5));

-- Location: IOOBUF_X58_Y0_N59
\HEX0[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H0|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX0(6));

-- Location: IOOBUF_X54_Y0_N53
\HEX1[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(0));

-- Location: IOOBUF_X40_Y0_N19
\HEX1[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(1));

-- Location: IOOBUF_X52_Y0_N2
\HEX1[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(2));

-- Location: IOOBUF_X56_Y0_N2
\HEX1[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(3));

-- Location: IOOBUF_X54_Y0_N36
\HEX1[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(4));

-- Location: IOOBUF_X54_Y0_N2
\HEX1[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(5));

-- Location: IOOBUF_X60_Y0_N36
\HEX1[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H1|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX1(6));

-- Location: IOOBUF_X36_Y0_N53
\HEX2[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(0));

-- Location: IOOBUF_X38_Y0_N19
\HEX2[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(1));

-- Location: IOOBUF_X36_Y0_N36
\HEX2[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(2));

-- Location: IOOBUF_X38_Y0_N53
\HEX2[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(3));

-- Location: IOOBUF_X36_Y0_N19
\HEX2[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(4));

-- Location: IOOBUF_X36_Y0_N2
\HEX2[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(5));

-- Location: IOOBUF_X34_Y0_N76
\HEX2[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H2|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX2(6));

-- Location: IOOBUF_X38_Y0_N2
\HEX3[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(0));

-- Location: IOOBUF_X40_Y0_N36
\HEX3[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(1));

-- Location: IOOBUF_X52_Y0_N19
\HEX3[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(2));

-- Location: IOOBUF_X56_Y0_N36
\HEX3[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(3));

-- Location: IOOBUF_X54_Y0_N19
\HEX3[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(4));

-- Location: IOOBUF_X38_Y0_N36
\HEX3[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(5));

-- Location: IOOBUF_X50_Y0_N76
\HEX3[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H3|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX3(6));

-- Location: IOOBUF_X64_Y0_N19
\HEX4[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(0));

-- Location: IOOBUF_X72_Y0_N53
\HEX4[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(1));

-- Location: IOOBUF_X68_Y0_N53
\HEX4[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(2));

-- Location: IOOBUF_X72_Y0_N36
\HEX4[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(3));

-- Location: IOOBUF_X72_Y0_N2
\HEX4[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(4));

-- Location: IOOBUF_X50_Y0_N59
\HEX4[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(5));

-- Location: IOOBUF_X68_Y0_N19
\HEX4[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H4|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX4(6));

-- Location: IOOBUF_X58_Y0_N93
\HEX5[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|WideOr6~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(0));

-- Location: IOOBUF_X58_Y0_N76
\HEX5[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|WideOr5~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(1));

-- Location: IOOBUF_X62_Y0_N53
\HEX5[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|WideOr4~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(2));

-- Location: IOOBUF_X60_Y0_N19
\HEX5[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|WideOr3~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(3));

-- Location: IOOBUF_X64_Y0_N2
\HEX5[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|WideOr2~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(4));

-- Location: IOOBUF_X58_Y0_N42
\HEX5[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|WideOr1~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(5));

-- Location: IOOBUF_X62_Y0_N19
\HEX5[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \H5|ALT_INV_WideOr0~0_combout\,
	devoe => ww_devoe,
	o => ww_HEX5(6));

-- Location: IOIBUF_X89_Y23_N21
\CLOCK_50~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: CLKCTRL_G11
\CLOCK_50~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_50~input_o\,
	outclk => \CLOCK_50~inputCLKENA0_outclk\);

-- Location: LABCELL_X62_Y4_N9
\CPU|FSM|CO|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux1~0_combout\ = ( \CPU|FSM|stateRegister|out\(2) & ( (\CPU|FSM|stateRegister|out\(1) & (\CPU|FSM|stateRegister|out\(0) & !\CPU|FSM|stateRegister|out\(3))) ) ) # ( !\CPU|FSM|stateRegister|out\(2) & ( (!\CPU|FSM|stateRegister|out\(1) & 
-- (!\CPU|FSM|stateRegister|out\(0) & \CPU|FSM|stateRegister|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000000000011000000000000001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CO|Mux1~0_combout\);

-- Location: LABCELL_X64_Y4_N36
\CPU|FSM|CO|Decoder0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Decoder0~1_combout\ = ( !\CPU|FSM|stateRegister|out\(3) & ( (!\CPU|FSM|stateRegister|out\(1) & (\CPU|FSM|stateRegister|out\(0) & \CPU|FSM|stateRegister|out\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	combout => \CPU|FSM|CO|Decoder0~1_combout\);

-- Location: MLABCELL_X59_Y4_N39
write : cyclonev_lcell_comb
-- Equation(s):
-- \write~combout\ = ( \rtl~27_combout\ & ( (!\rtl~26_combout\ & !\CPU|mem_addr[8]~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010101010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datad => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	dataf => \ALT_INV_rtl~27_combout\,
	combout => \write~combout\);

-- Location: LABCELL_X67_Y4_N3
\CPU|FSM|CO|Mux1~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux1~3_combout\ = ( !\CPU|FSM|stateRegister|out\(2) & ( !\CPU|FSM|stateRegister|out\(1) & ( (\CPU|FSM|stateRegister|out\(3) & !\CPU|FSM|stateRegister|out\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	combout => \CPU|FSM|CO|Mux1~3_combout\);

-- Location: FF_X63_Y4_N10
\CPU|InstructionRegister|register|out[14]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \read_data[14]~1_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out[14]~DUPLICATE_q\);

-- Location: LABCELL_X63_Y4_N12
\CPU|FSM|CO|Mux1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux1~1_combout\ = ( !\CPU|FSM|stateRegister|out\(1) & ( !\CPU|FSM|stateRegister|out\(2) & ( (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (!\CPU|InstructionRegister|register|out\(15) & (\CPU|FSM|stateRegister|out\(0) & 
-- !\CPU|InstructionRegister|register|out\(13)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CO|Mux1~1_combout\);

-- Location: LABCELL_X56_Y4_N42
\CPU|FSM|CO|Decoder0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Decoder0~3_combout\ = ( !\CPU|FSM|stateRegister|out\(2) & ( \CPU|FSM|stateRegister|out\(1) & ( (\CPU|FSM|stateRegister|out\(0) & \CPU|FSM|stateRegister|out\(3)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	combout => \CPU|FSM|CO|Decoder0~3_combout\);

-- Location: FF_X61_Y4_N8
\CPU|data_address|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(0),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(0));

-- Location: LABCELL_X64_Y5_N21
\rtl~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~4_combout\ = ( !\CPU|FSM|stateRegister|out\(1) & ( \CPU|FSM|stateRegister|out\(3) & ( (\CPU|FSM|stateRegister|out\(0) & !\CPU|FSM|stateRegister|out\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010000010100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	combout => \rtl~4_combout\);

-- Location: IOIBUF_X60_Y0_N1
\SW[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LABCELL_X63_Y5_N57
\CPU|FSM|CO|Decoder0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Decoder0~2_combout\ = ( !\CPU|FSM|stateRegister|out\(2) & ( (!\CPU|FSM|stateRegister|out\(0) & (!\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CO|Decoder0~2_combout\);

-- Location: LABCELL_X63_Y5_N51
\CPU|FSM|CO|Mux1~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux1~4_combout\ = ( \CPU|FSM|stateRegister|out\(2) & ( (\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000010001000000000001000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CO|Mux1~4_combout\);

-- Location: LABCELL_X67_Y4_N57
\CPU|FSM|storeFlag|regInput[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|storeFlag|regInput[0]~0_combout\ = ( \CPU|FSM|storeFlag|register|out\(0) & ( ((!\CPU|FSM|CO|Decoder0~2_combout\ & !\CPU|FSM|CO|Mux1~4_combout\)) # (\CPU|FSM|CO|Decoder0~1_combout\) ) ) # ( !\CPU|FSM|storeFlag|register|out\(0) & ( 
-- \CPU|FSM|CO|Decoder0~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010111110101010101011111010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Decoder0~2_combout\,
	datad => \CPU|FSM|CO|ALT_INV_Mux1~4_combout\,
	dataf => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	combout => \CPU|FSM|storeFlag|regInput[0]~0_combout\);

-- Location: FF_X64_Y4_N44
\CPU|FSM|storeFlag|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|FSM|storeFlag|regInput[0]~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|storeFlag|register|out\(0));

-- Location: IOIBUF_X62_Y0_N1
\SW[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: LABCELL_X62_Y3_N45
e : cyclonev_lcell_comb
-- Equation(s):
-- \e~combout\ = ( \rtl~27_combout\ & ( \rtl~26_combout\ & ( !\CPU|mem_addr[8]~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datae => \ALT_INV_rtl~27_combout\,
	dataf => \ALT_INV_rtl~26_combout\,
	combout => \e~combout\);

-- Location: IOIBUF_X68_Y0_N35
\SW[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: LABCELL_X64_Y4_N30
\CPU|FSM|CO|Selector7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector7~0_combout\ = ( \CPU|FSM|storeFlag|register|out\(0) & ( \CPU|FSM|CO|Decoder0~1_combout\ ) ) # ( !\CPU|FSM|storeFlag|register|out\(0) & ( (!\CPU|InstructionRegister|register|out\(13) & 
-- (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (\CPU|InstructionRegister|register|out\(15) & \CPU|FSM|CO|Decoder0~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000010000000000000001000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datab => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datad => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	dataf => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	combout => \CPU|FSM|CO|Selector7~0_combout\);

-- Location: IOIBUF_X66_Y0_N41
\SW[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: LABCELL_X60_Y4_N0
\CPU|FSM|CO|Mux9~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux9~1_combout\ = ( \CPU|FSM|CO|Mux9~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(13) & (!\CPU|InstructionRegister|register|out\(11) $ (\CPU|InstructionRegister|register|out\(12)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000000011001100000000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	dataf => \CPU|FSM|CO|ALT_INV_Mux9~0_combout\,
	combout => \CPU|FSM|CO|Mux9~1_combout\);

-- Location: LABCELL_X64_Y4_N57
\CPU|FSM|CO|Equal6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Equal6~0_combout\ = ( !\CPU|InstructionRegister|register|out\(15) & ( (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & \CPU|InstructionRegister|register|out\(13)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	combout => \CPU|FSM|CO|Equal6~0_combout\);

-- Location: LABCELL_X60_Y4_N21
\CPU|FSM|CO|Mux9~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux9~2_combout\ = ( \CPU|FSM|CO|Equal6~0_combout\ & ( (!\CPU|FSM|stateRegister|out\(2) & ((!\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|stateRegister|out\(1))) # (\CPU|FSM|stateRegister|out\(0) & (!\CPU|FSM|stateRegister|out\(1) & 
-- \CPU|FSM|CO|Mux9~1_combout\)))) ) ) # ( !\CPU|FSM|CO|Equal6~0_combout\ & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|CO|Mux9~1_combout\ & (!\CPU|FSM|stateRegister|out\(0) $ (!\CPU|FSM|stateRegister|out\(1))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001100000000000000110000000100000011000000010000001100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datad => \CPU|FSM|CO|ALT_INV_Mux9~1_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Equal6~0_combout\,
	combout => \CPU|FSM|CO|Mux9~2_combout\);

-- Location: LABCELL_X61_Y5_N15
\CPU|DP|WBmux|d|ShiftLeft0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ = ( \CPU|FSM|CO|Mux9~2_combout\ & ( (!\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(1) $ (!\CPU|FSM|stateRegister|out\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000110000000011000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|CO|ALT_INV_Mux9~2_combout\,
	combout => \CPU|DP|WBmux|d|ShiftLeft0~1_combout\);

-- Location: LABCELL_X63_Y5_N54
\CPU|FSM|CO|Decoder0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Decoder0~4_combout\ = ( !\CPU|FSM|stateRegister|out\(2) & ( (\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010000000100000001000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CO|Decoder0~4_combout\);

-- Location: FF_X63_Y2_N2
\CPU|DP|a|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(6),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(6));

-- Location: IOIBUF_X64_Y0_N35
\SW[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: LABCELL_X60_Y2_N54
\CPU|FSM|CO|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector0~0_combout\ = ( !\CPU|InstructionRegister|register|out\(13) & ( (\CPU|InstructionRegister|register|out\(12) & (\CPU|InstructionRegister|register|out\(14) & (!\CPU|InstructionRegister|register|out\(15) & 
-- \CPU|InstructionRegister|register|out\(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000001000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	combout => \CPU|FSM|CO|Selector0~0_combout\);

-- Location: IOIBUF_X62_Y0_N35
\SW[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: LABCELL_X63_Y2_N51
\CPU|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~29_sumout\ = SUM(( \CPU|program_counter|register|out\(6) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(6) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~26\ ))
-- \CPU|Add0~30\ = CARRY(( \CPU|program_counter|register|out\(6) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(6) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(6),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(6),
	cin => \CPU|Add0~26\,
	sumout => \CPU|Add0~29_sumout\,
	cout => \CPU|Add0~30\);

-- Location: LABCELL_X63_Y2_N54
\CPU|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~33_sumout\ = SUM(( \CPU|program_counter|register|out\(7) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(7) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~30\ ))
-- \CPU|Add0~34\ = CARRY(( \CPU|program_counter|register|out\(7) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(7) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111101110111000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(7),
	datac => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(7),
	dataf => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	cin => \CPU|Add0~30\,
	sumout => \CPU|Add0~33_sumout\,
	cout => \CPU|Add0~34\);

-- Location: LABCELL_X64_Y4_N45
\CPU|DP|ALU|plusMinus|ai|g[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(6) = ( \CPU|DP|Bin[6]~10_combout\ & ( (\CPU|DP|a|register|out\(6) & (!\CPU|FSM|CO|Selector7~0_combout\ & !\CPU|InstructionRegister|register|out\(11))) ) ) # ( !\CPU|DP|Bin[6]~10_combout\ & ( (\CPU|DP|a|register|out\(6) & 
-- (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|InstructionRegister|register|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|a|register|ALT_INV_out\(6),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[6]~10_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(6));

-- Location: LABCELL_X60_Y4_N9
\CPU|DP|WBmux|d|ShiftLeft0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ = ( !\CPU|FSM|CO|Mux9~2_combout\ & ( (!\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(1) $ (!\CPU|FSM|stateRegister|out\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001010001000001000101000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|CO|ALT_INV_Mux9~2_combout\,
	combout => \CPU|DP|WBmux|d|ShiftLeft0~3_combout\);

-- Location: LABCELL_X60_Y4_N3
\CPU|DP|WBmux|d|ShiftLeft0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ = ( \CPU|FSM|CO|Mux9~2_combout\ & ( (!\CPU|FSM|stateRegister|out\(2) $ (\CPU|FSM|stateRegister|out\(1))) # (\CPU|FSM|stateRegister|out\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010100101111111111010010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|CO|ALT_INV_Mux9~2_combout\,
	combout => \CPU|DP|WBmux|d|ShiftLeft0~0_combout\);

-- Location: LABCELL_X63_Y4_N24
\read_data[3]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[3]~15_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a3\ & ( ((\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\))) # (\SW[3]~input_o\) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a3\ & ( (\SW[3]~input_o\ & 
-- ((!\rtl~26_combout\) # ((!\rtl~27_combout\) # (\CPU|mem_addr[8]~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001011000011110000101100001111010011110000111101001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[3]~input_o\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\,
	combout => \read_data[3]~15_combout\);

-- Location: FF_X63_Y4_N26
\CPU|InstructionRegister|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[3]~15_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(3));

-- Location: IOIBUF_X66_Y0_N92
\SW[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: LABCELL_X67_Y4_N54
\CPU|FSM|CO|loadb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|loadb~0_combout\ = ( !\CPU|FSM|stateRegister|out\(0) & ( (\CPU|FSM|stateRegister|out\(2) & !\CPU|FSM|stateRegister|out\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000000011110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	combout => \CPU|FSM|CO|loadb~0_combout\);

-- Location: FF_X64_Y4_N2
\CPU|DP|b|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(2),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(2));

-- Location: LABCELL_X64_Y4_N3
\rtl~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~9_combout\ = !\CPU|FSM|stateRegister|out\(3) $ (!\CPU|FSM|stateRegister|out\(1))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111110000000011111111000000001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	combout => \rtl~9_combout\);

-- Location: LABCELL_X63_Y4_N51
\rtl~28\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~28_combout\ = (\CPU|InstructionRegister|register|out\(11) & \CPU|InstructionRegister|register|out\(12))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	combout => \rtl~28_combout\);

-- Location: LABCELL_X62_Y4_N45
\rtl~32\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~32_combout\ = ( \CPU|FSM|stateRegister|out\(0) & ( !\CPU|FSM|stateRegister|out\(2) & ( \CPU|FSM|stateRegister|out\(1) ) ) ) # ( !\CPU|FSM|stateRegister|out\(0) & ( !\CPU|FSM|stateRegister|out\(2) & ( (\CPU|FSM|stateRegister|out\(1) & 
-- (((!\CPU|InstructionRegister|register|out\(13)) # (!\CPU|InstructionRegister|register|out\(14))) # (\CPU|InstructionRegister|register|out\(15)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001101000011110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \rtl~32_combout\);

-- Location: FF_X66_Y4_N1
\CPU|DP|b|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(7),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(7));

-- Location: LABCELL_X63_Y2_N57
\CPU|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~1_sumout\ = SUM(( \CPU|program_counter|register|out\(8) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(7) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111101110111000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(7),
	datac => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(8),
	dataf => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	cin => \CPU|Add0~34\,
	sumout => \CPU|Add0~1_sumout\);

-- Location: LABCELL_X63_Y5_N48
\rtl~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~29_combout\ = ( !\CPU|InstructionRegister|register|out\(13) & ( (!\CPU|InstructionRegister|register|out\(12) & !\CPU|InstructionRegister|register|out\(11)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000000000000111100000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	combout => \rtl~29_combout\);

-- Location: LABCELL_X64_Y2_N51
\CPU|FSM|CO|Selector15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector15~0_combout\ = ( \rtl~4_combout\ & ( (!\CPU|InstructionRegister|register|out\(15) & (\rtl~29_combout\ & \CPU|InstructionRegister|register|out\(14))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datac => \ALT_INV_rtl~29_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	dataf => \ALT_INV_rtl~4_combout\,
	combout => \CPU|FSM|CO|Selector15~0_combout\);

-- Location: LABCELL_X63_Y5_N3
\CPU|FSM|CO|load_pc~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|load_pc~0_combout\ = ( \CPU|FSM|stateRegister|out\(3) & ( !\CPU|FSM|stateRegister|out\(2) & ( (\CPU|FSM|stateRegister|out\(0) & !\CPU|FSM|stateRegister|out\(1)) ) ) ) # ( !\CPU|FSM|stateRegister|out\(3) & ( !\CPU|FSM|stateRegister|out\(2) & ( 
-- (!\CPU|FSM|stateRegister|out\(0) & !\CPU|FSM|stateRegister|out\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000010100000010100000101000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CO|load_pc~0_combout\);

-- Location: FF_X63_Y2_N58
\CPU|program_counter|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~1_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(8),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(8));

-- Location: LABCELL_X60_Y4_N42
\CPU|DP|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~21_sumout\ = SUM(( \CPU|program_counter|register|out\(5) ) + ( GND ) + ( \CPU|DP|Add0~18\ ))
-- \CPU|DP|Add0~22\ = CARRY(( \CPU|program_counter|register|out\(5) ) + ( GND ) + ( \CPU|DP|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|program_counter|register|ALT_INV_out\(5),
	cin => \CPU|DP|Add0~18\,
	sumout => \CPU|DP|Add0~21_sumout\,
	cout => \CPU|DP|Add0~22\);

-- Location: LABCELL_X60_Y4_N45
\CPU|DP|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~25_sumout\ = SUM(( \CPU|program_counter|register|out\(6) ) + ( GND ) + ( \CPU|DP|Add0~22\ ))
-- \CPU|DP|Add0~26\ = CARRY(( \CPU|program_counter|register|out\(6) ) + ( GND ) + ( \CPU|DP|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|program_counter|register|ALT_INV_out\(6),
	cin => \CPU|DP|Add0~22\,
	sumout => \CPU|DP|Add0~25_sumout\,
	cout => \CPU|DP|Add0~26\);

-- Location: LABCELL_X60_Y4_N48
\CPU|DP|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~29_sumout\ = SUM(( \CPU|program_counter|register|out\(7) ) + ( GND ) + ( \CPU|DP|Add0~26\ ))
-- \CPU|DP|Add0~30\ = CARRY(( \CPU|program_counter|register|out\(7) ) + ( GND ) + ( \CPU|DP|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|program_counter|register|ALT_INV_out\(7),
	cin => \CPU|DP|Add0~26\,
	sumout => \CPU|DP|Add0~29_sumout\,
	cout => \CPU|DP|Add0~30\);

-- Location: LABCELL_X60_Y4_N51
\CPU|DP|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~1_sumout\ = SUM(( \CPU|program_counter|register|out\(8) ) + ( GND ) + ( \CPU|DP|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|program_counter|register|ALT_INV_out\(8),
	cin => \CPU|DP|Add0~30\,
	sumout => \CPU|DP|Add0~1_sumout\);

-- Location: LABCELL_X60_Y4_N6
\CPU|DP|WBmux|d|ShiftLeft0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ = ( !\CPU|FSM|CO|Mux9~2_combout\ & ( (!\CPU|FSM|stateRegister|out\(1) $ (\CPU|FSM|stateRegister|out\(2))) # (\CPU|FSM|stateRegister|out\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101110101110111110111010111011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|CO|ALT_INV_Mux9~2_combout\,
	combout => \CPU|DP|WBmux|d|ShiftLeft0~2_combout\);

-- Location: LABCELL_X61_Y3_N27
\CPU|DP|WBmux|m|b[8]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[8]~0_combout\ = ( !\CPU|DP|WBmux|m|comb~0_combout\ & ( (!\CPU|DP|c|register|out\(8)) # (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110000111111111111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|DP|c|register|ALT_INV_out\(8),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	dataf => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	combout => \CPU|DP|WBmux|m|b[8]~0_combout\);

-- Location: LABCELL_X61_Y3_N24
\CPU|DP|WBmux|m|b[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(8) = ( \read_data[8]~5_combout\ & ( (!\CPU|DP|WBmux|m|b[8]~0_combout\) # (((\CPU|DP|Add0~1_sumout\ & \CPU|DP|WBmux|d|ShiftLeft0~0_combout\)) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\)) ) ) # ( !\read_data[8]~5_combout\ & ( 
-- (!\CPU|DP|WBmux|m|b[8]~0_combout\) # ((\CPU|DP|Add0~1_sumout\ & \CPU|DP|WBmux|d|ShiftLeft0~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000111110001111100011111000111110001111111111111000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Add0~1_sumout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	datac => \CPU|DP|WBmux|m|ALT_INV_b[8]~0_combout\,
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	dataf => \ALT_INV_read_data[8]~5_combout\,
	combout => \CPU|DP|WBmux|m|b\(8));

-- Location: LABCELL_X60_Y5_N6
\CPU|DP|REGFILE|Reg5|register|out[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg5|register|out[8]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(8) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(8),
	combout => \CPU|DP|REGFILE|Reg5|register|out[8]~feeder_combout\);

-- Location: LABCELL_X62_Y5_N42
\rtl~36\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~36_combout\ = ( \rtl~28_combout\ & ( \CPU|FSM|CO|Mux1~1_combout\ & ( !\CPU|FSM|stateRegister|out\(1) $ (!\CPU|FSM|stateRegister|out\(3)) ) ) ) # ( !\rtl~28_combout\ & ( \CPU|FSM|CO|Mux1~1_combout\ & ( (!\CPU|FSM|stateRegister|out\(0) & 
-- (\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3))) ) ) ) # ( \rtl~28_combout\ & ( !\CPU|FSM|CO|Mux1~1_combout\ & ( (!\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3))) ) ) ) # ( 
-- !\rtl~28_combout\ & ( !\CPU|FSM|CO|Mux1~1_combout\ & ( (!\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000000000000010100000000000001010000000000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datae => \ALT_INV_rtl~28_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	combout => \rtl~36_combout\);

-- Location: LABCELL_X63_Y4_N21
\rtl~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~33_combout\ = ( \rtl~28_combout\ & ( (\rtl~9_combout\ & ((\CPU|FSM|CO|Mux1~1_combout\) # (\rtl~32_combout\))) ) ) # ( !\rtl~28_combout\ & ( (\rtl~32_combout\ & \rtl~9_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010100000000010111110000000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~32_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datad => \ALT_INV_rtl~9_combout\,
	dataf => \ALT_INV_rtl~28_combout\,
	combout => \rtl~33_combout\);

-- Location: LABCELL_X63_Y4_N15
\rtl~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~34_combout\ = ( !\CPU|InstructionRegister|register|out\(12) & ( !\CPU|InstructionRegister|register|out\(11) & ( (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (!\CPU|InstructionRegister|register|out\(15) & 
-- (!\CPU|InstructionRegister|register|out\(13) & \CPU|FSM|stateRegister|out\(0)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	combout => \rtl~34_combout\);

-- Location: LABCELL_X62_Y4_N6
\rtl~31\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~31_combout\ = ( \CPU|InstructionRegister|register|out\(14) & ( (\CPU|FSM|stateRegister|out\(1) & (((!\CPU|InstructionRegister|register|out\(15) & \CPU|InstructionRegister|register|out\(13))) # (\CPU|FSM|stateRegister|out\(2)))) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(14) & ( (\CPU|FSM|stateRegister|out\(2) & (((\CPU|InstructionRegister|register|out\(15) & !\CPU|InstructionRegister|register|out\(13))) # (\CPU|FSM|stateRegister|out\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010100010001000101010001000100010001001100010001000100110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	combout => \rtl~31_combout\);

-- Location: LABCELL_X62_Y4_N24
\rtl~35\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~35_combout\ = ( \rtl~34_combout\ & ( \rtl~31_combout\ & ( (!\CPU|FSM|stateRegister|out\(0)) # ((\CPU|FSM|stateRegister|out\(3) & ((!\CPU|FSM|stateRegister|out\(2)) # (!\CPU|FSM|stateRegister|out\(1))))) ) ) ) # ( !\rtl~34_combout\ & ( 
-- \rtl~31_combout\ & ( (!\CPU|FSM|stateRegister|out\(0)) # ((\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(2) $ (!\CPU|FSM|stateRegister|out\(1))))) ) ) ) # ( \rtl~34_combout\ & ( !\rtl~31_combout\ & ( (\CPU|FSM|stateRegister|out\(3) & 
-- ((!\CPU|FSM|stateRegister|out\(1)) # ((!\CPU|FSM|stateRegister|out\(2) & \CPU|FSM|stateRegister|out\(0))))) ) ) ) # ( !\rtl~34_combout\ & ( !\rtl~31_combout\ & ( (\CPU|FSM|stateRegister|out\(3) & ((!\CPU|FSM|stateRegister|out\(2) & 
-- (\CPU|FSM|stateRegister|out\(1) & \CPU|FSM|stateRegister|out\(0))) # (\CPU|FSM|stateRegister|out\(2) & (!\CPU|FSM|stateRegister|out\(1))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000110000011000000111011111111000001101111111100001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \ALT_INV_rtl~34_combout\,
	dataf => \ALT_INV_rtl~31_combout\,
	combout => \rtl~35_combout\);

-- Location: LABCELL_X62_Y4_N39
\CPU|ID|RWmux|comb~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|comb~3_combout\ = ( \CPU|InstructionRegister|register|out\(0) & ( \CPU|FSM|stateRegister|out\(2) & ( (!\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(1) & (!\CPU|FSM|stateRegister|out\(0) & !\CPU|FSM|CS|WideOr5~0_combout\))) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(0),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|ID|RWmux|comb~3_combout\);

-- Location: LABCELL_X62_Y4_N0
\CPU|ID|RWmux|b[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|b\(0) = ( !\CPU|ID|RWmux|comb~3_combout\ & ( (!\rtl~33_combout\ & ((!\rtl~35_combout\) # ((!\CPU|InstructionRegister|register|out\(5))))) # (\rtl~33_combout\ & (!\CPU|InstructionRegister|register|out\(8) & ((!\rtl~35_combout\) # 
-- (!\CPU|InstructionRegister|register|out\(5))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110010101000111111001010100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~33_combout\,
	datab => \ALT_INV_rtl~35_combout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(5),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(8),
	dataf => \CPU|ID|RWmux|ALT_INV_comb~3_combout\,
	combout => \CPU|ID|RWmux|b\(0));

-- Location: LABCELL_X64_Y4_N6
\CPU|ID|RWmux|comb~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|comb~5_combout\ = ( \CPU|InstructionRegister|register|out\(2) & ( !\CPU|FSM|CS|WideOr5~0_combout\ & ( (!\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|stateRegister|out\(2) & (!\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3)))) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(2),
	dataf => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	combout => \CPU|ID|RWmux|comb~5_combout\);

-- Location: FF_X63_Y2_N25
\CPU|DP|a|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(8),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(8));

-- Location: FF_X65_Y2_N8
\CPU|DP|a|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|readMux|m|b\(9),
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(9));

-- Location: LABCELL_X63_Y4_N36
\CPU|DP|Bin[1]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[1]~3_combout\ = ( \CPU|InstructionRegister|register|out\(14) & ( (\CPU|InstructionRegister|register|out\(15) & (!\CPU|InstructionRegister|register|out\(13) & ((\CPU|InstructionRegister|register|out\(3)) # 
-- (\CPU|InstructionRegister|register|out\(4))))) ) ) # ( !\CPU|InstructionRegister|register|out\(14) & ( (\CPU|InstructionRegister|register|out\(15) & (\CPU|InstructionRegister|register|out\(13) & ((\CPU|InstructionRegister|register|out\(3)) # 
-- (\CPU|InstructionRegister|register|out\(4))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010011000000000001001100010011000000000001001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(3),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	combout => \CPU|DP|Bin[1]~3_combout\);

-- Location: LABCELL_X61_Y3_N6
\CPU|DP|WBmux|m|b[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(10) = ( \read_data[10]~12_combout\ & ( (((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(10))) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) ) # ( !\read_data[10]~12_combout\ & ( 
-- ((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(10))) # (\CPU|DP|WBmux|m|comb~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011101010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(10),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	dataf => \ALT_INV_read_data[10]~12_combout\,
	combout => \CPU|DP|WBmux|m|b\(10));

-- Location: LABCELL_X60_Y2_N21
\CPU|DP|REGFILE|Reg0|register|out[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg0|register|out[10]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(10) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(10),
	combout => \CPU|DP|REGFILE|Reg0|register|out[10]~feeder_combout\);

-- Location: LABCELL_X61_Y5_N54
\CPU|DP|REGFILE|regSelect[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(0) = ( \CPU|ID|RWmux|b\(2) & ( (\rtl~36_combout\ & (\CPU|ID|RWmux|b\(1) & \CPU|ID|RWmux|b\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000110000000000000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_rtl~36_combout\,
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|regSelect\(0));

-- Location: FF_X60_Y2_N22
\CPU|DP|REGFILE|Reg0|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg0|register|out[10]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(10));

-- Location: LABCELL_X62_Y5_N0
\CPU|DP|REGFILE|regSelect[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(4) = ( \CPU|ID|RWmux|b\(0) & ( (\rtl~36_combout\ & (!\CPU|ID|RWmux|b\(2) & \CPU|ID|RWmux|b\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100000000000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_rtl~36_combout\,
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regSelect\(4));

-- Location: FF_X62_Y2_N55
\CPU|DP|REGFILE|Reg4|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(10));

-- Location: LABCELL_X61_Y5_N48
\CPU|DP|REGFILE|regSelect[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(2) = ( \CPU|ID|RWmux|b\(0) & ( \CPU|ID|RWmux|b\(2) & ( (\rtl~36_combout\ & !\CPU|ID|RWmux|b\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_rtl~36_combout\,
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|ID|RWmux|ALT_INV_b\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|regSelect\(2));

-- Location: FF_X60_Y2_N16
\CPU|DP|REGFILE|Reg2|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(10));

-- Location: LABCELL_X62_Y3_N21
\CPU|DP|REGFILE|readMux|m|b[10]~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[10]~30_combout\ = ( \CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg4|register|out\(10)))) # (\CPU|ID|RWmux|b\(2) & (\CPU|DP|REGFILE|Reg0|register|out\(10))) ) ) ) # ( 
-- !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & \CPU|DP|REGFILE|Reg2|register|out\(10)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110001110100011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(10),
	datab => \CPU|ID|RWmux|ALT_INV_b\(2),
	datac => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(10),
	datad => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(10),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[10]~30_combout\);

-- Location: LABCELL_X61_Y3_N21
\CPU|DP|REGFILE|regSelect[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(6) = ( \rtl~36_combout\ & ( (\CPU|ID|RWmux|b\(0) & (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001000000010000000100000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(0),
	datab => \CPU|ID|RWmux|ALT_INV_b\(1),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \ALT_INV_rtl~36_combout\,
	combout => \CPU|DP|REGFILE|regSelect\(6));

-- Location: FF_X61_Y3_N2
\CPU|DP|REGFILE|Reg6|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(10));

-- Location: LABCELL_X61_Y3_N33
\CPU|DP|REGFILE|regSelect[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(7) = ( !\CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (!\CPU|ID|RWmux|b\(1) & \rtl~36_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(2),
	datab => \CPU|ID|RWmux|ALT_INV_b\(1),
	datac => \ALT_INV_rtl~36_combout\,
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regSelect\(7));

-- Location: FF_X61_Y3_N8
\CPU|DP|REGFILE|Reg7|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(10),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(10));

-- Location: LABCELL_X61_Y3_N18
\CPU|DP|REGFILE|readMux|m|b[10]~32\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[10]~32_combout\ = ( \CPU|DP|REGFILE|Reg7|register|out\(10) & ( (!\CPU|ID|RWmux|b\(1) & (!\CPU|ID|RWmux|b\(2) & ((!\CPU|ID|RWmux|b\(0)) # (\CPU|DP|REGFILE|Reg6|register|out\(10))))) ) ) # ( 
-- !\CPU|DP|REGFILE|Reg7|register|out\(10) & ( (\CPU|ID|RWmux|b\(0) & (!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg6|register|out\(10) & !\CPU|ID|RWmux|b\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000000000001000000000010001100000000001000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(0),
	datab => \CPU|ID|RWmux|ALT_INV_b\(1),
	datac => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(10),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(10),
	combout => \CPU|DP|REGFILE|readMux|m|b[10]~32_combout\);

-- Location: FF_X60_Y3_N25
\CPU|DP|REGFILE|Reg5|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(10));

-- Location: LABCELL_X62_Y1_N39
\CPU|DP|REGFILE|Reg3|register|out[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[10]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(10) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(10),
	combout => \CPU|DP|REGFILE|Reg3|register|out[10]~feeder_combout\);

-- Location: LABCELL_X62_Y5_N3
\CPU|DP|REGFILE|regSelect[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(3) = ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & (\rtl~36_combout\ & !\CPU|ID|RWmux|b\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(2),
	datac => \ALT_INV_rtl~36_combout\,
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regSelect\(3));

-- Location: FF_X62_Y1_N40
\CPU|DP|REGFILE|Reg3|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[10]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(10));

-- Location: LABCELL_X62_Y5_N57
\CPU|DP|REGFILE|regSelect[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(1) = ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & (\CPU|ID|RWmux|b\(1) & \rtl~36_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000001000000010000000100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(2),
	datab => \CPU|ID|RWmux|ALT_INV_b\(1),
	datac => \ALT_INV_rtl~36_combout\,
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|regSelect\(1));

-- Location: FF_X62_Y2_N41
\CPU|DP|REGFILE|Reg1|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(10),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(10));

-- Location: LABCELL_X62_Y2_N39
\CPU|DP|REGFILE|readMux|m|b[10]~31\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[10]~31_combout\ = ( \CPU|ID|RWmux|b\(2) & ( (!\CPU|ID|RWmux|b\(0) & ((!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg3|register|out\(10))) # (\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg1|register|out\(10)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000010011000000100001001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(1),
	datab => \CPU|ID|RWmux|ALT_INV_b\(0),
	datac => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(10),
	datad => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(10),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[10]~31_combout\);

-- Location: LABCELL_X62_Y3_N54
\CPU|DP|REGFILE|readMux|m|b[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(10) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[10]~31_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[10]~31_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[10]~31_combout\ & ( ((\CPU|DP|REGFILE|Reg5|register|out\(10)) # (\CPU|DP|REGFILE|readMux|m|b[10]~32_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[10]~30_combout\) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[10]~31_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[10]~32_combout\) # (\CPU|DP|REGFILE|readMux|m|b[10]~30_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011101110111011111110111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~30_combout\,
	datab => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~32_combout\,
	datac => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(10),
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[10]~31_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(10));

-- Location: FF_X62_Y3_N58
\CPU|DP|b|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(10),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(10));

-- Location: LABCELL_X64_Y4_N24
\CPU|DP|Bin[1]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[1]~4_combout\ = ( \CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|InstructionRegister|register|out\(4) & ( (!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (\CPU|InstructionRegister|register|out\(15) & 
-- ((!\CPU|FSM|storeFlag|register|out\(0)) # (\CPU|InstructionRegister|register|out\(13))))) # (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (!\CPU|InstructionRegister|register|out\(15) $ ((!\CPU|InstructionRegister|register|out\(13))))) ) ) ) # 
-- ( !\CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|InstructionRegister|register|out\(4) & ( (\CPU|InstructionRegister|register|out\(15) & (!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ $ (!\CPU|InstructionRegister|register|out\(13)))) ) ) ) # ( 
-- \CPU|FSM|CO|Decoder0~1_combout\ & ( !\CPU|InstructionRegister|register|out\(4) & ( (!\CPU|InstructionRegister|register|out\(15) & (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (\CPU|InstructionRegister|register|out\(13)))) # 
-- (\CPU|InstructionRegister|register|out\(15) & (!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (!\CPU|InstructionRegister|register|out\(13) & !\CPU|FSM|storeFlag|register|out\(0)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010000100000001000010100000101000101011000010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datab => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datad => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datae => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	combout => \CPU|DP|Bin[1]~4_combout\);

-- Location: FF_X65_Y2_N44
\CPU|DP|b|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(9),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(9));

-- Location: MLABCELL_X65_Y2_N45
\CPU|DP|Bin[9]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[9]~13_combout\ = ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(9) & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(10)))) ) ) ) # ( 
-- !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(9) & ( (!\CPU|DP|Bin[1]~3_combout\) # (\CPU|DP|b|register|out\(8)) ) ) ) # ( \CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(9) & ( (!\CPU|DP|Bin[1]~3_combout\ & 
-- (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(10)))) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(9) & ( (\CPU|DP|b|register|out\(8) & \CPU|DP|Bin[1]~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011010100000101111111110011111100110101000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datab => \CPU|DP|b|register|ALT_INV_out\(8),
	datac => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datad => \CPU|DP|b|register|ALT_INV_out\(10),
	datae => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	dataf => \CPU|DP|b|register|ALT_INV_out\(9),
	combout => \CPU|DP|Bin[9]~13_combout\);

-- Location: MLABCELL_X65_Y2_N21
\CPU|DP|ALU|plusMinus|ai|g[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(9) = ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|DP|a|register|out\(9) & (!\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[9]~13_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100001100000000110000110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|a|register|ALT_INV_out\(9),
	datad => \CPU|DP|ALT_INV_Bin[9]~13_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(9));

-- Location: MLABCELL_X65_Y2_N18
\CPU|DP|ALU|out[10]~24\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[10]~24_combout\ = ( !\CPU|InstructionRegister|register|out\(12) & ( !\CPU|DP|ALU|plusMinus|ai|g\(9) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(9),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	combout => \CPU|DP|ALU|out[10]~24_combout\);

-- Location: FF_X62_Y3_N13
\CPU|DP|a|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(10),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(10));

-- Location: FF_X64_Y3_N14
\CPU|DP|b|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(11),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(11));

-- Location: LABCELL_X64_Y3_N6
\CPU|DP|Bin[10]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[10]~14_combout\ = ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(11) & ( (\CPU|InstructionRegister|register|out\(4)) # (\CPU|DP|Bin[1]~3_combout\) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(11) & ( 
-- (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(10)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out[9]~DUPLICATE_q\)) ) ) ) # ( \CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(11) & ( (!\CPU|DP|Bin[1]~3_combout\ & 
-- \CPU|InstructionRegister|register|out\(4)) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(11) & ( (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(10)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out[9]~DUPLICATE_q\)) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011010100110101000000001111000000110101001101010000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out[9]~DUPLICATE_q\,
	datab => \CPU|DP|b|register|ALT_INV_out\(10),
	datac => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datae => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	dataf => \CPU|DP|b|register|ALT_INV_out\(11),
	combout => \CPU|DP|Bin[10]~14_combout\);

-- Location: MLABCELL_X65_Y2_N30
\CPU|DP|ALU|out[10]~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[10]~23_combout\ = ( \CPU|DP|ALU|plusMinus|ai|g\(9) & ( \CPU|DP|Bin[10]~14_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & (((\CPU|DP|a|register|out\(10) & !\CPU|FSM|CO|Selector7~0_combout\)))) # 
-- (\CPU|InstructionRegister|register|out\(11) & (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|a|register|out\(10)) # (\CPU|FSM|CO|Selector7~0_combout\)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|g\(9) & ( \CPU|DP|Bin[10]~14_combout\ & ( 
-- (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(10) & !\CPU|FSM|CO|Selector7~0_combout\))))) # (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|a|register|out\(10) & 
-- (!\CPU|FSM|CO|Selector7~0_combout\ & !\CPU|InstructionRegister|register|out\(11)))) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|g\(9) & ( !\CPU|DP|Bin[10]~14_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ ((((\CPU|DP|a|register|out\(10) & 
-- !\CPU|FSM|CO|Selector7~0_combout\)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|g\(9) & ( !\CPU|DP|Bin[10]~14_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ ((((!\CPU|DP|a|register|out\(10)) # 
-- (\CPU|FSM|CO|Selector7~0_combout\)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000011011111100010100111010110011010001000000011000010001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|DP|a|register|ALT_INV_out\(10),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(9),
	dataf => \CPU|DP|ALT_INV_Bin[10]~14_combout\,
	combout => \CPU|DP|ALU|out[10]~23_combout\);

-- Location: LABCELL_X63_Y2_N6
\CPU|DP|a|register|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|a|register|out[7]~feeder_combout\ = ( \CPU|DP|REGFILE|readMux|m|b\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b\(7),
	combout => \CPU|DP|a|register|out[7]~feeder_combout\);

-- Location: FF_X63_Y2_N7
\CPU|DP|a|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|a|register|out[7]~feeder_combout\,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(7));

-- Location: LABCELL_X66_Y4_N24
\CPU|DP|ALU|plusMinus|ai|g[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(7) = ( \CPU|DP|a|register|out\(7) & ( (!\CPU|FSM|CO|Selector7~0_combout\ & (!\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[7]~11_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010000101000000101000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|ALT_INV_Bin[7]~11_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(7),
	combout => \CPU|DP|ALU|plusMinus|ai|g\(7));

-- Location: FF_X63_Y3_N31
\CPU|DP|a|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(3),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(3));

-- Location: IOIBUF_X68_Y0_N1
\SW[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: FF_X63_Y3_N26
\CPU|DP|a|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|readMux|m|b\(12),
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(12));

-- Location: LABCELL_X63_Y3_N27
\CPU|DP|Ain[12]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[12]~3_combout\ = ( \CPU|DP|a|register|out\(12) & ( !\CPU|FSM|CO|Selector7~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(12),
	combout => \CPU|DP|Ain[12]~3_combout\);

-- Location: LABCELL_X64_Y3_N57
\CPU|DP|ALU|plusMinus|ai|p[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(11) = ( \CPU|DP|Bin[11]~15_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(11) & !\CPU|FSM|CO|Selector7~0_combout\))) ) ) # ( !\CPU|DP|Bin[11]~15_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|a|register|out\(11)) # (\CPU|FSM|CO|Selector7~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101000001111010110100000111110100101111100001010010111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|a|register|ALT_INV_out\(11),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[11]~15_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(11));

-- Location: LABCELL_X64_Y3_N24
\CPU|DP|ALU|plusMinus|ai|g[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(10) = ( \CPU|DP|Bin[10]~14_combout\ & ( (\CPU|DP|a|register|out\(10) & (!\CPU|FSM|CO|Selector7~0_combout\ & !\CPU|InstructionRegister|register|out\(11))) ) ) # ( !\CPU|DP|Bin[10]~14_combout\ & ( (\CPU|DP|a|register|out\(10) & 
-- (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|InstructionRegister|register|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000000110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|a|register|ALT_INV_out\(10),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[10]~14_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(10));

-- Location: FF_X65_Y2_N56
\CPU|DP|a|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|readMux|m|b\(13),
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(13));

-- Location: LABCELL_X67_Y3_N36
\CPU|FSM|CO|Selector11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector11~0_combout\ = ( \CPU|InstructionRegister|register|out\(12) & ( \CPU|FSM|CO|Decoder0~1_combout\ ) ) # ( !\CPU|InstructionRegister|register|out\(12) & ( (!\CPU|InstructionRegister|register|out\(11) & \CPU|FSM|CO|Decoder0~1_combout\) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010000000001010101000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	combout => \CPU|FSM|CO|Selector11~0_combout\);

-- Location: FF_X66_Y3_N26
\CPU|DP|c|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[14]~19_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(14));

-- Location: LABCELL_X61_Y5_N3
\CPU|DP|WBmux|m|b[14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(14) = ( \read_data[14]~1_combout\ & ( (((\CPU|DP|c|register|out\(14) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\)) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\) ) ) # ( !\read_data[14]~1_combout\ & ( 
-- ((\CPU|DP|c|register|out\(14) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100111111001100110011111101110111011111110111011101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datab => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(14),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	dataf => \ALT_INV_read_data[14]~1_combout\,
	combout => \CPU|DP|WBmux|m|b\(14));

-- Location: FF_X61_Y5_N5
\CPU|DP|REGFILE|Reg7|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(14),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(14));

-- Location: LABCELL_X62_Y1_N51
\CPU|DP|REGFILE|Reg6|register|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg6|register|out[14]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(14) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(14),
	combout => \CPU|DP|REGFILE|Reg6|register|out[14]~feeder_combout\);

-- Location: FF_X62_Y1_N52
\CPU|DP|REGFILE|Reg6|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg6|register|out[14]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(14));

-- Location: LABCELL_X64_Y2_N24
\CPU|DP|REGFILE|readMux|m|b[14]~44\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[14]~44_combout\ = ( \CPU|ID|RWmux|b\(0) & ( !\CPU|ID|RWmux|b\(1) & ( (!\CPU|ID|RWmux|b\(2) & \CPU|DP|REGFILE|Reg6|register|out\(14)) ) ) ) # ( !\CPU|ID|RWmux|b\(0) & ( !\CPU|ID|RWmux|b\(1) & ( (!\CPU|ID|RWmux|b\(2) & 
-- \CPU|DP|REGFILE|Reg7|register|out\(14)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000000001100110000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|ID|RWmux|ALT_INV_b\(2),
	datac => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(14),
	datad => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(14),
	datae => \CPU|ID|RWmux|ALT_INV_b\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|readMux|m|b[14]~44_combout\);

-- Location: LABCELL_X60_Y5_N54
\CPU|DP|REGFILE|Reg5|register|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg5|register|out[14]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(14) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(14),
	combout => \CPU|DP|REGFILE|Reg5|register|out[14]~feeder_combout\);

-- Location: FF_X60_Y5_N56
\CPU|DP|REGFILE|Reg5|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg5|register|out[14]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(14));

-- Location: FF_X61_Y5_N53
\CPU|DP|REGFILE|Reg0|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(14));

-- Location: MLABCELL_X59_Y5_N6
\CPU|DP|REGFILE|Reg4|register|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg4|register|out[14]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(14) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(14),
	combout => \CPU|DP|REGFILE|Reg4|register|out[14]~feeder_combout\);

-- Location: FF_X59_Y5_N7
\CPU|DP|REGFILE|Reg4|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg4|register|out[14]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(14));

-- Location: FF_X60_Y2_N11
\CPU|DP|REGFILE|Reg2|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(14));

-- Location: LABCELL_X60_Y2_N12
\CPU|DP|REGFILE|readMux|m|b[14]~42\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[14]~42_combout\ = ( \CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg4|register|out\(14)))) # (\CPU|ID|RWmux|b\(2) & (\CPU|DP|REGFILE|Reg0|register|out\(14))) ) ) ) # ( 
-- !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & \CPU|DP|REGFILE|Reg2|register|out\(14)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110001110100011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(14),
	datab => \CPU|ID|RWmux|ALT_INV_b\(2),
	datac => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(14),
	datad => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(14),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[14]~42_combout\);

-- Location: FF_X61_Y5_N41
\CPU|DP|REGFILE|Reg3|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(14));

-- Location: FF_X62_Y1_N56
\CPU|DP|REGFILE|Reg1|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(14),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(14));

-- Location: LABCELL_X62_Y1_N54
\CPU|DP|REGFILE|readMux|m|b[14]~43\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[14]~43_combout\ = ( \CPU|DP|REGFILE|Reg1|register|out\(14) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((\CPU|ID|RWmux|b\(1)) # (\CPU|DP|REGFILE|Reg3|register|out\(14)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|Reg1|register|out\(14) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg3|register|out\(14) & (\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(14),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(14),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[14]~43_combout\);

-- Location: LABCELL_X64_Y2_N42
\CPU|DP|REGFILE|readMux|m|b[14]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(14) = ( \CPU|DP|REGFILE|readMux|m|b[14]~42_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[14]~43_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[14]~42_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[14]~43_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|m|b[14]~42_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[14]~43_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[14]~42_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[14]~43_combout\ & ( ((\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & 
-- \CPU|DP|REGFILE|Reg5|register|out\(14))) # (\CPU|DP|REGFILE|readMux|m|b[14]~44_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~44_combout\,
	datab => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	datac => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(14),
	datae => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~42_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[14]~43_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(14));

-- Location: LABCELL_X64_Y2_N6
\CPU|DP|b|register|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|b|register|out[14]~feeder_combout\ = ( \CPU|DP|REGFILE|readMux|m|b\(14) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b\(14),
	combout => \CPU|DP|b|register|out[14]~feeder_combout\);

-- Location: FF_X64_Y2_N7
\CPU|DP|b|register|out[14]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|b|register|out[14]~feeder_combout\,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out[14]~DUPLICATE_q\);

-- Location: FF_X65_Y2_N37
\CPU|DP|b|register|out[13]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(13),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out[13]~DUPLICATE_q\);

-- Location: LABCELL_X64_Y3_N51
\CPU|DP|Bin[13]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[13]~17_combout\ = ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|InstructionRegister|register|out\(4) & ( (!\CPU|DP|Bin[1]~3_combout\) # (\CPU|DP|b|register|out[14]~DUPLICATE_q\) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( 
-- \CPU|InstructionRegister|register|out\(4) & ( (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out[13]~DUPLICATE_q\))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out\(12))) ) ) ) # ( \CPU|DP|Bin[1]~4_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(4) & ( (\CPU|DP|b|register|out[14]~DUPLICATE_q\ & \CPU|DP|Bin[1]~3_combout\) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( !\CPU|InstructionRegister|register|out\(4) & ( (!\CPU|DP|Bin[1]~3_combout\ & 
-- ((\CPU|DP|b|register|out[13]~DUPLICATE_q\))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out\(12))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101010101000000000011001100001111010101011111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(12),
	datab => \CPU|DP|b|register|ALT_INV_out[14]~DUPLICATE_q\,
	datac => \CPU|DP|b|register|ALT_INV_out[13]~DUPLICATE_q\,
	datad => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datae => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	combout => \CPU|DP|Bin[13]~17_combout\);

-- Location: LABCELL_X62_Y2_N3
\CPU|DP|ALU|out[13]~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[13]~30_combout\ = ( \CPU|DP|Bin[13]~17_combout\ & ( \CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & \CPU|InstructionRegister|register|out\(11)) ) ) ) # ( !\CPU|DP|Bin[13]~17_combout\ & ( 
-- \CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|InstructionRegister|register|out\(12) $ (\CPU|InstructionRegister|register|out\(11)) ) ) ) # ( \CPU|DP|Bin[13]~17_combout\ & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & 
-- ((\CPU|DP|a|register|out\(13)))) # (\CPU|InstructionRegister|register|out\(11) & (!\CPU|InstructionRegister|register|out\(12) & !\CPU|DP|a|register|out\(13))) ) ) ) # ( !\CPU|DP|Bin[13]~17_combout\ & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(13)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1001100100110011001000101100110010011001100110010010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|DP|a|register|ALT_INV_out\(13),
	datae => \CPU|DP|ALT_INV_Bin[13]~17_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|out[13]~30_combout\);

-- Location: LABCELL_X64_Y3_N42
\CPU|DP|ALU|out[13]~31\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[13]~31_combout\ = ( \CPU|DP|ALU|plusMinus|comb~0_combout\ & ( \CPU|DP|ALU|plusMinus|ai|g\(10) & ( (!\CPU|DP|Ain[12]~3_combout\ & (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|p\(11) & 
-- !\CPU|DP|ALU|plusMinus|ai|g\(11)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|comb~0_combout\ & ( \CPU|DP|ALU|plusMinus|ai|g\(10) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|Ain[12]~3_combout\) # ((!\CPU|DP|ALU|plusMinus|ai|p\(11) & 
-- !\CPU|DP|ALU|plusMinus|ai|g\(11))))) ) ) ) # ( \CPU|DP|ALU|plusMinus|comb~0_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(10) & ( (!\CPU|DP|Ain[12]~3_combout\ & (!\CPU|InstructionRegister|register|out\(12) & !\CPU|DP|ALU|plusMinus|ai|g\(11))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|comb~0_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(10) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|Ain[12]~3_combout\) # (!\CPU|DP|ALU|plusMinus|ai|g\(11)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110010001000100010000000000011001000100010001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(11),
	datae => \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(10),
	combout => \CPU|DP|ALU|out[13]~31_combout\);

-- Location: LABCELL_X64_Y3_N21
\CPU|DP|ALU|plusMinus|ai|p[10]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(10) = ( \CPU|DP|a|register|out\(10) & ( !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[10]~14_combout\ $ (!\CPU|FSM|CO|Selector7~0_combout\)) ) ) # ( !\CPU|DP|a|register|out\(10) & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[10]~14_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001011010010110100101101010100101010110101010010101011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Bin[10]~14_combout\,
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(10),
	combout => \CPU|DP|ALU|plusMinus|ai|p\(10));

-- Location: LABCELL_X64_Y2_N0
\CPU|DP|ALU|plusMinus|ai|g[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(8) = ( \CPU|InstructionRegister|register|out\(11) & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|DP|a|register|out\(8) & !\CPU|DP|Bin[8]~12_combout\) ) ) ) # ( !\CPU|InstructionRegister|register|out\(11) & ( 
-- !\CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|DP|a|register|out\(8) & \CPU|DP|Bin[8]~12_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011001100000011000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|a|register|ALT_INV_out\(8),
	datac => \CPU|DP|ALT_INV_Bin[8]~12_combout\,
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(8));

-- Location: MLABCELL_X65_Y2_N48
\CPU|DP|ALU|plusMinus|ai|p[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(9) = ( \CPU|DP|a|register|out\(9) & ( !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|FSM|CO|Selector7~0_combout\ $ (!\CPU|DP|Bin[9]~13_combout\)) ) ) # ( !\CPU|DP|a|register|out\(9) & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[9]~13_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001111001100001100111100110011000011001111001100001100111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|ALT_INV_Bin[9]~13_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(9),
	combout => \CPU|DP|ALU|plusMinus|ai|p\(9));

-- Location: MLABCELL_X65_Y4_N6
\CPU|DP|ALU|plusMinus|ai|comb~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(8) & ( \CPU|DP|ALU|plusMinus|ai|p\(9) & ( (\CPU|DP|ALU|plusMinus|ai|p\(10) & ((\CPU|DP|ALU|plusMinus|ai|g\(9)) # (\CPU|DP|ALU|plusMinus|ai|g\(8)))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|ai|c\(8) & ( \CPU|DP|ALU|plusMinus|ai|p\(9) & ( (\CPU|DP|ALU|plusMinus|ai|p\(10) & (((\CPU|DP|ALU|plusMinus|ai|p\(8)) # (\CPU|DP|ALU|plusMinus|ai|g\(9))) # (\CPU|DP|ALU|plusMinus|ai|g\(8)))) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|c\(8) 
-- & ( !\CPU|DP|ALU|plusMinus|ai|p\(9) & ( (\CPU|DP|ALU|plusMinus|ai|p\(10) & \CPU|DP|ALU|plusMinus|ai|g\(9)) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(8) & ( !\CPU|DP|ALU|plusMinus|ai|p\(9) & ( (\CPU|DP|ALU|plusMinus|ai|p\(10) & \CPU|DP|ALU|plusMinus|ai|g\(9)) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100010101010101010001010100010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(10),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(8),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(9),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(8),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(8),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(9),
	combout => \CPU|DP|ALU|plusMinus|ai|comb~1_combout\);

-- Location: LABCELL_X64_Y3_N30
\CPU|DP|ALU|out[13]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[13]~18_combout\ = ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( \CPU|DP|ALU|plusMinus|ai|p\(11) & ( !\CPU|DP|ALU|out[13]~30_combout\ $ ((((!\CPU|DP|ALU|out[13]~31_combout\) # (\CPU|DP|Ain[12]~3_combout\)) # 
-- (\CPU|DP|ALU|plusMinus|comb~0_combout\))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( \CPU|DP|ALU|plusMinus|ai|p\(11) & ( !\CPU|DP|ALU|out[13]~30_combout\ $ (!\CPU|DP|ALU|out[13]~31_combout\) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ 
-- & ( !\CPU|DP|ALU|plusMinus|ai|p\(11) & ( !\CPU|DP|ALU|out[13]~30_combout\ $ (!\CPU|DP|ALU|out[13]~31_combout\) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|p\(11) & ( !\CPU|DP|ALU|out[13]~30_combout\ $ 
-- (!\CPU|DP|ALU|out[13]~31_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110101010010101011010101001010101101010100101010110010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[13]~30_combout\,
	datab => \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\,
	datac => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	datad => \CPU|DP|ALU|ALT_INV_out[13]~31_combout\,
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	combout => \CPU|DP|ALU|out[13]~18_combout\);

-- Location: FF_X64_Y3_N32
\CPU|DP|c|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[13]~18_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(13));

-- Location: LABCELL_X61_Y5_N0
\CPU|DP|WBmux|m|b[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(13) = ( \read_data[13]~0_combout\ & ( (((\CPU|DP|c|register|out\(13) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\)) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\) ) ) # ( !\read_data[13]~0_combout\ & ( 
-- ((\CPU|DP|c|register|out\(13) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100111111001100110011111101110111011111110111011101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datab => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(13),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	dataf => \ALT_INV_read_data[13]~0_combout\,
	combout => \CPU|DP|WBmux|m|b\(13));

-- Location: LABCELL_X60_Y5_N51
\CPU|DP|REGFILE|Reg5|register|out[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg5|register|out[13]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(13) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(13),
	combout => \CPU|DP|REGFILE|Reg5|register|out[13]~feeder_combout\);

-- Location: FF_X60_Y5_N53
\CPU|DP|REGFILE|Reg5|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg5|register|out[13]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(13));

-- Location: FF_X62_Y5_N46
\CPU|DP|REGFILE|Reg4|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(13));

-- Location: FF_X61_Y5_N7
\CPU|DP|REGFILE|Reg0|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(13));

-- Location: FF_X60_Y4_N25
\CPU|DP|REGFILE|Reg2|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(13));

-- Location: MLABCELL_X65_Y2_N12
\CPU|DP|REGFILE|readMux|m|b[13]~39\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[13]~39_combout\ = ( \CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg0|register|out\(13) & \CPU|ID|RWmux|b\(0)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(2) & ( 
-- (\CPU|DP|REGFILE|Reg2|register|out\(13) & \CPU|ID|RWmux|b\(0)) ) ) ) # ( \CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg4|register|out\(13) & \CPU|ID|RWmux|b\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000101010100000000000011110000000000110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(13),
	datab => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(13),
	datac => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(13),
	datad => \CPU|ID|RWmux|ALT_INV_b\(0),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[13]~39_combout\);

-- Location: FF_X61_Y5_N2
\CPU|DP|REGFILE|Reg7|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(13),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(13));

-- Location: FF_X61_Y2_N5
\CPU|DP|REGFILE|Reg6|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(13));

-- Location: LABCELL_X61_Y2_N3
\CPU|DP|REGFILE|readMux|m|b[13]~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[13]~41_combout\ = ( \CPU|DP|REGFILE|Reg6|register|out\(13) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2)) ) ) ) # ( \CPU|DP|REGFILE|Reg6|register|out\(13) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(13) & (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2))) ) ) ) # ( !\CPU|DP|REGFILE|Reg6|register|out\(13) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg7|register|out\(13) & (!\CPU|ID|RWmux|b\(1) & 
-- !\CPU|ID|RWmux|b\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(13),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(13),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[13]~41_combout\);

-- Location: FF_X62_Y5_N58
\CPU|DP|REGFILE|Reg1|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(13));

-- Location: FF_X61_Y5_N19
\CPU|DP|REGFILE|Reg3|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(13),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(13));

-- Location: LABCELL_X64_Y5_N6
\CPU|DP|REGFILE|readMux|m|b[13]~40\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[13]~40_combout\ = ( \CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg1|register|out\(13) & \CPU|ID|RWmux|b\(2)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg3|register|out\(13) & \CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(13),
	datac => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(13),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[13]~40_combout\);

-- Location: MLABCELL_X65_Y2_N54
\CPU|DP|REGFILE|readMux|m|b[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(13) = ( \CPU|DP|REGFILE|readMux|m|b[13]~40_combout\ ) # ( !\CPU|DP|REGFILE|readMux|m|b[13]~40_combout\ & ( (((\CPU|DP|REGFILE|Reg5|register|out\(13) & \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\)) # 
-- (\CPU|DP|REGFILE|readMux|m|b[13]~41_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[13]~39_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111101111111001111110111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(13),
	datab => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~39_combout\,
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~41_combout\,
	datad => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[13]~40_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(13));

-- Location: FF_X65_Y2_N38
\CPU|DP|b|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(13),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(13));

-- Location: FF_X64_Y3_N13
\CPU|DP|b|register|out[11]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(11),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out[11]~DUPLICATE_q\);

-- Location: MLABCELL_X65_Y2_N39
\CPU|DP|Bin[12]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[12]~16_combout\ = ( \CPU|DP|b|register|out[11]~DUPLICATE_q\ & ( \CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(13)))) ) ) ) # ( 
-- !\CPU|DP|b|register|out[11]~DUPLICATE_q\ & ( \CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(13)))) ) ) ) # ( 
-- \CPU|DP|b|register|out[11]~DUPLICATE_q\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( (\CPU|DP|Bin[1]~3_combout\) # (\CPU|DP|b|register|out\(12)) ) ) ) # ( !\CPU|DP|b|register|out[11]~DUPLICATE_q\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( (\CPU|DP|b|register|out\(12) & 
-- !\CPU|DP|Bin[1]~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000010111110101111100110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(12),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datac => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datad => \CPU|DP|b|register|ALT_INV_out\(13),
	datae => \CPU|DP|b|register|ALT_INV_out[11]~DUPLICATE_q\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[12]~16_combout\);

-- Location: MLABCELL_X65_Y2_N57
\CPU|DP|ALU|plusMinus|comb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|comb~0_combout\ = ( \CPU|DP|Bin[12]~16_combout\ & ( !\CPU|InstructionRegister|register|out\(11) ) ) # ( !\CPU|DP|Bin[12]~16_combout\ & ( \CPU|InstructionRegister|register|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[12]~16_combout\,
	combout => \CPU|DP|ALU|plusMinus|comb~0_combout\);

-- Location: LABCELL_X64_Y3_N0
\CPU|DP|ALU|plusMinus|ai|s[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|s\(12) = ( \CPU|DP|ALU|plusMinus|comb~0_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|Ain[12]~3_combout\ $ (((\CPU|DP|ALU|plusMinus|ai|g\(11)) # (\CPU|DP|ALU|plusMinus|ai|p\(11)))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|comb~0_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|Ain[12]~3_combout\ $ (((!\CPU|DP|ALU|plusMinus|ai|p\(11) & !\CPU|DP|ALU|plusMinus|ai|g\(11)))) ) ) ) # ( \CPU|DP|ALU|plusMinus|comb~0_combout\ & ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|Ain[12]~3_combout\ $ ((((\CPU|DP|ALU|plusMinus|ai|p\(11) & \CPU|DP|ALU|plusMinus|ai|g\(10))) # (\CPU|DP|ALU|plusMinus|ai|g\(11)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|comb~0_combout\ & ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|Ain[12]~3_combout\ $ (((!\CPU|DP|ALU|plusMinus|ai|g\(11) & ((!\CPU|DP|ALU|plusMinus|ai|p\(11)) # (!\CPU|DP|ALU|plusMinus|ai|g\(10)))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011010101010101010010101010101100110101010101001100101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(10),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(11),
	datae => \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|s\(12));

-- Location: LABCELL_X64_Y3_N18
\CPU|DP|ALU|out[12]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[12]~16_combout\ = ( \CPU|DP|Bin[12]~16_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & \CPU|DP|Ain[12]~3_combout\) ) ) # ( !\CPU|DP|Bin[12]~16_combout\ & ( \CPU|InstructionRegister|register|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010100001010000010100000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[12]~16_combout\,
	combout => \CPU|DP|ALU|out[12]~16_combout\);

-- Location: LABCELL_X64_Y3_N36
\CPU|DP|ALU|out[12]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[12]~17_combout\ = ( \CPU|DP|ALU|out[12]~16_combout\ & ( (\CPU|DP|ALU|plusMinus|ai|s\(12)) # (\CPU|InstructionRegister|register|out\(12)) ) ) # ( !\CPU|DP|ALU|out[12]~16_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & 
-- \CPU|DP|ALU|plusMinus|ai|s\(12)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011001100000000001100110000110011111111110011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_s\(12),
	dataf => \CPU|DP|ALU|ALT_INV_out[12]~16_combout\,
	combout => \CPU|DP|ALU|out[12]~17_combout\);

-- Location: FF_X64_Y3_N37
\CPU|DP|c|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[12]~17_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(12));

-- Location: LABCELL_X61_Y3_N57
\CPU|DP|WBmux|m|b[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(12) = ( \read_data[12]~4_combout\ & ( (((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(12))) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) ) # ( !\read_data[12]~4_combout\ & ( 
-- ((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(12))) # (\CPU|DP|WBmux|m|comb~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011101010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(12),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	dataf => \ALT_INV_read_data[12]~4_combout\,
	combout => \CPU|DP|WBmux|m|b\(12));

-- Location: FF_X61_Y3_N59
\CPU|DP|REGFILE|Reg7|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(12),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(12));

-- Location: FF_X60_Y3_N19
\CPU|DP|REGFILE|Reg6|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(12));

-- Location: LABCELL_X62_Y3_N30
\CPU|DP|REGFILE|readMux|m|b[12]~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[12]~38_combout\ = ( !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg6|register|out\(12) & !\CPU|ID|RWmux|b\(2)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(12) & !\CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000000000000000000000110011000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(12),
	datab => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(12),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[12]~38_combout\);

-- Location: FF_X62_Y2_N44
\CPU|DP|REGFILE|Reg1|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(12));

-- Location: LABCELL_X62_Y1_N15
\CPU|DP|REGFILE|Reg3|register|out[12]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[12]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(12) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(12),
	combout => \CPU|DP|REGFILE|Reg3|register|out[12]~feeder_combout\);

-- Location: FF_X62_Y1_N16
\CPU|DP|REGFILE|Reg3|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[12]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(12));

-- Location: LABCELL_X62_Y2_N30
\CPU|DP|REGFILE|readMux|m|b[12]~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[12]~37_combout\ = ( \CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg1|register|out\(12) & \CPU|ID|RWmux|b\(2)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg3|register|out\(12) & \CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(12),
	datac => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(12),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[12]~37_combout\);

-- Location: MLABCELL_X59_Y5_N27
\CPU|DP|REGFILE|Reg2|register|out[12]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg2|register|out[12]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(12) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(12),
	combout => \CPU|DP|REGFILE|Reg2|register|out[12]~feeder_combout\);

-- Location: FF_X59_Y5_N28
\CPU|DP|REGFILE|Reg2|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg2|register|out[12]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(12));

-- Location: FF_X62_Y5_N43
\CPU|DP|REGFILE|Reg4|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(12));

-- Location: FF_X62_Y5_N32
\CPU|DP|REGFILE|Reg0|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(12));

-- Location: LABCELL_X62_Y5_N30
\CPU|DP|REGFILE|readMux|m|b[12]~36\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[12]~36_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(12) & ( \CPU|ID|RWmux|b\(1) & ( (\CPU|ID|RWmux|b\(0) & ((\CPU|DP|REGFILE|Reg4|register|out\(12)) # (\CPU|ID|RWmux|b\(2)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(12) 
-- & ( \CPU|ID|RWmux|b\(1) & ( (\CPU|ID|RWmux|b\(0) & (!\CPU|ID|RWmux|b\(2) & \CPU|DP|REGFILE|Reg4|register|out\(12))) ) ) ) # ( \CPU|DP|REGFILE|Reg0|register|out\(12) & ( !\CPU|ID|RWmux|b\(1) & ( (\CPU|DP|REGFILE|Reg2|register|out\(12) & 
-- (\CPU|ID|RWmux|b\(0) & \CPU|ID|RWmux|b\(2))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(12) & ( !\CPU|ID|RWmux|b\(1) & ( (\CPU|DP|REGFILE|Reg2|register|out\(12) & (\CPU|ID|RWmux|b\(0) & \CPU|ID|RWmux|b\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000001000000010000000100000000001100000000001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(12),
	datab => \CPU|ID|RWmux|ALT_INV_b\(0),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(12),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(12),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|readMux|m|b[12]~36_combout\);

-- Location: FF_X62_Y3_N4
\CPU|DP|REGFILE|Reg5|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(12),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(12));

-- Location: LABCELL_X63_Y3_N24
\CPU|DP|REGFILE|readMux|m|b[12]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(12) = ( \CPU|DP|REGFILE|Reg5|register|out\(12) & ( (((\CPU|DP|REGFILE|readMux|m|b[12]~36_combout\) # (\CPU|DP|REGFILE|readMux|m|b[12]~37_combout\)) # (\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\)) # 
-- (\CPU|DP|REGFILE|readMux|m|b[12]~38_combout\) ) ) # ( !\CPU|DP|REGFILE|Reg5|register|out\(12) & ( ((\CPU|DP|REGFILE|readMux|m|b[12]~36_combout\) # (\CPU|DP|REGFILE|readMux|m|b[12]~37_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[12]~38_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111111111111010111111111111101111111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~38_combout\,
	datab => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~37_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[12]~36_combout\,
	dataf => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(12),
	combout => \CPU|DP|REGFILE|readMux|m|b\(12));

-- Location: FF_X64_Y3_N50
\CPU|DP|b|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(12),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(12));

-- Location: LABCELL_X64_Y3_N15
\CPU|DP|Bin[11]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[11]~15_combout\ = ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(12) & ( (\CPU|DP|Bin[1]~3_combout\) # (\CPU|InstructionRegister|register|out\(4)) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(12) & ( 
-- (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(11)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out\(10))) ) ) ) # ( \CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(12) & ( (\CPU|InstructionRegister|register|out\(4) & 
-- !\CPU|DP|Bin[1]~3_combout\) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(12) & ( (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(11)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out\(10))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100110011010101010000000000001111001100110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datab => \CPU|DP|b|register|ALT_INV_out\(10),
	datac => \CPU|DP|b|register|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datae => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	dataf => \CPU|DP|b|register|ALT_INV_out\(12),
	combout => \CPU|DP|Bin[11]~15_combout\);

-- Location: LABCELL_X64_Y3_N27
\CPU|DP|ALU|out[11]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[11]~14_combout\ = ( \CPU|DP|a|register|out\(11) & ( (!\CPU|InstructionRegister|register|out\(11) & (\CPU|DP|Bin[11]~15_combout\ & !\CPU|FSM|CO|Selector7~0_combout\)) # (\CPU|InstructionRegister|register|out\(11) & 
-- (!\CPU|DP|Bin[11]~15_combout\)) ) ) # ( !\CPU|DP|a|register|out\(11) & ( (\CPU|InstructionRegister|register|out\(11) & !\CPU|DP|Bin[11]~15_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001010000010100000101000001011010010100000101101001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Bin[11]~15_combout\,
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(11),
	combout => \CPU|DP|ALU|out[11]~14_combout\);

-- Location: LABCELL_X64_Y3_N39
\CPU|DP|ALU|out[11]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[11]~15_combout\ = ( \CPU|DP|ALU|plusMinus|ai|g\(10) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|ai|p\(11)))) # (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[11]~14_combout\)) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|ai|g\(10) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|ai|p\(11) $ (!\CPU|DP|ALU|plusMinus|ai|comb~1_combout\)))) # (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[11]~14_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001110111010001000111011101000111010001110100011101000111010001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[11]~14_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(10),
	combout => \CPU|DP|ALU|out[11]~15_combout\);

-- Location: FF_X64_Y3_N41
\CPU|DP|c|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[11]~15_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(11));

-- Location: LABCELL_X61_Y4_N51
\CPU|DP|WBmux|m|b[15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(15) = ( \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ( \read_data[15]~2_combout\ ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ( \read_data[15]~2_combout\ & ( ((\CPU|DP|c|register|out\(15) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # 
-- (\CPU|DP|WBmux|m|comb~0_combout\) ) ) ) # ( \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ( !\read_data[15]~2_combout\ & ( ((\CPU|DP|c|register|out\(15) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) ) ) # ( 
-- !\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ( !\read_data[15]~2_combout\ & ( ((\CPU|DP|c|register|out\(15) & \CPU|DP|WBmux|d|ShiftLeft0~2_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111111111000001011111111100000101111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(15),
	datac => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datad => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datae => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	dataf => \ALT_INV_read_data[15]~2_combout\,
	combout => \CPU|DP|WBmux|m|b\(15));

-- Location: FF_X62_Y5_N7
\CPU|DP|REGFILE|Reg4|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(15));

-- Location: FF_X60_Y4_N28
\CPU|DP|REGFILE|Reg2|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(15));

-- Location: FF_X63_Y1_N56
\CPU|DP|REGFILE|Reg0|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(15));

-- Location: LABCELL_X63_Y1_N54
\CPU|DP|REGFILE|readMux|m|b[15]~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[15]~45_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(15) & ( \CPU|ID|RWmux|b\(2) & ( (\CPU|ID|RWmux|b\(0) & ((\CPU|DP|REGFILE|Reg2|register|out\(15)) # (\CPU|ID|RWmux|b\(1)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(15) 
-- & ( \CPU|ID|RWmux|b\(2) & ( (!\CPU|ID|RWmux|b\(1) & (\CPU|ID|RWmux|b\(0) & \CPU|DP|REGFILE|Reg2|register|out\(15))) ) ) ) # ( \CPU|DP|REGFILE|Reg0|register|out\(15) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|ID|RWmux|b\(1) & 
-- (\CPU|DP|REGFILE|Reg4|register|out\(15) & \CPU|ID|RWmux|b\(0))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(15) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg4|register|out\(15) & \CPU|ID|RWmux|b\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000001000000010000000100000000000010100000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(1),
	datab => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(15),
	datac => \CPU|ID|RWmux|ALT_INV_b\(0),
	datad => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(15),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(15),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[15]~45_combout\);

-- Location: LABCELL_X60_Y5_N0
\CPU|DP|REGFILE|Reg5|register|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg5|register|out[15]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(15),
	combout => \CPU|DP|REGFILE|Reg5|register|out[15]~feeder_combout\);

-- Location: FF_X60_Y5_N2
\CPU|DP|REGFILE|Reg5|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg5|register|out[15]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(15));

-- Location: FF_X60_Y4_N58
\CPU|DP|REGFILE|Reg3|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(15));

-- Location: FF_X62_Y1_N20
\CPU|DP|REGFILE|Reg1|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(15));

-- Location: LABCELL_X62_Y1_N18
\CPU|DP|REGFILE|readMux|m|b[15]~46\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[15]~46_combout\ = ( \CPU|DP|REGFILE|Reg1|register|out\(15) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((\CPU|ID|RWmux|b\(1)) # (\CPU|DP|REGFILE|Reg3|register|out\(15)))) ) ) ) # ( 
-- !\CPU|DP|REGFILE|Reg1|register|out\(15) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg3|register|out\(15) & (\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(15),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(15),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[15]~46_combout\);

-- Location: FF_X61_Y4_N53
\CPU|DP|REGFILE|Reg7|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(15),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(15));

-- Location: FF_X62_Y1_N26
\CPU|DP|REGFILE|Reg6|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(15),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(15));

-- Location: LABCELL_X62_Y1_N24
\CPU|DP|REGFILE|readMux|m|b[15]~47\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[15]~47_combout\ = ( \CPU|DP|REGFILE|Reg6|register|out\(15) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1)) ) ) ) # ( \CPU|DP|REGFILE|Reg6|register|out\(15) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(15) & (!\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) ) # ( !\CPU|DP|REGFILE|Reg6|register|out\(15) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg7|register|out\(15) & (!\CPU|ID|RWmux|b\(2) & 
-- !\CPU|ID|RWmux|b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(15),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(15),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[15]~47_combout\);

-- Location: LABCELL_X63_Y1_N33
\CPU|DP|REGFILE|readMux|m|b[15]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(15) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[15]~47_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[15]~47_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[15]~47_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[15]~46_combout\) # (\CPU|DP|REGFILE|Reg5|register|out\(15))) # (\CPU|DP|REGFILE|readMux|m|b[15]~45_combout\) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[15]~47_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[15]~46_combout\) # (\CPU|DP|REGFILE|readMux|m|b[15]~45_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111011111110111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~45_combout\,
	datab => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(15),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~46_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[15]~47_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(15));

-- Location: FF_X63_Y1_N35
\CPU|DP|a|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|readMux|m|b\(15),
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(15));

-- Location: LABCELL_X63_Y1_N51
\CPU|DP|b|register|out[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|b|register|out[15]~feeder_combout\ = ( \CPU|DP|REGFILE|readMux|m|b\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b\(15),
	combout => \CPU|DP|b|register|out[15]~feeder_combout\);

-- Location: FF_X63_Y1_N52
\CPU|DP|b|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|b|register|out[15]~feeder_combout\,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(15));

-- Location: LABCELL_X64_Y1_N36
\CPU|ID|shift[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|shift[0]~0_combout\ = ( \CPU|InstructionRegister|register|out\(15) & ( \CPU|InstructionRegister|register|out\(14) & ( !\CPU|InstructionRegister|register|out\(13) ) ) ) # ( \CPU|InstructionRegister|register|out\(15) & ( 
-- !\CPU|InstructionRegister|register|out\(14) & ( \CPU|InstructionRegister|register|out\(13) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111111100000000000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	combout => \CPU|ID|shift[0]~0_combout\);

-- Location: LABCELL_X64_Y4_N33
\CPU|FSM|CO|Selector8~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector8~1_combout\ = ( \CPU|InstructionRegister|register|out\(15) & ( (!\CPU|InstructionRegister|register|out\(13) & (!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (!\CPU|FSM|storeFlag|register|out\(0) & 
-- \CPU|FSM|CO|Decoder0~1_combout\))) ) ) # ( !\CPU|InstructionRegister|register|out\(15) & ( (\CPU|InstructionRegister|register|out\(13) & (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & \CPU|FSM|CO|Decoder0~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010001000000000001000100000000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datab => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datac => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datad => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	combout => \CPU|FSM|CO|Selector8~1_combout\);

-- Location: LABCELL_X63_Y1_N6
\CPU|DP|Bin[15]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[15]~19_combout\ = ( \CPU|ID|shift[0]~0_combout\ & ( \CPU|FSM|CO|Selector8~1_combout\ & ( \CPU|InstructionRegister|register|out\(4) ) ) ) # ( !\CPU|ID|shift[0]~0_combout\ & ( \CPU|FSM|CO|Selector8~1_combout\ & ( 
-- \CPU|InstructionRegister|register|out\(4) ) ) ) # ( \CPU|ID|shift[0]~0_combout\ & ( !\CPU|FSM|CO|Selector8~1_combout\ & ( (!\CPU|InstructionRegister|register|out\(4) & ((!\CPU|InstructionRegister|register|out\(3) & ((\CPU|DP|b|register|out\(15)))) # 
-- (\CPU|InstructionRegister|register|out\(3) & (\CPU|DP|b|register|out[14]~DUPLICATE_q\)))) # (\CPU|InstructionRegister|register|out\(4) & (((\CPU|InstructionRegister|register|out\(3) & \CPU|DP|b|register|out\(15))))) ) ) ) # ( !\CPU|ID|shift[0]~0_combout\ 
-- & ( !\CPU|FSM|CO|Selector8~1_combout\ & ( \CPU|DP|b|register|out\(15) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000101010011101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datab => \CPU|DP|b|register|ALT_INV_out[14]~DUPLICATE_q\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(3),
	datad => \CPU|DP|b|register|ALT_INV_out\(15),
	datae => \CPU|ID|ALT_INV_shift[0]~0_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector8~1_combout\,
	combout => \CPU|DP|Bin[15]~19_combout\);

-- Location: LABCELL_X63_Y1_N27
\CPU|DP|ALU|out[15]~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[15]~20_combout\ = ( \CPU|InstructionRegister|register|out\(11) & ( \CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|DP|Bin[15]~19_combout\ ) ) ) # ( \CPU|InstructionRegister|register|out\(11) & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( 
-- !\CPU|DP|Bin[15]~19_combout\ ) ) ) # ( !\CPU|InstructionRegister|register|out\(11) & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|DP|a|register|out\(15) & \CPU|DP|Bin[15]~19_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100010001110011001100110000000000000000001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|a|register|ALT_INV_out\(15),
	datab => \CPU|DP|ALT_INV_Bin[15]~19_combout\,
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|out[15]~20_combout\);

-- Location: LABCELL_X67_Y3_N30
\CPU|DP|ALU|plusMinus|ai|p[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(13) = ( \CPU|DP|Bin[13]~17_combout\ & ( \CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|InstructionRegister|register|out\(11) ) ) ) # ( !\CPU|DP|Bin[13]~17_combout\ & ( \CPU|FSM|CO|Selector7~0_combout\ & ( 
-- \CPU|InstructionRegister|register|out\(11) ) ) ) # ( \CPU|DP|Bin[13]~17_combout\ & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|DP|a|register|out\(13) $ (\CPU|InstructionRegister|register|out\(11)) ) ) ) # ( !\CPU|DP|Bin[13]~17_combout\ & ( 
-- !\CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|DP|a|register|out\(13) $ (!\CPU|InstructionRegister|register|out\(11)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011110000111100110000111100001100001111000011111111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|a|register|ALT_INV_out\(13),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datae => \CPU|DP|ALT_INV_Bin[13]~17_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(13));

-- Location: LABCELL_X64_Y2_N18
\CPU|DP|a|register|out[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|a|register|out[14]~feeder_combout\ = ( \CPU|DP|REGFILE|readMux|m|b\(14) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b\(14),
	combout => \CPU|DP|a|register|out[14]~feeder_combout\);

-- Location: FF_X64_Y2_N19
\CPU|DP|a|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|a|register|out[14]~feeder_combout\,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(14));

-- Location: LABCELL_X63_Y3_N21
\CPU|DP|Ain[14]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[14]~4_combout\ = ( \CPU|DP|a|register|out\(14) & ( !\CPU|FSM|CO|Selector7~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(14),
	combout => \CPU|DP|Ain[14]~4_combout\);

-- Location: LABCELL_X67_Y3_N39
\CPU|DP|ALU|plusMinus|ai|g[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(13) = ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|DP|a|register|out\(13) & (!\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[13]~17_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100001010000001010000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|a|register|ALT_INV_out\(13),
	datad => \CPU|DP|ALT_INV_Bin[13]~17_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(13));

-- Location: FF_X64_Y2_N8
\CPU|DP|b|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|b|register|out[14]~feeder_combout\,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(14));

-- Location: LABCELL_X64_Y2_N39
\CPU|DP|Bin[14]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[14]~18_combout\ = ( \CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(15) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|InstructionRegister|register|out\(4) ) ) ) # ( 
-- \CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out[13]~DUPLICATE_q\ ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(14) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000000001111111100110011001100110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(15),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datac => \CPU|DP|b|register|ALT_INV_out\(14),
	datad => \CPU|DP|b|register|ALT_INV_out[13]~DUPLICATE_q\,
	datae => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[14]~18_combout\);

-- Location: MLABCELL_X65_Y4_N15
\CPU|DP|ALU|plusMinus|comb~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|comb~1_combout\ = ( \CPU|DP|Bin[14]~18_combout\ & ( !\CPU|InstructionRegister|register|out\(11) ) ) # ( !\CPU|DP|Bin[14]~18_combout\ & ( \CPU|InstructionRegister|register|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010110101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[14]~18_combout\,
	combout => \CPU|DP|ALU|plusMinus|comb~1_combout\);

-- Location: LABCELL_X63_Y1_N12
\CPU|DP|ALU|plusMinus|comb~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|comb~2_combout\ = ( !\CPU|InstructionRegister|register|out\(11) & ( \CPU|DP|Bin[15]~19_combout\ ) ) # ( \CPU|InstructionRegister|register|out\(11) & ( !\CPU|DP|Bin[15]~19_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111111111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[15]~19_combout\,
	combout => \CPU|DP|ALU|plusMinus|comb~2_combout\);

-- Location: LABCELL_X66_Y3_N30
\CPU|DP|ALU|plusMinus|as|p[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|as|p\(0) = ( \CPU|DP|ALU|plusMinus|comb~2_combout\ & ( (!\CPU|DP|a|register|out\(15)) # (\CPU|FSM|CO|Selector7~0_combout\) ) ) # ( !\CPU|DP|ALU|plusMinus|comb~2_combout\ & ( (!\CPU|FSM|CO|Selector7~0_combout\ & 
-- \CPU|DP|a|register|out\(15)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000011111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(15),
	dataf => \CPU|DP|ALU|plusMinus|ALT_INV_comb~2_combout\,
	combout => \CPU|DP|ALU|plusMinus|as|p\(0));

-- Location: LABCELL_X66_Y3_N18
\CPU|DP|ALU|out[15]~32\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[15]~32_combout\ = ( \CPU|DP|ALU|plusMinus|as|p\(0) & ( (!\CPU|DP|Ain[14]~4_combout\ & ((!\CPU|DP|ALU|plusMinus|ai|g\(13)) # (!\CPU|DP|ALU|plusMinus|comb~1_combout\))) # (\CPU|DP|Ain[14]~4_combout\ & (!\CPU|DP|ALU|plusMinus|ai|g\(13) & 
-- !\CPU|DP|ALU|plusMinus|comb~1_combout\)) ) ) # ( !\CPU|DP|ALU|plusMinus|as|p\(0) & ( (!\CPU|DP|Ain[14]~4_combout\ & (\CPU|DP|ALU|plusMinus|ai|g\(13) & \CPU|DP|ALU|plusMinus|comb~1_combout\)) # (\CPU|DP|Ain[14]~4_combout\ & 
-- ((\CPU|DP|ALU|plusMinus|comb~1_combout\) # (\CPU|DP|ALU|plusMinus|ai|g\(13)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010101011111000001010101111111111010101000001111101010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[14]~4_combout\,
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(13),
	datad => \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\,
	dataf => \CPU|DP|ALU|plusMinus|as|ALT_INV_p\(0),
	combout => \CPU|DP|ALU|out[15]~32_combout\);

-- Location: LABCELL_X66_Y3_N0
\CPU|DP|ALU|plusMinus|ai|c[13]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|c\(13) = ( \CPU|DP|ALU|plusMinus|comb~0_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( (!\CPU|DP|Ain[12]~3_combout\ & (!\CPU|DP|ALU|plusMinus|ai|p\(11) & !\CPU|DP|ALU|plusMinus|ai|g\(11))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|comb~0_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( (!\CPU|DP|Ain[12]~3_combout\) # ((!\CPU|DP|ALU|plusMinus|ai|p\(11) & !\CPU|DP|ALU|plusMinus|ai|g\(11))) ) ) ) # ( \CPU|DP|ALU|plusMinus|comb~0_combout\ & ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( (!\CPU|DP|Ain[12]~3_combout\ & (!\CPU|DP|ALU|plusMinus|ai|g\(11) & ((!\CPU|DP|ALU|plusMinus|ai|p\(11)) # (!\CPU|DP|ALU|plusMinus|ai|g\(10))))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|comb~0_combout\ & ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( (!\CPU|DP|Ain[12]~3_combout\) # ((!\CPU|DP|ALU|plusMinus|ai|g\(11) & ((!\CPU|DP|ALU|plusMinus|ai|p\(11)) # (!\CPU|DP|ALU|plusMinus|ai|g\(10))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111101011101010101000001000000011101010111010101000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(11),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(10),
	datae => \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|c\(13));

-- Location: LABCELL_X66_Y3_N21
\CPU|DP|ALU|out[15]~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[15]~33_combout\ = ( \CPU|DP|ALU|out[15]~32_combout\ & ( (!\CPU|DP|ALU|plusMinus|as|p\(0)) # ((!\CPU|DP|Ain[14]~4_combout\ & !\CPU|DP|ALU|plusMinus|comb~1_combout\)) ) ) # ( !\CPU|DP|ALU|out[15]~32_combout\ & ( 
-- (!\CPU|DP|ALU|plusMinus|as|p\(0) & ((\CPU|DP|ALU|plusMinus|comb~1_combout\) # (\CPU|DP|Ain[14]~4_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000011110000010100001111000011111010111100001111101011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[14]~4_combout\,
	datac => \CPU|DP|ALU|plusMinus|as|ALT_INV_p\(0),
	datad => \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_out[15]~32_combout\,
	combout => \CPU|DP|ALU|out[15]~33_combout\);

-- Location: LABCELL_X66_Y3_N45
\CPU|DP|ALU|out[15]~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[15]~21_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(13) & ( \CPU|DP|ALU|out[15]~33_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & ((\CPU|DP|ALU|out[15]~32_combout\))) # (\CPU|InstructionRegister|register|out\(12) & 
-- (\CPU|DP|ALU|out[15]~20_combout\)) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(13) & ( \CPU|DP|ALU|out[15]~33_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (((\CPU|DP|ALU|out[15]~32_combout\) # (\CPU|DP|ALU|plusMinus|ai|p\(13))))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[15]~20_combout\)) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|c\(13) & ( !\CPU|DP|ALU|out[15]~33_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & ((\CPU|DP|ALU|out[15]~32_combout\))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[15]~20_combout\)) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(13) & ( !\CPU|DP|ALU|out[15]~33_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (((!\CPU|DP|ALU|plusMinus|ai|p\(13) & 
-- \CPU|DP|ALU|out[15]~32_combout\)))) # (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[15]~20_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000110110001000100011011101100011011101110110001000110111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|DP|ALU|ALT_INV_out[15]~20_combout\,
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(13),
	datad => \CPU|DP|ALU|ALT_INV_out[15]~32_combout\,
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(13),
	dataf => \CPU|DP|ALU|ALT_INV_out[15]~33_combout\,
	combout => \CPU|DP|ALU|out[15]~21_combout\);

-- Location: FF_X66_Y3_N5
\CPU|DP|c|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|ALU|out[15]~21_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(15));

-- Location: M10K_X58_Y4_N0
\MEM|mem_rtl_0|auto_generated|ram_block1a0\ : cyclonev_ram_block
-- pragma translate_off
GENERIC MAP (
	mem_init4 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init3 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init2 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init1 => "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
	mem_init0 => "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000CCCC00000000FF00000040E0000000668000000066BF000000C004000000A485000000D501000000A485000000B8A5000000A2A3000000A08100000086BF0000008680000000E00000000084000000005F02000000D314000000D209000000D105000000D0010000008460000000D300000000D41900000066C0000000D618",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	init_file => "db/lab8_top.ram0_RAM_96749f86.hdl.mif",
	init_file_layout => "port_a",
	logical_ram_name => "RAM:MEM|altsyncram:mem_rtl_0|altsyncram_dcr1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "old",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 8,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 40,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 255,
	port_a_logical_ram_depth => 256,
	port_a_logical_ram_width => 16,
	port_a_read_during_write_mode => "new_data_no_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock0",
	port_b_address_width => 8,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 40,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 255,
	port_b_logical_ram_depth => 256,
	port_b_logical_ram_width => 16,
	port_b_read_during_write_mode => "new_data_no_nbe_read",
	port_b_read_enable_clock => "clock0",
	ram_block_type => "M20K")
-- pragma translate_on
PORT MAP (
	portawe => \write~combout\,
	portbre => VCC,
	clk0 => \CLOCK_50~inputCLKENA0_outclk\,
	portadatain => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \MEM|mem_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LABCELL_X67_Y4_N36
\read_data[1]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[1]~11_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a1\ & ( ((\rtl~27_combout\ & (\rtl~26_combout\ & !\CPU|mem_addr[8]~0_combout\))) # (\SW[1]~input_o\) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a1\ & ( (\SW[1]~input_o\ & 
-- ((!\rtl~27_combout\) # ((!\rtl~26_combout\) # (\CPU|mem_addr[8]~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010001010101010101000101010101010111010101010101011101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[1]~input_o\,
	datab => \ALT_INV_rtl~27_combout\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\,
	combout => \read_data[1]~11_combout\);

-- Location: FF_X67_Y4_N38
\CPU|InstructionRegister|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[1]~11_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(1));

-- Location: LABCELL_X63_Y2_N30
\CPU|Add0~38\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~38_cout\ = CARRY(( VCC ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	cin => GND,
	cout => \CPU|Add0~38_cout\);

-- Location: LABCELL_X63_Y2_N33
\CPU|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~5_sumout\ = SUM(( \CPU|program_counter|register|out\(0) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(0) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~38_cout\ ))
-- \CPU|Add0~6\ = CARRY(( \CPU|program_counter|register|out\(0) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(0) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~38_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111101110111000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(0),
	datac => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(0),
	dataf => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	cin => \CPU|Add0~38_cout\,
	sumout => \CPU|Add0~5_sumout\,
	cout => \CPU|Add0~6\);

-- Location: LABCELL_X63_Y2_N36
\CPU|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~9_sumout\ = SUM(( \CPU|program_counter|register|out\(1) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(1) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~6\ ))
-- \CPU|Add0~10\ = CARRY(( \CPU|program_counter|register|out\(1) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(1) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(1),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(1),
	cin => \CPU|Add0~6\,
	sumout => \CPU|Add0~9_sumout\,
	cout => \CPU|Add0~10\);

-- Location: LABCELL_X60_Y4_N30
\CPU|DP|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~5_sumout\ = SUM(( \CPU|program_counter|register|out\(0) ) + ( \CPU|program_counter|register|out\(1) ) + ( !VCC ))
-- \CPU|DP|Add0~6\ = CARRY(( \CPU|program_counter|register|out\(0) ) + ( \CPU|program_counter|register|out\(1) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|program_counter|register|ALT_INV_out\(1),
	datad => \CPU|program_counter|register|ALT_INV_out\(0),
	cin => GND,
	sumout => \CPU|DP|Add0~5_sumout\,
	cout => \CPU|DP|Add0~6\);

-- Location: LABCELL_X60_Y4_N24
\CPU|DP|WBmux|m|b[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[1]~2_combout\ = ( \CPU|DP|c|register|out\(1) & ( \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|InstructionRegister|register|out\(1) & (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ((!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\) # 
-- (!\CPU|DP|Add0~5_sumout\)))) ) ) ) # ( !\CPU|DP|c|register|out\(1) & ( \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|InstructionRegister|register|out\(1) & ((!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\) # (!\CPU|DP|Add0~5_sumout\))) ) ) ) # ( 
-- \CPU|DP|c|register|out\(1) & ( !\CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ((!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\) # (!\CPU|DP|Add0~5_sumout\))) ) ) ) # ( !\CPU|DP|c|register|out\(1) & ( 
-- !\CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\) # (!\CPU|DP|Add0~5_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110111011101110111011100000000011100000111000001110000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	datab => \CPU|DP|ALT_INV_Add0~5_sumout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(1),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datae => \CPU|DP|c|register|ALT_INV_out\(1),
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	combout => \CPU|DP|WBmux|m|b[1]~2_combout\);

-- Location: LABCELL_X60_Y3_N30
\CPU|DP|WBmux|m|b[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(1) = ( \CPU|DP|WBmux|m|b[1]~2_combout\ & ( (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ((!\e~combout\ & ((\SW[1]~input_o\))) # (\e~combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a1\)))) ) ) # ( !\CPU|DP|WBmux|m|b[1]~2_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000001001000110000000100100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_e~combout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\,
	datad => \ALT_INV_SW[1]~input_o\,
	dataf => \CPU|DP|WBmux|m|ALT_INV_b[1]~2_combout\,
	combout => \CPU|DP|WBmux|m|b\(1));

-- Location: FF_X59_Y3_N43
\CPU|DP|REGFILE|Reg5|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(1));

-- Location: FF_X59_Y3_N28
\CPU|DP|REGFILE|Reg2|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(1));

-- Location: MLABCELL_X59_Y5_N12
\CPU|DP|REGFILE|Reg4|register|out[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg4|register|out[1]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(1) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|Reg4|register|out[1]~feeder_combout\);

-- Location: FF_X59_Y5_N13
\CPU|DP|REGFILE|Reg4|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg4|register|out[1]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(1));

-- Location: FF_X63_Y3_N38
\CPU|DP|REGFILE|Reg0|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(1));

-- Location: LABCELL_X63_Y3_N36
\CPU|DP|REGFILE|readMux|m|b[1]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[1]~6_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(1) & ( \CPU|ID|RWmux|b\(2) & ( (\CPU|ID|RWmux|b\(0) & ((\CPU|ID|RWmux|b\(1)) # (\CPU|DP|REGFILE|Reg2|register|out\(1)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(1) & ( 
-- \CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg2|register|out\(1) & (!\CPU|ID|RWmux|b\(1) & \CPU|ID|RWmux|b\(0))) ) ) ) # ( \CPU|DP|REGFILE|Reg0|register|out\(1) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg4|register|out\(1) & (\CPU|ID|RWmux|b\(1) & 
-- \CPU|ID|RWmux|b\(0))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(1) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg4|register|out\(1) & (\CPU|ID|RWmux|b\(1) & \CPU|ID|RWmux|b\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001100000000010100000000000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(1),
	datab => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(1),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(0),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[1]~6_combout\);

-- Location: FF_X61_Y5_N29
\CPU|DP|REGFILE|Reg3|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(1));

-- Location: FF_X63_Y3_N20
\CPU|DP|REGFILE|Reg1|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(1));

-- Location: LABCELL_X63_Y3_N18
\CPU|DP|REGFILE|readMux|m|b[1]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[1]~7_combout\ = ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg3|register|out\(1))) # (\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg1|register|out\(1)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000111000000100000011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(1),
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(1),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[1]~7_combout\);

-- Location: FF_X60_Y3_N40
\CPU|DP|REGFILE|Reg6|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(1),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(1));

-- Location: FF_X60_Y3_N32
\CPU|DP|REGFILE|Reg7|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(1),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(1));

-- Location: LABCELL_X61_Y3_N3
\CPU|DP|REGFILE|readMux|m|b[1]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[1]~8_combout\ = ( !\CPU|ID|RWmux|b\(2) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg6|register|out\(1) & !\CPU|ID|RWmux|b\(1)) ) ) ) # ( !\CPU|ID|RWmux|b\(2) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(1) & !\CPU|ID|RWmux|b\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000000000000000000001010000010100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(1),
	datab => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(1),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[1]~8_combout\);

-- Location: LABCELL_X62_Y4_N33
\CPU|DP|REGFILE|readMux|m|b[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(1) = ( \CPU|DP|REGFILE|readMux|m|b[1]~7_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[1]~8_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[1]~7_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[1]~8_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|m|b[1]~7_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[1]~8_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[1]~7_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[1]~8_combout\ & ( ((\CPU|DP|REGFILE|Reg5|register|out\(1) & 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[1]~6_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(1),
	datac => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~6_combout\,
	datae => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~7_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[1]~8_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(1));

-- Location: FF_X63_Y2_N37
\CPU|program_counter|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~9_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(1),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(1));

-- Location: LABCELL_X60_Y4_N33
\CPU|DP|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~9_sumout\ = SUM(( \CPU|program_counter|register|out\(2) ) + ( GND ) + ( \CPU|DP|Add0~6\ ))
-- \CPU|DP|Add0~10\ = CARRY(( \CPU|program_counter|register|out\(2) ) + ( GND ) + ( \CPU|DP|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|program_counter|register|ALT_INV_out\(2),
	cin => \CPU|DP|Add0~6\,
	sumout => \CPU|DP|Add0~9_sumout\,
	cout => \CPU|DP|Add0~10\);

-- Location: LABCELL_X60_Y4_N36
\CPU|DP|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~13_sumout\ = SUM(( \CPU|program_counter|register|out\(3) ) + ( GND ) + ( \CPU|DP|Add0~10\ ))
-- \CPU|DP|Add0~14\ = CARRY(( \CPU|program_counter|register|out\(3) ) + ( GND ) + ( \CPU|DP|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|program_counter|register|ALT_INV_out\(3),
	cin => \CPU|DP|Add0~10\,
	sumout => \CPU|DP|Add0~13_sumout\,
	cout => \CPU|DP|Add0~14\);

-- Location: LABCELL_X60_Y4_N39
\CPU|DP|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Add0~17_sumout\ = SUM(( \CPU|program_counter|register|out\(4) ) + ( GND ) + ( \CPU|DP|Add0~14\ ))
-- \CPU|DP|Add0~18\ = CARRY(( \CPU|program_counter|register|out\(4) ) + ( GND ) + ( \CPU|DP|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|program_counter|register|ALT_INV_out\(4),
	cin => \CPU|DP|Add0~14\,
	sumout => \CPU|DP|Add0~17_sumout\,
	cout => \CPU|DP|Add0~18\);

-- Location: LABCELL_X60_Y4_N12
\CPU|DP|WBmux|m|b[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[4]~5_combout\ = ( \CPU|InstructionRegister|register|out\(4) & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(4) & (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ((!\CPU|DP|Add0~17_sumout\) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\)))) ) ) ) # ( !\CPU|InstructionRegister|register|out\(4) & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(4) & ((!\CPU|DP|Add0~17_sumout\) # (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\))) ) ) ) # 
-- ( \CPU|InstructionRegister|register|out\(4) & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ((!\CPU|DP|Add0~17_sumout\) # (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\))) ) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(4) & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|Add0~17_sumout\) # (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111100111111000000000010101000101010001010100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(4),
	datab => \CPU|DP|ALT_INV_Add0~17_sumout\,
	datac => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	combout => \CPU|DP|WBmux|m|b[4]~5_combout\);

-- Location: LABCELL_X61_Y2_N21
\CPU|DP|WBmux|m|b[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(4) = ( \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ( (!\CPU|DP|WBmux|m|b[4]~5_combout\) # ((!\e~combout\ & (\SW[4]~input_o\)) # (\e~combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a4\)))) ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ 
-- & ( !\CPU|DP|WBmux|m|b[4]~5_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111010100111111111101010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[4]~input_o\,
	datab => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\,
	datac => \ALT_INV_e~combout\,
	datad => \CPU|DP|WBmux|m|ALT_INV_b[4]~5_combout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	combout => \CPU|DP|WBmux|m|b\(4));

-- Location: FF_X62_Y2_N35
\CPU|DP|REGFILE|Reg4|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(4));

-- Location: FF_X62_Y2_N28
\CPU|DP|REGFILE|Reg0|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(4));

-- Location: FF_X60_Y2_N49
\CPU|DP|REGFILE|Reg2|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(4));

-- Location: LABCELL_X62_Y3_N6
\CPU|DP|REGFILE|readMux|m|b[4]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[4]~15_combout\ = ( \CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (\CPU|DP|REGFILE|Reg4|register|out\(4))) # (\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg0|register|out\(4)))) ) ) ) # ( 
-- !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg2|register|out\(4) & \CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110101010100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(4),
	datab => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(4),
	datac => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(4),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[4]~15_combout\);

-- Location: LABCELL_X62_Y1_N0
\CPU|DP|REGFILE|Reg3|register|out[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[4]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(4) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(4),
	combout => \CPU|DP|REGFILE|Reg3|register|out[4]~feeder_combout\);

-- Location: FF_X62_Y1_N1
\CPU|DP|REGFILE|Reg3|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[4]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(4));

-- Location: FF_X63_Y3_N47
\CPU|DP|REGFILE|Reg1|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(4));

-- Location: LABCELL_X62_Y3_N24
\CPU|DP|REGFILE|readMux|m|b[4]~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[4]~16_combout\ = ( \CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg1|register|out\(4) & \CPU|ID|RWmux|b\(2)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg3|register|out\(4) & \CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(4),
	datac => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(4),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[4]~16_combout\);

-- Location: FF_X62_Y3_N41
\CPU|DP|REGFILE|Reg5|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(4));

-- Location: FF_X61_Y2_N22
\CPU|DP|REGFILE|Reg7|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(4),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(4));

-- Location: FF_X61_Y3_N53
\CPU|DP|REGFILE|Reg6|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(4),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(4));

-- Location: LABCELL_X61_Y3_N51
\CPU|DP|REGFILE|readMux|m|b[4]~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[4]~17_combout\ = ( \CPU|DP|REGFILE|Reg6|register|out\(4) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2)) ) ) ) # ( \CPU|DP|REGFILE|Reg6|register|out\(4) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(4) & (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2))) ) ) ) # ( !\CPU|DP|REGFILE|Reg6|register|out\(4) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg7|register|out\(4) & (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(4),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(4),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[4]~17_combout\);

-- Location: LABCELL_X62_Y3_N0
\CPU|DP|REGFILE|readMux|m|b[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(4) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[4]~17_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[4]~17_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[4]~17_combout\ & ( ((\CPU|DP|REGFILE|Reg5|register|out\(4)) # (\CPU|DP|REGFILE|readMux|m|b[4]~16_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[4]~15_combout\) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[4]~17_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[4]~16_combout\) # (\CPU|DP|REGFILE|readMux|m|b[4]~15_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111001111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~15_combout\,
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~16_combout\,
	datad => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(4),
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[4]~17_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(4));

-- Location: FF_X66_Y4_N13
\CPU|DP|b|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(4),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(4));

-- Location: LABCELL_X66_Y4_N15
\CPU|DP|Bin[3]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[3]~7_combout\ = ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(4) & ( (\CPU|DP|Bin[1]~3_combout\) # (\CPU|InstructionRegister|register|out\(3)) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(4) & ( 
-- (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(3)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out\(2))) ) ) ) # ( \CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(4) & ( (\CPU|InstructionRegister|register|out\(3) & 
-- !\CPU|DP|Bin[1]~3_combout\) ) ) ) # ( !\CPU|DP|Bin[1]~4_combout\ & ( !\CPU|DP|b|register|out\(4) & ( (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(3)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010111110101001100000011000000000101111101010011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(2),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(3),
	datac => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datad => \CPU|DP|b|register|ALT_INV_out\(3),
	datae => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	dataf => \CPU|DP|b|register|ALT_INV_out\(4),
	combout => \CPU|DP|Bin[3]~7_combout\);

-- Location: LABCELL_X66_Y4_N27
\CPU|DP|ALU|plusMinus|ai|g[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(3) = ( \CPU|DP|Bin[3]~7_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(3))) ) ) # ( !\CPU|DP|Bin[3]~7_combout\ & ( 
-- (\CPU|InstructionRegister|register|out\(11) & (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000101000000000000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(3),
	dataf => \CPU|DP|ALT_INV_Bin[3]~7_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(3));

-- Location: FF_X62_Y3_N28
\CPU|DP|a|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(4),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(4));

-- Location: LABCELL_X66_Y4_N42
\CPU|DP|ALU|plusMinus|ai|g[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(4) = ( \CPU|DP|Bin[4]~8_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(4))) ) ) # ( !\CPU|DP|Bin[4]~8_combout\ & ( 
-- (\CPU|InstructionRegister|register|out\(11) & (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(4))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010000000000000101000000000000101000000000000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(4),
	dataf => \CPU|DP|ALT_INV_Bin[4]~8_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(4));

-- Location: LABCELL_X66_Y4_N45
\CPU|DP|ALU|plusMinus|ai|p[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(5) = ( \CPU|DP|Bin[5]~9_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(5)))) ) ) # ( !\CPU|DP|Bin[5]~9_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|a|register|out\(5)) # (\CPU|FSM|CO|Selector7~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110100101010101011010010110101010010110101010101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(5),
	dataf => \CPU|DP|ALT_INV_Bin[5]~9_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(5));

-- Location: LABCELL_X66_Y4_N18
\CPU|DP|ALU|plusMinus|ai|p[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(3) = ( \CPU|DP|Bin[3]~7_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(3) & !\CPU|FSM|CO|Selector7~0_combout\))) ) ) # ( !\CPU|DP|Bin[3]~7_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|a|register|out\(3)) # (\CPU|FSM|CO|Selector7~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001010101010110100101010110100101101010101010010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|a|register|ALT_INV_out\(3),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[3]~7_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(3));

-- Location: LABCELL_X66_Y4_N33
\CPU|DP|ALU|plusMinus|ai|p[4]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(4) = ( \CPU|DP|Bin[4]~8_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(4) & !\CPU|FSM|CO|Selector7~0_combout\))) ) ) # ( !\CPU|DP|Bin[4]~8_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|a|register|out\(4)) # (\CPU|FSM|CO|Selector7~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001010101010110100101010110100101101010101010010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|a|register|ALT_INV_out\(4),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[4]~8_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(4));

-- Location: MLABCELL_X65_Y4_N42
\CPU|DP|ALU|plusMinus|ai|comb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(3) & ( \CPU|DP|ALU|plusMinus|ai|p\(4) & ( (\CPU|DP|ALU|plusMinus|ai|p\(5) & (((\CPU|DP|ALU|plusMinus|ai|p\(3)) # (\CPU|DP|ALU|plusMinus|ai|g\(4))) # 
-- (\CPU|DP|ALU|plusMinus|ai|g\(3)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(3) & ( \CPU|DP|ALU|plusMinus|ai|p\(4) & ( (\CPU|DP|ALU|plusMinus|ai|p\(5) & ((\CPU|DP|ALU|plusMinus|ai|g\(4)) # (\CPU|DP|ALU|plusMinus|ai|g\(3)))) ) ) ) # ( 
-- \CPU|DP|ALU|plusMinus|ai|c\(3) & ( !\CPU|DP|ALU|plusMinus|ai|p\(4) & ( (\CPU|DP|ALU|plusMinus|ai|g\(4) & \CPU|DP|ALU|plusMinus|ai|p\(5)) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(3) & ( !\CPU|DP|ALU|plusMinus|ai|p\(4) & ( (\CPU|DP|ALU|plusMinus|ai|g\(4) & 
-- \CPU|DP|ALU|plusMinus|ai|p\(5)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000111000001110000011100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(3),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(4),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(5),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(3),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(3),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(4),
	combout => \CPU|DP|ALU|plusMinus|ai|comb~0_combout\);

-- Location: MLABCELL_X65_Y4_N0
\CPU|DP|ALU|plusMinus|ai|c[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|c\(8) = ( \CPU|DP|ALU|plusMinus|ai|p\(7) & ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (!\CPU|DP|ALU|plusMinus|ai|g\(6) & (!\CPU|DP|ALU|plusMinus|ai|p\(6) & !\CPU|DP|ALU|plusMinus|ai|g\(7))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|ai|p\(7) & ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(7) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|p\(7) & ( !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (!\CPU|DP|ALU|plusMinus|ai|g\(6) & 
-- (!\CPU|DP|ALU|plusMinus|ai|g\(7) & ((!\CPU|DP|ALU|plusMinus|ai|p\(6)) # (!\CPU|DP|ALU|plusMinus|ai|g\(5))))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|p\(7) & ( !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(7) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000101010000000000011111111000000001000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(6),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(6),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(5),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(7),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(7),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~0_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|c\(8));

-- Location: MLABCELL_X65_Y2_N24
\CPU|DP|ALU|out[10]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[10]~13_combout\ = ( \CPU|DP|ALU|plusMinus|ai|p\(8) & ( \CPU|DP|ALU|plusMinus|ai|p\(9) & ( !\CPU|DP|ALU|out[10]~23_combout\ $ (((!\CPU|DP|ALU|out[10]~24_combout\) # ((\CPU|DP|ALU|plusMinus|ai|c\(8) & !\CPU|DP|ALU|plusMinus|ai|g\(8))))) ) ) 
-- ) # ( !\CPU|DP|ALU|plusMinus|ai|p\(8) & ( \CPU|DP|ALU|plusMinus|ai|p\(9) & ( !\CPU|DP|ALU|out[10]~23_combout\ $ (((!\CPU|DP|ALU|out[10]~24_combout\) # (!\CPU|DP|ALU|plusMinus|ai|g\(8)))) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|p\(8) & ( 
-- !\CPU|DP|ALU|plusMinus|ai|p\(9) & ( \CPU|DP|ALU|out[10]~23_combout\ ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|p\(8) & ( !\CPU|DP|ALU|plusMinus|ai|p\(9) & ( \CPU|DP|ALU|out[10]~23_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011011001100110001101100110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[10]~24_combout\,
	datab => \CPU|DP|ALU|ALT_INV_out[10]~23_combout\,
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(8),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(8),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(8),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(9),
	combout => \CPU|DP|ALU|out[10]~13_combout\);

-- Location: FF_X65_Y2_N26
\CPU|DP|c|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[10]~13_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(10));

-- Location: MLABCELL_X59_Y4_N18
\read_data[9]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[9]~10_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ & ( (\rtl~26_combout\ & (\rtl~27_combout\ & !\CPU|mem_addr[8]~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101000000000000010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datac => \ALT_INV_rtl~27_combout\,
	datad => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\,
	combout => \read_data[9]~10_combout\);

-- Location: LABCELL_X61_Y3_N9
\CPU|DP|WBmux|m|b[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(9) = ( \read_data[9]~10_combout\ & ( (((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(9))) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) ) # ( !\read_data[9]~10_combout\ & ( 
-- ((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(9))) # (\CPU|DP|WBmux|m|comb~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011101010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(9),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	dataf => \ALT_INV_read_data[9]~10_combout\,
	combout => \CPU|DP|WBmux|m|b\(9));

-- Location: FF_X61_Y2_N44
\CPU|DP|REGFILE|Reg5|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(9));

-- Location: LABCELL_X62_Y1_N30
\CPU|DP|REGFILE|Reg3|register|out[9]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[9]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(9) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(9),
	combout => \CPU|DP|REGFILE|Reg3|register|out[9]~feeder_combout\);

-- Location: FF_X62_Y1_N31
\CPU|DP|REGFILE|Reg3|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[9]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(9));

-- Location: FF_X62_Y2_N38
\CPU|DP|REGFILE|Reg1|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(9));

-- Location: LABCELL_X62_Y2_N36
\CPU|DP|REGFILE|readMux|m|b[9]~28\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[9]~28_combout\ = ( \CPU|ID|RWmux|b\(2) & ( (!\CPU|ID|RWmux|b\(0) & ((!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg3|register|out\(9))) # (\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg1|register|out\(9)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000010011000000100001001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(1),
	datab => \CPU|ID|RWmux|ALT_INV_b\(0),
	datac => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(9),
	datad => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(9),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[9]~28_combout\);

-- Location: FF_X62_Y2_N13
\CPU|DP|REGFILE|Reg0|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(9));

-- Location: FF_X62_Y2_N53
\CPU|DP|REGFILE|Reg4|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(9));

-- Location: LABCELL_X60_Y2_N0
\CPU|DP|REGFILE|Reg2|register|out[9]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg2|register|out[9]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(9) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(9),
	combout => \CPU|DP|REGFILE|Reg2|register|out[9]~feeder_combout\);

-- Location: FF_X60_Y2_N2
\CPU|DP|REGFILE|Reg2|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg2|register|out[9]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(9));

-- Location: LABCELL_X64_Y2_N33
\CPU|DP|REGFILE|readMux|m|b[9]~27\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[9]~27_combout\ = ( \CPU|DP|REGFILE|Reg2|register|out\(9) & ( \CPU|ID|RWmux|b\(1) & ( (\CPU|ID|RWmux|b\(0) & ((!\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg4|register|out\(9)))) # (\CPU|ID|RWmux|b\(2) & 
-- (\CPU|DP|REGFILE|Reg0|register|out\(9))))) ) ) ) # ( !\CPU|DP|REGFILE|Reg2|register|out\(9) & ( \CPU|ID|RWmux|b\(1) & ( (\CPU|ID|RWmux|b\(0) & ((!\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg4|register|out\(9)))) # (\CPU|ID|RWmux|b\(2) & 
-- (\CPU|DP|REGFILE|Reg0|register|out\(9))))) ) ) ) # ( \CPU|DP|REGFILE|Reg2|register|out\(9) & ( !\CPU|ID|RWmux|b\(1) & ( (\CPU|ID|RWmux|b\(2) & \CPU|ID|RWmux|b\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000011001100000000000111010000000000011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(9),
	datab => \CPU|ID|RWmux|ALT_INV_b\(2),
	datac => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(9),
	datad => \CPU|ID|RWmux|ALT_INV_b\(0),
	datae => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(9),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|readMux|m|b[9]~27_combout\);

-- Location: FF_X61_Y3_N49
\CPU|DP|REGFILE|Reg6|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(9),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(9));

-- Location: FF_X61_Y3_N11
\CPU|DP|REGFILE|Reg7|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(9),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(9));

-- Location: LABCELL_X66_Y2_N51
\CPU|DP|REGFILE|readMux|m|b[9]~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[9]~29_combout\ = ( \CPU|ID|RWmux|b\(0) & ( !\CPU|ID|RWmux|b\(1) & ( (\CPU|DP|REGFILE|Reg6|register|out\(9) & !\CPU|ID|RWmux|b\(2)) ) ) ) # ( !\CPU|ID|RWmux|b\(0) & ( !\CPU|ID|RWmux|b\(1) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(9) & !\CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000000010101010000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(9),
	datac => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(9),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|readMux|m|b[9]~29_combout\);

-- Location: MLABCELL_X65_Y2_N6
\CPU|DP|REGFILE|readMux|m|b[9]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(9) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[9]~29_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[9]~29_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[9]~29_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[9]~27_combout\) # (\CPU|DP|REGFILE|readMux|m|b[9]~28_combout\)) # (\CPU|DP|REGFILE|Reg5|register|out\(9)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[9]~29_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[9]~27_combout\) # (\CPU|DP|REGFILE|readMux|m|b[9]~28_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111010111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(9),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~28_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~27_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[9]~29_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(9));

-- Location: FF_X65_Y2_N43
\CPU|DP|b|register|out[9]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(9),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out[9]~DUPLICATE_q\);

-- Location: LABCELL_X64_Y2_N54
\CPU|DP|Bin[8]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[8]~12_combout\ = ( \CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out[9]~DUPLICATE_q\ ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|InstructionRegister|register|out\(4) ) ) ) # ( 
-- \CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(7) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(8) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010101010101010100110011001100110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(7),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datac => \CPU|DP|b|register|ALT_INV_out\(8),
	datad => \CPU|DP|b|register|ALT_INV_out[9]~DUPLICATE_q\,
	datae => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[8]~12_combout\);

-- Location: MLABCELL_X65_Y2_N3
\CPU|DP|ALU|plusMinus|ai|p[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(8) = ( \CPU|DP|a|register|out\(8) & ( \CPU|DP|Bin[8]~12_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|FSM|CO|Selector7~0_combout\) ) ) ) # ( !\CPU|DP|a|register|out\(8) & ( \CPU|DP|Bin[8]~12_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) ) ) ) # ( \CPU|DP|a|register|out\(8) & ( !\CPU|DP|Bin[8]~12_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (\CPU|FSM|CO|Selector7~0_combout\) ) ) ) # ( !\CPU|DP|a|register|out\(8) & ( 
-- !\CPU|DP|Bin[8]~12_combout\ & ( \CPU|InstructionRegister|register|out\(11) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011110011000011001111001100110011000011001111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datae => \CPU|DP|a|register|ALT_INV_out\(8),
	dataf => \CPU|DP|ALT_INV_Bin[8]~12_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(8));

-- Location: MLABCELL_X65_Y2_N51
\CPU|DP|ALU|out[9]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[9]~11_combout\ = ( \CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|InstructionRegister|register|out\(11) & !\CPU|DP|Bin[9]~13_combout\) ) ) # ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & 
-- (\CPU|DP|a|register|out\(9) & \CPU|DP|Bin[9]~13_combout\)) # (\CPU|InstructionRegister|register|out\(11) & ((!\CPU|DP|Bin[9]~13_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100001100001100110000110000110011000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|a|register|ALT_INV_out\(9),
	datad => \CPU|DP|ALT_INV_Bin[9]~13_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|out[9]~11_combout\);

-- Location: MLABCELL_X65_Y4_N48
\CPU|DP|ALU|out[9]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[9]~12_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(8) & ( \CPU|DP|ALU|plusMinus|ai|g\(8) & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|p\(9))) # (\CPU|InstructionRegister|register|out\(12) & 
-- ((\CPU|DP|ALU|out[9]~11_combout\))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(8) & ( \CPU|DP|ALU|plusMinus|ai|g\(8) & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|p\(9))) # (\CPU|InstructionRegister|register|out\(12) & 
-- ((\CPU|DP|ALU|out[9]~11_combout\))) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|c\(8) & ( !\CPU|DP|ALU|plusMinus|ai|g\(8) & ( (!\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|plusMinus|ai|p\(9))) # (\CPU|InstructionRegister|register|out\(12) & 
-- ((\CPU|DP|ALU|out[9]~11_combout\))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(8) & ( !\CPU|DP|ALU|plusMinus|ai|g\(8) & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|p\(8) $ ((!\CPU|DP|ALU|plusMinus|ai|p\(9))))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (((\CPU|DP|ALU|out[9]~11_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110000001101111001100000011111111000000110011111100000011001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(8),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(9),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datad => \CPU|DP|ALU|ALT_INV_out[9]~11_combout\,
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(8),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(8),
	combout => \CPU|DP|ALU|out[9]~12_combout\);

-- Location: FF_X65_Y4_N50
\CPU|DP|c|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[9]~12_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(9));

-- Location: LABCELL_X61_Y3_N15
\read_data[10]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[10]~12_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( (\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000100000001000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~27_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~26_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\,
	combout => \read_data[10]~12_combout\);

-- Location: FF_X61_Y3_N17
\CPU|InstructionRegister|register|out[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[10]~12_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(10));

-- Location: LABCELL_X62_Y4_N3
\CPU|ID|RWmux|b[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|b\(2) = ( \CPU|InstructionRegister|register|out\(10) & ( (!\rtl~33_combout\ & (!\CPU|ID|RWmux|comb~5_combout\ & ((!\rtl~35_combout\) # (!\CPU|InstructionRegister|register|out\(7))))) ) ) # ( !\CPU|InstructionRegister|register|out\(10) & ( 
-- (!\CPU|ID|RWmux|comb~5_combout\ & ((!\rtl~35_combout\) # (!\CPU|InstructionRegister|register|out\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110000000000111111000000000010101000000000001010100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~33_combout\,
	datab => \ALT_INV_rtl~35_combout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(7),
	datad => \CPU|ID|RWmux|ALT_INV_comb~5_combout\,
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(10),
	combout => \CPU|ID|RWmux|b\(2));

-- Location: LABCELL_X61_Y5_N21
\CPU|DP|REGFILE|regSelect[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|regSelect\(5) = ( !\CPU|ID|RWmux|b\(0) & ( !\CPU|ID|RWmux|b\(2) & ( (\rtl~36_combout\ & \CPU|ID|RWmux|b\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_rtl~36_combout\,
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|ID|RWmux|ALT_INV_b\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|regSelect\(5));

-- Location: FF_X60_Y5_N7
\CPU|DP|REGFILE|Reg5|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg5|register|out[8]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(8));

-- Location: FF_X61_Y3_N26
\CPU|DP|REGFILE|Reg7|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(8),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(8));

-- Location: FF_X61_Y3_N34
\CPU|DP|REGFILE|Reg6|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(8));

-- Location: LABCELL_X61_Y2_N39
\CPU|DP|REGFILE|readMux|m|b[8]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[8]~2_combout\ = ( !\CPU|ID|RWmux|b\(2) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg6|register|out\(8) & !\CPU|ID|RWmux|b\(1)) ) ) ) # ( !\CPU|ID|RWmux|b\(2) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(8) & !\CPU|ID|RWmux|b\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000000000000000000000110011000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(8),
	datab => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(8),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[8]~2_combout\);

-- Location: FF_X62_Y5_N23
\CPU|DP|REGFILE|Reg0|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(8));

-- Location: FF_X59_Y3_N41
\CPU|DP|REGFILE|Reg2|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(8));

-- Location: FF_X62_Y5_N26
\CPU|DP|REGFILE|Reg4|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(8));

-- Location: LABCELL_X62_Y5_N24
\CPU|DP|REGFILE|readMux|m|b[8]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[8]~0_combout\ = ( \CPU|DP|REGFILE|Reg4|register|out\(8) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (((\CPU|ID|RWmux|b\(1))))) # (\CPU|ID|RWmux|b\(2) & ((!\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg2|register|out\(8)))) 
-- # (\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg0|register|out\(8))))) ) ) ) # ( !\CPU|DP|REGFILE|Reg4|register|out\(8) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((!\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg2|register|out\(8)))) # 
-- (\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg0|register|out\(8))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000011000001010000001111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(8),
	datab => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(8),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(8),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[8]~0_combout\);

-- Location: FF_X61_Y5_N23
\CPU|DP|REGFILE|Reg3|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(8));

-- Location: FF_X62_Y5_N56
\CPU|DP|REGFILE|Reg1|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(8),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(8));

-- Location: LABCELL_X62_Y5_N54
\CPU|DP|REGFILE|readMux|m|b[8]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[8]~1_combout\ = ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg3|register|out\(8))) # (\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg1|register|out\(8)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000010101000001000001010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(2),
	datab => \CPU|ID|RWmux|ALT_INV_b\(1),
	datac => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(8),
	datad => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(8),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[8]~1_combout\);

-- Location: LABCELL_X63_Y2_N27
\CPU|DP|REGFILE|readMux|m|b[8]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(8) = ( \CPU|DP|REGFILE|readMux|m|b[8]~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[8]~1_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[8]~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[8]~1_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|m|b[8]~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[8]~1_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[8]~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[8]~1_combout\ & ( ((\CPU|DP|REGFILE|Reg5|register|out\(8) & 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[8]~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(8),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~2_combout\,
	datad => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[8]~1_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(8));

-- Location: FF_X64_Y2_N53
\CPU|DP|b|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(8),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(8));

-- Location: LABCELL_X66_Y4_N3
\CPU|DP|Bin[7]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[7]~11_combout\ = ( \CPU|DP|b|register|out\(6) & ( \CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(8)))) ) ) ) # ( 
-- !\CPU|DP|b|register|out\(6) & ( \CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(8)))) ) ) ) # ( \CPU|DP|b|register|out\(6) & ( 
-- !\CPU|DP|Bin[1]~4_combout\ & ( (\CPU|DP|Bin[1]~3_combout\) # (\CPU|DP|b|register|out\(7)) ) ) ) # ( !\CPU|DP|b|register|out\(6) & ( !\CPU|DP|Bin[1]~4_combout\ & ( (\CPU|DP|b|register|out\(7) & !\CPU|DP|Bin[1]~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010100000000010101011111111100110011000011110011001100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(7),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datac => \CPU|DP|b|register|ALT_INV_out\(8),
	datad => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datae => \CPU|DP|b|register|ALT_INV_out\(6),
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[7]~11_combout\);

-- Location: LABCELL_X66_Y4_N21
\CPU|DP|ALU|plusMinus|ai|p[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(7) = ( \CPU|DP|a|register|out\(7) & ( !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|FSM|CO|Selector7~0_combout\ $ (!\CPU|DP|Bin[7]~11_combout\)) ) ) # ( !\CPU|DP|a|register|out\(7) & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Bin[7]~11_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110101010010101011010101010100101010110101010010101011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|ALT_INV_Bin[7]~11_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(7),
	combout => \CPU|DP|ALU|plusMinus|ai|p\(7));

-- Location: MLABCELL_X65_Y3_N57
\CPU|DP|ALU|out[8]~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[8]~29_combout\ = ( !\CPU|DP|ALU|plusMinus|ai|g\(7) & ( (!\CPU|InstructionRegister|register|out\(12) & ((\CPU|DP|ALU|plusMinus|ai|p\(6)) # (\CPU|DP|ALU|plusMinus|ai|g\(6)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101010101010000010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(6),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(6),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(7),
	combout => \CPU|DP|ALU|out[8]~29_combout\);

-- Location: LABCELL_X66_Y4_N54
\CPU|DP|ALU|out[8]~28\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[8]~28_combout\ = ( \CPU|DP|Bin[8]~12_combout\ & ( \CPU|DP|ALU|plusMinus|ai|g\(7) & ( (!\CPU|InstructionRegister|register|out\(11) & (\CPU|DP|a|register|out\(8) & ((!\CPU|FSM|CO|Selector7~0_combout\)))) # 
-- (\CPU|InstructionRegister|register|out\(11) & (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|a|register|out\(8)) # (\CPU|FSM|CO|Selector7~0_combout\)))) ) ) ) # ( !\CPU|DP|Bin[8]~12_combout\ & ( \CPU|DP|ALU|plusMinus|ai|g\(7) & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ ((((\CPU|DP|a|register|out\(8) & !\CPU|FSM|CO|Selector7~0_combout\)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) ) # ( \CPU|DP|Bin[8]~12_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(7) & ( 
-- (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(8) & !\CPU|FSM|CO|Selector7~0_combout\))))) # (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|a|register|out\(8) & 
-- (!\CPU|InstructionRegister|register|out\(11) & !\CPU|FSM|CO|Selector7~0_combout\))) ) ) ) # ( !\CPU|DP|Bin[8]~12_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(7) & ( !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|a|register|out\(8)) # 
-- ((\CPU|FSM|CO|Selector7~0_combout\) # (\CPU|InstructionRegister|register|out\(12))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100101100001111100101001100000010000111110000110101100000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|a|register|ALT_INV_out\(8),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datae => \CPU|DP|ALT_INV_Bin[8]~12_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(7),
	combout => \CPU|DP|ALU|out[8]~28_combout\);

-- Location: MLABCELL_X65_Y3_N36
\CPU|DP|ALU|out[8]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[8]~10_combout\ = ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( \CPU|DP|ALU|out[8]~28_combout\ & ( (!\CPU|DP|ALU|plusMinus|ai|p\(7)) # (!\CPU|DP|ALU|out[8]~29_combout\) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( 
-- \CPU|DP|ALU|out[8]~28_combout\ & ( (!\CPU|DP|ALU|plusMinus|ai|p\(7)) # ((!\CPU|DP|ALU|out[8]~29_combout\) # ((!\CPU|DP|ALU|plusMinus|ai|g\(6) & !\CPU|DP|ALU|plusMinus|ai|g\(5)))) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( 
-- !\CPU|DP|ALU|out[8]~28_combout\ & ( (\CPU|DP|ALU|plusMinus|ai|p\(7) & \CPU|DP|ALU|out[8]~29_combout\) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( !\CPU|DP|ALU|out[8]~28_combout\ & ( (\CPU|DP|ALU|plusMinus|ai|p\(7) & 
-- (\CPU|DP|ALU|out[8]~29_combout\ & ((\CPU|DP|ALU|plusMinus|ai|g\(5)) # (\CPU|DP|ALU|plusMinus|ai|g\(6))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010101000000000101010111111111111010101111111110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(7),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(6),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(5),
	datad => \CPU|DP|ALU|ALT_INV_out[8]~29_combout\,
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~0_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_out[8]~28_combout\,
	combout => \CPU|DP|ALU|out[8]~10_combout\);

-- Location: FF_X65_Y3_N38
\CPU|DP|c|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[8]~10_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(8));

-- Location: LABCELL_X61_Y3_N12
\read_data[8]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[8]~5_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a8\ & ( (\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010001000000000001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~27_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datad => \ALT_INV_rtl~26_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\,
	combout => \read_data[8]~5_combout\);

-- Location: FF_X61_Y3_N23
\CPU|InstructionRegister|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \read_data[8]~5_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(8));

-- Location: LABCELL_X62_Y4_N18
\CPU|ID|RWmux|b[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|b[0]~0_combout\ = ( \CPU|InstructionRegister|register|out\(8) & ( !\CPU|ID|RWmux|comb~3_combout\ & ( (!\rtl~9_combout\) # ((!\rtl~32_combout\ & ((!\CPU|FSM|CO|Mux1~1_combout\) # (!\rtl~28_combout\)))) ) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(8) & ( !\CPU|ID|RWmux|comb~3_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~9_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \ALT_INV_rtl~28_combout\,
	datad => \ALT_INV_rtl~32_combout\,
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(8),
	dataf => \CPU|ID|RWmux|ALT_INV_comb~3_combout\,
	combout => \CPU|ID|RWmux|b[0]~0_combout\);

-- Location: LABCELL_X62_Y4_N57
\CPU|ID|RWmux|b[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|b[2]~2_combout\ = ( !\CPU|ID|RWmux|comb~5_combout\ & ( \rtl~28_combout\ & ( (!\CPU|InstructionRegister|register|out\(10)) # ((!\rtl~9_combout\) # ((!\rtl~32_combout\ & !\CPU|FSM|CO|Mux1~1_combout\))) ) ) ) # ( !\CPU|ID|RWmux|comb~5_combout\ 
-- & ( !\rtl~28_combout\ & ( (!\CPU|InstructionRegister|register|out\(10)) # ((!\rtl~32_combout\) # (!\rtl~9_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111101110000000000000000011111111111010100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(10),
	datab => \ALT_INV_rtl~32_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datad => \ALT_INV_rtl~9_combout\,
	datae => \CPU|ID|RWmux|ALT_INV_comb~5_combout\,
	dataf => \ALT_INV_rtl~28_combout\,
	combout => \CPU|ID|RWmux|b[2]~2_combout\);

-- Location: FF_X59_Y4_N53
\CPU|InstructionRegister|register|out[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \read_data[9]~10_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(9));

-- Location: LABCELL_X62_Y4_N36
\CPU|ID|RWmux|comb~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|comb~4_combout\ = ( \CPU|FSM|stateRegister|out\(2) & ( \CPU|InstructionRegister|register|out\(1) & ( (!\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(1) & (!\CPU|FSM|CS|WideOr5~0_combout\ & !\CPU|FSM|stateRegister|out\(0)))) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(1),
	combout => \CPU|ID|RWmux|comb~4_combout\);

-- Location: LABCELL_X62_Y4_N21
\CPU|ID|RWmux|b[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|b[1]~1_combout\ = ( \CPU|InstructionRegister|register|out\(9) & ( !\CPU|ID|RWmux|comb~4_combout\ & ( (!\rtl~9_combout\) # ((!\rtl~32_combout\ & ((!\CPU|FSM|CO|Mux1~1_combout\) # (!\rtl~28_combout\)))) ) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(9) & ( !\CPU|ID|RWmux|comb~4_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111110101110101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~9_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \ALT_INV_rtl~32_combout\,
	datad => \ALT_INV_rtl~28_combout\,
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(9),
	dataf => \CPU|ID|RWmux|ALT_INV_comb~4_combout\,
	combout => \CPU|ID|RWmux|b[1]~1_combout\);

-- Location: LABCELL_X63_Y5_N24
\rtl~30\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~30_combout\ = ( \CPU|FSM|stateRegister|out\(2) & ( \rtl~29_combout\ & ( !\CPU|FSM|stateRegister|out\(1) ) ) ) # ( !\CPU|FSM|stateRegister|out\(2) & ( \rtl~29_combout\ & ( (\CPU|FSM|stateRegister|out\(0) & (((\CPU|InstructionRegister|register|out\(14) 
-- & !\CPU|InstructionRegister|register|out\(15))) # (\CPU|FSM|stateRegister|out\(1)))) ) ) ) # ( \CPU|FSM|stateRegister|out\(2) & ( !\rtl~29_combout\ & ( !\CPU|FSM|stateRegister|out\(1) ) ) ) # ( !\CPU|FSM|stateRegister|out\(2) & ( !\rtl~29_combout\ & ( 
-- (\CPU|FSM|stateRegister|out\(1) & \CPU|FSM|stateRegister|out\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111111100001111000000000000010011111111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \ALT_INV_rtl~29_combout\,
	combout => \rtl~30_combout\);

-- Location: LABCELL_X63_Y5_N36
\CPU|ID|RWmux|comb~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|comb~1_combout\ = ( \rtl~30_combout\ & ( (\CPU|InstructionRegister|register|out\(5) & (((!\CPU|FSM|stateRegister|out\(0) & \rtl~31_combout\)) # (\CPU|FSM|stateRegister|out\(3)))) ) ) # ( !\rtl~30_combout\ & ( (!\CPU|FSM|stateRegister|out\(0) 
-- & (\CPU|InstructionRegister|register|out\(5) & \rtl~31_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001100000000000000110000000101000011010000010100001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(5),
	datad => \ALT_INV_rtl~31_combout\,
	dataf => \ALT_INV_rtl~30_combout\,
	combout => \CPU|ID|RWmux|comb~1_combout\);

-- Location: LABCELL_X63_Y5_N21
\CPU|ID|RWmux|comb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|comb~0_combout\ = ( \rtl~31_combout\ & ( \rtl~30_combout\ & ( (\CPU|InstructionRegister|register|out\(7) & ((!\CPU|FSM|stateRegister|out\(0)) # (\CPU|FSM|stateRegister|out\(3)))) ) ) ) # ( !\rtl~31_combout\ & ( \rtl~30_combout\ & ( 
-- (\CPU|FSM|stateRegister|out\(3) & \CPU|InstructionRegister|register|out\(7)) ) ) ) # ( \rtl~31_combout\ & ( !\rtl~30_combout\ & ( (\CPU|InstructionRegister|register|out\(7) & !\CPU|FSM|stateRegister|out\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000000000000101000001010000111100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(7),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \ALT_INV_rtl~31_combout\,
	dataf => \ALT_INV_rtl~30_combout\,
	combout => \CPU|ID|RWmux|comb~0_combout\);

-- Location: LABCELL_X63_Y5_N12
\CPU|ID|RWmux|comb~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|comb~2_combout\ = ( \rtl~31_combout\ & ( \rtl~30_combout\ & ( (\CPU|InstructionRegister|register|out\(6) & ((!\CPU|FSM|stateRegister|out\(0)) # (\CPU|FSM|stateRegister|out\(3)))) ) ) ) # ( !\rtl~31_combout\ & ( \rtl~30_combout\ & ( 
-- (\CPU|InstructionRegister|register|out\(6) & \CPU|FSM|stateRegister|out\(3)) ) ) ) # ( \rtl~31_combout\ & ( !\rtl~30_combout\ & ( (!\CPU|FSM|stateRegister|out\(0) & \CPU|InstructionRegister|register|out\(6)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000001000100010001000000011000000110010001100100011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(6),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datae => \ALT_INV_rtl~31_combout\,
	dataf => \ALT_INV_rtl~30_combout\,
	combout => \CPU|ID|RWmux|comb~2_combout\);

-- Location: LABCELL_X62_Y4_N15
\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ = ( \CPU|ID|RWmux|comb~0_combout\ & ( !\CPU|ID|RWmux|comb~2_combout\ & ( (\CPU|ID|RWmux|b[1]~1_combout\ & ((!\CPU|ID|RWmux|b[0]~0_combout\) # (\CPU|ID|RWmux|comb~1_combout\))) ) ) ) # ( 
-- !\CPU|ID|RWmux|comb~0_combout\ & ( !\CPU|ID|RWmux|comb~2_combout\ & ( (!\CPU|ID|RWmux|b[2]~2_combout\ & (\CPU|ID|RWmux|b[1]~1_combout\ & ((!\CPU|ID|RWmux|b[0]~0_combout\) # (\CPU|ID|RWmux|comb~1_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001100000010100000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b[0]~0_combout\,
	datab => \CPU|ID|RWmux|ALT_INV_b[2]~2_combout\,
	datac => \CPU|ID|RWmux|ALT_INV_b[1]~1_combout\,
	datad => \CPU|ID|RWmux|ALT_INV_comb~1_combout\,
	datae => \CPU|ID|RWmux|ALT_INV_comb~0_combout\,
	dataf => \CPU|ID|RWmux|ALT_INV_comb~2_combout\,
	combout => \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\);

-- Location: LABCELL_X60_Y3_N54
\CPU|DP|WBmux|m|b[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[0]~1_combout\ = ( \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(0) & (\CPU|program_counter|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(0)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\)))) ) ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(0)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # ( \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (\CPU|program_counter|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(0)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|InstructionRegister|register|out\(0)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111001100000011110000110010101010100010000000101000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(0),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(0),
	datac => \CPU|program_counter|register|ALT_INV_out\(0),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	datae => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	combout => \CPU|DP|WBmux|m|b[0]~1_combout\);

-- Location: LABCELL_X60_Y3_N9
\CPU|DP|WBmux|m|b[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(0) = ( \CPU|DP|WBmux|m|b[0]~1_combout\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ((\e~combout\) # (\SW[0]~input_o\))) ) ) ) # ( !\CPU|DP|WBmux|m|b[0]~1_combout\ & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ ) ) # ( \CPU|DP|WBmux|m|b[0]~1_combout\ & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( (\SW[0]~input_o\ & (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & !\e~combout\)) ) ) ) # ( 
-- !\CPU|DP|WBmux|m|b[0]~1_combout\ & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000110000000011111111111111110000001100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_SW[0]~input_o\,
	datac => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datad => \ALT_INV_e~combout\,
	datae => \CPU|DP|WBmux|m|ALT_INV_b[0]~1_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	combout => \CPU|DP|WBmux|m|b\(0));

-- Location: FF_X60_Y3_N58
\CPU|DP|REGFILE|Reg5|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(0));

-- Location: FF_X60_Y5_N37
\CPU|DP|REGFILE|Reg3|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(0));

-- Location: FF_X62_Y5_N41
\CPU|DP|REGFILE|Reg1|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(0));

-- Location: LABCELL_X62_Y5_N39
\CPU|DP|REGFILE|readMux|m|b[0]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[0]~4_combout\ = ( \CPU|DP|REGFILE|Reg1|register|out\(0) & ( \CPU|ID|RWmux|b\(1) & ( (!\CPU|ID|RWmux|b\(0) & \CPU|ID|RWmux|b\(2)) ) ) ) # ( \CPU|DP|REGFILE|Reg1|register|out\(0) & ( !\CPU|ID|RWmux|b\(1) & ( 
-- (\CPU|DP|REGFILE|Reg3|register|out\(0) & (!\CPU|ID|RWmux|b\(0) & \CPU|ID|RWmux|b\(2))) ) ) ) # ( !\CPU|DP|REGFILE|Reg1|register|out\(0) & ( !\CPU|ID|RWmux|b\(1) & ( (\CPU|DP|REGFILE|Reg3|register|out\(0) & (!\CPU|ID|RWmux|b\(0) & \CPU|ID|RWmux|b\(2))) ) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110000000000000011000000000000000000000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(0),
	datac => \CPU|ID|RWmux|ALT_INV_b\(0),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(1),
	combout => \CPU|DP|REGFILE|readMux|m|b[0]~4_combout\);

-- Location: FF_X62_Y5_N11
\CPU|DP|REGFILE|Reg4|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(0));

-- Location: FF_X59_Y3_N23
\CPU|DP|REGFILE|Reg2|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(0));

-- Location: FF_X62_Y5_N50
\CPU|DP|REGFILE|Reg0|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(0));

-- Location: LABCELL_X62_Y5_N48
\CPU|DP|REGFILE|readMux|m|b[0]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[0]~3_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(0) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (\CPU|DP|REGFILE|Reg4|register|out\(0) & (\CPU|ID|RWmux|b\(1)))) # (\CPU|ID|RWmux|b\(2) & 
-- (((\CPU|DP|REGFILE|Reg2|register|out\(0)) # (\CPU|ID|RWmux|b\(1))))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(0) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (\CPU|DP|REGFILE|Reg4|register|out\(0) & (\CPU|ID|RWmux|b\(1)))) # 
-- (\CPU|ID|RWmux|b\(2) & (((!\CPU|ID|RWmux|b\(1) & \CPU|DP|REGFILE|Reg2|register|out\(0))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000010010100100000011101010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(2),
	datab => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(0),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(0),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[0]~3_combout\);

-- Location: FF_X60_Y3_N10
\CPU|DP|REGFILE|Reg7|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(0),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(0));

-- Location: FF_X60_Y3_N53
\CPU|DP|REGFILE|Reg6|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(0),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(0));

-- Location: LABCELL_X60_Y3_N51
\CPU|DP|REGFILE|readMux|m|b[0]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[0]~5_combout\ = ( \CPU|DP|REGFILE|Reg6|register|out\(0) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1)) ) ) ) # ( \CPU|DP|REGFILE|Reg6|register|out\(0) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(0) & (!\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) ) # ( !\CPU|DP|REGFILE|Reg6|register|out\(0) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg7|register|out\(0) & (!\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000000000000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(0),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[0]~5_combout\);

-- Location: LABCELL_X63_Y4_N0
\CPU|DP|REGFILE|readMux|m|b[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(0) = ( \CPU|DP|REGFILE|readMux|m|b[0]~5_combout\ ) # ( !\CPU|DP|REGFILE|readMux|m|b[0]~5_combout\ & ( (((\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & \CPU|DP|REGFILE|Reg5|register|out\(0))) # 
-- (\CPU|DP|REGFILE|readMux|m|b[0]~3_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[0]~4_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001111111111111000111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	datab => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(0),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~4_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~3_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[0]~5_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(0));

-- Location: FF_X63_Y4_N50
\CPU|DP|b|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(0),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(0));

-- Location: FF_X63_Y4_N34
\CPU|DP|b|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(1),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(1));

-- Location: MLABCELL_X65_Y4_N18
\CPU|DP|Bin[1]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[1]~5_combout\ = ( \CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(2) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|InstructionRegister|register|out\(1) ) ) ) # ( 
-- \CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(0) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(1) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100110011001100001111000011110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(2),
	datab => \CPU|DP|b|register|ALT_INV_out\(0),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(1),
	datad => \CPU|DP|b|register|ALT_INV_out\(1),
	datae => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[1]~5_combout\);

-- Location: FF_X62_Y4_N31
\CPU|DP|a|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(1),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(1));

-- Location: LABCELL_X64_Y5_N48
\CPU|DP|Ain[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[1]~1_combout\ = ( \CPU|InstructionRegister|register|out\(15) & ( \CPU|FSM|CO|Decoder0~1_combout\ & ( (!\CPU|FSM|storeFlag|register|out\(0) & (\CPU|DP|a|register|out\(1) & ((!\CPU|InstructionRegister|register|out\(14)) # 
-- (\CPU|InstructionRegister|register|out\(13))))) ) ) ) # ( !\CPU|InstructionRegister|register|out\(15) & ( \CPU|FSM|CO|Decoder0~1_combout\ & ( (!\CPU|FSM|storeFlag|register|out\(0) & \CPU|DP|a|register|out\(1)) ) ) ) # ( 
-- \CPU|InstructionRegister|register|out\(15) & ( !\CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|DP|a|register|out\(1) ) ) ) # ( !\CPU|InstructionRegister|register|out\(15) & ( !\CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|DP|a|register|out\(1) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000101010100000000010100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datad => \CPU|DP|a|register|ALT_INV_out\(1),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	dataf => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	combout => \CPU|DP|Ain[1]~1_combout\);

-- Location: FF_X63_Y4_N35
\CPU|DP|b|register|out[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(1),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out[1]~DUPLICATE_q\);

-- Location: LABCELL_X63_Y4_N39
\CPU|DP|Bin[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[0]~0_combout\ = ( \CPU|DP|b|register|out[1]~DUPLICATE_q\ & ( (\CPU|InstructionRegister|register|out\(4) & (\CPU|InstructionRegister|register|out\(15) & (!\CPU|InstructionRegister|register|out\(14) $ 
-- (!\CPU|InstructionRegister|register|out\(13))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000001000100000000000100010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	dataf => \CPU|DP|b|register|ALT_INV_out[1]~DUPLICATE_q\,
	combout => \CPU|DP|Bin[0]~0_combout\);

-- Location: LABCELL_X63_Y4_N6
\CPU|DP|Bin[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[0]~1_combout\ = ( \CPU|InstructionRegister|register|out\(13) & ( \CPU|DP|b|register|out\(0) & ( ((!\CPU|InstructionRegister|register|out\(15)) # ((!\CPU|InstructionRegister|register|out\(4) & !\CPU|InstructionRegister|register|out\(3)))) # 
-- (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\) ) ) ) # ( !\CPU|InstructionRegister|register|out\(13) & ( \CPU|DP|b|register|out\(0) & ( (!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\) # ((!\CPU|InstructionRegister|register|out\(15)) # 
-- ((!\CPU|InstructionRegister|register|out\(4) & !\CPU|InstructionRegister|register|out\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111010101111111111010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(3),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	dataf => \CPU|DP|b|register|ALT_INV_out\(0),
	combout => \CPU|DP|Bin[0]~1_combout\);

-- Location: MLABCELL_X65_Y4_N36
\CPU|DP|ALU|plusMinus|ai|c[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\ = ( \CPU|DP|Ain[0]~0_combout\ & ( \CPU|FSM|CO|Selector8~1_combout\ & ( (\CPU|InstructionRegister|register|out\(0)) # (\CPU|InstructionRegister|register|out\(11)) ) ) ) # ( !\CPU|DP|Ain[0]~0_combout\ & ( 
-- \CPU|FSM|CO|Selector8~1_combout\ & ( (\CPU|InstructionRegister|register|out\(11) & !\CPU|InstructionRegister|register|out\(0)) ) ) ) # ( \CPU|DP|Ain[0]~0_combout\ & ( !\CPU|FSM|CO|Selector8~1_combout\ & ( ((\CPU|DP|Bin[0]~1_combout\) # 
-- (\CPU|DP|Bin[0]~0_combout\)) # (\CPU|InstructionRegister|register|out\(11)) ) ) ) # ( !\CPU|DP|Ain[0]~0_combout\ & ( !\CPU|FSM|CO|Selector8~1_combout\ & ( (\CPU|InstructionRegister|register|out\(11) & (!\CPU|DP|Bin[0]~0_combout\ & 
-- !\CPU|DP|Bin[0]~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010111111111111101000100010001000111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(0),
	datac => \CPU|DP|ALT_INV_Bin[0]~0_combout\,
	datad => \CPU|DP|ALT_INV_Bin[0]~1_combout\,
	datae => \CPU|DP|ALT_INV_Ain[0]~0_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector8~1_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\);

-- Location: LABCELL_X63_Y4_N30
\CPU|DP|Bin[2]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[2]~6_combout\ = ( \CPU|DP|b|register|out\(3) & ( \CPU|DP|Bin[1]~4_combout\ & ( (\CPU|InstructionRegister|register|out\(2)) # (\CPU|DP|Bin[1]~3_combout\) ) ) ) # ( !\CPU|DP|b|register|out\(3) & ( \CPU|DP|Bin[1]~4_combout\ & ( 
-- (!\CPU|DP|Bin[1]~3_combout\ & \CPU|InstructionRegister|register|out\(2)) ) ) ) # ( \CPU|DP|b|register|out\(3) & ( !\CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(2)))) # (\CPU|DP|Bin[1]~3_combout\ & 
-- (\CPU|DP|b|register|out[1]~DUPLICATE_q\)) ) ) ) # ( !\CPU|DP|b|register|out\(3) & ( !\CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(2)))) # (\CPU|DP|Bin[1]~3_combout\ & (\CPU|DP|b|register|out[1]~DUPLICATE_q\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011010100110101001101010011010100000000111100000000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out[1]~DUPLICATE_q\,
	datab => \CPU|DP|b|register|ALT_INV_out\(2),
	datac => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(2),
	datae => \CPU|DP|b|register|ALT_INV_out\(3),
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[2]~6_combout\);

-- Location: MLABCELL_X65_Y4_N54
\CPU|DP|ALU|plusMinus|ai|c[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|c\(3) = ( \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\ & ( \CPU|DP|Bin[2]~6_combout\ & ( (!\CPU|DP|Ain[1]~1_combout\ & ((!\CPU|DP|Bin[1]~5_combout\ & (\CPU|DP|Ain[2]~2_combout\)) # (\CPU|DP|Bin[1]~5_combout\ & 
-- ((!\CPU|InstructionRegister|register|out\(11)))))) # (\CPU|DP|Ain[1]~1_combout\ & (((!\CPU|InstructionRegister|register|out\(11))) # (\CPU|DP|Ain[2]~2_combout\))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\ & ( \CPU|DP|Bin[2]~6_combout\ & ( 
-- (!\CPU|DP|Ain[1]~1_combout\ & (\CPU|DP|Ain[2]~2_combout\ & (!\CPU|InstructionRegister|register|out\(11)))) # (\CPU|DP|Ain[1]~1_combout\ & ((!\CPU|DP|Bin[1]~5_combout\ & (\CPU|DP|Ain[2]~2_combout\)) # (\CPU|DP|Bin[1]~5_combout\ & 
-- ((!\CPU|InstructionRegister|register|out\(11)))))) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\ & ( !\CPU|DP|Bin[2]~6_combout\ & ( (!\CPU|DP|Ain[1]~1_combout\ & ((!\CPU|DP|Bin[1]~5_combout\ & ((\CPU|InstructionRegister|register|out\(11)))) # 
-- (\CPU|DP|Bin[1]~5_combout\ & (\CPU|DP|Ain[2]~2_combout\)))) # (\CPU|DP|Ain[1]~1_combout\ & (((\CPU|InstructionRegister|register|out\(11))) # (\CPU|DP|Ain[2]~2_combout\))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\ & ( !\CPU|DP|Bin[2]~6_combout\ & 
-- ( (!\CPU|DP|Ain[1]~1_combout\ & (\CPU|DP|Ain[2]~2_combout\ & (\CPU|InstructionRegister|register|out\(11)))) # (\CPU|DP|Ain[1]~1_combout\ & ((!\CPU|DP|Bin[1]~5_combout\ & ((\CPU|InstructionRegister|register|out\(11)))) # (\CPU|DP|Bin[1]~5_combout\ & 
-- (\CPU|DP|Ain[2]~2_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100110101001101010111011101000100010111000101110011011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[2]~2_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Bin[1]~5_combout\,
	datad => \CPU|DP|ALT_INV_Ain[1]~1_combout\,
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[2]~6_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|c\(3));

-- Location: MLABCELL_X65_Y4_N24
\CPU|DP|ALU|out[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[3]~3_combout\ = ( \CPU|DP|Bin[3]~7_combout\ & ( \CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (\CPU|DP|ALU|plusMinus|ai|c\(3)))) ) ) ) # ( 
-- !\CPU|DP|Bin[3]~7_combout\ & ( \CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|ALU|plusMinus|ai|c\(3)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) ) # ( \CPU|DP|Bin[3]~7_combout\ & ( 
-- !\CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|ALU|plusMinus|ai|c\(3) $ (!\CPU|DP|a|register|out\(3))))) # (\CPU|InstructionRegister|register|out\(12) & 
-- (!\CPU|InstructionRegister|register|out\(11) & ((\CPU|DP|a|register|out\(3))))) ) ) ) # ( !\CPU|DP|Bin[3]~7_combout\ & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|ALU|plusMinus|ai|c\(3) $ 
-- (\CPU|DP|a|register|out\(3))) # (\CPU|InstructionRegister|register|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101100110010101100001000110101001011001010110011000010010000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(3),
	datad => \CPU|DP|a|register|ALT_INV_out\(3),
	datae => \CPU|DP|ALT_INV_Bin[3]~7_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|out[3]~3_combout\);

-- Location: FF_X65_Y4_N26
\CPU|DP|c|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[3]~3_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(3));

-- Location: LABCELL_X63_Y4_N27
\read_data[2]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[2]~13_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( ((\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\))) # (\SW[2]~input_o\) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( (\SW[2]~input_o\ & 
-- ((!\rtl~26_combout\) # ((!\rtl~27_combout\) # (\CPU|mem_addr[8]~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001011000011110000101100001111010011110000111101001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[2]~input_o\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\,
	combout => \read_data[2]~13_combout\);

-- Location: FF_X63_Y4_N29
\CPU|InstructionRegister|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[2]~13_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(2));

-- Location: LABCELL_X63_Y2_N39
\CPU|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~13_sumout\ = SUM(( \CPU|program_counter|register|out\(2) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(2) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~10\ ))
-- \CPU|Add0~14\ = CARRY(( \CPU|program_counter|register|out\(2) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(2) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(2),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(2),
	cin => \CPU|Add0~10\,
	sumout => \CPU|Add0~13_sumout\,
	cout => \CPU|Add0~14\);

-- Location: FF_X63_Y2_N40
\CPU|program_counter|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~13_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(2),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(2));

-- Location: LABCELL_X63_Y2_N42
\CPU|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~17_sumout\ = SUM(( \CPU|program_counter|register|out\(3) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(3) & ((\CPU|FSM|CO|Selector0~0_combout\) # (\CPU|FSM|CO|Selector0~1_combout\)))) ) + ( \CPU|Add0~14\ ))
-- \CPU|Add0~18\ = CARRY(( \CPU|program_counter|register|out\(3) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(3) & ((\CPU|FSM|CO|Selector0~0_combout\) # (\CPU|FSM|CO|Selector0~1_combout\)))) ) + ( \CPU|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(3),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(3),
	cin => \CPU|Add0~14\,
	sumout => \CPU|Add0~17_sumout\,
	cout => \CPU|Add0~18\);

-- Location: LABCELL_X63_Y2_N45
\CPU|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~21_sumout\ = SUM(( \CPU|program_counter|register|out\(4) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(4) & ((\CPU|FSM|CO|Selector0~0_combout\) # (\CPU|FSM|CO|Selector0~1_combout\)))) ) + ( \CPU|Add0~18\ ))
-- \CPU|Add0~22\ = CARRY(( \CPU|program_counter|register|out\(4) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(4) & ((\CPU|FSM|CO|Selector0~0_combout\) # (\CPU|FSM|CO|Selector0~1_combout\)))) ) + ( \CPU|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(4),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	cin => \CPU|Add0~18\,
	sumout => \CPU|Add0~21_sumout\,
	cout => \CPU|Add0~22\);

-- Location: FF_X63_Y2_N47
\CPU|program_counter|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~21_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(4),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(4));

-- Location: LABCELL_X61_Y4_N12
\CPU|DP|WBmux|m|b[5]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[5]~6_combout\ = ( \CPU|DP|Add0~21_sumout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(5) & (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ((!\CPU|InstructionRegister|register|out\(5)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\)))) ) ) ) # ( !\CPU|DP|Add0~21_sumout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(5) & ((!\CPU|InstructionRegister|register|out\(5)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # 
-- ( \CPU|DP|Add0~21_sumout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ((!\CPU|InstructionRegister|register|out\(5)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # ( !\CPU|DP|Add0~21_sumout\ & ( 
-- !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|InstructionRegister|register|out\(5)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110111011101110111011100000000011100000111000001110000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(5),
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(5),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|ALT_INV_Add0~21_sumout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	combout => \CPU|DP|WBmux|m|b[5]~6_combout\);

-- Location: LABCELL_X61_Y2_N48
\CPU|DP|WBmux|m|b[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(5) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a5\ & ( \CPU|DP|WBmux|m|b[5]~6_combout\ & ( (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ((\e~combout\) # (\SW[5]~input_o\))) ) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a5\ & ( 
-- \CPU|DP|WBmux|m|b[5]~6_combout\ & ( (\SW[5]~input_o\ & (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & !\e~combout\)) ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a5\ & ( !\CPU|DP|WBmux|m|b[5]~6_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a5\ 
-- & ( !\CPU|DP|WBmux|m|b[5]~6_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000101000000000000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[5]~input_o\,
	datac => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datad => \ALT_INV_e~combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\,
	dataf => \CPU|DP|WBmux|m|ALT_INV_b[5]~6_combout\,
	combout => \CPU|DP|WBmux|m|b\(5));

-- Location: FF_X61_Y2_N52
\CPU|DP|REGFILE|Reg5|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(5));

-- Location: FF_X60_Y2_N31
\CPU|DP|REGFILE|Reg3|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(5));

-- Location: FF_X62_Y2_N11
\CPU|DP|REGFILE|Reg1|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(5));

-- Location: LABCELL_X62_Y2_N9
\CPU|DP|REGFILE|readMux|m|b[5]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[5]~19_combout\ = ( \CPU|DP|REGFILE|Reg1|register|out\(5) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((\CPU|ID|RWmux|b\(1)) # (\CPU|DP|REGFILE|Reg3|register|out\(5)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg1|register|out\(5) & 
-- ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg3|register|out\(5) & (\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(5),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(5),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[5]~19_combout\);

-- Location: FF_X61_Y2_N25
\CPU|DP|REGFILE|Reg7|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(5));

-- Location: FF_X61_Y2_N31
\CPU|DP|REGFILE|Reg6|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(5));

-- Location: LABCELL_X62_Y2_N15
\CPU|DP|REGFILE|readMux|m|b[5]~20\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[5]~20_combout\ = ( !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & \CPU|DP|REGFILE|Reg6|register|out\(5)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(5) & !\CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110000000000000000000000000000111100000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(5),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(5),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[5]~20_combout\);

-- Location: LABCELL_X60_Y2_N45
\CPU|DP|REGFILE|Reg0|register|out[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg0|register|out[5]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(5) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(5),
	combout => \CPU|DP|REGFILE|Reg0|register|out[5]~feeder_combout\);

-- Location: FF_X60_Y2_N47
\CPU|DP|REGFILE|Reg0|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg0|register|out[5]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(5));

-- Location: FF_X62_Y2_N49
\CPU|DP|REGFILE|Reg4|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(5),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(5));

-- Location: LABCELL_X60_Y2_N24
\CPU|DP|REGFILE|Reg2|register|out[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg2|register|out[5]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(5) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(5),
	combout => \CPU|DP|REGFILE|Reg2|register|out[5]~feeder_combout\);

-- Location: FF_X60_Y2_N25
\CPU|DP|REGFILE|Reg2|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg2|register|out[5]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(5));

-- Location: LABCELL_X64_Y2_N12
\CPU|DP|REGFILE|readMux|m|b[5]~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[5]~18_combout\ = ( \CPU|ID|RWmux|b\(0) & ( \CPU|ID|RWmux|b\(2) & ( (!\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg2|register|out\(5)))) # (\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg0|register|out\(5))) ) ) ) # ( 
-- \CPU|ID|RWmux|b\(0) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg4|register|out\(5) & \CPU|ID|RWmux|b\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000110000001100000000000000000000010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(5),
	datab => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(5),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(5),
	datae => \CPU|ID|RWmux|ALT_INV_b\(0),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[5]~18_combout\);

-- Location: LABCELL_X63_Y2_N21
\CPU|DP|REGFILE|readMux|m|b[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(5) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[5]~18_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[5]~18_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[5]~18_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[5]~20_combout\) # (\CPU|DP|REGFILE|readMux|m|b[5]~19_combout\)) # (\CPU|DP|REGFILE|Reg5|register|out\(5)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[5]~18_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[5]~20_combout\) # (\CPU|DP|REGFILE|readMux|m|b[5]~19_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111011111110111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(5),
	datab => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~19_combout\,
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~20_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[5]~18_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(5));

-- Location: FF_X63_Y2_N19
\CPU|DP|a|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(5),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(5));

-- Location: LABCELL_X66_Y4_N30
\CPU|DP|ALU|plusMinus|ai|g[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(5) = ( \CPU|DP|Bin[5]~9_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & (\CPU|DP|a|register|out\(5) & !\CPU|FSM|CO|Selector7~0_combout\)) ) ) # ( !\CPU|DP|Bin[5]~9_combout\ & ( 
-- (\CPU|InstructionRegister|register|out\(11) & (\CPU|DP|a|register|out\(5) & !\CPU|FSM|CO|Selector7~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000000001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|a|register|ALT_INV_out\(5),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[5]~9_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(5));

-- Location: LABCELL_X66_Y2_N54
\CPU|DP|ALU|out[7]~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[7]~22_combout\ = ( \CPU|DP|Bin[7]~11_combout\ & ( \CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|InstructionRegister|register|out\(11) & !\CPU|InstructionRegister|register|out\(12)) ) ) ) # ( !\CPU|DP|Bin[7]~11_combout\ & ( 
-- \CPU|FSM|CO|Selector7~0_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (\CPU|InstructionRegister|register|out\(12)) ) ) ) # ( \CPU|DP|Bin[7]~11_combout\ & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & 
-- ((\CPU|DP|a|register|out\(7)))) # (\CPU|InstructionRegister|register|out\(11) & (!\CPU|InstructionRegister|register|out\(12) & !\CPU|DP|a|register|out\(7))) ) ) ) # ( !\CPU|DP|Bin[7]~11_combout\ & ( !\CPU|FSM|CO|Selector7~0_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((\CPU|DP|a|register|out\(7)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100001100110011001100001100110011000011110000110011000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datad => \CPU|DP|a|register|ALT_INV_out\(7),
	datae => \CPU|DP|ALT_INV_Bin[7]~11_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|out[7]~22_combout\);

-- Location: MLABCELL_X65_Y4_N30
\CPU|DP|ALU|out[7]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[7]~9_combout\ = ( \CPU|DP|ALU|out[7]~22_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( ((\CPU|InstructionRegister|register|out\(12)) # (\CPU|DP|ALU|plusMinus|ai|p\(6))) # (\CPU|DP|ALU|plusMinus|ai|g\(6)) ) ) ) # ( 
-- !\CPU|DP|ALU|out[7]~22_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (!\CPU|DP|ALU|plusMinus|ai|g\(6) & (!\CPU|DP|ALU|plusMinus|ai|p\(6) & !\CPU|InstructionRegister|register|out\(12))) ) ) ) # ( \CPU|DP|ALU|out[7]~22_combout\ & ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (((\CPU|DP|ALU|plusMinus|ai|p\(6) & \CPU|DP|ALU|plusMinus|ai|g\(5))) # (\CPU|InstructionRegister|register|out\(12))) # (\CPU|DP|ALU|plusMinus|ai|g\(6)) ) ) ) # ( !\CPU|DP|ALU|out[7]~22_combout\ & ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (!\CPU|DP|ALU|plusMinus|ai|g\(6) & (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|ai|p\(6)) # (!\CPU|DP|ALU|plusMinus|ai|g\(5))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010100000000000010101111111111110001000000000000111011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(6),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(6),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(5),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datae => \CPU|DP|ALU|ALT_INV_out[7]~22_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~0_combout\,
	combout => \CPU|DP|ALU|out[7]~9_combout\);

-- Location: FF_X65_Y4_N32
\CPU|DP|c|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[7]~9_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(7));

-- Location: LABCELL_X61_Y2_N6
\CPU|DP|WBmux|m|b[7]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[7]~8_combout\ = ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(7) & (!\CPU|DP|WBmux|m|comb~0_combout\ & ((!\CPU|DP|Add0~29_sumout\) # (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\)))) ) ) # ( 
-- !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|WBmux|m|comb~0_combout\ & ((!\CPU|DP|Add0~29_sumout\) # (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011000000101000001000000011110000110000001010000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(7),
	datab => \CPU|DP|ALT_INV_Add0~29_sumout\,
	datac => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	combout => \CPU|DP|WBmux|m|b[7]~8_combout\);

-- Location: LABCELL_X61_Y2_N27
\CPU|DP|WBmux|m|b[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(7) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a7\ & ( \CPU|DP|WBmux|m|b[7]~8_combout\ & ( (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ((\e~combout\) # (\SW[7]~input_o\))) ) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a7\ & ( 
-- \CPU|DP|WBmux|m|b[7]~8_combout\ & ( (\SW[7]~input_o\ & (!\e~combout\ & \CPU|DP|WBmux|d|ShiftLeft0~1_combout\)) ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a7\ & ( !\CPU|DP|WBmux|m|b[7]~8_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a7\ 
-- & ( !\CPU|DP|WBmux|m|b[7]~8_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000010100000000000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[7]~input_o\,
	datac => \ALT_INV_e~combout\,
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\,
	dataf => \CPU|DP|WBmux|m|ALT_INV_b[7]~8_combout\,
	combout => \CPU|DP|WBmux|m|b\(7));

-- Location: FF_X61_Y2_N58
\CPU|DP|REGFILE|Reg5|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(7));

-- Location: FF_X62_Y2_N2
\CPU|DP|REGFILE|Reg4|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(7));

-- Location: FF_X60_Y2_N58
\CPU|DP|REGFILE|Reg2|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(7));

-- Location: FF_X62_Y2_N20
\CPU|DP|REGFILE|Reg0|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(7));

-- Location: LABCELL_X62_Y2_N18
\CPU|DP|REGFILE|readMux|m|b[7]~24\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[7]~24_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(7) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (((\CPU|DP|REGFILE|Reg2|register|out\(7) & \CPU|ID|RWmux|b\(2))))) # (\CPU|ID|RWmux|b\(1) & (((\CPU|ID|RWmux|b\(2))) # 
-- (\CPU|DP|REGFILE|Reg4|register|out\(7)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(7) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (((\CPU|DP|REGFILE|Reg2|register|out\(7) & \CPU|ID|RWmux|b\(2))))) # (\CPU|ID|RWmux|b\(1) & 
-- (\CPU|DP|REGFILE|Reg4|register|out\(7) & ((!\CPU|ID|RWmux|b\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101001100000000010100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(7),
	datab => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(7),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(7),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[7]~24_combout\);

-- Location: FF_X61_Y2_N13
\CPU|DP|REGFILE|Reg6|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(7));

-- Location: FF_X61_Y2_N29
\CPU|DP|REGFILE|Reg7|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(7),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(7));

-- Location: LABCELL_X62_Y2_N51
\CPU|DP|REGFILE|readMux|m|b[7]~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[7]~26_combout\ = ( \CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg6|register|out\(7) & (!\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) # ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg7|register|out\(7) & (!\CPU|ID|RWmux|b\(2) 
-- & !\CPU|ID|RWmux|b\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000001100000000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(7),
	datab => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(7),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[7]~26_combout\);

-- Location: MLABCELL_X59_Y2_N18
\CPU|DP|REGFILE|Reg3|register|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[7]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(7),
	combout => \CPU|DP|REGFILE|Reg3|register|out[7]~feeder_combout\);

-- Location: FF_X59_Y2_N19
\CPU|DP|REGFILE|Reg3|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[7]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(7));

-- Location: FF_X62_Y2_N47
\CPU|DP|REGFILE|Reg1|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(7),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(7));

-- Location: LABCELL_X62_Y2_N45
\CPU|DP|REGFILE|readMux|m|b[7]~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[7]~25_combout\ = ( \CPU|DP|REGFILE|Reg1|register|out\(7) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((\CPU|ID|RWmux|b\(1)) # (\CPU|DP|REGFILE|Reg3|register|out\(7)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg1|register|out\(7) & 
-- ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg3|register|out\(7) & (\CPU|ID|RWmux|b\(2) & !\CPU|ID|RWmux|b\(1))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(7),
	datac => \CPU|ID|RWmux|ALT_INV_b\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(7),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[7]~25_combout\);

-- Location: LABCELL_X63_Y2_N15
\CPU|DP|REGFILE|readMux|m|b[7]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(7) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[7]~25_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[7]~25_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[7]~25_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[7]~26_combout\) # (\CPU|DP|REGFILE|readMux|m|b[7]~24_combout\)) # (\CPU|DP|REGFILE|Reg5|register|out\(7)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[7]~25_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[7]~26_combout\) # (\CPU|DP|REGFILE|readMux|m|b[7]~24_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111010111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(7),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~24_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~26_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[7]~25_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(7));

-- Location: FF_X63_Y2_N55
\CPU|program_counter|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~33_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(7),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(7));

-- Location: LABCELL_X56_Y4_N6
\CPU|data_address|register|out[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|data_address|register|out[7]~feeder_combout\ = ( \CPU|DP|c|register|out\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|c|register|ALT_INV_out\(7),
	combout => \CPU|data_address|register|out[7]~feeder_combout\);

-- Location: FF_X56_Y4_N7
\CPU|data_address|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|data_address|register|out[7]~feeder_combout\,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(7));

-- Location: LABCELL_X62_Y5_N15
\CPU|mem_addr[7]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[7]~8_combout\ = ( \CPU|data_address|register|out\(7) & ( \CPU|FSM|CO|Mux1~0_combout\ & ( \CPU|program_counter|register|out\(7) ) ) ) # ( !\CPU|data_address|register|out\(7) & ( \CPU|FSM|CO|Mux1~0_combout\ & ( 
-- \CPU|program_counter|register|out\(7) ) ) ) # ( \CPU|data_address|register|out\(7) & ( !\CPU|FSM|CO|Mux1~0_combout\ & ( ((!\CPU|FSM|CO|Mux1~2_combout\) # (!\CPU|FSM|CO|Mux1~1_combout\)) # (\CPU|program_counter|register|out\(7)) ) ) ) # ( 
-- !\CPU|data_address|register|out\(7) & ( !\CPU|FSM|CO|Mux1~0_combout\ & ( (\CPU|program_counter|register|out\(7) & (\CPU|FSM|CO|Mux1~2_combout\ & \CPU|FSM|CO|Mux1~1_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101111111111111010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|program_counter|register|ALT_INV_out\(7),
	datac => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datad => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datae => \CPU|data_address|register|ALT_INV_out\(7),
	dataf => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	combout => \CPU|mem_addr[7]~8_combout\);

-- Location: MLABCELL_X59_Y4_N54
\read_data[5]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[5]~7_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a5\ & ( ((\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\))) # (\SW[5]~input_o\) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a5\ & ( (\SW[5]~input_o\ & 
-- ((!\rtl~26_combout\) # ((!\rtl~27_combout\) # (\CPU|mem_addr[8]~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001011000011110000101100001111010011110000111101001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[5]~input_o\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\,
	combout => \read_data[5]~7_combout\);

-- Location: FF_X59_Y4_N56
\CPU|InstructionRegister|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[5]~7_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(5));

-- Location: LABCELL_X63_Y2_N48
\CPU|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|Add0~25_sumout\ = SUM(( \CPU|program_counter|register|out\(5) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(5) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~22\ ))
-- \CPU|Add0~26\ = CARRY(( \CPU|program_counter|register|out\(5) ) + ( (\rtl~4_combout\ & (\CPU|InstructionRegister|register|out\(5) & ((\CPU|FSM|CO|Selector0~1_combout\) # (\CPU|FSM|CO|Selector0~0_combout\)))) ) + ( \CPU|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111110101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~4_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Selector0~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector0~1_combout\,
	datad => \CPU|program_counter|register|ALT_INV_out\(5),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(5),
	cin => \CPU|Add0~22\,
	sumout => \CPU|Add0~25_sumout\,
	cout => \CPU|Add0~26\);

-- Location: FF_X63_Y2_N49
\CPU|program_counter|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~25_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(5),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(5));

-- Location: FF_X63_Y2_N53
\CPU|program_counter|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~29_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(6),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(6));

-- Location: FF_X61_Y4_N23
\CPU|data_address|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(6),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(6));

-- Location: LABCELL_X61_Y4_N21
\CPU|mem_addr[6]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[6]~7_combout\ = ( \CPU|FSM|CO|Mux1~2_combout\ & ( (!\CPU|FSM|CO|Mux1~0_combout\ & ((!\CPU|FSM|CO|Mux1~1_combout\ & ((\CPU|data_address|register|out\(6)))) # (\CPU|FSM|CO|Mux1~1_combout\ & (\CPU|program_counter|register|out\(6))))) # 
-- (\CPU|FSM|CO|Mux1~0_combout\ & (((\CPU|program_counter|register|out\(6))))) ) ) # ( !\CPU|FSM|CO|Mux1~2_combout\ & ( (!\CPU|FSM|CO|Mux1~0_combout\ & ((\CPU|data_address|register|out\(6)))) # (\CPU|FSM|CO|Mux1~0_combout\ & 
-- (\CPU|program_counter|register|out\(6))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111000001011010111100000111100011110000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|program_counter|register|ALT_INV_out\(6),
	datad => \CPU|data_address|register|ALT_INV_out\(6),
	dataf => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	combout => \CPU|mem_addr[6]~7_combout\);

-- Location: LABCELL_X63_Y4_N42
\read_data[4]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[4]~14_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a4\ & ( ((\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\))) # (\SW[4]~input_o\) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a4\ & ( (\SW[4]~input_o\ & 
-- ((!\rtl~26_combout\) # ((!\rtl~27_combout\) # (\CPU|mem_addr[8]~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001011000011110000101100001111010011110000111101001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[4]~input_o\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\,
	combout => \read_data[4]~14_combout\);

-- Location: FF_X63_Y4_N44
\CPU|InstructionRegister|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[4]~14_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(4));

-- Location: FF_X66_Y4_N50
\CPU|DP|b|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(5),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(5));

-- Location: FF_X66_Y4_N37
\CPU|DP|b|register|out[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(6),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out[6]~DUPLICATE_q\);

-- Location: LABCELL_X64_Y4_N15
\CPU|DP|Bin[6]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[6]~10_combout\ = ( \CPU|DP|b|register|out[6]~DUPLICATE_q\ & ( \CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(7)))) ) ) ) # ( 
-- !\CPU|DP|b|register|out[6]~DUPLICATE_q\ & ( \CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\ & (\CPU|InstructionRegister|register|out\(4))) # (\CPU|DP|Bin[1]~3_combout\ & ((\CPU|DP|b|register|out\(7)))) ) ) ) # ( 
-- \CPU|DP|b|register|out[6]~DUPLICATE_q\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( (!\CPU|DP|Bin[1]~3_combout\) # (\CPU|DP|b|register|out\(5)) ) ) ) # ( !\CPU|DP|b|register|out[6]~DUPLICATE_q\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( (\CPU|DP|b|register|out\(5) & 
-- \CPU|DP|Bin[1]~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011111111110011001101010101000011110101010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datab => \CPU|DP|b|register|ALT_INV_out\(5),
	datac => \CPU|DP|b|register|ALT_INV_out\(7),
	datad => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	datae => \CPU|DP|b|register|ALT_INV_out[6]~DUPLICATE_q\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[6]~10_combout\);

-- Location: MLABCELL_X65_Y4_N12
\CPU|DP|ALU|plusMinus|ai|p[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|p\(6) = ( \CPU|DP|Bin[6]~10_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(6)))) ) ) # ( !\CPU|DP|Bin[6]~10_combout\ & ( 
-- !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|a|register|out\(6)) # (\CPU|FSM|CO|Selector7~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010110100101010101011010010110101010010110101010101001011010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(6),
	dataf => \CPU|DP|ALT_INV_Bin[6]~10_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|p\(6));

-- Location: LABCELL_X64_Y4_N42
\CPU|DP|ALU|out[6]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[6]~7_combout\ = ( \CPU|DP|Bin[6]~10_combout\ & ( (\CPU|DP|a|register|out\(6) & (!\CPU|FSM|CO|Selector7~0_combout\ & !\CPU|InstructionRegister|register|out\(11))) ) ) # ( !\CPU|DP|Bin[6]~10_combout\ & ( 
-- \CPU|InstructionRegister|register|out\(11) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111101000000010000000100000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|a|register|ALT_INV_out\(6),
	datab => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[6]~10_combout\,
	combout => \CPU|DP|ALU|out[6]~7_combout\);

-- Location: MLABCELL_X65_Y3_N54
\CPU|DP|ALU|out[6]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[6]~8_combout\ = ( \CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|p\(6))) # (\CPU|InstructionRegister|register|out\(12) & ((\CPU|DP|ALU|out[6]~7_combout\))) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|ai|comb~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|p\(6) $ ((!\CPU|DP|ALU|plusMinus|ai|g\(5))))) # (\CPU|InstructionRegister|register|out\(12) & (((\CPU|DP|ALU|out[6]~7_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010100001111101001010000111110110001000110111011000100011011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(6),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(5),
	datad => \CPU|DP|ALU|ALT_INV_out[6]~7_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~0_combout\,
	combout => \CPU|DP|ALU|out[6]~8_combout\);

-- Location: FF_X65_Y3_N56
\CPU|DP|c|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[6]~8_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(6));

-- Location: LABCELL_X60_Y4_N54
\CPU|DP|WBmux|m|b[6]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[6]~7_combout\ = ( \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|DP|Add0~25_sumout\ & (!\CPU|InstructionRegister|register|out\(6) & ((!\CPU|DP|c|register|out\(6)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\)))) ) ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|InstructionRegister|register|out\(6) & ((!\CPU|DP|c|register|out\(6)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\))) ) ) ) # ( \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|DP|Add0~25_sumout\ & ((!\CPU|DP|c|register|out\(6)) # (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\))) ) ) ) # ( 
-- !\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( (!\CPU|DP|c|register|out\(6)) # (!\CPU|DP|WBmux|d|ShiftLeft0~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111110101010110011001000100011110000101000001100000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(6),
	datab => \CPU|DP|ALT_INV_Add0~25_sumout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(6),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datae => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	combout => \CPU|DP|WBmux|m|b[6]~7_combout\);

-- Location: LABCELL_X61_Y2_N42
\CPU|DP|WBmux|m|b[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(6) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( \CPU|DP|WBmux|m|b[6]~7_combout\ & ( (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ((\e~combout\) # (\SW[6]~input_o\))) ) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( 
-- \CPU|DP|WBmux|m|b[6]~7_combout\ & ( (\SW[6]~input_o\ & (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & !\e~combout\)) ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( !\CPU|DP|WBmux|m|b[6]~7_combout\ ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a6\ 
-- & ( !\CPU|DP|WBmux|m|b[6]~7_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000101000000000000010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[6]~input_o\,
	datac => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datad => \ALT_INV_e~combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\,
	dataf => \CPU|DP|WBmux|m|ALT_INV_b[6]~7_combout\,
	combout => \CPU|DP|WBmux|m|b\(6));

-- Location: FF_X61_Y2_N50
\CPU|DP|REGFILE|Reg5|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(6));

-- Location: MLABCELL_X59_Y5_N42
\CPU|DP|REGFILE|Reg4|register|out[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg4|register|out[6]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(6) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(6),
	combout => \CPU|DP|REGFILE|Reg4|register|out[6]~feeder_combout\);

-- Location: FF_X59_Y5_N43
\CPU|DP|REGFILE|Reg4|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg4|register|out[6]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(6));

-- Location: LABCELL_X60_Y2_N36
\CPU|DP|REGFILE|Reg2|register|out[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg2|register|out[6]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(6) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(6),
	combout => \CPU|DP|REGFILE|Reg2|register|out[6]~feeder_combout\);

-- Location: FF_X60_Y2_N38
\CPU|DP|REGFILE|Reg2|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg2|register|out[6]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(6));

-- Location: FF_X62_Y2_N26
\CPU|DP|REGFILE|Reg0|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(6));

-- Location: LABCELL_X62_Y2_N24
\CPU|DP|REGFILE|readMux|m|b[6]~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[6]~21_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(6) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (((\CPU|DP|REGFILE|Reg2|register|out\(6) & \CPU|ID|RWmux|b\(2))))) # (\CPU|ID|RWmux|b\(1) & (((\CPU|ID|RWmux|b\(2))) # 
-- (\CPU|DP|REGFILE|Reg4|register|out\(6)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(6) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (((\CPU|DP|REGFILE|Reg2|register|out\(6) & \CPU|ID|RWmux|b\(2))))) # (\CPU|ID|RWmux|b\(1) & 
-- (\CPU|DP|REGFILE|Reg4|register|out\(6) & ((!\CPU|ID|RWmux|b\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101001100000000010100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(6),
	datab => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(6),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(6),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[6]~21_combout\);

-- Location: FF_X61_Y2_N20
\CPU|DP|REGFILE|Reg7|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(6));

-- Location: FF_X61_Y2_N35
\CPU|DP|REGFILE|Reg6|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(6));

-- Location: LABCELL_X61_Y2_N33
\CPU|DP|REGFILE|readMux|m|b[6]~23\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[6]~23_combout\ = ( \CPU|DP|REGFILE|Reg6|register|out\(6) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2)) ) ) ) # ( \CPU|DP|REGFILE|Reg6|register|out\(6) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(6) & (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2))) ) ) ) # ( !\CPU|DP|REGFILE|Reg6|register|out\(6) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg7|register|out\(6) & (!\CPU|ID|RWmux|b\(1) & !\CPU|ID|RWmux|b\(2))) ) 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000000000000010100000000000000000000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(6),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(6),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[6]~23_combout\);

-- Location: FF_X62_Y5_N38
\CPU|DP|REGFILE|Reg1|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(6));

-- Location: FF_X61_Y5_N37
\CPU|DP|REGFILE|Reg3|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(6),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(6));

-- Location: LABCELL_X62_Y5_N9
\CPU|DP|REGFILE|readMux|m|b[6]~22\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[6]~22_combout\ = ( \CPU|ID|RWmux|b\(2) & ( !\CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg3|register|out\(6)))) # (\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg1|register|out\(6))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000001011111010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(6),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(6),
	datae => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[6]~22_combout\);

-- Location: LABCELL_X63_Y2_N3
\CPU|DP|REGFILE|readMux|m|b[6]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(6) = ( \CPU|DP|REGFILE|readMux|m|b[6]~23_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[6]~22_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[6]~23_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[6]~22_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|m|b[6]~23_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[6]~22_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|m|b[6]~23_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[6]~22_combout\ & ( ((\CPU|DP|REGFILE|Reg5|register|out\(6) & 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[6]~21_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(6),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~21_combout\,
	datad => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~23_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[6]~22_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(6));

-- Location: FF_X66_Y4_N38
\CPU|DP|b|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(6),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(6));

-- Location: LABCELL_X66_Y4_N39
\CPU|DP|Bin[5]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[5]~9_combout\ = ( \CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(6) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|InstructionRegister|register|out\(4) ) ) ) # ( 
-- \CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(4) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(5) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100110011001100001111000011110101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(6),
	datab => \CPU|DP|b|register|ALT_INV_out\(4),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datad => \CPU|DP|b|register|ALT_INV_out\(5),
	datae => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[5]~9_combout\);

-- Location: MLABCELL_X65_Y3_N48
\CPU|DP|ALU|plusMinus|ai|c[5]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|c\(5) = ( \CPU|DP|ALU|plusMinus|ai|c\(3) & ( (!\CPU|DP|ALU|plusMinus|ai|g\(4) & ((!\CPU|DP|ALU|plusMinus|ai|p\(4)) # ((!\CPU|DP|ALU|plusMinus|ai|g\(3) & !\CPU|DP|ALU|plusMinus|ai|p\(3))))) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(3) & 
-- ( (!\CPU|DP|ALU|plusMinus|ai|g\(4) & ((!\CPU|DP|ALU|plusMinus|ai|g\(3)) # (!\CPU|DP|ALU|plusMinus|ai|p\(4)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010100010101000101010001010100010101000101000001010100010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(4),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(3),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(4),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(3),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(3),
	combout => \CPU|DP|ALU|plusMinus|ai|c\(5));

-- Location: MLABCELL_X65_Y3_N12
\CPU|DP|ALU|out[5]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[5]~6_combout\ = ( \CPU|DP|a|register|out\(5) & ( \CPU|DP|ALU|plusMinus|ai|c\(5) & ( (!\CPU|FSM|CO|Selector7~0_combout\ & (!\CPU|InstructionRegister|register|out\(12) $ (!\CPU|InstructionRegister|register|out\(11) $ 
-- (!\CPU|DP|Bin[5]~9_combout\)))) # (\CPU|FSM|CO|Selector7~0_combout\ & ((!\CPU|InstructionRegister|register|out\(11) & (!\CPU|InstructionRegister|register|out\(12) & \CPU|DP|Bin[5]~9_combout\)) # (\CPU|InstructionRegister|register|out\(11) & 
-- ((!\CPU|DP|Bin[5]~9_combout\))))) ) ) ) # ( !\CPU|DP|a|register|out\(5) & ( \CPU|DP|ALU|plusMinus|ai|c\(5) & ( (!\CPU|InstructionRegister|register|out\(11) & (!\CPU|InstructionRegister|register|out\(12) & \CPU|DP|Bin[5]~9_combout\)) # 
-- (\CPU|InstructionRegister|register|out\(11) & ((!\CPU|DP|Bin[5]~9_combout\))) ) ) ) # ( \CPU|DP|a|register|out\(5) & ( !\CPU|DP|ALU|plusMinus|ai|c\(5) & ( (!\CPU|FSM|CO|Selector7~0_combout\ & ((!\CPU|InstructionRegister|register|out\(11) $ 
-- (!\CPU|DP|Bin[5]~9_combout\)))) # (\CPU|FSM|CO|Selector7~0_combout\ & ((!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (\CPU|DP|Bin[5]~9_combout\))) # (\CPU|InstructionRegister|register|out\(12) & 
-- (\CPU|InstructionRegister|register|out\(11) & !\CPU|DP|Bin[5]~9_combout\)))) ) ) ) # ( !\CPU|DP|a|register|out\(5) & ( !\CPU|DP|ALU|plusMinus|ai|c\(5) & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ 
-- (\CPU|DP|Bin[5]~9_combout\))) # (\CPU|InstructionRegister|register|out\(12) & (\CPU|InstructionRegister|register|out\(11) & !\CPU|DP|Bin[5]~9_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100001100001100010010111010010000001111110000001000011101101000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|DP|ALT_INV_Bin[5]~9_combout\,
	datae => \CPU|DP|a|register|ALT_INV_out\(5),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(5),
	combout => \CPU|DP|ALU|out[5]~6_combout\);

-- Location: FF_X65_Y3_N14
\CPU|DP|c|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[5]~6_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(5));

-- Location: FF_X61_Y4_N38
\CPU|data_address|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(5),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(5));

-- Location: LABCELL_X61_Y4_N36
\CPU|mem_addr[5]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[5]~6_combout\ = ( \CPU|program_counter|register|out\(5) & ( (((\CPU|FSM|CO|Mux1~1_combout\ & \CPU|FSM|CO|Mux1~2_combout\)) # (\CPU|data_address|register|out\(5))) # (\CPU|FSM|CO|Mux1~0_combout\) ) ) # ( !\CPU|program_counter|register|out\(5) 
-- & ( (!\CPU|FSM|CO|Mux1~0_combout\ & (\CPU|data_address|register|out\(5) & ((!\CPU|FSM|CO|Mux1~1_combout\) # (!\CPU|FSM|CO|Mux1~2_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101000000000001010100001010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datad => \CPU|data_address|register|ALT_INV_out\(5),
	dataf => \CPU|program_counter|register|ALT_INV_out\(5),
	combout => \CPU|mem_addr[5]~6_combout\);

-- Location: LABCELL_X60_Y3_N27
\CPU|DP|WBmux|m|b[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[3]~4_combout\ = ( \CPU|DP|Add0~13_sumout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(3) & (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ((!\CPU|InstructionRegister|register|out\(3)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\)))) ) ) ) # ( !\CPU|DP|Add0~13_sumout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(3) & ((!\CPU|InstructionRegister|register|out\(3)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # 
-- ( \CPU|DP|Add0~13_sumout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ((!\CPU|InstructionRegister|register|out\(3)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # ( !\CPU|DP|Add0~13_sumout\ & ( 
-- !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|InstructionRegister|register|out\(3)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111100111111000000000010101000101010001010100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(3),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(3),
	datac => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	datae => \CPU|DP|ALT_INV_Add0~13_sumout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	combout => \CPU|DP|WBmux|m|b[3]~4_combout\);

-- Location: LABCELL_X60_Y3_N18
\CPU|DP|WBmux|m|b[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(3) = ( \CPU|DP|WBmux|m|b[3]~4_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ( (!\e~combout\ & (\SW[3]~input_o\)) # (\e~combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a3\))) ) ) ) # ( !\CPU|DP|WBmux|m|b[3]~4_combout\ & ( 
-- \CPU|DP|WBmux|d|ShiftLeft0~1_combout\ ) ) # ( !\CPU|DP|WBmux|m|b[3]~4_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111110010011100100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_e~combout\,
	datab => \ALT_INV_SW[3]~input_o\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\,
	datae => \CPU|DP|WBmux|m|ALT_INV_b[3]~4_combout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	combout => \CPU|DP|WBmux|m|b\(3));

-- Location: FF_X60_Y3_N28
\CPU|DP|REGFILE|Reg5|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(3));

-- Location: FF_X59_Y5_N37
\CPU|DP|REGFILE|Reg4|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(3));

-- Location: FF_X59_Y3_N50
\CPU|DP|REGFILE|Reg2|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(3));

-- Location: FF_X63_Y3_N14
\CPU|DP|REGFILE|Reg0|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(3));

-- Location: LABCELL_X63_Y3_N12
\CPU|DP|REGFILE|readMux|m|b[3]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[3]~12_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(3) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (((\CPU|DP|REGFILE|Reg2|register|out\(3) & \CPU|ID|RWmux|b\(2))))) # (\CPU|ID|RWmux|b\(1) & (((\CPU|ID|RWmux|b\(2))) # 
-- (\CPU|DP|REGFILE|Reg4|register|out\(3)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(3) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (((\CPU|DP|REGFILE|Reg2|register|out\(3) & \CPU|ID|RWmux|b\(2))))) # (\CPU|ID|RWmux|b\(1) & 
-- (\CPU|DP|REGFILE|Reg4|register|out\(3) & ((!\CPU|ID|RWmux|b\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000101001100000000010100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(3),
	datab => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(3),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(3),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[3]~12_combout\);

-- Location: LABCELL_X60_Y5_N45
\CPU|DP|REGFILE|Reg3|register|out[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[3]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(3) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(3),
	combout => \CPU|DP|REGFILE|Reg3|register|out[3]~feeder_combout\);

-- Location: FF_X60_Y5_N46
\CPU|DP|REGFILE|Reg3|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[3]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(3));

-- Location: FF_X63_Y3_N44
\CPU|DP|REGFILE|Reg1|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(3));

-- Location: LABCELL_X63_Y3_N3
\CPU|DP|REGFILE|readMux|m|b[3]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[3]~13_combout\ = ( \CPU|ID|RWmux|b\(2) & ( !\CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg3|register|out\(3))) # (\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg1|register|out\(3)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(3),
	datac => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(3),
	datad => \CPU|ID|RWmux|ALT_INV_b\(1),
	datae => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[3]~13_combout\);

-- Location: FF_X60_Y3_N8
\CPU|DP|REGFILE|Reg7|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(3));

-- Location: FF_X60_Y3_N46
\CPU|DP|REGFILE|Reg6|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(3),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(3));

-- Location: LABCELL_X63_Y3_N6
\CPU|DP|REGFILE|readMux|m|b[3]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[3]~14_combout\ = ( !\CPU|ID|RWmux|b\(2) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & \CPU|DP|REGFILE|Reg6|register|out\(3)) ) ) ) # ( !\CPU|ID|RWmux|b\(2) & ( !\CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & 
-- \CPU|DP|REGFILE|Reg7|register|out\(3)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001000100010000000000000000000001010000010100000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(1),
	datab => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(3),
	datac => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(3),
	datae => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[3]~14_combout\);

-- Location: LABCELL_X63_Y3_N33
\CPU|DP|REGFILE|readMux|m|b[3]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(3) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[3]~14_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[3]~14_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[3]~14_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[3]~13_combout\) # (\CPU|DP|REGFILE|readMux|m|b[3]~12_combout\)) # (\CPU|DP|REGFILE|Reg5|register|out\(3)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[3]~14_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[3]~13_combout\) # (\CPU|DP|REGFILE|readMux|m|b[3]~12_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111010111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(3),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~12_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~13_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[3]~14_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(3));

-- Location: FF_X63_Y4_N32
\CPU|DP|b|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(3),
	sload => VCC,
	ena => \CPU|FSM|CO|loadb~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|b|register|out\(3));

-- Location: LABCELL_X66_Y4_N51
\CPU|DP|Bin[4]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[4]~8_combout\ = ( \CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(5) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( \CPU|DP|Bin[1]~4_combout\ & ( \CPU|InstructionRegister|register|out\(4) ) ) ) # ( 
-- \CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(3) ) ) ) # ( !\CPU|DP|Bin[1]~3_combout\ & ( !\CPU|DP|Bin[1]~4_combout\ & ( \CPU|DP|b|register|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111010101010101010100110011001100110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|b|register|ALT_INV_out\(3),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(4),
	datac => \CPU|DP|b|register|ALT_INV_out\(4),
	datad => \CPU|DP|b|register|ALT_INV_out\(5),
	datae => \CPU|DP|ALT_INV_Bin[1]~3_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~4_combout\,
	combout => \CPU|DP|Bin[4]~8_combout\);

-- Location: LABCELL_X66_Y4_N6
\CPU|DP|ALU|out[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[4]~4_combout\ = ( \CPU|FSM|CO|Selector7~0_combout\ & ( (\CPU|InstructionRegister|register|out\(11) & !\CPU|DP|Bin[4]~8_combout\) ) ) # ( !\CPU|FSM|CO|Selector7~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & 
-- (\CPU|DP|Bin[4]~8_combout\ & \CPU|DP|a|register|out\(4))) # (\CPU|InstructionRegister|register|out\(11) & (!\CPU|DP|Bin[4]~8_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101000001011010010100000101101001010000010100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Bin[4]~8_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(4),
	dataf => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	combout => \CPU|DP|ALU|out[4]~4_combout\);

-- Location: MLABCELL_X65_Y3_N42
\CPU|DP|ALU|out[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[4]~5_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(3) & ( \CPU|DP|ALU|plusMinus|ai|g\(3) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|ai|p\(4)))) # (\CPU|InstructionRegister|register|out\(12) & 
-- (\CPU|DP|ALU|out[4]~4_combout\)) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(3) & ( \CPU|DP|ALU|plusMinus|ai|g\(3) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|ai|p\(4)))) # (\CPU|InstructionRegister|register|out\(12) & 
-- (\CPU|DP|ALU|out[4]~4_combout\)) ) ) ) # ( \CPU|DP|ALU|plusMinus|ai|c\(3) & ( !\CPU|DP|ALU|plusMinus|ai|g\(3) & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|ai|p\(4) $ (!\CPU|DP|ALU|plusMinus|ai|p\(3))))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[4]~4_combout\)) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(3) & ( !\CPU|DP|ALU|plusMinus|ai|g\(3) & ( (!\CPU|InstructionRegister|register|out\(12) & ((\CPU|DP|ALU|plusMinus|ai|p\(4)))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|ALU|out[4]~4_combout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001101100011011000110111011000110110001101100011011000110110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|DP|ALU|ALT_INV_out[4]~4_combout\,
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(4),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(3),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(3),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(3),
	combout => \CPU|DP|ALU|out[4]~5_combout\);

-- Location: FF_X65_Y3_N44
\CPU|DP|c|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[4]~5_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(4));

-- Location: FF_X61_Y4_N20
\CPU|data_address|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(4),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(4));

-- Location: LABCELL_X61_Y4_N18
\CPU|mem_addr[4]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[4]~5_combout\ = ( \CPU|program_counter|register|out\(4) & ( (((\CPU|FSM|CO|Mux1~1_combout\ & \CPU|FSM|CO|Mux1~2_combout\)) # (\CPU|data_address|register|out\(4))) # (\CPU|FSM|CO|Mux1~0_combout\) ) ) # ( !\CPU|program_counter|register|out\(4) 
-- & ( (!\CPU|FSM|CO|Mux1~0_combout\ & (\CPU|data_address|register|out\(4) & ((!\CPU|FSM|CO|Mux1~1_combout\) # (!\CPU|FSM|CO|Mux1~2_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101000000000001010100001010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datad => \CPU|data_address|register|ALT_INV_out\(4),
	dataf => \CPU|program_counter|register|ALT_INV_out\(4),
	combout => \CPU|mem_addr[4]~5_combout\);

-- Location: MLABCELL_X59_Y4_N57
\read_data[7]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[7]~6_combout\ = ( \rtl~27_combout\ & ( (!\rtl~26_combout\ & (((\SW[7]~input_o\)))) # (\rtl~26_combout\ & ((!\CPU|mem_addr[8]~0_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a7\))) # (\CPU|mem_addr[8]~0_combout\ & (\SW[7]~input_o\)))) ) ) 
-- # ( !\rtl~27_combout\ & ( \SW[7]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001011010011110000101101001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[7]~input_o\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\,
	dataf => \ALT_INV_rtl~27_combout\,
	combout => \read_data[7]~6_combout\);

-- Location: FF_X59_Y4_N59
\CPU|InstructionRegister|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[7]~6_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(7));

-- Location: LABCELL_X61_Y4_N0
\CPU|DP|WBmux|m|comb~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|comb~0_combout\ = ( \CPU|DP|WBmux|d|ShiftLeft0~3_combout\ & ( \CPU|InstructionRegister|register|out\(7) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(7),
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	combout => \CPU|DP|WBmux|m|comb~0_combout\);

-- Location: LABCELL_X61_Y3_N54
\CPU|DP|WBmux|m|b[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(11) = ( \read_data[11]~3_combout\ & ( (((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(11))) # (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\)) # (\CPU|DP|WBmux|m|comb~0_combout\) ) ) # ( !\read_data[11]~3_combout\ & ( 
-- ((\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & \CPU|DP|c|register|out\(11))) # (\CPU|DP|WBmux|m|comb~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101011101010111010101110101011101010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|WBmux|m|ALT_INV_comb~0_combout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(11),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	dataf => \ALT_INV_read_data[11]~3_combout\,
	combout => \CPU|DP|WBmux|m|b\(11));

-- Location: FF_X62_Y3_N20
\CPU|DP|REGFILE|Reg5|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(11));

-- Location: FF_X62_Y2_N7
\CPU|DP|REGFILE|Reg1|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(11));

-- Location: LABCELL_X62_Y1_N6
\CPU|DP|REGFILE|Reg3|register|out[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg3|register|out[11]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(11) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(11),
	combout => \CPU|DP|REGFILE|Reg3|register|out[11]~feeder_combout\);

-- Location: FF_X62_Y1_N7
\CPU|DP|REGFILE|Reg3|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg3|register|out[11]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(11));

-- Location: LABCELL_X62_Y2_N54
\CPU|DP|REGFILE|readMux|m|b[11]~34\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[11]~34_combout\ = ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((!\CPU|ID|RWmux|b\(1) & ((\CPU|DP|REGFILE|Reg3|register|out\(11)))) # (\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg1|register|out\(11))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110101000000000011010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(11),
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(11),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[11]~34_combout\);

-- Location: FF_X62_Y2_N58
\CPU|DP|REGFILE|Reg4|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(11));

-- Location: FF_X59_Y3_N59
\CPU|DP|REGFILE|Reg2|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(11));

-- Location: FF_X62_Y2_N22
\CPU|DP|REGFILE|Reg0|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(11));

-- Location: LABCELL_X62_Y3_N48
\CPU|DP|REGFILE|readMux|m|b[11]~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[11]~33_combout\ = ( \CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (\CPU|DP|REGFILE|Reg4|register|out\(11))) # (\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg0|register|out\(11)))) ) ) ) # ( 
-- !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (\CPU|DP|REGFILE|Reg2|register|out\(11) & \CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000001100110101010100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(11),
	datab => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(11),
	datac => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(11),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[11]~33_combout\);

-- Location: FF_X61_Y3_N56
\CPU|DP|REGFILE|Reg7|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(11),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(11));

-- Location: FF_X61_Y3_N32
\CPU|DP|REGFILE|Reg6|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(11),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(11));

-- Location: LABCELL_X61_Y3_N30
\CPU|DP|REGFILE|readMux|m|b[11]~35\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[11]~35_combout\ = ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (!\CPU|ID|RWmux|b\(1) & \CPU|DP|REGFILE|Reg6|register|out\(11))) ) ) # ( !\CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & (!\CPU|ID|RWmux|b\(1) & 
-- \CPU|DP|REGFILE|Reg7|register|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100000000000100010000000000010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(2),
	datab => \CPU|ID|RWmux|ALT_INV_b\(1),
	datac => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(11),
	datad => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(11),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[11]~35_combout\);

-- Location: LABCELL_X62_Y3_N36
\CPU|DP|REGFILE|readMux|m|b[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(11) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[11]~35_combout\ ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|REGFILE|readMux|m|b[11]~35_combout\ ) ) # ( 
-- \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[11]~35_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[11]~33_combout\) # (\CPU|DP|REGFILE|readMux|m|b[11]~34_combout\)) # (\CPU|DP|REGFILE|Reg5|register|out\(11)) ) ) ) # ( 
-- !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|REGFILE|readMux|m|b[11]~35_combout\ & ( (\CPU|DP|REGFILE|readMux|m|b[11]~33_combout\) # (\CPU|DP|REGFILE|readMux|m|b[11]~34_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111010111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(11),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~34_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~33_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[11]~35_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(11));

-- Location: FF_X62_Y3_N43
\CPU|DP|a|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(11),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(11));

-- Location: LABCELL_X64_Y3_N54
\CPU|DP|ALU|plusMinus|ai|g[11]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|g\(11) = ( \CPU|DP|Bin[11]~15_combout\ & ( (\CPU|DP|a|register|out\(11) & (!\CPU|InstructionRegister|register|out\(11) & !\CPU|FSM|CO|Selector7~0_combout\)) ) ) # ( !\CPU|DP|Bin[11]~15_combout\ & ( (\CPU|DP|a|register|out\(11) & 
-- (\CPU|InstructionRegister|register|out\(11) & !\CPU|FSM|CO|Selector7~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000000000001010000000001010000000000000101000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|a|register|ALT_INV_out\(11),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[11]~15_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|g\(11));

-- Location: LABCELL_X66_Y3_N33
\CPU|DP|ALU|out[14]~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[14]~26_combout\ = ( \CPU|DP|ALU|plusMinus|ai|p\(11) & ( (!\CPU|DP|Ain[12]~3_combout\ & ((!\CPU|DP|ALU|plusMinus|comb~0_combout\) # ((!\CPU|DP|ALU|plusMinus|ai|g\(11) & !\CPU|DP|ALU|plusMinus|ai|g\(10))))) # (\CPU|DP|Ain[12]~3_combout\ & 
-- (!\CPU|DP|ALU|plusMinus|ai|g\(11) & (!\CPU|DP|ALU|plusMinus|comb~0_combout\ & !\CPU|DP|ALU|plusMinus|ai|g\(10)))) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|p\(11) & ( (!\CPU|DP|ALU|plusMinus|ai|g\(11) & ((!\CPU|DP|Ain[12]~3_combout\) # 
-- (!\CPU|DP|ALU|plusMinus|comb~0_combout\))) # (\CPU|DP|ALU|plusMinus|ai|g\(11) & (!\CPU|DP|Ain[12]~3_combout\ & !\CPU|DP|ALU|plusMinus|comb~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110100011101000111010001110100011101000110000001110100011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(11),
	datab => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	datac => \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\,
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(10),
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	combout => \CPU|DP|ALU|out[14]~26_combout\);

-- Location: LABCELL_X66_Y4_N9
\CPU|DP|ALU|out[14]~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[14]~25_combout\ = ( \CPU|DP|Bin[14]~18_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|comb~1_combout\ $ (\CPU|DP|Ain[14]~4_combout\)))) # (\CPU|InstructionRegister|register|out\(12) & 
-- (!\CPU|InstructionRegister|register|out\(11) & ((\CPU|DP|Ain[14]~4_combout\)))) ) ) # ( !\CPU|DP|Bin[14]~18_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|plusMinus|comb~1_combout\ $ (\CPU|DP|Ain[14]~4_combout\)))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (\CPU|InstructionRegister|register|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100010100110101110001010011010111000000001110101100000000111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datab => \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datad => \CPU|DP|ALT_INV_Ain[14]~4_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[14]~18_combout\,
	combout => \CPU|DP|ALU|out[14]~25_combout\);

-- Location: LABCELL_X66_Y3_N54
\CPU|DP|ALU|out[14]~27\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[14]~27_combout\ = ( \CPU|DP|ALU|out[14]~26_combout\ & ( (\CPU|DP|ALU|plusMinus|ai|p\(11) & (\CPU|DP|ALU|plusMinus|ai|p\(13) & ((\CPU|DP|ALU|plusMinus|comb~0_combout\) # (\CPU|DP|Ain[12]~3_combout\)))) ) ) # ( 
-- !\CPU|DP|ALU|out[14]~26_combout\ & ( (\CPU|DP|ALU|plusMinus|ai|p\(13) & (((!\CPU|DP|ALU|plusMinus|ai|p\(11)) # (\CPU|DP|ALU|plusMinus|comb~0_combout\)) # (\CPU|DP|Ain[12]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110100001111000011010000111100000001000000110000000100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALT_INV_Ain[12]~3_combout\,
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(11),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(13),
	datad => \CPU|DP|ALU|plusMinus|ALT_INV_comb~0_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_out[14]~26_combout\,
	combout => \CPU|DP|ALU|out[14]~27_combout\);

-- Location: LABCELL_X66_Y3_N24
\CPU|DP|ALU|out[14]~19\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[14]~19_combout\ = ( \CPU|DP|ALU|out[14]~27_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( \CPU|DP|ALU|out[14]~25_combout\ ) ) ) # ( !\CPU|DP|ALU|out[14]~27_combout\ & ( \CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( 
-- !\CPU|DP|ALU|out[14]~25_combout\ $ (((\CPU|DP|ALU|plusMinus|ai|g\(13)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) ) # ( \CPU|DP|ALU|out[14]~27_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|ALU|out[14]~25_combout\ $ 
-- (((!\CPU|DP|ALU|out[14]~26_combout\) # ((\CPU|DP|ALU|plusMinus|ai|g\(13)) # (\CPU|InstructionRegister|register|out\(12))))) ) ) ) # ( !\CPU|DP|ALU|out[14]~27_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|comb~1_combout\ & ( !\CPU|DP|ALU|out[14]~25_combout\ $ 
-- (((\CPU|DP|ALU|plusMinus|ai|g\(13)) # (\CPU|InstructionRegister|register|out\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000111111010000001011111111000000001111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[14]~26_combout\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(13),
	datad => \CPU|DP|ALU|ALT_INV_out[14]~25_combout\,
	datae => \CPU|DP|ALU|ALT_INV_out[14]~27_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_comb~1_combout\,
	combout => \CPU|DP|ALU|out[14]~19_combout\);

-- Location: LABCELL_X66_Y3_N48
\CPU|DP|ALU|plusMinus|as|s[0]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|as|s\(0) = ( \CPU|DP|ALU|plusMinus|ai|c\(13) & ( \CPU|DP|Ain[14]~4_combout\ & ( !\CPU|DP|ALU|plusMinus|as|p\(0) $ (((!\CPU|DP|ALU|plusMinus|comb~1_combout\ & !\CPU|DP|ALU|plusMinus|ai|g\(13)))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|ai|c\(13) & ( \CPU|DP|Ain[14]~4_combout\ & ( !\CPU|DP|ALU|plusMinus|as|p\(0) $ (((!\CPU|DP|ALU|plusMinus|comb~1_combout\ & (!\CPU|DP|ALU|plusMinus|ai|g\(13) & !\CPU|DP|ALU|plusMinus|ai|p\(13))))) ) ) ) # ( 
-- \CPU|DP|ALU|plusMinus|ai|c\(13) & ( !\CPU|DP|Ain[14]~4_combout\ & ( !\CPU|DP|ALU|plusMinus|as|p\(0) $ (((!\CPU|DP|ALU|plusMinus|comb~1_combout\) # (!\CPU|DP|ALU|plusMinus|ai|g\(13)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(13) & ( 
-- !\CPU|DP|Ain[14]~4_combout\ & ( !\CPU|DP|ALU|plusMinus|as|p\(0) $ (((!\CPU|DP|ALU|plusMinus|comb~1_combout\) # ((!\CPU|DP|ALU|plusMinus|ai|g\(13) & !\CPU|DP|ALU|plusMinus|ai|p\(13))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011001100110001101100011011001101100110011000110110001101100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\,
	datab => \CPU|DP|ALU|plusMinus|as|ALT_INV_p\(0),
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(13),
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(13),
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(13),
	dataf => \CPU|DP|ALT_INV_Ain[14]~4_combout\,
	combout => \CPU|DP|ALU|plusMinus|as|s\(0));

-- Location: MLABCELL_X65_Y3_N24
\CPU|DP|ALU|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|WideOr0~0_combout\ = ( !\CPU|DP|ALU|out[3]~3_combout\ & ( (!\CPU|DP|ALU|out[1]~1_combout\ & (!\CPU|DP|ALU|out[0]~0_combout\ & !\CPU|DP|ALU|out[2]~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[1]~1_combout\,
	datac => \CPU|DP|ALU|ALT_INV_out[0]~0_combout\,
	datad => \CPU|DP|ALU|ALT_INV_out[2]~2_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_out[3]~3_combout\,
	combout => \CPU|DP|ALU|WideOr0~0_combout\);

-- Location: MLABCELL_X65_Y3_N27
\CPU|DP|ALU|WideOr0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|WideOr0~1_combout\ = ( \CPU|DP|ALU|WideOr0~0_combout\ & ( (!\CPU|DP|ALU|out[4]~5_combout\ & (!\CPU|DP|ALU|out[5]~6_combout\ & !\CPU|DP|ALU|out[6]~8_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000000000001100000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALU|ALT_INV_out[4]~5_combout\,
	datac => \CPU|DP|ALU|ALT_INV_out[5]~6_combout\,
	datad => \CPU|DP|ALU|ALT_INV_out[6]~8_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_WideOr0~0_combout\,
	combout => \CPU|DP|ALU|WideOr0~1_combout\);

-- Location: MLABCELL_X65_Y3_N30
\CPU|DP|ALU|WideOr0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|WideOr0~2_combout\ = ( \CPU|DP|ALU|WideOr0~1_combout\ & ( (!\CPU|DP|ALU|out[7]~9_combout\ & (!\CPU|DP|ALU|out[9]~12_combout\ & !\CPU|DP|ALU|out[8]~10_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010100000000000001010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[7]~9_combout\,
	datac => \CPU|DP|ALU|ALT_INV_out[9]~12_combout\,
	datad => \CPU|DP|ALU|ALT_INV_out[8]~10_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_WideOr0~1_combout\,
	combout => \CPU|DP|ALU|WideOr0~2_combout\);

-- Location: MLABCELL_X65_Y3_N18
\CPU|DP|ALU|WideOr0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|WideOr0~3_combout\ = ( !\CPU|DP|ALU|out[10]~13_combout\ & ( \CPU|DP|ALU|WideOr0~2_combout\ & ( (!\CPU|DP|ALU|out[11]~15_combout\ & ((!\CPU|InstructionRegister|register|out\(12) & (!\CPU|DP|ALU|plusMinus|ai|s\(12))) # 
-- (\CPU|InstructionRegister|register|out\(12) & ((!\CPU|DP|ALU|out[12]~16_combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011010000100000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datab => \CPU|DP|ALU|plusMinus|ai|ALT_INV_s\(12),
	datac => \CPU|DP|ALU|ALT_INV_out[11]~15_combout\,
	datad => \CPU|DP|ALU|ALT_INV_out[12]~16_combout\,
	datae => \CPU|DP|ALU|ALT_INV_out[10]~13_combout\,
	dataf => \CPU|DP|ALU|ALT_INV_WideOr0~2_combout\,
	combout => \CPU|DP|ALU|WideOr0~3_combout\);

-- Location: MLABCELL_X65_Y3_N6
\CPU|DP|ALU|WideOr0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|WideOr0~combout\ = ( \CPU|DP|ALU|plusMinus|as|s\(0) & ( \CPU|DP|ALU|WideOr0~3_combout\ & ( (!\CPU|DP|ALU|out[14]~19_combout\ & (!\CPU|DP|ALU|out[15]~20_combout\ & (!\CPU|DP|ALU|out[13]~18_combout\ & 
-- \CPU|InstructionRegister|register|out\(12)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|as|s\(0) & ( \CPU|DP|ALU|WideOr0~3_combout\ & ( (!\CPU|DP|ALU|out[14]~19_combout\ & (!\CPU|DP|ALU|out[13]~18_combout\ & ((!\CPU|DP|ALU|out[15]~20_combout\) # 
-- (!\CPU|InstructionRegister|register|out\(12))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010100000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|ALT_INV_out[14]~19_combout\,
	datab => \CPU|DP|ALU|ALT_INV_out[15]~20_combout\,
	datac => \CPU|DP|ALU|ALT_INV_out[13]~18_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datae => \CPU|DP|ALU|plusMinus|as|ALT_INV_s\(0),
	dataf => \CPU|DP|ALU|ALT_INV_WideOr0~3_combout\,
	combout => \CPU|DP|ALU|WideOr0~combout\);

-- Location: MLABCELL_X65_Y3_N51
\CPU|FSM|CO|Selector12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector12~0_combout\ = ( !\CPU|InstructionRegister|register|out\(12) & ( (\CPU|FSM|CO|Decoder0~1_combout\ & \CPU|InstructionRegister|register|out\(11)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	combout => \CPU|FSM|CO|Selector12~0_combout\);

-- Location: FF_X65_Y3_N8
\CPU|DP|status|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|WideOr0~combout\,
	ena => \CPU|FSM|CO|Selector12~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|register|out\(0));

-- Location: LABCELL_X66_Y3_N57
\CPU|DP|ALU|plusMinus|ovf~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ovf~2_combout\ = ( \CPU|DP|Ain[14]~4_combout\ & ( \CPU|DP|ALU|plusMinus|comb~1_combout\ ) ) # ( !\CPU|DP|Ain[14]~4_combout\ & ( !\CPU|DP|ALU|plusMinus|comb~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\,
	dataf => \CPU|DP|ALT_INV_Ain[14]~4_combout\,
	combout => \CPU|DP|ALU|plusMinus|ovf~2_combout\);

-- Location: LABCELL_X66_Y3_N36
\CPU|DP|ALU|plusMinus|ovf~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ovf~1_combout\ = ( \CPU|DP|ALU|plusMinus|comb~1_combout\ & ( \CPU|DP|ALU|plusMinus|ai|g\(13) & ( (!\CPU|DP|ALU|plusMinus|comb~2_combout\ & ((!\CPU|DP|a|register|out\(15)) # (\CPU|FSM|CO|Selector7~0_combout\))) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|comb~1_combout\ & ( \CPU|DP|ALU|plusMinus|ai|g\(13) & ( (!\CPU|DP|ALU|plusMinus|comb~2_combout\ & (\CPU|DP|Ain[14]~4_combout\ & ((!\CPU|DP|a|register|out\(15)) # (\CPU|FSM|CO|Selector7~0_combout\)))) # 
-- (\CPU|DP|ALU|plusMinus|comb~2_combout\ & (!\CPU|DP|Ain[14]~4_combout\ & (!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(15)))) ) ) ) # ( \CPU|DP|ALU|plusMinus|comb~1_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(13) & ( 
-- (!\CPU|DP|ALU|plusMinus|comb~2_combout\ & (((!\CPU|DP|a|register|out\(15)) # (\CPU|FSM|CO|Selector7~0_combout\)))) # (\CPU|DP|ALU|plusMinus|comb~2_combout\ & (!\CPU|DP|Ain[14]~4_combout\ & (!\CPU|FSM|CO|Selector7~0_combout\ & 
-- \CPU|DP|a|register|out\(15)))) ) ) ) # ( !\CPU|DP|ALU|plusMinus|comb~1_combout\ & ( !\CPU|DP|ALU|plusMinus|ai|g\(13) & ( (!\CPU|DP|ALU|plusMinus|comb~2_combout\ & (\CPU|DP|Ain[14]~4_combout\ & ((!\CPU|DP|a|register|out\(15)) # 
-- (\CPU|FSM|CO|Selector7~0_combout\)))) # (\CPU|DP|ALU|plusMinus|comb~2_combout\ & (((!\CPU|FSM|CO|Selector7~0_combout\ & \CPU|DP|a|register|out\(15))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010001001010010101010100100101000100010010000101010101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ALT_INV_comb~2_combout\,
	datab => \CPU|DP|ALT_INV_Ain[14]~4_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector7~0_combout\,
	datad => \CPU|DP|a|register|ALT_INV_out\(15),
	datae => \CPU|DP|ALU|plusMinus|ALT_INV_comb~1_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(13),
	combout => \CPU|DP|ALU|plusMinus|ovf~1_combout\);

-- Location: LABCELL_X66_Y3_N12
\CPU|DP|ALU|plusMinus|ovf~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ovf~0_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(13) & ( \CPU|DP|ALU|plusMinus|ovf~1_combout\ & ( ((\CPU|DP|ALU|plusMinus|comb~2_combout\) # (\CPU|DP|ALU|plusMinus|ai|g\(13))) # (\CPU|DP|ALU|plusMinus|ovf~2_combout\) ) ) ) # ( 
-- !\CPU|DP|ALU|plusMinus|ai|c\(13) & ( \CPU|DP|ALU|plusMinus|ovf~1_combout\ & ( ((!\CPU|DP|ALU|plusMinus|ai|p\(13) & ((\CPU|DP|ALU|plusMinus|comb~2_combout\) # (\CPU|DP|ALU|plusMinus|ai|g\(13)))) # (\CPU|DP|ALU|plusMinus|ai|p\(13) & 
-- ((!\CPU|DP|ALU|plusMinus|comb~2_combout\)))) # (\CPU|DP|ALU|plusMinus|ovf~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001111111101110110011111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|ALU|plusMinus|ai|ALT_INV_p\(13),
	datab => \CPU|DP|ALU|plusMinus|ALT_INV_ovf~2_combout\,
	datac => \CPU|DP|ALU|plusMinus|ai|ALT_INV_g\(13),
	datad => \CPU|DP|ALU|plusMinus|ALT_INV_comb~2_combout\,
	datae => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(13),
	dataf => \CPU|DP|ALU|plusMinus|ALT_INV_ovf~1_combout\,
	combout => \CPU|DP|ALU|plusMinus|ovf~0_combout\);

-- Location: FF_X66_Y3_N13
\CPU|DP|status|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|plusMinus|ovf~0_combout\,
	ena => \CPU|FSM|CO|Selector12~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|register|out\(2));

-- Location: FF_X66_Y3_N16
\CPU|DP|status|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|ALU|out[15]~21_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Selector12~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|status|register|out\(1));

-- Location: MLABCELL_X59_Y3_N18
\CPU|FSM|CO|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux0~0_combout\ = ( \CPU|DP|status|register|out\(2) & ( \CPU|DP|status|register|out\(1) & ( (!\CPU|DP|status|register|out\(0) & (((!\CPU|InstructionRegister|register|out\(8) & !\CPU|InstructionRegister|register|out\(10))))) # 
-- (\CPU|DP|status|register|out\(0) & (!\CPU|InstructionRegister|register|out\(9) & ((!\CPU|InstructionRegister|register|out\(8)) # (!\CPU|InstructionRegister|register|out\(10))))) ) ) ) # ( !\CPU|DP|status|register|out\(2) & ( 
-- \CPU|DP|status|register|out\(1) & ( (!\CPU|DP|status|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(9) & (!\CPU|InstructionRegister|register|out\(8))) # (\CPU|InstructionRegister|register|out\(9) & 
-- ((!\CPU|InstructionRegister|register|out\(10)))))) # (\CPU|DP|status|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(8) & (!\CPU|InstructionRegister|register|out\(9))) # (\CPU|InstructionRegister|register|out\(8) & 
-- ((!\CPU|InstructionRegister|register|out\(10)))))) ) ) ) # ( \CPU|DP|status|register|out\(2) & ( !\CPU|DP|status|register|out\(1) & ( (!\CPU|DP|status|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(9) & 
-- (!\CPU|InstructionRegister|register|out\(8))) # (\CPU|InstructionRegister|register|out\(9) & ((!\CPU|InstructionRegister|register|out\(10)))))) # (\CPU|DP|status|register|out\(0) & ((!\CPU|InstructionRegister|register|out\(8) & 
-- (!\CPU|InstructionRegister|register|out\(9))) # (\CPU|InstructionRegister|register|out\(8) & ((!\CPU|InstructionRegister|register|out\(10)))))) ) ) ) # ( !\CPU|DP|status|register|out\(2) & ( !\CPU|DP|status|register|out\(1) & ( 
-- (!\CPU|DP|status|register|out\(0) & (((!\CPU|InstructionRegister|register|out\(8) & !\CPU|InstructionRegister|register|out\(10))))) # (\CPU|DP|status|register|out\(0) & (!\CPU|InstructionRegister|register|out\(9) & 
-- ((!\CPU|InstructionRegister|register|out\(8)) # (!\CPU|InstructionRegister|register|out\(10))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110010001000000111001111100000011100111110000001110010001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|status|register|ALT_INV_out\(0),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(9),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(8),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(10),
	datae => \CPU|DP|status|register|ALT_INV_out\(2),
	dataf => \CPU|DP|status|register|ALT_INV_out\(1),
	combout => \CPU|FSM|CO|Mux0~0_combout\);

-- Location: LABCELL_X60_Y2_N33
\CPU|FSM|CO|Selector0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector0~1_combout\ = ( \CPU|FSM|CO|Mux0~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(14) & (\CPU|InstructionRegister|register|out\(13) & !\CPU|InstructionRegister|register|out\(15))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011000000000000000000000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datae => \CPU|FSM|CO|ALT_INV_Mux0~0_combout\,
	combout => \CPU|FSM|CO|Selector0~1_combout\);

-- Location: FF_X63_Y2_N44
\CPU|program_counter|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~17_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(3),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(3));

-- Location: FF_X61_Y4_N59
\CPU|data_address|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(3),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(3));

-- Location: LABCELL_X61_Y4_N57
\CPU|mem_addr[3]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[3]~4_combout\ = ( \CPU|FSM|CO|Mux1~2_combout\ & ( (!\CPU|FSM|CO|Mux1~0_combout\ & ((!\CPU|FSM|CO|Mux1~1_combout\ & ((\CPU|data_address|register|out\(3)))) # (\CPU|FSM|CO|Mux1~1_combout\ & (\CPU|program_counter|register|out\(3))))) # 
-- (\CPU|FSM|CO|Mux1~0_combout\ & (((\CPU|program_counter|register|out\(3))))) ) ) # ( !\CPU|FSM|CO|Mux1~2_combout\ & ( (!\CPU|FSM|CO|Mux1~0_combout\ & ((\CPU|data_address|register|out\(3)))) # (\CPU|FSM|CO|Mux1~0_combout\ & 
-- (\CPU|program_counter|register|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111000001011010111100000111100011110000011110001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|program_counter|register|ALT_INV_out\(3),
	datad => \CPU|data_address|register|ALT_INV_out\(3),
	dataf => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	combout => \CPU|mem_addr[3]~4_combout\);

-- Location: LABCELL_X61_Y4_N3
\read_data[6]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[6]~8_combout\ = ( \CPU|mem_addr[8]~0_combout\ & ( \SW[6]~input_o\ ) ) # ( !\CPU|mem_addr[8]~0_combout\ & ( (!\rtl~26_combout\ & (((\SW[6]~input_o\)))) # (\rtl~26_combout\ & ((!\rtl~27_combout\ & ((\SW[6]~input_o\))) # (\rtl~27_combout\ & 
-- (\MEM|mem_rtl_0|auto_generated|ram_block1a6\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000111101111000000011110111100000000111111110000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \ALT_INV_rtl~27_combout\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\,
	datad => \ALT_INV_SW[6]~input_o\,
	dataf => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	combout => \read_data[6]~8_combout\);

-- Location: FF_X61_Y4_N5
\CPU|InstructionRegister|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[6]~8_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(6));

-- Location: LABCELL_X62_Y4_N48
\CPU|ID|RWmux|b[1]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|ID|RWmux|b\(1) = ( \rtl~35_combout\ & ( (!\CPU|InstructionRegister|register|out\(6) & (!\CPU|ID|RWmux|comb~4_combout\ & ((!\CPU|InstructionRegister|register|out\(9)) # (!\rtl~33_combout\)))) ) ) # ( !\rtl~35_combout\ & ( 
-- (!\CPU|ID|RWmux|comb~4_combout\ & ((!\CPU|InstructionRegister|register|out\(9)) # (!\rtl~33_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110000000000111111000000000010101000000000001010100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(6),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(9),
	datac => \ALT_INV_rtl~33_combout\,
	datad => \CPU|ID|RWmux|ALT_INV_comb~4_combout\,
	dataf => \ALT_INV_rtl~35_combout\,
	combout => \CPU|ID|RWmux|b\(1));

-- Location: LABCELL_X60_Y3_N36
\CPU|DP|WBmux|m|b[2]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b[2]~3_combout\ = ( \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(2) & (!\CPU|DP|Add0~9_sumout\ & ((!\CPU|InstructionRegister|register|out\(2)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\)))) ) ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( \CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|c|register|out\(2) & ((!\CPU|InstructionRegister|register|out\(2)) # 
-- (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) ) ) ) # ( \CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|DP|Add0~9_sumout\ & ((!\CPU|InstructionRegister|register|out\(2)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\))) 
-- ) ) ) # ( !\CPU|DP|WBmux|d|ShiftLeft0~0_combout\ & ( !\CPU|DP|WBmux|d|ShiftLeft0~2_combout\ & ( (!\CPU|InstructionRegister|register|out\(2)) # (!\CPU|DP|WBmux|d|ShiftLeft0~3_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110000110011001100000010101010101000001000100010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|c|register|ALT_INV_out\(2),
	datab => \CPU|DP|ALT_INV_Add0~9_sumout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(2),
	datad => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~3_combout\,
	datae => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~0_combout\,
	dataf => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~2_combout\,
	combout => \CPU|DP|WBmux|m|b[2]~3_combout\);

-- Location: LABCELL_X60_Y3_N33
\CPU|DP|WBmux|m|b[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|WBmux|m|b\(2) = ( \MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( (!\CPU|DP|WBmux|m|b[2]~3_combout\) # ((\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & ((\SW[2]~input_o\) # (\e~combout\)))) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( 
-- (!\CPU|DP|WBmux|m|b[2]~3_combout\) # ((!\e~combout\ & (\CPU|DP|WBmux|d|ShiftLeft0~1_combout\ & \SW[2]~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000010111111110000001011111111000100111111111100010011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_e~combout\,
	datab => \CPU|DP|WBmux|d|ALT_INV_ShiftLeft0~1_combout\,
	datac => \ALT_INV_SW[2]~input_o\,
	datad => \CPU|DP|WBmux|m|ALT_INV_b[2]~3_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\,
	combout => \CPU|DP|WBmux|m|b\(2));

-- Location: FF_X59_Y3_N5
\CPU|DP|REGFILE|Reg3|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg3|register|out\(2));

-- Location: FF_X63_Y3_N53
\CPU|DP|REGFILE|Reg1|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg1|register|out\(2));

-- Location: LABCELL_X63_Y3_N51
\CPU|DP|REGFILE|readMux|m|b[2]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[2]~10_combout\ = ( \CPU|DP|REGFILE|Reg1|register|out\(2) & ( !\CPU|ID|RWmux|b\(0) & ( (\CPU|ID|RWmux|b\(2) & ((\CPU|DP|REGFILE|Reg3|register|out\(2)) # (\CPU|ID|RWmux|b\(1)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg1|register|out\(2) & 
-- ( !\CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(1) & (\CPU|DP|REGFILE|Reg3|register|out\(2) & \CPU|ID|RWmux|b\(2))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000100010000000000111011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ID|RWmux|ALT_INV_b\(1),
	datab => \CPU|DP|REGFILE|Reg3|register|ALT_INV_out\(2),
	datad => \CPU|ID|RWmux|ALT_INV_b\(2),
	datae => \CPU|DP|REGFILE|Reg1|register|ALT_INV_out\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[2]~10_combout\);

-- Location: FF_X60_Y3_N13
\CPU|DP|REGFILE|Reg5|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg5|register|out\(2));

-- Location: FF_X59_Y3_N17
\CPU|DP|REGFILE|Reg2|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg2|register|out\(2));

-- Location: MLABCELL_X59_Y5_N18
\CPU|DP|REGFILE|Reg4|register|out[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|Reg4|register|out[2]~feeder_combout\ = ( \CPU|DP|WBmux|m|b\(2) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|WBmux|m|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|Reg4|register|out[2]~feeder_combout\);

-- Location: FF_X59_Y5_N19
\CPU|DP|REGFILE|Reg4|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|REGFILE|Reg4|register|out[2]~feeder_combout\,
	ena => \CPU|DP|REGFILE|regSelect\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg4|register|out\(2));

-- Location: FF_X63_Y3_N56
\CPU|DP|REGFILE|Reg0|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg0|register|out\(2));

-- Location: LABCELL_X63_Y3_N54
\CPU|DP|REGFILE|readMux|m|b[2]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[2]~9_combout\ = ( \CPU|DP|REGFILE|Reg0|register|out\(2) & ( \CPU|ID|RWmux|b\(2) & ( (\CPU|ID|RWmux|b\(0) & ((\CPU|ID|RWmux|b\(1)) # (\CPU|DP|REGFILE|Reg2|register|out\(2)))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(2) & ( 
-- \CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg2|register|out\(2) & (!\CPU|ID|RWmux|b\(1) & \CPU|ID|RWmux|b\(0))) ) ) ) # ( \CPU|DP|REGFILE|Reg0|register|out\(2) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg4|register|out\(2) & (\CPU|ID|RWmux|b\(1) & 
-- \CPU|ID|RWmux|b\(0))) ) ) ) # ( !\CPU|DP|REGFILE|Reg0|register|out\(2) & ( !\CPU|ID|RWmux|b\(2) & ( (\CPU|DP|REGFILE|Reg4|register|out\(2) & (\CPU|ID|RWmux|b\(1) & \CPU|ID|RWmux|b\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000011000000000000001100000000010100000000000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg2|register|ALT_INV_out\(2),
	datab => \CPU|DP|REGFILE|Reg4|register|ALT_INV_out\(2),
	datac => \CPU|ID|RWmux|ALT_INV_b\(1),
	datad => \CPU|ID|RWmux|ALT_INV_b\(0),
	datae => \CPU|DP|REGFILE|Reg0|register|ALT_INV_out\(2),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(2),
	combout => \CPU|DP|REGFILE|readMux|m|b[2]~9_combout\);

-- Location: FF_X60_Y3_N34
\CPU|DP|REGFILE|Reg7|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|WBmux|m|b\(2),
	ena => \CPU|DP|REGFILE|regSelect\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg7|register|out\(2));

-- Location: FF_X61_Y3_N5
\CPU|DP|REGFILE|Reg6|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|WBmux|m|b\(2),
	sload => VCC,
	ena => \CPU|DP|REGFILE|regSelect\(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|REGFILE|Reg6|register|out\(2));

-- Location: LABCELL_X62_Y3_N15
\CPU|DP|REGFILE|readMux|m|b[2]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b[2]~11_combout\ = ( !\CPU|ID|RWmux|b\(1) & ( \CPU|ID|RWmux|b\(0) & ( (!\CPU|ID|RWmux|b\(2) & \CPU|DP|REGFILE|Reg6|register|out\(2)) ) ) ) # ( !\CPU|ID|RWmux|b\(1) & ( !\CPU|ID|RWmux|b\(0) & ( 
-- (\CPU|DP|REGFILE|Reg7|register|out\(2) & !\CPU|ID|RWmux|b\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100000000000000000000001100000011000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|Reg7|register|ALT_INV_out\(2),
	datab => \CPU|ID|RWmux|ALT_INV_b\(2),
	datac => \CPU|DP|REGFILE|Reg6|register|ALT_INV_out\(2),
	datae => \CPU|ID|RWmux|ALT_INV_b\(1),
	dataf => \CPU|ID|RWmux|ALT_INV_b\(0),
	combout => \CPU|DP|REGFILE|readMux|m|b[2]~11_combout\);

-- Location: LABCELL_X63_Y3_N42
\CPU|DP|REGFILE|readMux|m|b[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|REGFILE|readMux|m|b\(2) = ( \CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( (((\CPU|DP|REGFILE|readMux|m|b[2]~11_combout\) # (\CPU|DP|REGFILE|readMux|m|b[2]~9_combout\)) # (\CPU|DP|REGFILE|Reg5|register|out\(2))) # 
-- (\CPU|DP|REGFILE|readMux|m|b[2]~10_combout\) ) ) # ( !\CPU|DP|REGFILE|readMux|d|ShiftLeft0~0_combout\ & ( ((\CPU|DP|REGFILE|readMux|m|b[2]~11_combout\) # (\CPU|DP|REGFILE|readMux|m|b[2]~9_combout\)) # (\CPU|DP|REGFILE|readMux|m|b[2]~10_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111111111111011111111111111101011111111111110111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~10_combout\,
	datab => \CPU|DP|REGFILE|Reg5|register|ALT_INV_out\(2),
	datac => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~9_combout\,
	datad => \CPU|DP|REGFILE|readMux|m|ALT_INV_b[2]~11_combout\,
	datae => \CPU|DP|REGFILE|readMux|d|ALT_INV_ShiftLeft0~0_combout\,
	combout => \CPU|DP|REGFILE|readMux|m|b\(2));

-- Location: FF_X63_Y3_N1
\CPU|DP|a|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(2),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(2));

-- Location: LABCELL_X64_Y4_N51
\CPU|DP|Ain[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[2]~2_combout\ = ( \CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|DP|a|register|out\(2) & ( (!\CPU|FSM|storeFlag|register|out\(0) & (((!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\) # (!\CPU|InstructionRegister|register|out\(15))) # 
-- (\CPU|InstructionRegister|register|out\(13)))) ) ) ) # ( !\CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|DP|a|register|out\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111100110011000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datab => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datac => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datae => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(2),
	combout => \CPU|DP|Ain[2]~2_combout\);

-- Location: MLABCELL_X65_Y3_N33
\CPU|DP|ALU|plusMinus|ai|c[2]\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|plusMinus|ai|c\(2) = ( \CPU|DP|Bin[1]~5_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & ((\CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\) # (\CPU|DP|Ain[1]~1_combout\))) # (\CPU|InstructionRegister|register|out\(11) & 
-- (\CPU|DP|Ain[1]~1_combout\ & \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\)) ) ) # ( !\CPU|DP|Bin[1]~5_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & (\CPU|DP|Ain[1]~1_combout\ & \CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\)) # 
-- (\CPU|InstructionRegister|register|out\(11) & ((\CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\) # (\CPU|DP|Ain[1]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100111111000000110011111100001100110011110000110011001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datac => \CPU|DP|ALT_INV_Ain[1]~1_combout\,
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~5_combout\,
	combout => \CPU|DP|ALU|plusMinus|ai|c\(2));

-- Location: MLABCELL_X65_Y3_N0
\CPU|DP|ALU|out[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[2]~2_combout\ = ( \CPU|DP|ALU|plusMinus|ai|c\(2) & ( (!\CPU|InstructionRegister|register|out\(11) & ((!\CPU|DP|Ain[2]~2_combout\ & (!\CPU|InstructionRegister|register|out\(12) & !\CPU|DP|Bin[2]~6_combout\)) # (\CPU|DP|Ain[2]~2_combout\ & 
-- ((\CPU|DP|Bin[2]~6_combout\))))) # (\CPU|InstructionRegister|register|out\(11) & (!\CPU|DP|Bin[2]~6_combout\ $ (((!\CPU|InstructionRegister|register|out\(12) & !\CPU|DP|Ain[2]~2_combout\))))) ) ) # ( !\CPU|DP|ALU|plusMinus|ai|c\(2) & ( 
-- (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Ain[2]~2_combout\ $ (\CPU|DP|Bin[2]~6_combout\)))) # (\CPU|InstructionRegister|register|out\(12) & ((!\CPU|InstructionRegister|register|out\(11) & 
-- (\CPU|DP|Ain[2]~2_combout\ & \CPU|DP|Bin[2]~6_combout\)) # (\CPU|InstructionRegister|register|out\(11) & ((!\CPU|DP|Bin[2]~6_combout\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101100110000110010110011000011010010101010010101001010101001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Ain[2]~2_combout\,
	datad => \CPU|DP|ALT_INV_Bin[2]~6_combout\,
	dataf => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c\(2),
	combout => \CPU|DP|ALU|out[2]~2_combout\);

-- Location: FF_X65_Y3_N2
\CPU|DP|c|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[2]~2_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(2));

-- Location: FF_X61_Y4_N56
\CPU|data_address|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(2),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(2));

-- Location: LABCELL_X61_Y4_N54
\CPU|mem_addr[2]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[2]~3_combout\ = ( \CPU|program_counter|register|out\(2) & ( (((\CPU|FSM|CO|Mux1~1_combout\ & \CPU|FSM|CO|Mux1~2_combout\)) # (\CPU|data_address|register|out\(2))) # (\CPU|FSM|CO|Mux1~0_combout\) ) ) # ( !\CPU|program_counter|register|out\(2) 
-- & ( (!\CPU|FSM|CO|Mux1~0_combout\ & (\CPU|data_address|register|out\(2) & ((!\CPU|FSM|CO|Mux1~1_combout\) # (!\CPU|FSM|CO|Mux1~2_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101000000000001010100001010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datad => \CPU|data_address|register|ALT_INV_out\(2),
	dataf => \CPU|program_counter|register|ALT_INV_out\(2),
	combout => \CPU|mem_addr[2]~3_combout\);

-- Location: LABCELL_X61_Y4_N45
\read_data[12]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[12]~4_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( (\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010001000000000001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\,
	combout => \read_data[12]~4_combout\);

-- Location: FF_X61_Y4_N26
\CPU|InstructionRegister|register|out[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \read_data[12]~4_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(12));

-- Location: MLABCELL_X65_Y3_N3
\CPU|DP|ALU|out[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[1]~1_combout\ = ( \CPU|DP|Bin[1]~5_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) $ (!\CPU|DP|Ain[1]~1_combout\ $ (!\CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\)))) # 
-- (\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(11) & (\CPU|DP|Ain[1]~1_combout\))) ) ) # ( !\CPU|DP|Bin[1]~5_combout\ & ( !\CPU|InstructionRegister|register|out\(11) $ (((!\CPU|DP|Ain[1]~1_combout\ $ 
-- (\CPU|DP|ALU|plusMinus|ai|c[1]~0_combout\)) # (\CPU|InstructionRegister|register|out\(12)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101100110010101010110011001010110000110010010101000011001001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|DP|ALT_INV_Ain[1]~1_combout\,
	datad => \CPU|DP|ALU|plusMinus|ai|ALT_INV_c[1]~0_combout\,
	dataf => \CPU|DP|ALT_INV_Bin[1]~5_combout\,
	combout => \CPU|DP|ALU|out[1]~1_combout\);

-- Location: FF_X65_Y3_N5
\CPU|DP|c|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[1]~1_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(1));

-- Location: FF_X61_Y4_N41
\CPU|data_address|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(1),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(1));

-- Location: LABCELL_X61_Y4_N39
\CPU|mem_addr[1]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[1]~2_combout\ = ( \CPU|program_counter|register|out\(1) & ( (((\CPU|FSM|CO|Mux1~1_combout\ & \CPU|FSM|CO|Mux1~2_combout\)) # (\CPU|data_address|register|out\(1))) # (\CPU|FSM|CO|Mux1~0_combout\) ) ) # ( !\CPU|program_counter|register|out\(1) 
-- & ( (!\CPU|FSM|CO|Mux1~0_combout\ & (\CPU|data_address|register|out\(1) & ((!\CPU|FSM|CO|Mux1~1_combout\) # (!\CPU|FSM|CO|Mux1~2_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101000000000001010100001010111111111110101011111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datad => \CPU|data_address|register|ALT_INV_out\(1),
	dataf => \CPU|program_counter|register|ALT_INV_out\(1),
	combout => \CPU|mem_addr[1]~2_combout\);

-- Location: MLABCELL_X59_Y4_N12
\read_data[0]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[0]~9_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( \rtl~27_combout\ & ( ((!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\)) # (\SW[0]~input_o\) ) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( 
-- \rtl~27_combout\ & ( (\SW[0]~input_o\ & ((!\rtl~26_combout\) # (\CPU|mem_addr[8]~0_combout\))) ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( !\rtl~27_combout\ & ( \SW[0]~input_o\ ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( !\rtl~27_combout\ & ( \SW[0]~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110001001100010011101100111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datab => \ALT_INV_SW[0]~input_o\,
	datac => \ALT_INV_rtl~26_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	dataf => \ALT_INV_rtl~27_combout\,
	combout => \read_data[0]~9_combout\);

-- Location: FF_X59_Y4_N14
\CPU|InstructionRegister|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[0]~9_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(0));

-- Location: FF_X63_Y2_N34
\CPU|program_counter|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|Add0~5_sumout\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(0),
	sclr => \CPU|FSM|CO|Decoder0~2_combout\,
	sload => \CPU|FSM|CO|Selector15~0_combout\,
	ena => \CPU|FSM|CO|load_pc~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|program_counter|register|out\(0));

-- Location: LABCELL_X61_Y4_N6
\CPU|mem_addr[0]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[0]~1_combout\ = ( \CPU|program_counter|register|out\(0) & ( (((\CPU|FSM|CO|Mux1~2_combout\ & \CPU|FSM|CO|Mux1~1_combout\)) # (\CPU|data_address|register|out\(0))) # (\CPU|FSM|CO|Mux1~0_combout\) ) ) # ( !\CPU|program_counter|register|out\(0) 
-- & ( (!\CPU|FSM|CO|Mux1~0_combout\ & (\CPU|data_address|register|out\(0) & ((!\CPU|FSM|CO|Mux1~2_combout\) # (!\CPU|FSM|CO|Mux1~1_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011100000000000001110000000011111111111110001111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datad => \CPU|data_address|register|ALT_INV_out\(0),
	dataf => \CPU|program_counter|register|ALT_INV_out\(0),
	combout => \CPU|mem_addr[0]~1_combout\);

-- Location: LABCELL_X63_Y4_N45
\read_data[15]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[15]~2_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( (\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000100000001000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\,
	combout => \read_data[15]~2_combout\);

-- Location: FF_X63_Y4_N47
\CPU|InstructionRegister|register|out[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[15]~2_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(15));

-- Location: LABCELL_X63_Y4_N3
\CPU|FSM|CS|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr5~0_combout\ = ( !\CPU|InstructionRegister|register|out\(14) & ( (!\CPU|InstructionRegister|register|out\(13) & \CPU|InstructionRegister|register|out\(15)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	combout => \CPU|FSM|CS|WideOr5~0_combout\);

-- Location: LABCELL_X67_Y4_N48
\CPU|FSM|CS|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr1~0_combout\ = ( \CPU|FSM|CS|WideOr5~0_combout\ & ( \CPU|FSM|storeFlag|register|out\(0) & ( !\CPU|FSM|CO|Mux1~0_combout\ ) ) ) # ( !\CPU|FSM|CS|WideOr5~0_combout\ & ( \CPU|FSM|storeFlag|register|out\(0) & ( !\CPU|FSM|CO|Mux1~0_combout\ ) 
-- ) ) # ( \CPU|FSM|CS|WideOr5~0_combout\ & ( !\CPU|FSM|storeFlag|register|out\(0) & ( (!\CPU|FSM|CO|Mux1~0_combout\ & !\CPU|FSM|CO|Decoder0~1_combout\) ) ) ) # ( !\CPU|FSM|CS|WideOr5~0_combout\ & ( !\CPU|FSM|storeFlag|register|out\(0) & ( 
-- !\CPU|FSM|CO|Mux1~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101000001010000010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	datae => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	dataf => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	combout => \CPU|FSM|CS|WideOr1~0_combout\);

-- Location: LABCELL_X56_Y4_N51
\CPU|FSM|CO|Selector14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector14~0_combout\ = ( !\CPU|FSM|stateRegister|out\(2) & ( !\CPU|FSM|stateRegister|out\(1) & ( (\CPU|InstructionRegister|register|out\(15) & (!\CPU|FSM|stateRegister|out\(3) & \CPU|FSM|stateRegister|out\(0))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000100000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	combout => \CPU|FSM|CO|Selector14~0_combout\);

-- Location: LABCELL_X67_Y4_N9
\CPU|FSM|CS|WideNor4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideNor4~combout\ = ( \CPU|FSM|CO|Selector14~0_combout\ & ( (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (\CPU|InstructionRegister|register|out\(12) & (!\CPU|InstructionRegister|register|out\(13) & 
-- !\CPU|InstructionRegister|register|out\(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000010000000000000001000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|FSM|CO|ALT_INV_Selector14~0_combout\,
	combout => \CPU|FSM|CS|WideNor4~combout\);

-- Location: LABCELL_X63_Y5_N39
\CPU|FSM|CO|Decoder0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Decoder0~0_combout\ = ( \CPU|FSM|stateRegister|out\(0) & ( (!\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011110000000000001111000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	combout => \CPU|FSM|CO|Decoder0~0_combout\);

-- Location: LABCELL_X63_Y5_N6
\CPU|FSM|CS|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr3~0_combout\ = ( \CPU|FSM|stateRegister|out\(2) & ( \rtl~28_combout\ & ( (\CPU|InstructionRegister|register|out\(13) & (\CPU|FSM|CO|Decoder0~0_combout\ & (\CPU|InstructionRegister|register|out\(14) & 
-- !\CPU|InstructionRegister|register|out\(15)))) ) ) ) # ( !\CPU|FSM|stateRegister|out\(2) & ( \rtl~28_combout\ & ( (\CPU|FSM|CO|Decoder0~0_combout\ & ((!\CPU|InstructionRegister|register|out\(13) & (!\CPU|InstructionRegister|register|out\(14) & 
-- \CPU|InstructionRegister|register|out\(15))) # (\CPU|InstructionRegister|register|out\(13) & (\CPU|InstructionRegister|register|out\(14) & !\CPU|InstructionRegister|register|out\(15))))) ) ) ) # ( \CPU|FSM|stateRegister|out\(2) & ( !\rtl~28_combout\ & ( 
-- (\CPU|InstructionRegister|register|out\(13) & (\CPU|FSM|CO|Decoder0~0_combout\ & (\CPU|InstructionRegister|register|out\(14) & !\CPU|InstructionRegister|register|out\(15)))) ) ) ) # ( !\CPU|FSM|stateRegister|out\(2) & ( !\rtl~28_combout\ & ( 
-- (\CPU|FSM|CO|Decoder0~0_combout\ & ((!\CPU|InstructionRegister|register|out\(14) & ((\CPU|InstructionRegister|register|out\(15)))) # (\CPU|InstructionRegister|register|out\(14) & (\CPU|InstructionRegister|register|out\(13) & 
-- !\CPU|InstructionRegister|register|out\(15))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100110000000000010000000000000001001000000000000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datab => \CPU|FSM|CO|ALT_INV_Decoder0~0_combout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \ALT_INV_rtl~28_combout\,
	combout => \CPU|FSM|CS|WideOr3~0_combout\);

-- Location: LABCELL_X57_Y4_N27
\CPU|FSM|CO|Selector14~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector14~1_combout\ = ( \CPU|InstructionRegister|register|out\(14) & ( \CPU|FSM|CO|Selector14~0_combout\ & ( \CPU|InstructionRegister|register|out\(13) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	dataf => \CPU|FSM|CO|ALT_INV_Selector14~0_combout\,
	combout => \CPU|FSM|CO|Selector14~1_combout\);

-- Location: LABCELL_X67_Y4_N21
\CPU|FSM|CS|WideOr3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr3~1_combout\ = ( \CPU|FSM|stateRegister|out\(3) & ( \CPU|FSM|stateRegister|out\(1) & ( (!\CPU|FSM|stateRegister|out\(2) & !\CPU|FSM|stateRegister|out\(0)) ) ) ) # ( \CPU|FSM|stateRegister|out\(3) & ( !\CPU|FSM|stateRegister|out\(1) & ( 
-- !\CPU|FSM|stateRegister|out\(2) $ (!\CPU|FSM|stateRegister|out\(0)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010110100101101000000000000000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	combout => \CPU|FSM|CS|WideOr3~1_combout\);

-- Location: LABCELL_X64_Y4_N0
\CPU|FSM|CS|WideOr3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr3~2_combout\ = ( !\CPU|FSM|CS|WideOr3~1_combout\ & ( (\CPU|FSM|CS|WideOr1~0_combout\ & (!\CPU|FSM|CS|WideNor4~combout\ & (!\CPU|FSM|CS|WideOr3~0_combout\ & !\CPU|FSM|CO|Selector14~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000010000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CS|ALT_INV_WideOr1~0_combout\,
	datab => \CPU|FSM|CS|ALT_INV_WideNor4~combout\,
	datac => \CPU|FSM|CS|ALT_INV_WideOr3~0_combout\,
	datad => \CPU|FSM|CO|ALT_INV_Selector14~1_combout\,
	dataf => \CPU|FSM|CS|ALT_INV_WideOr3~1_combout\,
	combout => \CPU|FSM|CS|WideOr3~2_combout\);

-- Location: IOIBUF_X64_Y0_N52
\KEY[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: FF_X64_Y4_N29
\CPU|FSM|stateRegister|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|FSM|CS|WideOr3~2_combout\,
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|stateRegister|out\(2));

-- Location: LABCELL_X67_Y4_N27
\CPU|FSM|CS|WideNor7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideNor7~0_combout\ = ( !\CPU|FSM|stateRegister|out\(0) & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|stateRegister|out\(1) & \CPU|FSM|stateRegister|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	combout => \CPU|FSM|CS|WideNor7~0_combout\);

-- Location: LABCELL_X67_Y4_N39
\CPU|FSM|CS|WideNor27~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideNor27~0_combout\ = ( \CPU|FSM|stateRegister|out\(1) & ( (\CPU|InstructionRegister|register|out\(13) & !\CPU|FSM|stateRegister|out\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	combout => \CPU|FSM|CS|WideNor27~0_combout\);

-- Location: LABCELL_X67_Y4_N24
\CPU|FSM|CS|WideOr1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr1~1_combout\ = ( \CPU|FSM|storeFlag|register|out\(0) & ( (\CPU|FSM|CO|Decoder0~1_combout\ & ((\CPU|FSM|CS|WideOr5~0_combout\) # (\CPU|FSM|CO|Equal6~0_combout\))) ) ) # ( !\CPU|FSM|storeFlag|register|out\(0) & ( 
-- (\CPU|FSM|CO|Equal6~0_combout\ & \CPU|FSM|CO|Decoder0~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000011000011110000001100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|CO|ALT_INV_Equal6~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	datad => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	dataf => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	combout => \CPU|FSM|CS|WideOr1~1_combout\);

-- Location: LABCELL_X67_Y4_N30
\CPU|FSM|CS|WideOr1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr1~combout\ = ( \CPU|FSM|CO|Selector14~1_combout\ & ( \CPU|FSM|CS|WideOr1~1_combout\ ) ) # ( !\CPU|FSM|CO|Selector14~1_combout\ & ( \CPU|FSM|CS|WideOr1~1_combout\ ) ) # ( \CPU|FSM|CO|Selector14~1_combout\ & ( 
-- !\CPU|FSM|CS|WideOr1~1_combout\ ) ) # ( !\CPU|FSM|CO|Selector14~1_combout\ & ( !\CPU|FSM|CS|WideOr1~1_combout\ & ( ((!\CPU|FSM|CS|WideOr1~0_combout\) # ((\CPU|FSM|CO|Mux9~0_combout\ & \CPU|FSM|CS|WideNor27~0_combout\))) # (\CPU|FSM|CS|WideNor7~0_combout\) 
-- ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CS|ALT_INV_WideNor7~0_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux9~0_combout\,
	datac => \CPU|FSM|CS|ALT_INV_WideOr1~0_combout\,
	datad => \CPU|FSM|CS|ALT_INV_WideNor27~0_combout\,
	datae => \CPU|FSM|CO|ALT_INV_Selector14~1_combout\,
	dataf => \CPU|FSM|CS|ALT_INV_WideOr1~1_combout\,
	combout => \CPU|FSM|CS|WideOr1~combout\);

-- Location: FF_X64_Y4_N14
\CPU|FSM|stateRegister|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|FSM|CS|WideOr1~combout\,
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|stateRegister|out\(3));

-- Location: LABCELL_X64_Y5_N12
\rtl~27\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~27_combout\ = ( \CPU|FSM|stateRegister|out\(1) & ( \CPU|FSM|CO|Equal6~0_combout\ & ( !\CPU|FSM|stateRegister|out\(0) $ (((!\CPU|FSM|stateRegister|out\(3) & \CPU|FSM|stateRegister|out\(2)))) ) ) ) # ( !\CPU|FSM|stateRegister|out\(1) & ( 
-- \CPU|FSM|CO|Equal6~0_combout\ & ( (\CPU|FSM|stateRegister|out\(3) & ((!\CPU|FSM|stateRegister|out\(0)) # (\CPU|FSM|stateRegister|out\(2)))) ) ) ) # ( \CPU|FSM|stateRegister|out\(1) & ( !\CPU|FSM|CO|Equal6~0_combout\ & ( (!\CPU|FSM|stateRegister|out\(3) & 
-- (\CPU|FSM|stateRegister|out\(0) & \CPU|FSM|stateRegister|out\(2))) # (\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(0))) ) ) ) # ( !\CPU|FSM|stateRegister|out\(1) & ( !\CPU|FSM|CO|Equal6~0_combout\ & ( (\CPU|FSM|stateRegister|out\(3) & 
-- ((!\CPU|FSM|stateRegister|out\(0)) # (\CPU|FSM|stateRegister|out\(2)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000110011001100000011110000110000001100111111000000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	dataf => \CPU|FSM|CO|ALT_INV_Equal6~0_combout\,
	combout => \rtl~27_combout\);

-- Location: LABCELL_X61_Y4_N42
\read_data[11]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[11]~3_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( (\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010001000000000001000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\,
	combout => \read_data[11]~3_combout\);

-- Location: FF_X61_Y4_N34
\CPU|InstructionRegister|register|out[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \read_data[11]~3_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(11));

-- Location: LABCELL_X62_Y4_N51
\CPU|FSM|CO|Mux1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux1~2_combout\ = ( !\CPU|InstructionRegister|register|out\(12) & ( (!\CPU|InstructionRegister|register|out\(11) & \CPU|FSM|stateRegister|out\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	combout => \CPU|FSM|CO|Mux1~2_combout\);

-- Location: FF_X61_Y4_N17
\CPU|data_address|register|out[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(8),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|data_address|register|out\(8));

-- Location: LABCELL_X61_Y4_N9
\CPU|mem_addr[8]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|mem_addr[8]~0_combout\ = ( \CPU|program_counter|register|out\(8) & ( (((\CPU|FSM|CO|Mux1~2_combout\ & \CPU|FSM|CO|Mux1~1_combout\)) # (\CPU|FSM|CO|Mux1~0_combout\)) # (\CPU|data_address|register|out\(8)) ) ) # ( !\CPU|program_counter|register|out\(8) 
-- & ( (\CPU|data_address|register|out\(8) & (!\CPU|FSM|CO|Mux1~0_combout\ & ((!\CPU|FSM|CO|Mux1~2_combout\) # (!\CPU|FSM|CO|Mux1~1_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111000000000000011100000000000011111111111110001111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	datac => \CPU|data_address|register|ALT_INV_out\(8),
	datad => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	dataf => \CPU|program_counter|register|ALT_INV_out\(8),
	combout => \CPU|mem_addr[8]~0_combout\);

-- Location: LABCELL_X63_Y4_N54
\read_data[14]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[14]~1_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( (!\CPU|mem_addr[8]~0_combout\ & (\rtl~26_combout\ & \rtl~27_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011000000000000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\,
	combout => \read_data[14]~1_combout\);

-- Location: FF_X63_Y4_N11
\CPU|InstructionRegister|register|out[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \read_data[14]~1_combout\,
	sload => VCC,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(14));

-- Location: LABCELL_X60_Y4_N18
\CPU|FSM|CO|Mux9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Mux9~0_combout\ = ( !\CPU|InstructionRegister|register|out\(15) & ( (\CPU|FSM|stateRegister|out\(0) & (\CPU|InstructionRegister|register|out\(14) & \CPU|FSM|stateRegister|out\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	combout => \CPU|FSM|CO|Mux9~0_combout\);

-- Location: LABCELL_X67_Y4_N6
\CPU|FSM|CS|WideOr7~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr7~3_combout\ = ( \CPU|InstructionRegister|register|out\(13) & ( (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & \CPU|FSM|CO|Selector14~0_combout\) ) ) # ( !\CPU|InstructionRegister|register|out\(13) & ( 
-- (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\ & (\CPU|InstructionRegister|register|out\(12) & (\CPU|FSM|CO|Selector14~0_combout\ & !\CPU|InstructionRegister|register|out\(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000000000000010000000000000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|FSM|CO|ALT_INV_Selector14~0_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	combout => \CPU|FSM|CS|WideOr7~3_combout\);

-- Location: LABCELL_X67_Y4_N12
\CPU|FSM|CS|WideOr7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr7~0_combout\ = ( \CPU|FSM|CS|WideOr5~0_combout\ & ( \CPU|FSM|stateRegister|out\(0) & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|stateRegister|out\(1) & \CPU|FSM|stateRegister|out\(3))) ) ) ) # ( \CPU|FSM|CS|WideOr5~0_combout\ & ( 
-- !\CPU|FSM|stateRegister|out\(0) & ( (\CPU|FSM|stateRegister|out\(3) & (!\CPU|FSM|stateRegister|out\(2) $ (!\CPU|FSM|stateRegister|out\(1)))) ) ) ) # ( !\CPU|FSM|CS|WideOr5~0_combout\ & ( !\CPU|FSM|stateRegister|out\(0) & ( (\CPU|FSM|stateRegister|out\(3) 
-- & (!\CPU|FSM|stateRegister|out\(2) $ (!\CPU|FSM|stateRegister|out\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011000000110000001100000011000000000000000000000001000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datae => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	combout => \CPU|FSM|CS|WideOr7~0_combout\);

-- Location: LABCELL_X63_Y5_N33
\CPU|FSM|CS|WideOr7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr7~1_combout\ = ( \CPU|InstructionRegister|register|out\(11) & ( \CPU|InstructionRegister|register|out\(12) & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ 
-- (!\CPU|InstructionRegister|register|out\(13))))) # (\CPU|FSM|stateRegister|out\(2) & (!\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ (!\CPU|InstructionRegister|register|out\(13))))) ) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(11) & ( \CPU|InstructionRegister|register|out\(12) & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ 
-- (!\CPU|InstructionRegister|register|out\(13))))) # (\CPU|FSM|stateRegister|out\(2) & (!\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ (!\CPU|InstructionRegister|register|out\(13))))) ) ) ) # ( 
-- \CPU|InstructionRegister|register|out\(11) & ( !\CPU|InstructionRegister|register|out\(12) & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ 
-- (!\CPU|InstructionRegister|register|out\(13))))) # (\CPU|FSM|stateRegister|out\(2) & (\CPU|InstructionRegister|register|out\(14) & (!\CPU|FSM|stateRegister|out\(1) & !\CPU|InstructionRegister|register|out\(13)))) ) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(11) & ( !\CPU|InstructionRegister|register|out\(12) & ( (!\CPU|FSM|stateRegister|out\(2) & (\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ 
-- (!\CPU|InstructionRegister|register|out\(13))))) # (\CPU|FSM|stateRegister|out\(2) & (!\CPU|FSM|stateRegister|out\(1) & (!\CPU|InstructionRegister|register|out\(14) $ (!\CPU|InstructionRegister|register|out\(13))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001001001001000000100100000100000010010010010000001001001001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	combout => \CPU|FSM|CS|WideOr7~1_combout\);

-- Location: LABCELL_X56_Y4_N24
\CPU|FSM|CS|WideOr7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr7~2_combout\ = ( \CPU|FSM|stateRegister|out\(0) & ( \CPU|FSM|CS|WideOr7~1_combout\ & ( (!\CPU|FSM|stateRegister|out\(3) & \CPU|InstructionRegister|register|out\(15)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	dataf => \CPU|FSM|CS|ALT_INV_WideOr7~1_combout\,
	combout => \CPU|FSM|CS|WideOr7~2_combout\);

-- Location: LABCELL_X67_Y4_N42
\CPU|FSM|CS|WideOr5~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr5~3_combout\ = ( !\CPU|FSM|CO|Mux1~4_combout\ & ( \CPU|FSM|CO|Selector14~0_combout\ & ( (!\CPU|InstructionRegister|register|out\(11) & (((!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\) # 
-- (\CPU|InstructionRegister|register|out\(13))) # (\CPU|InstructionRegister|register|out\(12)))) # (\CPU|InstructionRegister|register|out\(11) & ((!\CPU|InstructionRegister|register|out\(12)) # ((!\CPU|InstructionRegister|register|out\(13)) # 
-- (\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\)))) ) ) ) # ( !\CPU|FSM|CO|Mux1~4_combout\ & ( !\CPU|FSM|CO|Selector14~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011110111111011110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	datab => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datac => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datae => \CPU|FSM|CO|ALT_INV_Mux1~4_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Selector14~0_combout\,
	combout => \CPU|FSM|CS|WideOr5~3_combout\);

-- Location: LABCELL_X64_Y4_N18
\CPU|FSM|CS|WideOr7~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr7~4_combout\ = ( !\CPU|FSM|CS|WideOr7~2_combout\ & ( \CPU|FSM|CS|WideOr5~3_combout\ & ( (!\CPU|FSM|CS|WideOr7~3_combout\ & (!\CPU|FSM|CS|WideOr7~0_combout\ & ((!\CPU|FSM|CO|Mux9~0_combout\) # (!\CPU|FSM|CS|WideNor27~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011100000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux9~0_combout\,
	datab => \CPU|FSM|CS|ALT_INV_WideNor27~0_combout\,
	datac => \CPU|FSM|CS|ALT_INV_WideOr7~3_combout\,
	datad => \CPU|FSM|CS|ALT_INV_WideOr7~0_combout\,
	datae => \CPU|FSM|CS|ALT_INV_WideOr7~2_combout\,
	dataf => \CPU|FSM|CS|ALT_INV_WideOr5~3_combout\,
	combout => \CPU|FSM|CS|WideOr7~4_combout\);

-- Location: FF_X64_Y4_N20
\CPU|FSM|stateRegister|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|CS|WideOr7~4_combout\,
	sclr => \ALT_INV_KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|stateRegister|out\(0));

-- Location: LABCELL_X60_Y5_N18
\CPU|FSM|CS|WideOr5~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr5~1_combout\ = ( \CPU|InstructionRegister|register|out\(14) & ( \CPU|InstructionRegister|register|out\(13) & ( (!\CPU|FSM|stateRegister|out\(3) & !\CPU|InstructionRegister|register|out\(15)) ) ) ) # ( 
-- !\CPU|InstructionRegister|register|out\(14) & ( \CPU|InstructionRegister|register|out\(13) & ( (!\CPU|FSM|stateRegister|out\(3) & \CPU|InstructionRegister|register|out\(15)) ) ) ) # ( \CPU|InstructionRegister|register|out\(14) & ( 
-- !\CPU|InstructionRegister|register|out\(13) & ( (!\CPU|FSM|stateRegister|out\(3) & \CPU|InstructionRegister|register|out\(15)) ) ) ) # ( !\CPU|InstructionRegister|register|out\(14) & ( !\CPU|InstructionRegister|register|out\(13) & ( 
-- \CPU|InstructionRegister|register|out\(15) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011000000110000001100000011001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	combout => \CPU|FSM|CS|WideOr5~1_combout\);

-- Location: LABCELL_X60_Y5_N27
\CPU|FSM|CS|WideOr5~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr5~2_combout\ = ( !\CPU|FSM|stateRegister|out\(2) & ( (\CPU|FSM|stateRegister|out\(1) & (\CPU|FSM|stateRegister|out\(0) & \CPU|FSM|CS|WideOr5~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datad => \CPU|FSM|CS|ALT_INV_WideOr5~1_combout\,
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	combout => \CPU|FSM|CS|WideOr5~2_combout\);

-- Location: LABCELL_X62_Y5_N18
\CPU|FSM|CS|WideOr5~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr5~4_combout\ = ( \CPU|FSM|stateRegister|out\(0) & ( \CPU|FSM|stateRegister|out\(3) & ( (!\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(2)) ) ) ) # ( !\CPU|FSM|stateRegister|out\(0) & ( \CPU|FSM|stateRegister|out\(3) & ( 
-- (!\CPU|FSM|stateRegister|out\(1) & !\CPU|FSM|stateRegister|out\(2)) ) ) ) # ( \CPU|FSM|stateRegister|out\(0) & ( !\CPU|FSM|stateRegister|out\(3) & ( (!\CPU|FSM|stateRegister|out\(1) & (\CPU|FSM|storeFlag|register|out\(0) & (\CPU|FSM|stateRegister|out\(2) 
-- & \CPU|FSM|CS|WideOr5~0_combout\))) ) ) ) # ( !\CPU|FSM|stateRegister|out\(0) & ( !\CPU|FSM|stateRegister|out\(3) & ( (!\CPU|FSM|stateRegister|out\(1) & \CPU|FSM|stateRegister|out\(2)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000000000000001010100000101000001010000010100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datab => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datac => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	datad => \CPU|FSM|CS|ALT_INV_WideOr5~0_combout\,
	datae => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	combout => \CPU|FSM|CS|WideOr5~4_combout\);

-- Location: LABCELL_X64_Y4_N54
\CPU|FSM|CS|WideOr5~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CS|WideOr5~5_combout\ = ( \CPU|FSM|CS|WideOr5~3_combout\ & ( (!\CPU|FSM|CS|WideOr5~2_combout\ & (!\CPU|FSM|CS|WideOr5~4_combout\ & ((!\CPU|FSM|CO|Mux9~0_combout\) # (!\CPU|FSM|CS|WideNor27~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000010100000100000001010000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CS|ALT_INV_WideOr5~2_combout\,
	datab => \CPU|FSM|CO|ALT_INV_Mux9~0_combout\,
	datac => \CPU|FSM|CS|ALT_INV_WideOr5~4_combout\,
	datad => \CPU|FSM|CS|ALT_INV_WideNor27~0_combout\,
	dataf => \CPU|FSM|CS|ALT_INV_WideOr5~3_combout\,
	combout => \CPU|FSM|CS|WideOr5~5_combout\);

-- Location: FF_X64_Y4_N56
\CPU|FSM|stateRegister|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|FSM|CS|WideOr5~5_combout\,
	sclr => \ALT_INV_KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|FSM|stateRegister|out\(1));

-- Location: LABCELL_X64_Y4_N39
\rtl~26\ : cyclonev_lcell_comb
-- Equation(s):
-- \rtl~26_combout\ = ( \CPU|FSM|stateRegister|out\(3) & ( !\CPU|FSM|stateRegister|out\(0) ) ) # ( !\CPU|FSM|stateRegister|out\(3) & ( (\CPU|FSM|stateRegister|out\(1) & ((!\CPU|FSM|stateRegister|out\(0) & (\CPU|FSM|CO|Equal6~0_combout\ & 
-- !\CPU|FSM|stateRegister|out\(2))) # (\CPU|FSM|stateRegister|out\(0) & ((\CPU|FSM|stateRegister|out\(2)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000010001000001000001000111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|stateRegister|ALT_INV_out\(1),
	datab => \CPU|FSM|stateRegister|ALT_INV_out\(0),
	datac => \CPU|FSM|CO|ALT_INV_Equal6~0_combout\,
	datad => \CPU|FSM|stateRegister|ALT_INV_out\(2),
	dataf => \CPU|FSM|stateRegister|ALT_INV_out\(3),
	combout => \rtl~26_combout\);

-- Location: LABCELL_X63_Y4_N57
\read_data[13]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \read_data[13]~0_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( (\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000100000001000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~27_combout\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\,
	combout => \read_data[13]~0_combout\);

-- Location: FF_X63_Y4_N59
\CPU|InstructionRegister|register|out[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \read_data[13]~0_combout\,
	ena => \CPU|FSM|CO|Mux1~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|InstructionRegister|register|out\(13));

-- Location: FF_X64_Y4_N11
\CPU|DP|a|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|REGFILE|readMux|m|b\(0),
	sload => VCC,
	ena => \CPU|FSM|CO|Decoder0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|a|register|out\(0));

-- Location: LABCELL_X64_Y4_N48
\CPU|DP|Ain[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Ain[0]~0_combout\ = ( \CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|DP|a|register|out\(0) & ( (!\CPU|FSM|storeFlag|register|out\(0) & (((!\CPU|InstructionRegister|register|out\(15)) # (!\CPU|InstructionRegister|register|out[14]~DUPLICATE_q\)) # 
-- (\CPU|InstructionRegister|register|out\(13)))) ) ) ) # ( !\CPU|FSM|CO|Decoder0~1_combout\ & ( \CPU|DP|a|register|out\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111100110011000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	datab => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	datad => \CPU|InstructionRegister|register|ALT_INV_out[14]~DUPLICATE_q\,
	datae => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	dataf => \CPU|DP|a|register|ALT_INV_out\(0),
	combout => \CPU|DP|Ain[0]~0_combout\);

-- Location: LABCELL_X63_Y5_N42
\CPU|FSM|CO|Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|FSM|CO|Selector8~0_combout\ = ( !\CPU|InstructionRegister|register|out\(15) & ( \CPU|InstructionRegister|register|out\(13) & ( \CPU|InstructionRegister|register|out\(14) ) ) ) # ( \CPU|InstructionRegister|register|out\(15) & ( 
-- !\CPU|InstructionRegister|register|out\(13) & ( (!\CPU|FSM|storeFlag|register|out\(0) & !\CPU|InstructionRegister|register|out\(14)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101000001010000000001111000011110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|storeFlag|register|ALT_INV_out\(0),
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(14),
	datae => \CPU|InstructionRegister|register|ALT_INV_out\(15),
	dataf => \CPU|InstructionRegister|register|ALT_INV_out\(13),
	combout => \CPU|FSM|CO|Selector8~0_combout\);

-- Location: LABCELL_X63_Y4_N48
\CPU|DP|Bin[0]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|Bin[0]~2_combout\ = ( \CPU|DP|Bin[0]~1_combout\ & ( (\CPU|FSM|CO|Decoder0~1_combout\ & (\CPU|FSM|CO|Selector8~0_combout\ & !\CPU|InstructionRegister|register|out\(0))) ) ) # ( !\CPU|DP|Bin[0]~1_combout\ & ( (!\CPU|FSM|CO|Decoder0~1_combout\ & 
-- (!\CPU|DP|Bin[0]~0_combout\)) # (\CPU|FSM|CO|Decoder0~1_combout\ & ((!\CPU|FSM|CO|Selector8~0_combout\ & (!\CPU|DP|Bin[0]~0_combout\)) # (\CPU|FSM|CO|Selector8~0_combout\ & ((!\CPU|InstructionRegister|register|out\(0)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110111001000110011011100100000000101000000000000010100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Decoder0~1_combout\,
	datab => \CPU|DP|ALT_INV_Bin[0]~0_combout\,
	datac => \CPU|FSM|CO|ALT_INV_Selector8~0_combout\,
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(0),
	dataf => \CPU|DP|ALT_INV_Bin[0]~1_combout\,
	combout => \CPU|DP|Bin[0]~2_combout\);

-- Location: LABCELL_X63_Y4_N18
\CPU|DP|ALU|out[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \CPU|DP|ALU|out[0]~0_combout\ = ( \CPU|DP|Bin[0]~2_combout\ & ( (!\CPU|InstructionRegister|register|out\(12) & (\CPU|DP|Ain[0]~0_combout\)) # (\CPU|InstructionRegister|register|out\(12) & ((\CPU|InstructionRegister|register|out\(11)))) ) ) # ( 
-- !\CPU|DP|Bin[0]~2_combout\ & ( (!\CPU|DP|Ain[0]~0_combout\ & (!\CPU|InstructionRegister|register|out\(12))) # (\CPU|DP|Ain[0]~0_combout\ & (\CPU|InstructionRegister|register|out\(12) & !\CPU|InstructionRegister|register|out\(11))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100001111000000110000111100000000110000001111110011000000111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|DP|ALT_INV_Ain[0]~0_combout\,
	datac => \CPU|InstructionRegister|register|ALT_INV_out\(12),
	datad => \CPU|InstructionRegister|register|ALT_INV_out\(11),
	dataf => \CPU|DP|ALT_INV_Bin[0]~2_combout\,
	combout => \CPU|DP|ALU|out[0]~0_combout\);

-- Location: FF_X63_Y4_N20
\CPU|DP|c|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \CPU|DP|ALU|out[0]~0_combout\,
	ena => \CPU|FSM|CO|Selector11~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CPU|DP|c|register|out\(0));

-- Location: LABCELL_X61_Y5_N42
\io|setLEDs~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \io|setLEDs~0_combout\ = ( \CPU|mem_addr[8]~0_combout\ & ( (!\CPU|mem_addr[0]~1_combout\ & (\rtl~27_combout\ & !\rtl~26_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001010000000000000101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datac => \ALT_INV_rtl~27_combout\,
	datad => \ALT_INV_rtl~26_combout\,
	dataf => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	combout => \io|setLEDs~0_combout\);

-- Location: LABCELL_X61_Y5_N33
\io|setLEDs~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \io|setLEDs~1_combout\ = ( !\CPU|mem_addr[5]~6_combout\ & ( (!\CPU|mem_addr[6]~7_combout\ & (!\CPU|mem_addr[4]~5_combout\ & !\CPU|mem_addr[7]~8_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	datac => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	datad => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	dataf => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	combout => \io|setLEDs~1_combout\);

-- Location: LABCELL_X61_Y5_N36
\io|setLEDs\ : cyclonev_lcell_comb
-- Equation(s):
-- \io|setLEDs~combout\ = ( \io|setLEDs~1_combout\ & ( (!\CPU|mem_addr[2]~3_combout\ & (!\CPU|mem_addr[1]~2_combout\ & (!\CPU|mem_addr[3]~4_combout\ & \io|setLEDs~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	datab => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datac => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	datad => \io|ALT_INV_setLEDs~0_combout\,
	dataf => \io|ALT_INV_setLEDs~1_combout\,
	combout => \io|setLEDs~combout\);

-- Location: FF_X62_Y4_N13
\io|LEDregister|register|out[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(0),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(0));

-- Location: FF_X60_Y4_N16
\io|LEDregister|register|out[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(1),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(1));

-- Location: FF_X62_Y4_N59
\io|LEDregister|register|out[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(2),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(2));

-- Location: FF_X60_Y4_N37
\io|LEDregister|register|out[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(3),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(3));

-- Location: LABCELL_X60_Y5_N15
\io|LEDregister|register|out[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \io|LEDregister|register|out[4]~feeder_combout\ = ( \CPU|DP|c|register|out\(4) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \CPU|DP|c|register|ALT_INV_out\(4),
	combout => \io|LEDregister|register|out[4]~feeder_combout\);

-- Location: FF_X60_Y5_N17
\io|LEDregister|register|out[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	d => \io|LEDregister|register|out[4]~feeder_combout\,
	sclr => \ALT_INV_KEY[1]~input_o\,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(4));

-- Location: FF_X62_Y4_N29
\io|LEDregister|register|out[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(5),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(5));

-- Location: FF_X62_Y4_N55
\io|LEDregister|register|out[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(6),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(6));

-- Location: FF_X60_Y4_N46
\io|LEDregister|register|out[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_50~inputCLKENA0_outclk\,
	asdata => \CPU|DP|c|register|out\(7),
	sclr => \ALT_INV_KEY[1]~input_o\,
	sload => VCC,
	ena => \io|setLEDs~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \io|LEDregister|register|out\(7));

-- Location: IOIBUF_X66_Y0_N75
\SW[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: LABCELL_X61_Y4_N24
\write~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \write~0_combout\ = ( \rtl~27_combout\ & ( \CPU|FSM|CO|Mux1~1_combout\ & ( (!\CPU|FSM|CO|Mux1~0_combout\ & ((!\CPU|FSM|CO|Mux1~2_combout\ & ((!\CPU|data_address|register|out\(8)))) # (\CPU|FSM|CO|Mux1~2_combout\ & 
-- (!\CPU|program_counter|register|out\(8))))) # (\CPU|FSM|CO|Mux1~0_combout\ & (!\CPU|program_counter|register|out\(8))) ) ) ) # ( \rtl~27_combout\ & ( !\CPU|FSM|CO|Mux1~1_combout\ & ( (!\CPU|FSM|CO|Mux1~0_combout\ & ((!\CPU|data_address|register|out\(8)))) 
-- # (\CPU|FSM|CO|Mux1~0_combout\ & (!\CPU|program_counter|register|out\(8))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111011100100010000000000000000001110110001001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|FSM|CO|ALT_INV_Mux1~0_combout\,
	datab => \CPU|program_counter|register|ALT_INV_out\(8),
	datac => \CPU|FSM|CO|ALT_INV_Mux1~2_combout\,
	datad => \CPU|data_address|register|ALT_INV_out\(8),
	datae => \ALT_INV_rtl~27_combout\,
	dataf => \CPU|FSM|CO|ALT_INV_Mux1~1_combout\,
	combout => \write~0_combout\);

-- Location: MLABCELL_X59_Y3_N24
\hexOut[3]~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[3]~3_combout\ = ( \SW[3]~input_o\ & ( \write~0_combout\ & ( (!\SW[9]~input_o\ & (((\CPU|DP|c|register|out\(3))))) # (\SW[9]~input_o\ & ((!\rtl~26_combout\) # ((\MEM|mem_rtl_0|auto_generated|ram_block1a3\)))) ) ) ) # ( !\SW[3]~input_o\ & ( 
-- \write~0_combout\ & ( (!\SW[9]~input_o\ & (((\CPU|DP|c|register|out\(3))))) # (\SW[9]~input_o\ & (\rtl~26_combout\ & ((\MEM|mem_rtl_0|auto_generated|ram_block1a3\)))) ) ) ) # ( \SW[3]~input_o\ & ( !\write~0_combout\ & ( (\CPU|DP|c|register|out\(3)) # 
-- (\SW[9]~input_o\) ) ) ) # ( !\SW[3]~input_o\ & ( !\write~0_combout\ & ( (!\SW[9]~input_o\ & \CPU|DP|c|register|out\(3)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010010111110101111100001010000110110100111001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[9]~input_o\,
	datab => \ALT_INV_rtl~26_combout\,
	datac => \CPU|DP|c|register|ALT_INV_out\(3),
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a3\,
	datae => \ALT_INV_SW[3]~input_o\,
	dataf => \ALT_INV_write~0_combout\,
	combout => \hexOut[3]~3_combout\);

-- Location: LABCELL_X60_Y3_N15
\hexOut[2]~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[2]~2_combout\ = ( \SW[9]~input_o\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( ((\write~0_combout\ & \rtl~26_combout\)) # (\SW[2]~input_o\) ) ) ) # ( !\SW[9]~input_o\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( 
-- \CPU|DP|c|register|out\(2) ) ) ) # ( \SW[9]~input_o\ & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( (\SW[2]~input_o\ & ((!\write~0_combout\) # (!\rtl~26_combout\))) ) ) ) # ( !\SW[9]~input_o\ & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a2\ & ( 
-- \CPU|DP|c|register|out\(2) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100100011001000000000111111110011011100110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_write~0_combout\,
	datab => \ALT_INV_SW[2]~input_o\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \CPU|DP|c|register|ALT_INV_out\(2),
	datae => \ALT_INV_SW[9]~input_o\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a2\,
	combout => \hexOut[2]~2_combout\);

-- Location: LABCELL_X60_Y3_N42
\hexOut[1]~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[1]~1_combout\ = ( \SW[1]~input_o\ & ( \write~0_combout\ & ( (!\SW[9]~input_o\ & (((\CPU|DP|c|register|out\(1))))) # (\SW[9]~input_o\ & ((!\rtl~26_combout\) # ((\MEM|mem_rtl_0|auto_generated|ram_block1a1\)))) ) ) ) # ( !\SW[1]~input_o\ & ( 
-- \write~0_combout\ & ( (!\SW[9]~input_o\ & (((\CPU|DP|c|register|out\(1))))) # (\SW[9]~input_o\ & (\rtl~26_combout\ & (\MEM|mem_rtl_0|auto_generated|ram_block1a1\))) ) ) ) # ( \SW[1]~input_o\ & ( !\write~0_combout\ & ( (\CPU|DP|c|register|out\(1)) # 
-- (\SW[9]~input_o\) ) ) ) # ( !\SW[1]~input_o\ & ( !\write~0_combout\ & ( (!\SW[9]~input_o\ & \CPU|DP|c|register|out\(1)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010101010010101011111111100000001101010110100010111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[9]~input_o\,
	datab => \ALT_INV_rtl~26_combout\,
	datac => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a1\,
	datad => \CPU|DP|c|register|ALT_INV_out\(1),
	datae => \ALT_INV_SW[1]~input_o\,
	dataf => \ALT_INV_write~0_combout\,
	combout => \hexOut[1]~1_combout\);

-- Location: LABCELL_X60_Y3_N0
\hexOut[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[0]~0_combout\ = ( \CPU|DP|c|register|out\(0) & ( \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( ((!\SW[9]~input_o\) # ((\write~0_combout\ & \rtl~26_combout\))) # (\SW[0]~input_o\) ) ) ) # ( !\CPU|DP|c|register|out\(0) & ( 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( (\SW[9]~input_o\ & (((\write~0_combout\ & \rtl~26_combout\)) # (\SW[0]~input_o\))) ) ) ) # ( \CPU|DP|c|register|out\(0) & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( 
-- (!\SW[9]~input_o\) # ((\SW[0]~input_o\ & ((!\write~0_combout\) # (!\rtl~26_combout\)))) ) ) ) # ( !\CPU|DP|c|register|out\(0) & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a0~portbdataout\ & ( (\SW[0]~input_o\ & (\SW[9]~input_o\ & ((!\write~0_combout\) # 
-- (!\rtl~26_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000010111100111111001000000011000001111111001111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_write~0_combout\,
	datab => \ALT_INV_SW[0]~input_o\,
	datac => \ALT_INV_SW[9]~input_o\,
	datad => \ALT_INV_rtl~26_combout\,
	datae => \CPU|DP|c|register|ALT_INV_out\(0),
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a0~portbdataout\,
	combout => \hexOut[0]~0_combout\);

-- Location: LABCELL_X56_Y3_N51
\H0|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr6~0_combout\ = ( \hexOut[0]~0_combout\ & ( (!\hexOut[3]~3_combout\ & (!\hexOut[2]~2_combout\ & !\hexOut[1]~1_combout\)) # (\hexOut[3]~3_combout\ & (!\hexOut[2]~2_combout\ $ (!\hexOut[1]~1_combout\))) ) ) # ( !\hexOut[0]~0_combout\ & ( 
-- (!\hexOut[3]~3_combout\ & (\hexOut[2]~2_combout\ & !\hexOut[1]~1_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000100101001001010000100000001000001001010010010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[3]~3_combout\,
	datab => \ALT_INV_hexOut[2]~2_combout\,
	datac => \ALT_INV_hexOut[1]~1_combout\,
	datae => \ALT_INV_hexOut[0]~0_combout\,
	combout => \H0|WideOr6~0_combout\);

-- Location: MLABCELL_X59_Y3_N45
\H0|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr5~0_combout\ = ( \hexOut[0]~0_combout\ & ( (!\hexOut[1]~1_combout\ & (\hexOut[2]~2_combout\ & !\hexOut[3]~3_combout\)) # (\hexOut[1]~1_combout\ & ((\hexOut[3]~3_combout\))) ) ) # ( !\hexOut[0]~0_combout\ & ( (\hexOut[2]~2_combout\ & 
-- ((\hexOut[3]~3_combout\) # (\hexOut[1]~1_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010101010101010100000000111100000101010101010101000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[2]~2_combout\,
	datac => \ALT_INV_hexOut[1]~1_combout\,
	datad => \ALT_INV_hexOut[3]~3_combout\,
	datae => \ALT_INV_hexOut[0]~0_combout\,
	combout => \H0|WideOr5~0_combout\);

-- Location: MLABCELL_X59_Y3_N12
\H0|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr4~0_combout\ = ( \hexOut[0]~0_combout\ & ( (\hexOut[3]~3_combout\ & (\hexOut[1]~1_combout\ & \hexOut[2]~2_combout\)) ) ) # ( !\hexOut[0]~0_combout\ & ( (!\hexOut[3]~3_combout\ & (\hexOut[1]~1_combout\ & !\hexOut[2]~2_combout\)) # 
-- (\hexOut[3]~3_combout\ & ((\hexOut[2]~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010010100100101000000010000000100100101001001010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[3]~3_combout\,
	datab => \ALT_INV_hexOut[1]~1_combout\,
	datac => \ALT_INV_hexOut[2]~2_combout\,
	datae => \ALT_INV_hexOut[0]~0_combout\,
	combout => \H0|WideOr4~0_combout\);

-- Location: MLABCELL_X59_Y3_N6
\H0|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr3~0_combout\ = ( \hexOut[3]~3_combout\ & ( (!\hexOut[2]~2_combout\ & (!\hexOut[1]~1_combout\ $ (!\hexOut[0]~0_combout\))) # (\hexOut[2]~2_combout\ & (\hexOut[1]~1_combout\ & \hexOut[0]~0_combout\)) ) ) # ( !\hexOut[3]~3_combout\ & ( 
-- (!\hexOut[2]~2_combout\ & (!\hexOut[1]~1_combout\ & \hexOut[0]~0_combout\)) # (\hexOut[2]~2_combout\ & (!\hexOut[1]~1_combout\ $ (\hexOut[0]~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100100101001001010010010100100100101001001010010010100100101001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[2]~2_combout\,
	datab => \ALT_INV_hexOut[1]~1_combout\,
	datac => \ALT_INV_hexOut[0]~0_combout\,
	dataf => \ALT_INV_hexOut[3]~3_combout\,
	combout => \H0|WideOr3~0_combout\);

-- Location: MLABCELL_X59_Y3_N9
\H0|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr2~0_combout\ = ( \hexOut[3]~3_combout\ & ( (!\hexOut[2]~2_combout\ & (!\hexOut[1]~1_combout\ & \hexOut[0]~0_combout\)) ) ) # ( !\hexOut[3]~3_combout\ & ( ((\hexOut[2]~2_combout\ & !\hexOut[1]~1_combout\)) # (\hexOut[0]~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010011111111010001001111111100000000100010000000000010001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[2]~2_combout\,
	datab => \ALT_INV_hexOut[1]~1_combout\,
	datad => \ALT_INV_hexOut[0]~0_combout\,
	dataf => \ALT_INV_hexOut[3]~3_combout\,
	combout => \H0|WideOr2~0_combout\);

-- Location: LABCELL_X56_Y3_N6
\H0|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr1~0_combout\ = ( \hexOut[0]~0_combout\ & ( !\hexOut[3]~3_combout\ $ (((!\hexOut[1]~1_combout\ & \hexOut[2]~2_combout\))) ) ) # ( !\hexOut[0]~0_combout\ & ( (\hexOut[1]~1_combout\ & (!\hexOut[3]~3_combout\ & !\hexOut[2]~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000000000000111100000011110000110000000000001111000000111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_hexOut[1]~1_combout\,
	datac => \ALT_INV_hexOut[3]~3_combout\,
	datad => \ALT_INV_hexOut[2]~2_combout\,
	datae => \ALT_INV_hexOut[0]~0_combout\,
	combout => \H0|WideOr1~0_combout\);

-- Location: MLABCELL_X59_Y3_N0
\H0|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H0|WideOr0~0_combout\ = ( \hexOut[0]~0_combout\ & ( (!\hexOut[1]~1_combout\ $ (!\hexOut[2]~2_combout\)) # (\hexOut[3]~3_combout\) ) ) # ( !\hexOut[0]~0_combout\ & ( (!\hexOut[3]~3_combout\ $ (!\hexOut[2]~2_combout\)) # (\hexOut[1]~1_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111101101111011011111010111110101111011011110110111110101111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[3]~3_combout\,
	datab => \ALT_INV_hexOut[1]~1_combout\,
	datac => \ALT_INV_hexOut[2]~2_combout\,
	datae => \ALT_INV_hexOut[0]~0_combout\,
	combout => \H0|WideOr0~0_combout\);

-- Location: MLABCELL_X59_Y4_N30
\hexOut[7]~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[7]~7_combout\ = ( \CPU|DP|c|register|out\(7) & ( \SW[9]~input_o\ & ( (!\write~0_combout\ & (((\SW[7]~input_o\)))) # (\write~0_combout\ & ((!\rtl~26_combout\ & ((\SW[7]~input_o\))) # (\rtl~26_combout\ & 
-- (\MEM|mem_rtl_0|auto_generated|ram_block1a7\)))) ) ) ) # ( !\CPU|DP|c|register|out\(7) & ( \SW[9]~input_o\ & ( (!\write~0_combout\ & (((\SW[7]~input_o\)))) # (\write~0_combout\ & ((!\rtl~26_combout\ & ((\SW[7]~input_o\))) # (\rtl~26_combout\ & 
-- (\MEM|mem_rtl_0|auto_generated|ram_block1a7\)))) ) ) ) # ( \CPU|DP|c|register|out\(7) & ( !\SW[9]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000001111110110000000111111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_write~0_combout\,
	datab => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a7\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \ALT_INV_SW[7]~input_o\,
	datae => \CPU|DP|c|register|ALT_INV_out\(7),
	dataf => \ALT_INV_SW[9]~input_o\,
	combout => \hexOut[7]~7_combout\);

-- Location: MLABCELL_X59_Y4_N0
\hexOut[5]~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[5]~5_combout\ = ( \SW[9]~input_o\ & ( \SW[5]~input_o\ & ( (!\rtl~26_combout\) # ((!\write~0_combout\) # (\MEM|mem_rtl_0|auto_generated|ram_block1a5\)) ) ) ) # ( !\SW[9]~input_o\ & ( \SW[5]~input_o\ & ( \CPU|DP|c|register|out\(5) ) ) ) # ( 
-- \SW[9]~input_o\ & ( !\SW[5]~input_o\ & ( (\rtl~26_combout\ & (\write~0_combout\ & \MEM|mem_rtl_0|auto_generated|ram_block1a5\)) ) ) ) # ( !\SW[9]~input_o\ & ( !\SW[5]~input_o\ & ( \CPU|DP|c|register|out\(5) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000000000000010100110011001100111111101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|DP|c|register|ALT_INV_out\(5),
	datac => \ALT_INV_write~0_combout\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a5\,
	datae => \ALT_INV_SW[9]~input_o\,
	dataf => \ALT_INV_SW[5]~input_o\,
	combout => \hexOut[5]~5_combout\);

-- Location: LABCELL_X56_Y3_N36
\hexOut[4]~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[4]~4_combout\ = ( \SW[4]~input_o\ & ( \SW[9]~input_o\ & ( (!\rtl~26_combout\) # ((!\write~0_combout\) # (\MEM|mem_rtl_0|auto_generated|ram_block1a4\)) ) ) ) # ( !\SW[4]~input_o\ & ( \SW[9]~input_o\ & ( (\rtl~26_combout\ & (\write~0_combout\ & 
-- \MEM|mem_rtl_0|auto_generated|ram_block1a4\)) ) ) ) # ( \SW[4]~input_o\ & ( !\SW[9]~input_o\ & ( \CPU|DP|c|register|out\(4) ) ) ) # ( !\SW[4]~input_o\ & ( !\SW[9]~input_o\ & ( \CPU|DP|c|register|out\(4) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100000000000001011111101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|DP|c|register|ALT_INV_out\(4),
	datac => \ALT_INV_write~0_combout\,
	datad => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a4\,
	datae => \ALT_INV_SW[4]~input_o\,
	dataf => \ALT_INV_SW[9]~input_o\,
	combout => \hexOut[4]~4_combout\);

-- Location: LABCELL_X61_Y4_N30
\hexOut[6]~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[6]~6_combout\ = ( \SW[6]~input_o\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( (\SW[9]~input_o\) # (\CPU|DP|c|register|out\(6)) ) ) ) # ( !\SW[6]~input_o\ & ( \MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( (!\SW[9]~input_o\ & 
-- (((\CPU|DP|c|register|out\(6))))) # (\SW[9]~input_o\ & (\rtl~26_combout\ & ((\write~0_combout\)))) ) ) ) # ( \SW[6]~input_o\ & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( (!\SW[9]~input_o\ & (((\CPU|DP|c|register|out\(6))))) # (\SW[9]~input_o\ & 
-- ((!\rtl~26_combout\) # ((!\write~0_combout\)))) ) ) ) # ( !\SW[6]~input_o\ & ( !\MEM|mem_rtl_0|auto_generated|ram_block1a6\ & ( (\CPU|DP|c|register|out\(6) & !\SW[9]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100111111101000110011000001010011001111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|DP|c|register|ALT_INV_out\(6),
	datac => \ALT_INV_write~0_combout\,
	datad => \ALT_INV_SW[9]~input_o\,
	datae => \ALT_INV_SW[6]~input_o\,
	dataf => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a6\,
	combout => \hexOut[6]~6_combout\);

-- Location: MLABCELL_X59_Y3_N54
\H1|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr6~0_combout\ = ( \hexOut[6]~6_combout\ & ( (!\hexOut[5]~5_combout\ & (!\hexOut[7]~7_combout\ $ (\hexOut[4]~4_combout\))) ) ) # ( !\hexOut[6]~6_combout\ & ( (\hexOut[4]~4_combout\ & (!\hexOut[7]~7_combout\ $ (\hexOut[5]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100101000000001010010110100000010100001010000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[7]~7_combout\,
	datac => \ALT_INV_hexOut[5]~5_combout\,
	datad => \ALT_INV_hexOut[4]~4_combout\,
	dataf => \ALT_INV_hexOut[6]~6_combout\,
	combout => \H1|WideOr6~0_combout\);

-- Location: MLABCELL_X59_Y3_N57
\H1|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr5~0_combout\ = ( \hexOut[5]~5_combout\ & ( (!\hexOut[4]~4_combout\ & ((\hexOut[6]~6_combout\))) # (\hexOut[4]~4_combout\ & (\hexOut[7]~7_combout\)) ) ) # ( !\hexOut[5]~5_combout\ & ( (\hexOut[6]~6_combout\ & (!\hexOut[7]~7_combout\ $ 
-- (!\hexOut[4]~4_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011000000110000001100000011000011101000111010001110100011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[7]~7_combout\,
	datab => \ALT_INV_hexOut[4]~4_combout\,
	datac => \ALT_INV_hexOut[6]~6_combout\,
	dataf => \ALT_INV_hexOut[5]~5_combout\,
	combout => \H1|WideOr5~0_combout\);

-- Location: MLABCELL_X59_Y3_N36
\H1|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr4~0_combout\ = (!\hexOut[7]~7_combout\ & (!\hexOut[6]~6_combout\ & (\hexOut[5]~5_combout\ & !\hexOut[4]~4_combout\))) # (\hexOut[7]~7_combout\ & (\hexOut[6]~6_combout\ & ((!\hexOut[4]~4_combout\) # (\hexOut[5]~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001100100000001000110010000000100011001000000010001100100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[7]~7_combout\,
	datab => \ALT_INV_hexOut[6]~6_combout\,
	datac => \ALT_INV_hexOut[5]~5_combout\,
	datad => \ALT_INV_hexOut[4]~4_combout\,
	combout => \H1|WideOr4~0_combout\);

-- Location: MLABCELL_X59_Y3_N39
\H1|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr3~0_combout\ = ( \hexOut[5]~5_combout\ & ( (!\hexOut[6]~6_combout\ & (\hexOut[7]~7_combout\ & !\hexOut[4]~4_combout\)) # (\hexOut[6]~6_combout\ & ((\hexOut[4]~4_combout\))) ) ) # ( !\hexOut[5]~5_combout\ & ( (!\hexOut[6]~6_combout\ & 
-- ((\hexOut[4]~4_combout\))) # (\hexOut[6]~6_combout\ & (!\hexOut[7]~7_combout\ & !\hexOut[4]~4_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010110000101100001011000010110001000011010000110100001101000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[7]~7_combout\,
	datab => \ALT_INV_hexOut[6]~6_combout\,
	datac => \ALT_INV_hexOut[4]~4_combout\,
	dataf => \ALT_INV_hexOut[5]~5_combout\,
	combout => \H1|WideOr3~0_combout\);

-- Location: MLABCELL_X59_Y3_N30
\H1|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr2~0_combout\ = ( \hexOut[6]~6_combout\ & ( (!\hexOut[7]~7_combout\ & ((!\hexOut[5]~5_combout\) # (\hexOut[4]~4_combout\))) ) ) # ( !\hexOut[6]~6_combout\ & ( (\hexOut[4]~4_combout\ & ((!\hexOut[7]~7_combout\) # (!\hexOut[5]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111010000000001111101010100000101010101010000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[7]~7_combout\,
	datac => \ALT_INV_hexOut[5]~5_combout\,
	datad => \ALT_INV_hexOut[4]~4_combout\,
	dataf => \ALT_INV_hexOut[6]~6_combout\,
	combout => \H1|WideOr2~0_combout\);

-- Location: MLABCELL_X59_Y3_N51
\H1|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr1~0_combout\ = ( \hexOut[7]~7_combout\ & ( (!\hexOut[5]~5_combout\ & (\hexOut[4]~4_combout\ & \hexOut[6]~6_combout\)) ) ) # ( !\hexOut[7]~7_combout\ & ( (!\hexOut[5]~5_combout\ & (\hexOut[4]~4_combout\ & !\hexOut[6]~6_combout\)) # 
-- (\hexOut[5]~5_combout\ & ((!\hexOut[6]~6_combout\) # (\hexOut[4]~4_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000101110001000000100000001001110001011100010000001000000010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[5]~5_combout\,
	datab => \ALT_INV_hexOut[4]~4_combout\,
	datac => \ALT_INV_hexOut[6]~6_combout\,
	datae => \ALT_INV_hexOut[7]~7_combout\,
	combout => \H1|WideOr1~0_combout\);

-- Location: MLABCELL_X59_Y3_N33
\H1|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H1|WideOr0~0_combout\ = ( \hexOut[5]~5_combout\ & ( ((!\hexOut[6]~6_combout\) # (!\hexOut[4]~4_combout\)) # (\hexOut[7]~7_combout\) ) ) # ( !\hexOut[5]~5_combout\ & ( (!\hexOut[7]~7_combout\ & (\hexOut[6]~6_combout\)) # (\hexOut[7]~7_combout\ & 
-- ((!\hexOut[6]~6_combout\) # (\hexOut[4]~4_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0110011101100111011001110110011111111101111111011111110111111101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[7]~7_combout\,
	datab => \ALT_INV_hexOut[6]~6_combout\,
	datac => \ALT_INV_hexOut[4]~4_combout\,
	dataf => \ALT_INV_hexOut[5]~5_combout\,
	combout => \H1|WideOr0~0_combout\);

-- Location: LABCELL_X61_Y3_N42
\hexOut[8]~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[8]~8_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a8\ & ( \CPU|DP|c|register|out\(8) & ( (!\SW[9]~input_o\) # ((\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\))) ) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a8\ & 
-- ( \CPU|DP|c|register|out\(8) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a8\ & ( !\CPU|DP|c|register|out\(8) & ( (\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & (\SW[9]~input_o\ & \rtl~26_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010011110000111100001111000011110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~27_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[9]~input_o\,
	datad => \ALT_INV_rtl~26_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a8\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(8),
	combout => \hexOut[8]~8_combout\);

-- Location: LABCELL_X61_Y3_N45
\hexOut[9]~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[9]~9_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ & ( \CPU|DP|c|register|out\(9) & ( (!\SW[9]~input_o\) # ((\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\))) ) ) ) # ( !\MEM|mem_rtl_0|auto_generated|ram_block1a9\ & 
-- ( \CPU|DP|c|register|out\(9) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a9\ & ( !\CPU|DP|c|register|out\(9) & ( (\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & (\rtl~26_combout\ & \SW[9]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010011111111000000001111111100000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~27_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \ALT_INV_SW[9]~input_o\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a9\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(9),
	combout => \hexOut[9]~9_combout\);

-- Location: LABCELL_X61_Y3_N36
\hexOut[10]~10\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[10]~10_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( \CPU|DP|c|register|out\(10) & ( (!\SW[9]~input_o\) # ((\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\))) ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( \CPU|DP|c|register|out\(10) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a10\ & ( !\CPU|DP|c|register|out\(10) & ( (\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & 
-- (\SW[9]~input_o\ & \rtl~26_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010011110000111100001111000011110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~27_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[9]~input_o\,
	datad => \ALT_INV_rtl~26_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a10\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(10),
	combout => \hexOut[10]~10_combout\);

-- Location: LABCELL_X61_Y3_N39
\hexOut[11]~11\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[11]~11_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( \CPU|DP|c|register|out\(11) & ( (!\SW[9]~input_o\) # ((\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~26_combout\))) ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( \CPU|DP|c|register|out\(11) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a11\ & ( !\CPU|DP|c|register|out\(11) & ( (\rtl~27_combout\ & (!\CPU|mem_addr[8]~0_combout\ & 
-- (\rtl~26_combout\ & \SW[9]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010011111111000000001111111100000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~27_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \ALT_INV_SW[9]~input_o\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a11\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(11),
	combout => \hexOut[11]~11_combout\);

-- Location: MLABCELL_X39_Y3_N27
\H2|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr6~0_combout\ = ( \hexOut[11]~11_combout\ & ( (\hexOut[8]~8_combout\ & (!\hexOut[9]~9_combout\ $ (!\hexOut[10]~10_combout\))) ) ) # ( !\hexOut[11]~11_combout\ & ( (!\hexOut[9]~9_combout\ & (!\hexOut[8]~8_combout\ $ (!\hexOut[10]~10_combout\))) ) 
-- )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100100001001000010010000100100000010100000101000001010000010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr6~0_combout\);

-- Location: MLABCELL_X39_Y3_N6
\H2|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr5~0_combout\ = ( \hexOut[11]~11_combout\ & ( (!\hexOut[8]~8_combout\ & ((\hexOut[10]~10_combout\))) # (\hexOut[8]~8_combout\ & (\hexOut[9]~9_combout\)) ) ) # ( !\hexOut[11]~11_combout\ & ( (\hexOut[10]~10_combout\ & (!\hexOut[8]~8_combout\ $ 
-- (!\hexOut[9]~9_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011000000110000001100000011000011011000110110001101100011011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr5~0_combout\);

-- Location: MLABCELL_X39_Y3_N0
\H2|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr4~0_combout\ = ( \hexOut[11]~11_combout\ & ( (\hexOut[10]~10_combout\ & ((!\hexOut[8]~8_combout\) # (\hexOut[9]~9_combout\))) ) ) # ( !\hexOut[11]~11_combout\ & ( (!\hexOut[8]~8_combout\ & (\hexOut[9]~9_combout\ & !\hexOut[10]~10_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000000001011000010110000101100001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr4~0_combout\);

-- Location: MLABCELL_X39_Y3_N33
\H2|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr3~0_combout\ = ( \hexOut[11]~11_combout\ & ( (!\hexOut[8]~8_combout\ & (\hexOut[9]~9_combout\ & !\hexOut[10]~10_combout\)) # (\hexOut[8]~8_combout\ & (!\hexOut[9]~9_combout\ $ (\hexOut[10]~10_combout\))) ) ) # ( !\hexOut[11]~11_combout\ & ( 
-- (!\hexOut[8]~8_combout\ & (!\hexOut[9]~9_combout\ & \hexOut[10]~10_combout\)) # (\hexOut[8]~8_combout\ & (!\hexOut[9]~9_combout\ $ (\hexOut[10]~10_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100100101001001010010010100100101100001011000010110000101100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr3~0_combout\);

-- Location: MLABCELL_X39_Y3_N48
\H2|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr2~0_combout\ = ( \hexOut[11]~11_combout\ & ( (\hexOut[8]~8_combout\ & (!\hexOut[9]~9_combout\ & !\hexOut[10]~10_combout\)) ) ) # ( !\hexOut[11]~11_combout\ & ( ((!\hexOut[9]~9_combout\ & \hexOut[10]~10_combout\)) # (\hexOut[8]~8_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101110101011101010111010101110101000000010000000100000001000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr2~0_combout\);

-- Location: MLABCELL_X39_Y3_N57
\H2|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr1~0_combout\ = ( \hexOut[11]~11_combout\ & ( (\hexOut[8]~8_combout\ & (!\hexOut[9]~9_combout\ & \hexOut[10]~10_combout\)) ) ) # ( !\hexOut[11]~11_combout\ & ( (!\hexOut[8]~8_combout\ & (\hexOut[9]~9_combout\ & !\hexOut[10]~10_combout\)) # 
-- (\hexOut[8]~8_combout\ & ((!\hexOut[10]~10_combout\) # (\hexOut[9]~9_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000101110001011100010111000100000100000001000000010000000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr1~0_combout\);

-- Location: MLABCELL_X39_Y3_N36
\H2|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H2|WideOr0~0_combout\ = ( \hexOut[11]~11_combout\ & ( ((!\hexOut[10]~10_combout\) # (\hexOut[9]~9_combout\)) # (\hexOut[8]~8_combout\) ) ) # ( !\hexOut[11]~11_combout\ & ( (!\hexOut[9]~9_combout\ & ((\hexOut[10]~10_combout\))) # (\hexOut[9]~9_combout\ & 
-- ((!\hexOut[8]~8_combout\) # (!\hexOut[10]~10_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111000111110001111100011111011110111111101111111011111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[8]~8_combout\,
	datab => \ALT_INV_hexOut[9]~9_combout\,
	datac => \ALT_INV_hexOut[10]~10_combout\,
	dataf => \ALT_INV_hexOut[11]~11_combout\,
	combout => \H2|WideOr0~0_combout\);

-- Location: LABCELL_X66_Y3_N9
\hexOut[14]~14\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[14]~14_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( \CPU|DP|c|register|out\(14) & ( (!\SW[9]~input_o\) # ((!\CPU|mem_addr[8]~0_combout\ & (\rtl~27_combout\ & \rtl~26_combout\))) ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( \CPU|DP|c|register|out\(14) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a14\ & ( !\CPU|DP|c|register|out\(14) & ( (!\CPU|mem_addr[8]~0_combout\ & (\SW[9]~input_o\ & 
-- (\rtl~27_combout\ & \rtl~26_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000001011001100110011001100110011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datab => \ALT_INV_SW[9]~input_o\,
	datac => \ALT_INV_rtl~27_combout\,
	datad => \ALT_INV_rtl~26_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a14\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(14),
	combout => \hexOut[14]~14_combout\);

-- Location: MLABCELL_X59_Y4_N9
\hexOut[12]~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[12]~12_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( \CPU|DP|c|register|out\(12) & ( (!\SW[9]~input_o\) # ((\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\))) ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( \CPU|DP|c|register|out\(12) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a12\ & ( !\CPU|DP|c|register|out\(12) & ( (\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & 
-- (\SW[9]~input_o\ & \rtl~27_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010011110000111100001111000011110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_SW[9]~input_o\,
	datad => \ALT_INV_rtl~27_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a12\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(12),
	combout => \hexOut[12]~12_combout\);

-- Location: LABCELL_X66_Y3_N6
\hexOut[15]~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[15]~15_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( \CPU|DP|c|register|out\(15) & ( (!\SW[9]~input_o\) # ((!\CPU|mem_addr[8]~0_combout\ & (\rtl~26_combout\ & \rtl~27_combout\))) ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( \CPU|DP|c|register|out\(15) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a15\ & ( !\CPU|DP|c|register|out\(15) & ( (!\CPU|mem_addr[8]~0_combout\ & (\SW[9]~input_o\ & 
-- (\rtl~26_combout\ & \rtl~27_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000001011001100110011001100110011001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datab => \ALT_INV_SW[9]~input_o\,
	datac => \ALT_INV_rtl~26_combout\,
	datad => \ALT_INV_rtl~27_combout\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a15\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(15),
	combout => \hexOut[15]~15_combout\);

-- Location: MLABCELL_X59_Y4_N6
\hexOut[13]~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \hexOut[13]~13_combout\ = ( \MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( \CPU|DP|c|register|out\(13) & ( (!\SW[9]~input_o\) # ((\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & \rtl~27_combout\))) ) ) ) # ( 
-- !\MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( \CPU|DP|c|register|out\(13) & ( !\SW[9]~input_o\ ) ) ) # ( \MEM|mem_rtl_0|auto_generated|ram_block1a13\ & ( !\CPU|DP|c|register|out\(13) & ( (\rtl~26_combout\ & (!\CPU|mem_addr[8]~0_combout\ & 
-- (\rtl~27_combout\ & \SW[9]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000010011111111000000001111111100000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_rtl~26_combout\,
	datab => \CPU|ALT_INV_mem_addr[8]~0_combout\,
	datac => \ALT_INV_rtl~27_combout\,
	datad => \ALT_INV_SW[9]~input_o\,
	datae => \MEM|mem_rtl_0|auto_generated|ALT_INV_ram_block1a13\,
	dataf => \CPU|DP|c|register|ALT_INV_out\(13),
	combout => \hexOut[13]~13_combout\);

-- Location: MLABCELL_X59_Y4_N51
\H3|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr6~0_combout\ = ( \hexOut[13]~13_combout\ & ( (!\hexOut[14]~14_combout\ & (\hexOut[12]~12_combout\ & \hexOut[15]~15_combout\)) ) ) # ( !\hexOut[13]~13_combout\ & ( (!\hexOut[14]~14_combout\ & (\hexOut[12]~12_combout\ & !\hexOut[15]~15_combout\)) 
-- # (\hexOut[14]~14_combout\ & (!\hexOut[12]~12_combout\ $ (\hexOut[15]~15_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101000000101010110100000010100000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[14]~14_combout\,
	datac => \ALT_INV_hexOut[12]~12_combout\,
	datad => \ALT_INV_hexOut[15]~15_combout\,
	dataf => \ALT_INV_hexOut[13]~13_combout\,
	combout => \H3|WideOr6~0_combout\);

-- Location: MLABCELL_X59_Y4_N21
\H3|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr5~0_combout\ = ( \hexOut[13]~13_combout\ & ( (!\hexOut[12]~12_combout\ & ((\hexOut[14]~14_combout\))) # (\hexOut[12]~12_combout\ & (\hexOut[15]~15_combout\)) ) ) # ( !\hexOut[13]~13_combout\ & ( (\hexOut[14]~14_combout\ & 
-- (!\hexOut[12]~12_combout\ $ (!\hexOut[15]~15_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000111100000000000011110000000011110011110000001111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_hexOut[12]~12_combout\,
	datac => \ALT_INV_hexOut[15]~15_combout\,
	datad => \ALT_INV_hexOut[14]~14_combout\,
	dataf => \ALT_INV_hexOut[13]~13_combout\,
	combout => \H3|WideOr5~0_combout\);

-- Location: MLABCELL_X59_Y4_N42
\H3|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr4~0_combout\ = ( \hexOut[12]~12_combout\ & ( (\hexOut[14]~14_combout\ & (\hexOut[15]~15_combout\ & \hexOut[13]~13_combout\)) ) ) # ( !\hexOut[12]~12_combout\ & ( (!\hexOut[14]~14_combout\ & (!\hexOut[15]~15_combout\ & \hexOut[13]~13_combout\)) # 
-- (\hexOut[14]~14_combout\ & (\hexOut[15]~15_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001100100011001000110010001100100000001000000010000000100000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[14]~14_combout\,
	datab => \ALT_INV_hexOut[15]~15_combout\,
	datac => \ALT_INV_hexOut[13]~13_combout\,
	dataf => \ALT_INV_hexOut[12]~12_combout\,
	combout => \H3|WideOr4~0_combout\);

-- Location: MLABCELL_X59_Y4_N45
\H3|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr3~0_combout\ = ( \hexOut[12]~12_combout\ & ( !\hexOut[14]~14_combout\ $ (\hexOut[13]~13_combout\) ) ) # ( !\hexOut[12]~12_combout\ & ( (!\hexOut[14]~14_combout\ & (\hexOut[15]~15_combout\ & \hexOut[13]~13_combout\)) # (\hexOut[14]~14_combout\ & 
-- (!\hexOut[15]~15_combout\ & !\hexOut[13]~13_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100001001000010010000100100001010100101101001011010010110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[14]~14_combout\,
	datab => \ALT_INV_hexOut[15]~15_combout\,
	datac => \ALT_INV_hexOut[13]~13_combout\,
	dataf => \ALT_INV_hexOut[12]~12_combout\,
	combout => \H3|WideOr3~0_combout\);

-- Location: MLABCELL_X59_Y4_N36
\H3|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr2~0_combout\ = ( \hexOut[12]~12_combout\ & ( (!\hexOut[15]~15_combout\) # ((!\hexOut[14]~14_combout\ & !\hexOut[13]~13_combout\)) ) ) # ( !\hexOut[12]~12_combout\ & ( (!\hexOut[15]~15_combout\ & (\hexOut[14]~14_combout\ & 
-- !\hexOut[13]~13_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000011111100110011001111110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_hexOut[15]~15_combout\,
	datac => \ALT_INV_hexOut[14]~14_combout\,
	datad => \ALT_INV_hexOut[13]~13_combout\,
	dataf => \ALT_INV_hexOut[12]~12_combout\,
	combout => \H3|WideOr2~0_combout\);

-- Location: MLABCELL_X59_Y4_N48
\H3|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr1~0_combout\ = (!\hexOut[14]~14_combout\ & (!\hexOut[15]~15_combout\ & ((\hexOut[13]~13_combout\) # (\hexOut[12]~12_combout\)))) # (\hexOut[14]~14_combout\ & (\hexOut[12]~12_combout\ & (!\hexOut[13]~13_combout\ $ (!\hexOut[15]~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010101100010000001010110001000000101011000100000010101100010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[14]~14_combout\,
	datab => \ALT_INV_hexOut[12]~12_combout\,
	datac => \ALT_INV_hexOut[13]~13_combout\,
	datad => \ALT_INV_hexOut[15]~15_combout\,
	combout => \H3|WideOr1~0_combout\);

-- Location: MLABCELL_X59_Y4_N27
\H3|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H3|WideOr0~0_combout\ = ( \hexOut[15]~15_combout\ & ( \hexOut[13]~13_combout\ ) ) # ( !\hexOut[15]~15_combout\ & ( \hexOut[13]~13_combout\ & ( (!\hexOut[14]~14_combout\) # (!\hexOut[12]~12_combout\) ) ) ) # ( \hexOut[15]~15_combout\ & ( 
-- !\hexOut[13]~13_combout\ & ( (!\hexOut[14]~14_combout\) # (\hexOut[12]~12_combout\) ) ) ) # ( !\hexOut[15]~15_combout\ & ( !\hexOut[13]~13_combout\ & ( \hexOut[14]~14_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101101011111010111111111010111110101111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_hexOut[14]~14_combout\,
	datac => \ALT_INV_hexOut[12]~12_combout\,
	datae => \ALT_INV_hexOut[15]~15_combout\,
	dataf => \ALT_INV_hexOut[13]~13_combout\,
	combout => \H3|WideOr0~0_combout\);

-- Location: LABCELL_X61_Y5_N45
\H4|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr6~0_combout\ = (!\CPU|mem_addr[3]~4_combout\ & (!\CPU|mem_addr[1]~2_combout\ & (!\CPU|mem_addr[0]~1_combout\ $ (!\CPU|mem_addr[2]~3_combout\)))) # (\CPU|mem_addr[3]~4_combout\ & (\CPU|mem_addr[0]~1_combout\ & (!\CPU|mem_addr[1]~2_combout\ $ 
-- (!\CPU|mem_addr[2]~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000110000100010000011000010001000001100001000100000110000100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datab => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datac => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	datad => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	combout => \H4|WideOr6~0_combout\);

-- Location: LABCELL_X61_Y5_N57
\H4|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr5~0_combout\ = ( \CPU|mem_addr[3]~4_combout\ & ( (!\CPU|mem_addr[0]~1_combout\ & ((\CPU|mem_addr[2]~3_combout\))) # (\CPU|mem_addr[0]~1_combout\ & (\CPU|mem_addr[1]~2_combout\)) ) ) # ( !\CPU|mem_addr[3]~4_combout\ & ( 
-- (\CPU|mem_addr[2]~3_combout\ & (!\CPU|mem_addr[0]~1_combout\ $ (!\CPU|mem_addr[1]~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001011010000000000101101000000101101011110000010110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datac => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datad => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	dataf => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	combout => \H4|WideOr5~0_combout\);

-- Location: LABCELL_X61_Y5_N27
\H4|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr4~0_combout\ = ( \CPU|mem_addr[2]~3_combout\ & ( (\CPU|mem_addr[3]~4_combout\ & ((!\CPU|mem_addr[0]~1_combout\) # (\CPU|mem_addr[1]~2_combout\))) ) ) # ( !\CPU|mem_addr[2]~3_combout\ & ( (!\CPU|mem_addr[0]~1_combout\ & 
-- (\CPU|mem_addr[1]~2_combout\ & !\CPU|mem_addr[3]~4_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000000001011000010110000101100001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datab => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datac => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	dataf => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	combout => \H4|WideOr4~0_combout\);

-- Location: LABCELL_X61_Y5_N30
\H4|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr3~0_combout\ = ( \CPU|mem_addr[3]~4_combout\ & ( (!\CPU|mem_addr[2]~3_combout\ & (!\CPU|mem_addr[0]~1_combout\ $ (!\CPU|mem_addr[1]~2_combout\))) # (\CPU|mem_addr[2]~3_combout\ & (\CPU|mem_addr[0]~1_combout\ & \CPU|mem_addr[1]~2_combout\)) ) ) # 
-- ( !\CPU|mem_addr[3]~4_combout\ & ( (!\CPU|mem_addr[2]~3_combout\ & (\CPU|mem_addr[0]~1_combout\ & !\CPU|mem_addr[1]~2_combout\)) # (\CPU|mem_addr[2]~3_combout\ & (!\CPU|mem_addr[0]~1_combout\ $ (\CPU|mem_addr[1]~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101000000101010110100000010100001010101001010000101010100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	datac => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datad => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	dataf => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	combout => \H4|WideOr3~0_combout\);

-- Location: LABCELL_X61_Y5_N12
\H4|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr2~0_combout\ = ( \CPU|mem_addr[0]~1_combout\ & ( (!\CPU|mem_addr[3]~4_combout\) # ((!\CPU|mem_addr[2]~3_combout\ & !\CPU|mem_addr[1]~2_combout\)) ) ) # ( !\CPU|mem_addr[0]~1_combout\ & ( (!\CPU|mem_addr[3]~4_combout\ & 
-- (\CPU|mem_addr[2]~3_combout\ & !\CPU|mem_addr[1]~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000000000000010100000000011111010101010101111101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	datac => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	datad => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	dataf => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	combout => \H4|WideOr2~0_combout\);

-- Location: LABCELL_X61_Y5_N9
\H4|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr1~0_combout\ = (!\CPU|mem_addr[0]~1_combout\ & (\CPU|mem_addr[1]~2_combout\ & (!\CPU|mem_addr[3]~4_combout\ & !\CPU|mem_addr[2]~3_combout\))) # (\CPU|mem_addr[0]~1_combout\ & (!\CPU|mem_addr[3]~4_combout\ $ (((!\CPU|mem_addr[1]~2_combout\ & 
-- \CPU|mem_addr[2]~3_combout\)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000000010100011100000001010001110000000101000111000000010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datab => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datac => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	datad => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	combout => \H4|WideOr1~0_combout\);

-- Location: LABCELL_X61_Y5_N24
\H4|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H4|WideOr0~0_combout\ = (!\CPU|mem_addr[0]~1_combout\ & ((!\CPU|mem_addr[3]~4_combout\ $ (!\CPU|mem_addr[2]~3_combout\)) # (\CPU|mem_addr[1]~2_combout\))) # (\CPU|mem_addr[0]~1_combout\ & ((!\CPU|mem_addr[1]~2_combout\ $ (!\CPU|mem_addr[2]~3_combout\)) # 
-- (\CPU|mem_addr[3]~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111111100111001111111110011100111111111001110011111111100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[0]~1_combout\,
	datab => \CPU|ALT_INV_mem_addr[1]~2_combout\,
	datac => \CPU|ALT_INV_mem_addr[3]~4_combout\,
	datad => \CPU|ALT_INV_mem_addr[2]~3_combout\,
	combout => \H4|WideOr0~0_combout\);

-- Location: MLABCELL_X59_Y2_N39
\H5|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr6~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (\CPU|mem_addr[4]~5_combout\ & (!\CPU|mem_addr[5]~6_combout\ $ (!\CPU|mem_addr[6]~7_combout\))) ) ) # ( !\CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[5]~6_combout\ & 
-- (!\CPU|mem_addr[4]~5_combout\ $ (!\CPU|mem_addr[6]~7_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010100000101000001010000010100000010010000100100001001000010010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datab => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	datac => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr6~0_combout\);

-- Location: MLABCELL_X59_Y2_N6
\H5|WideOr5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr5~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[4]~5_combout\ & (\CPU|mem_addr[6]~7_combout\)) # (\CPU|mem_addr[4]~5_combout\ & ((\CPU|mem_addr[5]~6_combout\))) ) ) # ( !\CPU|mem_addr[7]~8_combout\ & ( 
-- (\CPU|mem_addr[6]~7_combout\ & (!\CPU|mem_addr[5]~6_combout\ $ (!\CPU|mem_addr[4]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100110000000000110011000000110011000011110011001100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	datac => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datad => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr5~0_combout\);

-- Location: MLABCELL_X59_Y2_N51
\H5|WideOr4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr4~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (\CPU|mem_addr[6]~7_combout\ & ((!\CPU|mem_addr[4]~5_combout\) # (\CPU|mem_addr[5]~6_combout\))) ) ) # ( !\CPU|mem_addr[7]~8_combout\ & ( (\CPU|mem_addr[5]~6_combout\ & 
-- (!\CPU|mem_addr[4]~5_combout\ & !\CPU|mem_addr[6]~7_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001000000010000000100000000001101000011010000110100001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datab => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	datac => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr4~0_combout\);

-- Location: MLABCELL_X59_Y2_N57
\H5|WideOr3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr3~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[5]~6_combout\ & (\CPU|mem_addr[4]~5_combout\ & !\CPU|mem_addr[6]~7_combout\)) # (\CPU|mem_addr[5]~6_combout\ & (!\CPU|mem_addr[4]~5_combout\ $ (\CPU|mem_addr[6]~7_combout\))) ) ) # 
-- ( !\CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[5]~6_combout\ & (!\CPU|mem_addr[4]~5_combout\ $ (!\CPU|mem_addr[6]~7_combout\))) # (\CPU|mem_addr[5]~6_combout\ & (\CPU|mem_addr[4]~5_combout\ & \CPU|mem_addr[6]~7_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010100100101001001010010010100101100001011000010110000101100001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datab => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	datac => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr3~0_combout\);

-- Location: MLABCELL_X59_Y2_N27
\H5|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr2~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[5]~6_combout\ & (\CPU|mem_addr[4]~5_combout\ & !\CPU|mem_addr[6]~7_combout\)) ) ) # ( !\CPU|mem_addr[7]~8_combout\ & ( ((!\CPU|mem_addr[5]~6_combout\ & 
-- \CPU|mem_addr[6]~7_combout\)) # (\CPU|mem_addr[4]~5_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011101100111011001110110011101100100000001000000010000000100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datab => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	datac => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr2~0_combout\);

-- Location: MLABCELL_X59_Y2_N42
\H5|WideOr1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr1~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (\CPU|mem_addr[6]~7_combout\ & (!\CPU|mem_addr[5]~6_combout\ & \CPU|mem_addr[4]~5_combout\)) ) ) # ( !\CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[6]~7_combout\ & ((\CPU|mem_addr[4]~5_combout\) 
-- # (\CPU|mem_addr[5]~6_combout\))) # (\CPU|mem_addr[6]~7_combout\ & (\CPU|mem_addr[5]~6_combout\ & \CPU|mem_addr[4]~5_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110011001111000011001100111100000000001100000000000000110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	datac => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datad => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr1~0_combout\);

-- Location: MLABCELL_X59_Y2_N12
\H5|WideOr0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \H5|WideOr0~0_combout\ = ( \CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[6]~7_combout\) # ((\CPU|mem_addr[4]~5_combout\) # (\CPU|mem_addr[5]~6_combout\)) ) ) # ( !\CPU|mem_addr[7]~8_combout\ & ( (!\CPU|mem_addr[6]~7_combout\ & 
-- (\CPU|mem_addr[5]~6_combout\)) # (\CPU|mem_addr[6]~7_combout\ & ((!\CPU|mem_addr[5]~6_combout\) # (!\CPU|mem_addr[4]~5_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111100001111110011110011001111111111111100111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \CPU|ALT_INV_mem_addr[6]~7_combout\,
	datac => \CPU|ALT_INV_mem_addr[5]~6_combout\,
	datad => \CPU|ALT_INV_mem_addr[4]~5_combout\,
	dataf => \CPU|ALT_INV_mem_addr[7]~8_combout\,
	combout => \H5|WideOr0~0_combout\);

-- Location: IOIBUF_X86_Y0_N35
\KEY[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X2_Y81_N41
\KEY[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: IOIBUF_X86_Y0_N52
\KEY[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: IOIBUF_X89_Y8_N4
\SW[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: LABCELL_X29_Y39_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


