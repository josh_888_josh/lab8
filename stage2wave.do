onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R0
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R1
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R2
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R3
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R4
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R5
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R6
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/REGFILE/R7
add wave -noupdate {/lab8_stage2_tb/DUT/MEM/mem[255]}
add wave -noupdate {/lab8_stage2_tb/DUT/MEM/mem[25]}
add wave -noupdate /lab8_stage2_tb/DUT/CPU/FSM/state
add wave -noupdate /lab8_stage2_tb/DUT/CPU/instructionIn
add wave -noupdate {/lab8_stage2_tb/DUT/MEM/mem[20]}
add wave -noupdate /lab8_stage2_tb/DUT/CPU/PC
add wave -noupdate /lab8_stage2_tb/DUT/CPU/R7toPC
add wave -noupdate /lab8_stage2_tb/DUT/CPU/DP/data_out
add wave -noupdate /lab8_stage2_tb/DUT/CPU/ID/Rn
add wave -noupdate /lab8_stage2_tb/DUT/CPU/FSM/nsel
add wave -noupdate /lab8_stage2_tb/DUT/CPU/opcode
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1528 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {367 ps} {1556 ps}
