onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R0
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R1
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R2
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R3
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R4
add wave -noupdate /lab8_check_tb/DUT/CPU/DP/REGFILE/R5
add wave -noupdate /lab8_check_tb/DUT/h
add wave -noupdate /lab8_check_tb/DUT/CPU/N
add wave -noupdate /lab8_check_tb/DUT/CPU/Z
add wave -noupdate /lab8_check_tb/DUT/CPU/branchsel
add wave -noupdate /lab8_check_tb/DUT/CPU/cond
add wave -noupdate /lab8_check_tb/DUT/CPU/sx9imm8
add wave -noupdate /lab8_check_tb/DUT/CPU/branchAdder
add wave -noupdate /lab8_check_tb/DUT/CPU/instructionIn
add wave -noupdate /lab8_check_tb/DUT/CPU/CU/state
add wave -noupdate /lab8_check_tb/DUT/CPU/CU/load_pc
add wave -noupdate /lab8_check_tb/DUT/CPU/CU/opcode
add wave -noupdate /lab8_check_tb/DUT/CPU/CU/clk
add wave -noupdate /lab8_check_tb/DUT/read_data
add wave -noupdate /lab8_check_tb/DUT/CPU/CU/load_ir
add wave -noupdate {/lab8_check_tb/DUT/MEM/mem[20]}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2312 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {2149 ps} {2303 ps}
